# coding=utf-8

"""
find common signatures from several phf's signatures
"""

import os

def _dict_value_statistics(dictionary):
	value2count_dict = dict()

	for phf,signatures in dictionary.items():
		for sig in signatures:
			if value2count_dict.has_key(sig):
				value2count_dict[sig] += 1
			else:
				value2count_dict[sig] = 1

	return value2count_dict


def ngram_statistics(phf2ngrams_dict):
	ngram2count_dict = _dict_value_statistics(phf2ngrams_dict)

	for ngram, count in sorted(ngram2count_dict.items(), key=lambda x:x[1]):
		print ngram + " " + str(count)

	return ngram2count_dict

	
def syscall_statistics(phf2ngrams_dict):
	
	phf2syscalls_dict = dict()

	# split ngram into syscalls
	for phf,ngrams in phf2ngrams_dict.items():
		for ngram in ngrams:
			
			if not phf2syscalls_dict.has_key(phf):
				phf2syscalls_dict[phf] = set()

			for syscall in ngram.split("--"):
				phf2syscalls_dict[phf].add(syscall)

	# find common syscalls
	syscall2count_dict = _dict_value_statistics(phf2syscalls_dict)

	for syscall, count in sorted(syscall2count_dict.items(), key=lambda x:x[1]):
		print syscall + " " + str(count)

	return syscall2count_dict

if __name__ == "__main__":
	
	sig_dirs_path = "signatures/"

	phf2ngrams_dict = dict()

	# read signatures
	for signature_file_name in os.listdir(sig_dirs_path):
		signature_file_path = os.path.join(sig_dirs_path, signature_file_name)
		with open(signature_file_path) as signature_file:
			signatures = signature_file.readlines()
			phf2ngrams_dict[signature_file_name] = {sig.strip() for sig in signatures}

	# find common syscalls/signatures
	ngram2count_dict = ngram_statistics(phf2ngrams_dict)
	common_ngram = [ngram for ngram,count in ngram2count_dict.items() if count>=2 ]
	print common_ngram

	# remove common ngram in  signatures
	for signature_file_name in os.listdir(sig_dirs_path):
		signature_file_path = os.path.join(sig_dirs_path, signature_file_name)

		with open(signature_file_path, "r+") as signature_file:
			old_signatures = [ngram.strip() for ngram in signature_file.readlines()]
			new_signature = [ngram for ngram in old_signatures if ngram not in common_ngram]
			print signature_file_name, len(old_signatures),len(new_signature)
			print set(old_signatures) - set(new_signature)

			signature_file.seek(0)
			signature_file.truncate()
			signature_file.write("\n".join(new_signature))
	# syscall_statistics(phf2ngrams_dicngramt)