[
  {
    "traces": [
      "DarkComet51",
      "DarkComet53"
    ],
    "sig": [
      "SystemCall @ SystemCall:NtUserMapVirtualKeyEx",
      "SystemCall @ SystemCall:NtUserGetKeyState",
      "SystemCall @ SystemCall:NtUserGetKeyboardState",
      "SystemCall @ SystemCall:NtUserMapVirtualKeyEx",
      "SystemCall @ SystemCall:NtUserToUnicodeEx",
      "SystemCall @ SystemCall:NtUserGetForegroundWindow",
      "SystemCall @ SystemCall:NtUserQueryWindow",
      "FileIoCreate @ FileName:XXX.dc",
      "FileIoQueryInfo @ FileName:XXX.dc",
      "SystemCall @ SystemCall:NtCreateFile",
      "FileIoCreate @ FileName:XXX.dc",
      "FileIoQueryInfo @ FileName:XXX.dc",
      "SystemCall @ SystemCall:NtSetInformationFile",
      "SystemCall @ SystemCall:NtReadFile",
      "FileIoRead @ FileName:XXX.dc",
      "SystemCall @ SystemCall:NtUserIsClipboardFormatAvailable",
      "SystemCall @ SystemCall:NtWriteFile",
      "FileIoWrite @ FileName:XXX.dc",
      "SystemCall @ SystemCall:NtCallbackReturn",
      "SystemCall @ SystemCall:NtUserMapVirtualKeyEx",
      "SystemCall @ SystemCall:NtUserGetKeyState",
      "SystemCall @ SystemCall:NtUserGetKeyboardState",
      "SystemCall @ SystemCall:NtUserMapVirtualKeyEx",
      "SystemCall @ SystemCall:NtUserToUnicodeEx",
      "SystemCall @ SystemCall:NtUserGetForegroundWindow",
      "SystemCall @ SystemCall:NtUserQueryWindow",
      "SystemCall @ SystemCall:NtCallbackReturn",
      "SystemCall @ SystemCall:NtUserMapVirtualKeyEx",
      "SystemCall @ SystemCall:NtUserGetForegroundWindow",
      "SystemCall @ SystemCall:NtUserQueryWindow",
      "SystemCall @ SystemCall:NtCallbackReturn"
    ],
    "windowsize": 71
  },
  {
    "traces": [
      "DarkComet51",
      "imminentMonitor20"
    ],
    "sig": [
      "SystemCall @ SystemCall:NtCallbackReturn",
      "SystemCall @ SystemCall:NtUserGetKeyState"
    ],
    "windowsize": 4
  },
  {
    "traces": [
      "DarkComet51",
      "NanoCore1220"
    ],
    "sig": [
      "SystemCall @ SystemCall:NtUserGetKeyboardState",
      "SystemCall @ SystemCall:NtUserMapVirtualKeyEx",
      "SystemCall @ SystemCall:NtUserToUnicodeEx",
      "SystemCall @ SystemCall:NtCallbackReturn"
    ],
    "windowsize": 29
  },
  {
    "traces": [
      "imminentMonitor20",
      "DarkComet53"
    ],
    "sig": [
      "SystemCall @ SystemCall:NtCallbackReturn",
      "SystemCall @ SystemCall:NtUserGetKeyState"
    ],
    "windowsize": 4
  },
  {
    "traces": [
      "NanoCore1220",
      "DarkComet53"
    ],
    "sig": [
      "SystemCall @ SystemCall:NtUserGetKeyboardState",
      "SystemCall @ SystemCall:NtUserMapVirtualKeyEx",
      "SystemCall @ SystemCall:NtUserToUnicodeEx",
      "SystemCall @ SystemCall:NtCallbackReturn"
    ],
    "windowsize": 14
  },
  {
    "traces": [
      "NanoCore1220",
      "imminentMonitor20"
    ],
    "sig": [
      "DiskIoDrvMjFnCall @ MajorFunction:5",
      "DiskIoDrvMjFnCall @ MajorFunction:18",
      "DiskIoDrvMjFnCall @ MajorFunction:2",
      "DiskIoDrvMjFnCall @ MajorFunction:0",
      "DiskIoDrvMjFnCall @ MajorFunction:18",
      "DiskIoDrvMjFnCall @ MajorFunction:2",
      "DiskIoDrvMjFnCall @ MajorFunction:4"
    ],
    "windowsize": 32
  }
]