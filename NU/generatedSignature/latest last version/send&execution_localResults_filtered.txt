[
  {
    "traces": [
      "njrat0.7-se",
      "imminent2.0-se"
    ],
    "sig": [
      "SystemCall @ SystemCall:NtUserGetForegroundWindow",
      "SystemCall @ SystemCall:NtUserQueryWindow",
      "SystemCall @ SystemCall:NtAllocateVirtualMemory",
      "SystemCall @ SystemCall:NtUserQueryWindow"
    ],
    "windowsize": 10,
    "coverage": 0.2857142857142857,
    "falsePositiveShare": 0.2777777777777778
  },
  {
    "traces": [
      "spynet2.6-se",
      "imminent2.0-se"
    ],
    "sig": [
      "SystemCall @ SystemCall:NtCreateFile",
      "SystemCall @ SystemCall:NtWriteFile",
      "SystemCall @ SystemCall:NtUserGetForegroundWindow",
      "SystemCall @ SystemCall:NtUserQueryWindow"
    ],
    "windowsize": 17,
    "coverage": 0.2857142857142857,
    "falsePositiveShare": 0.2777777777777778
  },
  {
    "traces": [
      "darkcomet5.1-se",
      "imminent2.0-se"
    ],
    "sig": [
      "ImageDCStart @ FileName:wshbth.dll",
      "ImageDCStart @ FileName:winrnr.dll",
      "ImageDCStart @ FileName:pnrpnsp.dll",
      "ImageDCStart @ FileName:NapiNSP.dll"
    ],
    "windowsize": 7,
    "coverage": 0.35714285714285715,
    "falsePositiveShare": 0.3333333333333333
  },
  {
    "traces": [
      "xrat2.0-se",
      "nanocore1.2.2.0-se"
    ],
    "sig": [
      "ImageDCStart @ FileName:WMINet_Utils.dll",
      "ImageDCStart @ FileName:System.Xml.ni.dll",
      "ImageDCStart @ FileName:System.Windows.Forms.ni.dll",
      "ImageDCStart @ FileName:mscorlib.ni.dll",
      "ImageDCStart @ FileName:mscorwks.dll",
      "ImageDCStart @ FileName:System.Configuration.ni.dll"
    ],
    "windowsize": 17,
    "coverage": 0.2857142857142857,
    "falsePositiveShare": 0.1111111111111111
  },
  {
    "traces": [
      "xrat2.0-se",
      "nanocore1.2.2.0-se"
    ],
    "sig": [
      "ImageDCStart @ FileName:WMINet_Utils.dll",
      "ImageDCStart @ FileName:System.Xml.ni.dll",
      "ImageDCStart @ FileName:System.Windows.Forms.ni.dll",
      "ImageDCStart @ FileName:System.ni.dll",
      "ImageDCStart @ FileName:mscorwks.dll",
      "ImageDCStart @ FileName:System.Configuration.ni.dll"
    ],
    "windowsize": 17,
    "coverage": 0.2857142857142857,
    "falsePositiveShare": 0.1111111111111111
  },
  {
    "traces": [
      "xena2.0-se",
      "xrat2.0-se"
    ],
    "sig": [
      "ImageDCStart @ FileName:rpcrt4.dll",
      "ImageDCStart @ FileName:advapi32.dll",
      "ImageDCStart @ FileName:ole32.dll",
      "ImageDCStart @ FileName:nsi.dll"
    ],
    "windowsize": 14,
    "coverage": 1.0,
    "falsePositiveShare": 0.0
  },
  {
    "traces": [
      "xrat2.0-se",
      "njrat0.7-se"
    ],
    "sig": [
      "ImageDCStart @ FileName:msctf.dll",
      "ImageDCStart @ FileName:shlwapi.dll",
      "ImageDCStart @ FileName:ole32.dll",
      "ImageDCStart @ FileName:nsi.dll",
      "ImageDCStart @ FileName:sechost.dll",
      "SystemCall @ SystemCall:NtUserGetKeyState",
      "SystemCall @ SystemCall:NtUserGetForegroundWindow",
      "SystemCall @ SystemCall:NtUserQueryWindow"
    ],
    "windowsize": 33,
    "coverage": 0.14285714285714285,
    "falsePositiveShare": 0.0
  },
  {
    "traces": [
      "nanocore1.2.2.0-se",
      "imminent2.0-se"
    ],
    "sig": [
      "ImageDCStart @ FileName:msctf.dll",
      "ImageDCStart @ FileName:shlwapi.dll",
      "ImageDCStart @ FileName:ole32.dll",
      "ImageDCStart @ FileName:nsi.dll",
      "ImageDCStart @ FileName:sechost.dll",
      "SystemCall @ SystemCall:NtUserGetForegroundWindow",
      "SystemCall @ SystemCall:NtUserQueryWindow",
      "SystemCall @ SystemCall:NtSetTimer",
      "SystemCall @ SystemCall:NtWaitForWorkViaWorkerFactory",
      "SystemCall @ SystemCall:NtUserGetForegroundWindow",
      "SystemCall @ SystemCall:NtUserQueryWindow"
    ],
    "windowsize": 134,
    "coverage": 0.14285714285714285,
    "falsePositiveShare": 0.0
  }
]