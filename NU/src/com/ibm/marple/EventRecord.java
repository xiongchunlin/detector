package com.ibm.marple;


import java.util.Map;

import org.json.JSONObject;

public class EventRecord {
	
	public String eventName;
	public String threadID;
	public String processID;
	public Map<String, String> arguments;
	
	public EventRecord() {
		
	}
	
}
