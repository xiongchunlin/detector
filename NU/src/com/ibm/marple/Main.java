package com.ibm.marple;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import edu.nu.marple.detectsystem.EventRecordM;
import edu.nu.marple.detectsystem.SequenceSignatureWFAMatch;
import edu.nu.marple.tokenbaseddetection.TokenBasedMatch;
import edu.nu.marple.EventRecordG;
import edu.nu.marple.Utils;

public class Main {
	
	static int dataCount = 0 ;
	static int windowSize = 10000;
	
	static HashSet<String> uselessSyscallPHF = new HashSet<String>();
	static HashSet<String> uselessSyscallInit = new HashSet<String>();
	
	
	static ArrayList<EventRecord> unClassifiedRecords = new ArrayList<>(); //unclassified records
	static HashMap<String, ArrayList<EventRecord>> bufferedRecords = new HashMap<>(); // store buffered syscalls of different process(UUID)
	
	static String nowFile = "";
	
	static String testDataDir = "";
	static boolean isPhfNameWritten = false;
	private static String reportPath;
	private static String reportPath_credit;
	
	public static int IOTime = 0, AnalysisTime = 0, SyscallRecordCnt = 0, FileCnt = 0, 
			falsePositive = 0, falsePositive_credit = 0, falsE = 0, truePositive = 0, truePositive_credit = 0, truE = 0;
	public static int localTrue = 0, localTruePositive = 0, localFalse = 0, localFalsePositive = 0;
	public static int localTrue_credit = 0, localTruePositive_credit = 0, localFalse_credit = 0, localFalsePositive_credit = 0;
	public static Map<String, Double> detectionThreshold = new HashMap();
	
	/**
	 * 1. reading cdm data from kafka stream
	 * 2. splitting the stream into several segments according to time stamp
	 * 3. matching every segments with signatures
	 * @throws IOException 
	 */

	static {
		try {
			BufferedReader br = new BufferedReader(new FileReader("./importantList/useless_e2.txt"));
			String name = "";
			while ((name = br.readLine()) != null){
				uselessSyscallPHF.add(name.trim());
			}
			br.close();
			
			br = new BufferedReader(new FileReader("./importantList/useless_haitao_init.txt"));
			while ((name = br.readLine()) != null){
				uselessSyscallInit.add(name.trim());
			}
			br.close(); 
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		detectionThreshold.put("audiorecord", 1.0);
		detectionThreshold.put("keylogger", 0.76);
		detectionThreshold.put("remotedesktop", 25.0);
		detectionThreshold.put("remoteshell", 6.0);
		detectionThreshold.put("send&exec", Double.POSITIVE_INFINITY);
		detectionThreshold.put("download&exec", 1.0);
		detectionThreshold.put("urldownload", Double.POSITIVE_INFINITY);
	}
	public static void readdata(String filePath) throws JsonSyntaxException, IOException{
		String data;
		BufferedReader br = new BufferedReader(new FileReader(filePath));
		while ((data = br.readLine()) != null) {
			System.out.println(data);
			EventRecord record = new Gson().fromJson(data, EventRecord.class);
			System.out.println(record.eventName+" "+record.arguments);
		}
	}
	
	public static void main2(String args[]) throws IOException{
		readdata("G:\\source\\traceOfETW\\traces\\621\\TheadId904.output");
//		matchFromKafka();
	}
	
	private static EventRecordG convertEventRecordGJson2Object(String json) {
		return new Gson().fromJson(json, EventRecordG.class);
	}
	
	public static void matchFromTracefile(String fileName) throws IOException {
		/*Input from trace file*/
		++FileCnt;
		Map<String, List<EventRecordM>> fileBufferedRecords = new HashMap<String, List<EventRecordM>>();
		fileBufferedRecords.put("0", new ArrayList<EventRecordM>());
		BufferedReader br = new BufferedReader(new FileReader(fileName));
		Date startTime = new Date();
		List<EventRecordG> sequence = Utils.EventRecordGFile2EventRecordGenerationList(fileName);
		for (EventRecordG eve : sequence) {
			fileBufferedRecords.get("0").add(new EventRecordM(eve.name, eve.parameter, "-1"));
		}
		br.close();
		Date endTime = new Date();
		IOTime += endTime.getTime() - startTime.getTime();
		startTime = new Date();
		SequenceSignatureWFAMatch ssm = new SequenceSignatureWFAMatch(nowFile, fileBufferedRecords);
		//SequenceSignatureWFAMatchAllArgsVersion ssm = new SequenceSignatureWFAMatchAllArgsVersion(nowFile, bufferedRecords);
		System.out.println("Start to match tracefile: " + nowFile);
		boolean result = ssm.match();
		endTime = new Date();
		AnalysisTime += endTime.getTime() - startTime.getTime();
		
		// generate csv format detection report
		List<Map<String, Integer>> allProcessResults = ssm.getAllProcessResults();
		List<Map<String, Double>> allProcessResults_credit = ssm.getAllProcessResults_credit();
		Map<String, Integer> phf2Sigcnt = ssm.getPhf2Sigcnt();
		List<String> phfs = new ArrayList<String>(phf2Sigcnt.keySet());
		BufferedWriter writer = new BufferedWriter(new FileWriter(reportPath, true));
		BufferedWriter writer_credit = new BufferedWriter(new FileWriter(reportPath_credit, true));
		
		if (!isPhfNameWritten) {
			writer.write("\t" + phfs.stream().collect(Collectors.joining("\t")));
			writer_credit.write("\t" + phfs.stream().collect(Collectors.joining("\t")));
			isPhfNameWritten = true;
		}
		
		writer.write("\n" + nowFile);
		writer_credit.write("\n" + nowFile);
		
		// match percentage report
		for (Map<String, Integer> singleProcessResult : allProcessResults) {
			if (allProcessResults.indexOf(singleProcessResult) > 0)
				writer.write("\n");
			phfs.stream().map(phf -> singleProcessResult.get(phf) + " \\ " + phf2Sigcnt.get(phf))
					.forEach(matchResult -> {
						try {
							writer.write("\t" + matchResult);
						} catch (IOException e) {
							e.printStackTrace();
						}
					});
		}
		for (Map<String, Integer> singleProcessResult : allProcessResults) {
			for (String nowPhf : phfs) 
			if (nowFile.toLowerCase().contains("\\" + nowPhf)) {
				++localTrue;
				++truE;
				if (singleProcessResult.get(nowPhf) > 0) {
					++localTruePositive;
					++truePositive;
				}
			} else {
				++falsE;
				++localFalse;
				if (singleProcessResult.get(nowPhf) > 0) {
					++falsePositive;
					++localFalsePositive;
				}
			}
		}
		writer.close();
		
		//credit report
		for (Map<String, Double> singleProcessResult_credit : allProcessResults_credit) {
			if (allProcessResults_credit.indexOf(singleProcessResult_credit) > 0)
				writer_credit.write("\n");
			phfs.stream().map(phf -> singleProcessResult_credit.get(phf))
					.forEach(matchResult -> {
						try {
							writer_credit.write("\t" + matchResult);
						} catch (IOException e) {
							e.printStackTrace();
						}
					});
		}
		
		for (Map<String, Double> singleProcessResult_credit : allProcessResults_credit) {
			for (String nowPhf : phfs) 
			if (nowFile.toLowerCase().contains("\\" + nowPhf + "\\")) {
				++localTrue_credit;
				if (singleProcessResult_credit.get(nowPhf) >= detectionThreshold.get(nowPhf)) {
					++localTruePositive_credit;
					++truePositive_credit;
				}
			} else {
				++localFalse_credit;
				if (singleProcessResult_credit.get(nowPhf) >= detectionThreshold.get(nowPhf)) {
					++falsePositive_credit;
					++localFalsePositive_credit;
				}
			}
		}
		writer_credit.close();
	}
	
	public static void main(String args[]) throws IOException {
		List<String> testDirs = Arrays.asList(
											  "data/test/download&exec",
											  "data/test/remotedesktop/",
											  "data/test/audiorecord/",
											  "data/test/remoteshell/",
											  "data/test/keylogger/",
											  "data/test/send&exec/",
//											  "data/test/allphf-2"
											  //"data/test/benignxiaoruan/"
											  "data/test/FPs/"
											  );
		/*List<String> testDirs = Arrays.asList("data/non-phf-filter/audiorecord/",
				  							  "data/non-phf-filter/keylogger/",
				  							  "data/non-phf-filter/remotedesktop/",
				  							  "data/non-phf-filter/remoteshell/",
				  							  "data/non-phf-filter/send&execution",
				  							  "data/non-phf-filter/urldownload/",
				  							  "data/non-phf-filter/benign_for_score/",
				  							  "data/non-phf-filter/injectdll_trace/");*/
		String traceExtendtion = ".output.tracePreprocessed";
		String inittraceExtention = ".txt";
				//".initArgPreProcessed";
		testDirs.forEach(t -> {
			try {
				isPhfNameWritten = false;
				detectFromDir(t, traceExtendtion);
				//initdetectFromDir(t, inittraceExtention);
			} catch (IOException e) {
				e.printStackTrace();
			}
		});
		SequenceSignatureWFAMatch.detectionDetails.close();
		SequenceSignatureWFAMatch.detectionReport.close();
		System.out.println("IOTime: " + IOTime);
		System.out.println("AnalysisTime: " + AnalysisTime);
		System.out.println("SyscallRecordCnt: " + SyscallRecordCnt);
		System.out.println("FileCnt: " + FileCnt);
		System.out.println("Fpr: " + falsePositive + "/" + falsE);
		System.out.println("Tpr: " + truePositive + "/" + truE);
		System.out.println("CreditFpr: " + falsePositive_credit + "/" + falsE);
		System.out.println("CreditTpr: " + truePositive + "/" + truE);
	}
	
	public static void detectFromDir(String dir, final String traceExtention) throws IOException{
		Date startTime = new Date();
		System.out.println("===============start=============== " + " time: " + startTime.getTime());
		//String defaultCDMFile = "data/benign/filezilla.avro";
		//matchFromAvrofile(defaultCDMFile);		
		testDataDir = dir;
		reportPath = "detectionReport/csvFormatReport_" + testDataDir.replace("/","#") + ".txt";
		reportPath_credit = "detectionReport/csvFormatCreditReport_" + testDataDir.replace("/","#") + ".txt";
		BufferedWriter writer = new BufferedWriter(new FileWriter(reportPath));
		BufferedWriter writer_credit = new BufferedWriter(new FileWriter(reportPath_credit));
		writer.write("\n=============" + testDataDir + "=============\n");
		writer.close();
		writer_credit.write("\n=============" + testDataDir + "=============\n");
		writer_credit.close();
		File path = new File(testDataDir);
		File[] files = path.listFiles(new FilenameFilter() {
			public boolean accept(File dir, String name) {
				return name.endsWith(traceExtention);
			}
		});
		localTrue = 0; localTruePositive = 0; localFalse = 0; localFalsePositive = 0;
		localTrue_credit = 0; localTruePositive_credit = 0; localFalse_credit = 0; localFalsePositive_credit = 0;
		for (File file : files) 
		if (file.length() > 0){			
			nowFile = file.getPath();
			matchFromTracefile(nowFile);
			System.out.println("");
			unClassifiedRecords.clear();
			bufferedRecords.clear();
		}
		writer = new BufferedWriter(new FileWriter(reportPath, true));
		writer.write("\n");
		writer.write("TPR: " + String.valueOf(localTruePositive) + "/" + String.valueOf(localTrue) + "\n");
		writer.write("FPR: " + String.valueOf(localFalsePositive) + "/" + String.valueOf(localFalse) + "\n");
		writer.close();
		
		writer_credit = new BufferedWriter(new FileWriter(reportPath_credit, true));
		writer_credit.write("\n");
		writer_credit.write("TPR: " + String.valueOf(localTruePositive_credit) + "/" + String.valueOf(localTrue_credit) + "\n");
		writer_credit.write("FPR: " + String.valueOf(localFalsePositive_credit) + "/" + String.valueOf(localFalse_credit) + "\n");
		writer_credit.close();
		Date endTime = new Date();
		System.out.println("===============end=============== " + " time: " + endTime.getTime());
	}
}
