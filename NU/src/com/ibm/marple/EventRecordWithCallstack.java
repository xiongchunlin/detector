package com.ibm.marple;


import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;

public class EventRecordWithCallstack {
	
	public String eventName;
	public String threadID;
	public String processID;
	public String callStack;
	public Map<String, String> arguments;
	
	public EventRecordWithCallstack() {
		eventName = "";
		threadID = "";
		processID = "";
		callStack = "";
		arguments = new HashMap<String, String>();
	}
	
}
