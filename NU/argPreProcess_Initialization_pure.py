import urllib2
import re
import traceback
#from bs4 import BeautifulSoup
import time
import os
from threading import Thread
from datetime import datetime
import math
import sys

# pre-process traces
#def processTrace(inputFileName, outputFileName, keyword):
if len(sys.argv) != 4:
    print "4 arguments only!"
else:
    inputFileName = sys.argv[1]
    outputFileName = sys.argv[2]
    keyword = sys.argv[3]
    if keyword == "initialization":
        ignoreSyscallList = ['NtAllocateUuids', 'NtAllocateVirtualMemory', 'NtAlpcDeletePortSection', \
                             'NtAlpcDeleteSectionView', 'NtAlpcSendWaitReceivePort', 'NtCancelTimer', \
                             'NtClearEvent', 'NtClose', 'NtCreateEvent', 'NtCreateThreadEx', 'NtDelayExecution', \
                             'NtDeleteAtom', 'NtDeviceIoControlFile', 'NtDuplicateObject', 'NtFreeVirtualMemory', \
                             'NtOpenProcessTokenEx', 'NtOpenThreadToken', 'NtQueryInformationToken', \
                             'NtQueryPerformanceCounter', 'NtQuerySystemTime', 'NtReleaseMutant', 'NtRequestWaitReplyPort', \
                             'NtResumeThread', 'NtSetEvent', 'NtTestAlert', 'NtUserCallNoParam', 'NtUserCallOneParam', \
                             'NtUserCallTwoParam', 'NtUserGetMessage', 'NtUserGetThreadState', 'NtUserMessageCall', \
                             'NtUserPeekMessage', 'NtUserPostMessage', 'NtUserWaitMessage', 'NtWaitForMultipleObjects', \
                             'NtWaitForSingleObject', 'NtWriteVirtualMemory', 'NtYieldExecution', 'NtMapViewOfSection', \
                             'NtQueryInformationFile', 'NtQueryInformationProcess', 'NtQueryInformationThread', \
                             'NtQueryVirtualMemory', 'NtQueryVolumeInformationFile', 'NtSetInformationProcess', 'NtTraceEvent', \
                             'NtUserQueryWindow', 'NtQueryVirtualMemory', 'NtSetInformationThread', 'NtWaitForWorkViaWorkerFactory', \
                             'NtUserGetAsyncKeyState', 'NtReleaseWorkerFactoryWorker', 'NtWorkerFactoryWorkerReady', 'NtUserToUnicodeEx',\
                             'NtUserGetKeyState', 'NtUserGetKeyboardState', 'NtUserGetDC', 'NtProtectVirtualMemory', 'NtGdiOpenDCW', \
                             'NtGdiGetDIBitsInternal', 'NtGdiGetDeviceCaps', 'NtGdiFlush', 'NtGdiExtGetObjectW', 'NtGdiDeleteObjectApp', \
                             'NtGdiCreateDIBSection', 'NtGdiCreateCompatibleDC', 'NtGdiCreateCompatibleBitmap', 'NtGdiBitBlt', 'GreSelectBitmap']        
        
    
    inputFile = open(inputFileName, 'r')
    outputFile = open(outputFileName, 'w')

    input_list = inputFile.readlines()

    
    #last_line = ""
    #sameNum = 0
    for input_line in input_list:
     
        input_line = input_line.split('\t')
        syscall = input_line[0].strip('\n')
        if syscall in ignoreSyscallList or len(syscall) == 0:
            continue
        
        if len(input_line) < 2:
            sysArg = ''
        else:
            sysArg = input_line[1].strip('\n')
        sysArg_lower = sysArg.lower()

        if 'NtDeviceIoControlFile' in syscall:
            sysArg = sysArg.split('<<')[0]        
        elif sysArg_lower.endswith('.exe') or sysArg_lower.endswith('.dll'):
            sysArg = "%r" % sysArg
            sysArg = sysArg.split("\\")[-1].strip("'")
        if keyword != "initialization":
            sysArg = sysArg.split("\\")[-1].strip("'")
        if sysArg_lower.startswith('@'):
            temp = sysArg.split("\\")[-1].strip("'")
            temp = temp.split(",")
            if len(temp) == 2:
                sysArg = temp[0]            

        if "____" in sysArg_lower:
            sysArg = ''
            
        if sysArg_lower.endswith('cmd.exe'):
            sysArg = 'cmd.exe'
        else:
            sysArg = re.sub(r'[^\\]*\.dat', "XXX.dat", sysArg)
            sysArg = re.sub(r'[^\\]*\.(temp|TMP|tmp)', "XXX.temp", sysArg)
            sysArg = re.sub(r'[^\\]*\.(nls|NLS)', "XXX.nls", sysArg)
            sysArg = re.sub(r'[^\\]*\.(png|PNG)', "XXX.png", sysArg)
            sysArg = re.sub(r'[^\\]*\.css$', "XXX.css", sysArg)
            sysArg = re.sub(r'[^\\]*\.(js|JS)$', "XXX.js", sysArg)
            sysArg = re.sub(r'[^\\]*\.json$', "XXX.json", sysArg)  
            sysArg = re.sub(r'[^\\]*\.(htm|html)$', "XXX.htm", sysArg)        
            sysArg = re.sub(r'\{.+-.+-.+-.+\}', "XXX", sysArg)
            sysArg = re.sub(r'\\[^\\]+-[^\\]+-[^\\]+-[^\\]+\\', "\\\\", sysArg)
            sysArg = re.sub(r'\\[^\\]+-[^\\]+-[^\\]+-[^\\]+$', "\XXX", sysArg) #\Registry\User\S-1-5-21-2749001191-3991546447-1369892179-1001_Classes
            sysArg = re.sub(r'\{.+-.+-.+-.+-.+\}', "XXX", sysArg)
            sysArg = re.sub(r'[A-Z0-9]+-[^\\]+-[^\\]+-[0-9]+(_Classes)*', "XXX", sysArg)    
            sysArg = re.sub(r'Plane\d+$', "Plane", sysArg)
            sysArg = re.sub(r'msvideo\d+$', "msvideo", sysArg)       
            sysArg = re.sub(r'wave\d+$', "wave", sysArg)
            sysArg = re.sub(r'midi\d+$', "midi", sysArg)
            sysArg = re.sub(r'countrycode\[\d+\]$', "countrycode", sysArg)
            sysArg = re.sub(r'MonitorFunction\d+$', "MonitorFunction", sysArg)       
            sysArg = re.sub(r'^#\d+$', "#", sysArg)
            sysArg = re.sub(r'Categories\\.*$', "Categories", sysArg)
            sysArg = re.sub(r'Transforms\\[^\\]*?[0-9][^\\]*?$', "Transforms", sysArg)
            sysArg = re.sub(r'UnitVersioning_\d+$', "UnitVersioning", sysArg)
            sysArg = re.sub(r'_[a-z0-9\._]{30,}', "_XXX", sysArg)
            sysArg = re.sub(r'[a-z0-9]{25,}', "XXX", sysArg)    
            sysArg = re.sub(r'(_\d+){4,}', "", sysArg)
            sysArg = re.sub(r'flag[A-Z0-9]{15,}', "flag", sysArg)
            sysArg = re.sub(r'(\\[a-f0-9]+){2,}', "", sysArg)
            sysArg = re.sub(r'([a-f0-9]+-){3,}[a-f0-9]+', "XXX", sysArg)
            sysArg = re.sub(r'0x[0-9A-F]{7,}', "XXX", sysArg)
            sysArg = re.sub(r'[a-f0-9A-F]{10,}', "XXX", sysArg)
            sysArg = re.sub(r'(\d+\.){2,}\d+', "XXX", sysArg)
            sysArg = re.sub(r'\d{7,}', "", sysArg)
            sysArg = re.sub(r'XXX,\d+', "XXX", sysArg)
            sysArg = re.sub(r'\{.*XXX.*\}', "", sysArg)
            #print "regex executed!"
        #if sysArg == '':
        #    outputFile.write(syscall + '\n')
        #else:
        outputFile.write(syscall + '\t' + sysArg + '\n')

    print inputFileName + " processTrace() done!"
    inputFile.close()
    outputFile.close()
