# -*- coding: utf-8 -*-
#!/usr/bin/python
import re
import sys


# pre-process traces
#def processTrace(inputFileName, outputFileName):
#def processTrace():
if len(sys.argv) != 3:
    print "3 arguments only!"
else:
    inputFileName = sys.argv[1]
    outputFileName = sys.argv[2]

    inputFile = open(inputFileName, 'r')
    outputFile = open(outputFileName, 'w')

    input_list = inputFile.readlines()
    for input_line in input_list:
        input_line = input_line.split('\t')
        syscall = input_line[0].strip('\n')
        
        if len(input_line) < 2:
            sysArg = ''
        else:
            sysArg = input_line[1].strip('\n')
            sysArg_lower = sysArg.lower()

            if sysArg_lower.endswith('.dll'):  # compare file name
                sysArg = "%r" % sysArg
                sysArg = sysArg.split("\\")[-1].strip("'")
            elif sysArg_lower.endswith('.cnf') or sysArg_lower.endswith('.conf') or \
               sysArg_lower.endswith('.config') or sysArg_lower.endswith('.ini') or \
               sysArg_lower.endswith('.manifest'):
                sysArg = "@ConfigurationFile"
            elif sysArg_lower.endswith('.bak') or sysArg_lower.endswith('.aff') or \
                 sysArg_lower.endswith('.bk') or sysArg_lower.endswith('.bkl') or \
                 sysArg_lower.endswith('.cab') or sysArg_lower.endswith('.crx') or \
                 sysArg_lower.endswith('.dat') or sysArg_lower.endswith('.data') or \
                 sysArg_lower.endswith('.db') or sysArg_lower.endswith('.db-journal') or \
                 sysArg_lower.endswith('.db-mj48724A972') or sysArg_lower.endswith('.db-mjD176209F0') or \
                 sysArg_lower.endswith('.db-shm') or sysArg_lower.endswith('.db-wal') or \
                 sysArg_lower.endswith('.dmp') or sysArg_lower.endswith('.docx') or \
                 sysArg_lower.endswith('.dotm') or sysArg_lower.endswith('.hdmp') or \
                 sysArg_lower.endswith('.json') or sysArg_lower.endswith('.ldb') or \
                 sysArg_lower.endswith('.log') or sysArg_lower.endswith('.pdf') or \
                 sysArg_lower.endswith('.sdb') or sysArg_lower.endswith('.sqlite') or \
                 sysArg_lower.endswith('.sqlite3') or sysArg_lower.endswith('.sqlite-journal') or \
                 sysArg_lower.endswith('.txt') or sysArg_lower.endswith('.wps') or sysArg_lower.endswith('.xml') or \
                 sysArg_lower.endswith('.xml~') or sysArg_lower.endswith('.xpi') or sysArg_lower.endswith('.xrc') or \
                 sysArg_lower.endswith('.zip') or sysArg_lower.endswith('.etl'):
                sysArg = "@DataFile"
            elif sysArg_lower.endswith('.drv'):
                sysArg = "@DeviceFile"
            elif sysArg_lower.endswith('.bat') or sysArg_lower.endswith('.bin') or \
                 sysArg_lower.endswith('.exe') or sysArg_lower.endswith('.hta') or \
                 sysArg_lower.endswith('.lnk') or sysArg_lower.endswith('.msi') or \
                 sysArg_lower.endswith('.php') or sysArg_lower.endswith('.py') or \
                 sysArg_lower.endswith('.vbs'):
                sysArg = "@ExecutableFile"
            elif sysArg_lower.endswith('.eot') or sysArg_lower.endswith('.fon') or \
                 sysArg_lower.endswith('.ttc') or sysArg_lower.endswith('.ttf'):
                sysArg = "@FontFile"
            elif sysArg_lower.endswith('.alias') or sysArg_lower.endswith('.bitmap') or \
                 sysArg_lower.endswith('.bmp') or sysArg_lower.endswith('.gif') or \
                 sysArg_lower.endswith('.icm') or sysArg_lower.endswith('.ico') or \
                 sysArg_lower.endswith('.jpg') or sysArg_lower.endswith('.png'):
                sysArg = "@ImageFile"
            elif sysArg_lower.endswith('.mui') or sysArg_lower.endswith('.nls'):
                sysArg = "@LanguageFile"                
            elif sysArg_lower.endswith('.pyd') or sysArg_lower.endswith('.bpl') or sysArg_lower.endswith('.jar'):
                sysArg = "@LibraryFile"                 
            elif sysArg_lower.endswith('.wav') or sysArg_lower.endswith('.wma') or \
                 sysArg_lower.endswith('.wmdb') or sysArg_lower.endswith('.wmv') or \
                 sysArg_lower.endswith('.wpl'):
                sysArg = "@MediaFile"
            elif sysArg_lower.endswith('.library-ms') or sysArg_lower.endswith('.sys'):
                sysArg = "@SystemFile"                       
            elif sysArg_lower.endswith('.cache') or sysArg_lower.endswith('.temp') or \
                 sysArg_lower.endswith('.temp-tmp'):
                sysArg = "@TemporaryFile"
            elif sysArg_lower.endswith('.ashx') or sysArg_lower.endswith('.aspx') or \
                 sysArg_lower.endswith('.css') or sysArg_lower.endswith('.dtd') or \
                 sysArg_lower.endswith('.htm') or sysArg_lower.endswith('.js') or \
                 sysArg_lower.endswith('.url'):
                sysArg = "@WebpageFile"                
                
            else:
                sysArg = re.sub(r'[^\\]*\.dat', "XXX.dat", sysArg)
                sysArg = re.sub(r'[^\\]*\.(temp|TMP|tmp)', "XXX.temp", sysArg)
                sysArg = re.sub(r'[^\\]*\.(nls|NLS)', "XXX.nls", sysArg)
                sysArg = re.sub(r'[^\\]*\.(png|PNG)', "XXX.png", sysArg)
                sysArg = re.sub(r'[^\\]*\.css$', "XXX.css", sysArg)
                sysArg = re.sub(r'[^\\]*\.(js|JS)$', "XXX.js", sysArg)
                sysArg = re.sub(r'[^\\]*\.json$', "XXX.json", sysArg)  
                sysArg = re.sub(r'[^\\]*\.(htm|html)$', "XXX.htm", sysArg)        
                sysArg = re.sub(r'\{.+-.+-.+-.+\}', "XXX", sysArg)
                sysArg = re.sub(r'[A-Z0-9]+-[^\\]+-[^\\]+-[0-9]+(_Classes)*', "XXX", sysArg)    
                sysArg = re.sub(r'Plane\d+$', "Plane", sysArg)
                sysArg = re.sub(r'msvideo\d+$', "msvideo", sysArg)       
                sysArg = re.sub(r'wave\d+$', "wave", sysArg)
                sysArg = re.sub(r'midi\d+$', "midi", sysArg)
                sysArg = re.sub(r'countrycode\[\d+\]$', "countrycode", sysArg)
                sysArg = re.sub(r'MonitorFunction\d+$', "MonitorFunction", sysArg)       
                sysArg = re.sub(r'^#\d+$', "#", sysArg)
                sysArg = re.sub(r'Categories\\.*$', "Categories", sysArg)
                sysArg = re.sub(r'Transforms\\[^\\]*?[0-9][^\\]*?$', "Transforms", sysArg)
                sysArg = re.sub(r'UnitVersioning_\d+$', "UnitVersioning", sysArg)
                sysArg = re.sub(r'_[a-z0-9\._]{30,}', "_XXX", sysArg)
                sysArg = re.sub(r'[a-z0-9]{25,}', "XXX", sysArg)    
                sysArg = re.sub(r'(_\d+){4,}', "", sysArg)
                sysArg = re.sub(r'flag[A-Z0-9]{15,}', "flag", sysArg)
                sysArg = re.sub(r'(\\[a-f0-9]+){2,}', "", sysArg)
                sysArg = re.sub(r'([a-f0-9]+-){3,}[a-f0-9]+', "XXX", sysArg)
                sysArg = re.sub(r'0x[0-9A-F]{7,}', "XXX", sysArg)
                sysArg = re.sub(r'[a-f0-9A-F]{10,}', "XXX", sysArg)
                sysArg = re.sub(r'(\d+\.){2,}\d+', "XXX", sysArg)
                sysArg = re.sub(r'\d{7,}', "", sysArg)  
                sysArg = re.sub(r'XXX,\d+', "XXX", sysArg)          

        nonStringArgs = input_line[2]
        outputFile.write("{0}\t{1}\t{2}".format(syscall, sysArg, nonStringArgs))

    inputFile.close()
    outputFile.close()