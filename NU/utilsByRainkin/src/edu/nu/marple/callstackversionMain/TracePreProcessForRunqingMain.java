package edu.nu.marple.callstackversionMain;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import edu.nu.marple.GlobalConfig;
import edu.nu.marple.callstacktool.CallstackLoopUtils;
import edu.nu.marple.callstacktool.CallstackLoopUtils.PossibleResult;
import edu.nu.marple.callstacktool.CallstackUtils;

public class TracePreProcessForRunqingMain {

	public static String dirPath = "";
	public static String phfsName = "";
	public static int concurrentCores = 1;
	public static void main(String[] args) throws IOException {
		
		String command = "";
		if (args.length >= 1){
			command = args[0];
			switch (command) {
			case "extractTopAPISetSigs":
				if (args.length == 4){
					dirPath = args[1];
					phfsName = args[2];
					concurrentCores = new Integer(args[3]);
				} 
				extractTopAPISetSigs();
				break;
				
			case "extractOnePossibleTopAPISigsByChoosingLongestLoopForNonPhfSet":
				if (args.length == 4){
					dirPath = args[1];
					phfsName = args[2];
					concurrentCores = new Integer(args[3]);
				} 
				extractOnePossibleTopAPISigsByChoosingLongestLoopForNonPhfSet();
				break;
				
			case "extractExeControlFlowGraphForNonPhfSet":
				if (args.length == 3){
					dirPath = args[1];
					phfsName = args[2];
				} 
				extractExeControlFlowGraphForNonPhfSet();
				break;
			default:
				break;
			}
		}
		
		
//		extractExeCallTreeForNonPhfSet();
//		extractAllPossibleSyscallSigsForNonPhfSet();
//		extractOnePossibleSyscallSigsByChoosingLongestLoopForNonPhfSet();
//		extractExeControlFlowGraphForNonPhfSet();
//		extractOnePossibleTopAPISigsByChoosingLongestLoopForNonPhfSet();
//		extractTopAPISigsForNonPhfSet();
//		processNonPhfSet();
		//processTrainingSet();
		//processTestingSet();
	}

//	public static void processNonPhfSet() {
//		List<String> phfs = Arrays.asList( "remotedesktop"
//				//,  "send&exec"
//				);
//		phfs.forEach(t -> {
//			try {
//				CallstackUtils.extractCallGraph("data/withcallstack_pdbparser/training_set/", t, ".callstackoutput", ".dot");
//			} catch (Exception e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();			}
//		});
//	}
	
	public static void extractAllPossibleTopAPISigsForNonPhfSet(){
		List<String> phfs = Arrays.asList(
//				"benign"
				"remotedesktop"
//				"keylogger"
//				"audiorecord"
//				"test"
				//,  "send&exec"
				);
		phfs.forEach(t -> {
			try {
				CallstackLoopUtils.extractTopAPISigForEachTrace(concurrentCores, "data/withcallstack_pdbparser/training_set/", t, ".callstackoutput", ".APISigs", PossibleResult.All, false);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();			}
		});
	}
	
	public static void extractOnePossibleTopAPISigsByChoosingLongestLoopForNonPhfSet(){
		List<String> phfs = new ArrayList<String>();
		
		if (dirPath.isEmpty() && phfsName.isEmpty()){
			phfs = Arrays.asList(
//					"benign"
					"remotedesktop"
//					"keylogger"
//					"download&exec"
//					"audiorecord"
//					"test"
					//,  "send&exec"
					);
			
			dirPath = "data/withcallstack_5_24/training set/";
		} else {
			
			phfs = Arrays.asList(phfsName.split("@"));
		}
		
		System.out.println(phfs);
		System.out.println(dirPath);
		
		phfs.forEach(t -> {
			try {
				CallstackLoopUtils.extractTopAPISigForEachTrace(concurrentCores, dirPath, t, ".callstackoutput", ".APISigs", PossibleResult.Longest, false);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();			}
		});
	}
	
	public static void extractAllPossibleSyscallSigsForNonPhfSet(){
		List<String> phfs = Arrays.asList( 
				"keylogger"
//				"audiorecord"
//				"test"
				//,  "send&exec"
				);
		phfs.forEach(t -> {
			try {
				CallstackLoopUtils.extractSyscallSigForEachTrace("data/withcallstack_pdbparser/training_set/", t, ".callstackoutput", ".syscallSigs", CallstackLoopUtils.PossibleResult.All, false);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();			}
		});
	}
	
	public static void extractOnePossibleSyscallSigsByChoosingLongestLoopForNonPhfSet(){
		List<String> phfs = Arrays.asList(
//				"remotedesktop"
				"hahaha"
//				"audiorecord"
//				"test"
				//,  "send&exec"
				);
		phfs.forEach(t -> {
			try {
				CallstackLoopUtils.extractSyscallSigForEachTrace("data/withcallstack_pdbparser/training_set/", t, ".callstackoutput", ".syscallSigs", CallstackLoopUtils.PossibleResult.Longest, false);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();			}
		});
	}
	
	
	public static void extractTopAPISetSigs(){
		List<String> phfs = new ArrayList<String>();
		
		if (dirPath.isEmpty() && phfsName.isEmpty()){
			phfs = Arrays.asList(
//					"benign"
					"remotedesktop"
//					"keylogger"
//					"download&exec"
//					"audiorecord"
//					"test"
					//,  "send&exec"
					);
			
			dirPath = "data/withcallstack_5_24/training set/";
		} else {
			
			phfs = Arrays.asList(phfsName.split("@"));
		}
		
		System.out.println(phfs);
		System.out.println(dirPath);
		
		phfs.forEach(t -> {
			try {
				CallstackLoopUtils.extractTopAPISetSigForEachTrace(dirPath, t, ".callstackoutput", ".APISetSigs");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();			}
		});
	}
	
	public static void extractExeControlFlowGraphForNonPhfSet(){
		List<String> phfs;
		if (dirPath.isEmpty() && phfsName.isEmpty()){
			phfs = Arrays.asList(
//					"benign"
					"remotedesktop"
//					"keylogger"
//					"download&exec"
//					"audiorecord"
//					"test"
					//,  "send&exec"
					);
			
			dirPath = "data/withcallstack_5_24/training set/";
		} else {
			
			phfs = Arrays.asList(phfsName.split("@"));
		}
		
		phfs.forEach(t -> {
			try {
				CallstackLoopUtils.extractExeControlFlowGraph(dirPath, t, ".callstackoutput", ".exe_cfg_dot", "F://Graphviz//bin//dot.exe");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();			}
		});
	}
	
	public static void extractExeCallTreeForNonPhfSet(){
		List<String> phfs = Arrays.asList(
//				"benign"
//				"keylogger"
				"remotedesktop"
//				"audiorecord"
//				"test"
				//,  "send&exec"
				);
		phfs.forEach(t -> {
			try {
				CallstackLoopUtils.extractExeCallTree("data/withcallstack_pdbparser/training_set/", t, ".callstackoutput", ".calltree_dot", "F://Graphviz//bin//dot.exe", 1000);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();			}
		});
	}
	
	
	

}
