package edu.nu.marple.callstackversionMain;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jboss.netty.handler.queue.BufferedWriteHandler;

import com.google.gson.GsonBuilder;

import edu.nu.marple.callstacktool.CallstackLoopUtils;

public class PreprocessOnlineDumpFile {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
//		String dirPath = "C:/Users/rainkin1993/Desktop/bak/";
		String dirPath = args[0];
		String fileExtension = ".txt";
		
		File[] listFiles = new File(dirPath).listFiles();
		List<File> files = new ArrayList<File>();
	    for(File f: listFiles){
	        if(f.isFile() && f.getName().endsWith(fileExtension)){
	            files.add(f);
	        }
	    }
	    
	    for (File onlineDumpFile : files){
	    	List<String> topAPINameList = new ArrayList<String>();
	    	BufferedReader reader = new BufferedReader(new FileReader(onlineDumpFile));
	    	String line = reader.readLine();
	    	while (line != null) {
				String topAPIName = line.replaceAll("\\[.*\\{apiName=\\'(.*)\\'}\\[.*\\]", "$1");
//				System.out.println(topAPIName);
				topAPINameList.add(topAPIName);
				line = reader.readLine();
			}
	    	
	    	topAPINameList = CallstackLoopUtils.removeFromBlackListsStringVersion(topAPINameList);
	    	
	    	BufferedWriter writer = new BufferedWriter(new FileWriter(onlineDumpFile.getAbsolutePath() + ".callstackoutput.topAPI"));
	    	writer.write(new GsonBuilder().setPrettyPrinting().create().toJson(topAPINameList));
	    	writer.close();
	    }
	}

}
