package edu.nu.marple.callstackversionMain;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import edu.nu.marple.SigResults;
import edu.nu.marple.callstacktool.CallstackLoopUtils;

public class APISigsMatchUtils implements Runnable {

	public  List<String> phfs;	
	public  String signatureFolder;
	public  String signatureFileExtention;
	public  List<String> detectionFolders;
	public  int windowSize;
	public  int slideWindow;
	public  String matchedResultDirPath;
	
	
	
	

    public APISigsMatchUtils(List<String> phfs, String signatureFolder, String signatureFileExtention,
			List<String> detectionFolders, int windowSize, int slideWindow, String matchedResultDirPath) {
		super();
		this.phfs = phfs;
		this.signatureFolder = signatureFolder;
		this.signatureFileExtention = signatureFileExtention;
		this.detectionFolders = detectionFolders;
		this.windowSize = windowSize;
		this.slideWindow = slideWindow;
		this.matchedResultDirPath = matchedResultDirPath;
	}
    
    
    
    MatchResultAndScore isMatched(List<String> input, List<String> sig, String phfName) {
    	System.out.println("fake isMatched");
    	return null;
    };

	public void matchPhfsSigs2Folders() throws IOException {
		String uniquePhfsPath = phfs.stream().collect(Collectors.joining("@")) + "_sigs";
		String uniqueFoldersPath = detectionFolders.stream().map(folder -> new File(folder).getName()).collect(Collectors.joining("@")) + "_folders";
		String uniquePath = uniquePhfsPath + "_" + uniqueFoldersPath;
		
		BufferedWriter detectionSummaryWriter = new BufferedWriter(new FileWriter(matchedResultDirPath + uniquePath +  "_onePhf2OneFolder_detection_summary.txt"));		
		Map<SigResults, MatchedResult> sig2matchedProcess = new HashMap<SigResults, MatchedResult>();	
		Map<String, List<SigResults>> sigResults = new HashMap<String, List<SigResults>>();
		
		for( int phfNo = 0; phfNo < phfs.size(); phfNo++ ){
			
			// load sigs
			String sigFilePath = signatureFolder + phfs.get(phfNo) + signatureFileExtention;
			File sigFile = new File(sigFilePath);
			sigResults.put(phfs.get(phfNo), new Gson().fromJson(new FileReader(sigFile), new TypeToken<List<SigResults>>(){}.getType())) ;
			
			// matching one phf sig for all detection folders
			Map<SigResults, MatchedResult> onePhfSig2matchedProcess = new ConcurrentHashMap<SigResults, MatchedResult>();		
			matchOnePhfSigForDetectionFolders(detectionSummaryWriter, detectionFolders, phfs.get(phfNo), onePhfSig2matchedProcess, sigResults);
			
			// summary : one phf sig for all detection folder
			BufferedWriter matchedSigsWriter = new BufferedWriter(new FileWriter(matchedResultDirPath + phfs.get(phfNo) + "_" + uniqueFoldersPath + "_onePhf2AllFolder_detection_result.txt"));
		    matchedSigsWriter.write(new GsonBuilder().setPrettyPrinting().create().toJson(onePhfSig2matchedProcess.values().stream().collect(Collectors.toSet())));
		    matchedSigsWriter.flush();
		    matchedSigsWriter.close();
		    
		    // update onePhfSigs2matchedProcess to sig2matchedProcess
		    for (SigResults onePhfkey : onePhfSig2matchedProcess.keySet()){
		    	if (sig2matchedProcess.containsKey(onePhfkey)){
		    		sig2matchedProcess.get(sigResults).merge(onePhfSig2matchedProcess.get(onePhfkey));
		    	} else {
		    		sig2matchedProcess.put(onePhfkey, onePhfSig2matchedProcess.get(onePhfkey));
		    	}
		    }
		}
	
		// summary: all phf for all detection folder
		BufferedWriter matchedSigsWriter = new BufferedWriter(new FileWriter(matchedResultDirPath + uniquePath + "_allPhfs2AllFolders_summary.txt"));
	    matchedSigsWriter.write(new GsonBuilder().setPrettyPrinting().create().toJson(sig2matchedProcess.values().stream().collect(Collectors.toSet())));
	    matchedSigsWriter.flush();
	    matchedSigsWriter.close();
	    
	    detectionSummaryWriter.close();
		}

	void matchOnePhfSigForDetectionFolders(BufferedWriter detectionSummaryWriter, List<String> folders, String phfName, 
			Map<SigResults, MatchedResult> onePhfSig2matchedProcess, Map<String, List<SigResults>> sigResults) throws IOException {
		for (String detectionFolder : folders){
			ExecutorService pool = Executors.newFixedThreadPool(20);

			detectionSummaryWriter.write("\n\n\n\n\ndetecting " + detectionFolder + "\n");
			detectionSummaryWriter.write("using PHF " + phfName + "\n");
			System.out.println("\n\n\n\n\ndetecting " + detectionFolder + "\n");
			System.out.println("using PHF " + phfName + "\n");
			
			File[] listFiles = new File(detectionFolder).listFiles();
			List<File> files = new ArrayList<File>();
		    for(File f: listFiles){
		        if(f.isFile() && f.getName().endsWith(".topAPI")){
		            files.add(f);
		        }
		    }
		    
		    // start matching one phf sig for one detection folder
		    Map<String, Boolean> thread2matchResult = new ConcurrentHashMap<String, Boolean>();
		    Map<String, List<MatchResultAndScore>> thread2matchResultAndScore = new ConcurrentHashMap<String, List<MatchResultAndScore>>();
		    for (File inputFile : files){
		    	String processName = inputFile.getPath().substring(0, inputFile.getPath().indexOf(".callstackoutput"));
		    	matchOnePhfSigForOneFile(inputFile, processName, phfName, thread2matchResult, thread2matchResultAndScore, onePhfSig2matchedProcess, sigResults, windowSize, slideWindow, pool);
		    }
		    
		    pool.shutdown();
		    
		    try {
		    	pool.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
		    } catch (InterruptedException e) {
		    	e.printStackTrace();
		    }
		    
		    // summary: one phf sig for one detection folder
		    int totalNumber = thread2matchResult.keySet().size();
		    List<String> matchedTraces = new ArrayList<String>();
		    List<String> unmatchedTraces = new ArrayList<String>();
			    for (String key : thread2matchResult.keySet()){
		    	boolean matchResult = thread2matchResult.get(key);
		    	if (matchResult){
		    		matchedTraces.add(key);
//		    		System.out.println(key + "  MATCHING =============");
		    	} else {
		    		unmatchedTraces.add(key);
//		    		System.out.println(key + "  not matched *********");
		    	}
		    }
		    Gson gson = new GsonBuilder().setPrettyPrinting().create();
		    FileWriter writer = new FileWriter("thread2matchResultAndScore.json");
		    gson.toJson(thread2matchResultAndScore, writer);
		    writer.close();
		    
			detectionSummaryWriter.write("total " + totalNumber + "\n");
		    detectionSummaryWriter.write("+++++++++++++++++++++++++++++++++++++++++" + "\n");
		    detectionSummaryWriter.write("matched " + matchedTraces.size() + "\n");
		    detectionSummaryWriter.write(matchedTraces.stream().collect(Collectors.joining("\n")) + "\n");
		    detectionSummaryWriter.write("+++++++++++++++++++++++++++++++++++++++++" + "\n");
		    detectionSummaryWriter.write("unmatched " + unmatchedTraces.size() + "\n");
		    detectionSummaryWriter.write(unmatchedTraces.stream().collect(Collectors.joining("\n")) + "\n\n\n\n");	
		    detectionSummaryWriter.flush();
		    
		}
		
	}
	
	protected File inputFile;
	protected String processName;
	protected String phfName;
	protected Map<String, Boolean> thread2matchResult;
	protected Map<SigResults, MatchedResult> onePhfSig2matchedProcess;
	protected Map<String, List<SigResults>> mySigResults;
	protected Map<String, List<MatchResultAndScore>> thread2matchResultAndScore;
	
	public APISigsMatchUtils(File inputFile, String processName, String phfName, 
			Map<String, Boolean> thread2matchResult, Map<String, List<MatchResultAndScore>> thread2matchResultAndScore,
			Map<SigResults, MatchedResult> onePhfSig2matchedProcess,
			Map<String, List<SigResults>> mySigResults, int windowSize, int slideWindow) {
		this.inputFile = inputFile;
		this.processName = processName;
		this.phfName = phfName;
		this.thread2matchResult = thread2matchResult;
		this.thread2matchResultAndScore = thread2matchResultAndScore;
		this.onePhfSig2matchedProcess = onePhfSig2matchedProcess;
		this.mySigResults = mySigResults;
		this.windowSize = windowSize;
		this.slideWindow = slideWindow;
	}

	void matchOnePhfSigForOneFile(File inputFile, String processName, String phfName, 
					Map<String, Boolean> thread2matchResult, Map<String, List<MatchResultAndScore>> thread2matchResultAndScore,
					Map<SigResults, MatchedResult> onePhfSig2matchedProcess, Map<String, List<SigResults>> sigResults,
					int windowSize, int slideWindow, ExecutorService pool) 
					throws JsonIOException, JsonSyntaxException, IOException {
			/*
                    
                    Runnable r = new APISigsLCSUtils(inputFile, processName, phfName, thread2matchResult, onePhfSig2matchedProcess, sigResults, windowSize, slideWindow);
                    pool.execute(r);
                    //Thread t = new Thread(r);
                    //t.start();
                    //System.out.println("Detecting "+processName);
                    */
			System.out.println("Fake matchOnePhfSigForOneFile");
	}
	
	@Override
	public void run() {
		List<String> sequence = null;
		try {
			sequence = new Gson().fromJson(new FileReader(inputFile), new TypeToken<List<String>>(){}.getType());
		} catch (Exception e) {
			e.printStackTrace();
		}
		sequence = CallstackLoopUtils.removeFromBlackListsStringVersion(sequence);
		
    	boolean isMatch = false;
    	for (SigResults sigResult : mySigResults.get(phfName)){
    		List<String> sig = sigResult.sig;
    		
    		int i = 0;
	    	for (; i < sequence.size()-windowSize; i += slideWindow){
    			List<String> subSeq = sequence.subList(i, i+windowSize);
    			MatchResultAndScore matchResultAndScore = isMatched(subSeq, sig, phfName);
                List tmpList = thread2matchResultAndScore.get(processName);
                if (tmpList != null) {
                    tmpList.add(matchResultAndScore);
                }
                else {
                    tmpList = new ArrayList<MatchResultAndScore>();
                    tmpList.add(matchResultAndScore);
                    thread2matchResultAndScore.put(processName, tmpList);
                }
    			//thread2matchResultAndScore.put(processName, matchResultAndScore);
    			//System.out.println(Thread.currentThread().getId() + " " + processName + " " + matchResultAndScore);
    			boolean isMatchCurrentSig = matchResultAndScore.isMatched;
	    		if (isMatchCurrentSig){		    			
	    			thread2matchResult.put(processName, isMatch);				    			
	    			System.out.println("==========================================");
		    		System.out.println("testing traces: \n" + processName + "\n");
		    		System.out.println("matched sig: \n" + sig + "\n");
		    		System.out.println("the traces which generate the above matched sig: \n " + sigResult.traces + "\n");
		    		isMatch = true;
		    		
		    		if (!onePhfSig2matchedProcess.containsKey(sigResult)){
		    			Map<String, Integer> matchedProcessWithMatchedNumber = new HashMap<String, Integer>();
		    			matchedProcessWithMatchedNumber.put(processName, 1);
		    			onePhfSig2matchedProcess.put(sigResult, new MatchedResult(sigResult, matchedProcessWithMatchedNumber));
		    		} else {
		    			MatchedResult matchedResult = onePhfSig2matchedProcess.get(sigResult);
		    			if (matchedResult.matchedProcessWithMatchedNumber.containsKey(processName)){
		    				int matchedBumber = matchedResult.matchedProcessWithMatchedNumber.get(processName) + 1;
		    				matchedResult.matchedProcessWithMatchedNumber.put(processName, matchedBumber);
		    			} else {
		    				matchedResult.matchedProcessWithMatchedNumber.put(processName, 1);
		    			}
		    		}
	    		}
	    	}		
	    	
	    	
	    	// the rest of sequence
	    	List<String> restSeq = sequence.subList(i, sequence.size());
			MatchResultAndScore matchResultAndScore = isMatched(restSeq, sig, phfName);
            List tmpList = thread2matchResultAndScore.get(processName);
            if (tmpList != null) {
                tmpList.add(matchResultAndScore);
            }
            else {
                tmpList = new ArrayList<MatchResultAndScore>();
                tmpList.add(matchResultAndScore);
                thread2matchResultAndScore.put(processName, tmpList);
            }
    		//System.out.println(Thread.currentThread().getId() + " " + processName + " " + matchResultAndScore);
			boolean isMatchCurrentSig = matchResultAndScore.isMatched;
    		if (isMatchCurrentSig){		    			
    			thread2matchResult.put(processName, isMatch);
    			System.out.println("==========================================");
	    		System.out.println("testing traces: \n" + processName + "\n");
	    		System.out.println("matched sig: \n" + sig + "\n");
	    		System.out.println("the traces which generate the above matched sig: \n " + sigResult.traces + "\n");
	    		isMatch = true;
	    		
	    		if (!onePhfSig2matchedProcess.containsKey(sigResult)){
	    			Map<String, Integer> matchedProcessWithMatchedNumber = new HashMap<String, Integer>();
	    			matchedProcessWithMatchedNumber.put(processName, 1);
	    			onePhfSig2matchedProcess.put(sigResult, new MatchedResult(sigResult, matchedProcessWithMatchedNumber));
	    		} else {
	    			MatchedResult matchedResult = onePhfSig2matchedProcess.get(sigResult);
	    			if (matchedResult.matchedProcessWithMatchedNumber.containsKey(processName)){
	    				int matchedBumber = matchedResult.matchedProcessWithMatchedNumber.get(processName) + 1;
	    				matchedResult.matchedProcessWithMatchedNumber.put(processName, matchedBumber);
	    			} else {
	    				matchedResult.matchedProcessWithMatchedNumber.put(processName, 1);
	    			}
	    		}				    		
    		}			    		
    	}


    	if (thread2matchResult.containsKey(processName) && thread2matchResult.get(processName) == true)
    		return;
    	thread2matchResult.put(processName, isMatch);
		
	}
	
	protected static String getMD5(String str) throws NoSuchAlgorithmException {
		MessageDigest md = MessageDigest.getInstance("MD5");
	    md.update(str.getBytes());
	    
	    byte byteData[] = md.digest();
	 
	    //convert the byte to hex format method 1
	    StringBuffer sb = new StringBuffer();
	    for (int i = 0; i < byteData.length; i++) {
	    	sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
	    }
	    return sb.toString();
	}

}

class MatchResultAndScore{
	public boolean isMatched;
	public double score;
	public int difference;
	public String sigMd5;
	public MatchResultAndScore(boolean isMatched, double score, int difference, String sigMd5) {
		super();
		this.isMatched = isMatched;
		this.score = score;
		this.difference = difference;
		this.sigMd5 = sigMd5;
	}
	@Override
	public String toString() {
		return "isMatched: " + isMatched + ", score: " + score;
	}
	
	
}

class SigMD5ToSignature {
	private Map<String, Map<String, SigResults>> phf2map = new HashMap<String, Map<String, SigResults>>();
	public SigMD5ToSignature(String signatureFolder, String signatureFileExtension, List<String> phfs) throws JsonIOException, JsonSyntaxException, FileNotFoundException, NoSuchAlgorithmException {
		for (int phfNo = 0; phfNo < phfs.size(); phfNo++) {
			Map<String, SigResults> sigMD5ToSignatureMap = new HashMap<String, SigResults>();
			String phf = phfs.get(phfNo);
			phf2map.put(phf, sigMD5ToSignatureMap);
		}
		for (int phfNo = 0; phfNo < phfs.size(); phfNo++) {
			String sigFilePath = signatureFolder + phfs.get(phfNo) + signatureFileExtension;
			File sigFile = new File(sigFilePath);
			List<SigResults> sigResults = new Gson().fromJson(new FileReader(sigFile), new TypeToken<List<SigResults>>(){}.getType());
			for (SigResults s : sigResults) {
				String MD5 = APISigsMatchUtils.getMD5(s.sig.toString());
				(phf2map.get(phfs.get(phfNo))).put(MD5, s);
			}
		}
	}
	public SigResults convertMD5ToSignature(String MD5) {
		String[] splittedMD5 = MD5.split("@");
		String phf = splittedMD5[0];
		String MD5Value = splittedMD5[1];
		SigResults result = phf2map.get(phf).get(MD5Value);
		return result;
	}
}
