package edu.nu.marple.callstackversionMain;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.Buffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.google.common.io.Files;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import edu.nu.marple.SigResults;


public class APISigMatchMain {

    
	public static List<String> phfs = new ArrayList<String>();
	
	public static String signatureFolder = "generatedSignature/";
	public static String signatureFileExtention = ".APISigs";
	public static List<String> detectionFolders = new ArrayList<String>();
	public static int windowSize = 1000;
	public static int slideWindow = 1000;
	public static String matchedResultDirPath = "./";
    public static String command;
	public static Double threadHold = null;
	public static int threadPoolCapacity = 20;
	
	public static void main(String[] args) throws JsonIOException, JsonSyntaxException, IOException {
		// TODO Auto-generated method stub
		
		if (args.length >= 9){
			command = args[0];
			detectionFolders = Arrays.asList(args[1].split("@"));
			phfs = Arrays.asList(args[2].split("@"));
			signatureFolder = args[3];
			signatureFileExtention = args[4];
			windowSize = new Integer(args[5]);
			slideWindow = new Integer(args[6]);
			matchedResultDirPath = args[7];
			threadPoolCapacity = new Integer(args[8]);
			
			if (args.length == 10){
				threadHold = new Double(args[9]);
			}
			
		} else {
			String[] detectionFoldersArray = new String[] {
//					"data/withcallstack_pdbparser/test_set/benign", 
//					"data/withcallstack_5_24/training set/remoteshell",
//					"data/withcallstack_pdbparser/test_set/audiorecord",
//					"data/withcallstack_pdbparser/test_set/download&exec",
					"data/withcallstack_5_24/testing set/remotedesktop",
//					"data/withcallstack_pdbparser/test_set/obfuscated_remotedesktop"
					"data/withcallstack_5_24/testing set/benign"
					};
			detectionFolders = Arrays.asList(detectionFoldersArray);
			
			String [] phfsArray = new String[] {
//				"audiorecord",
				"keylogger",
				"remotedesktop"
//				"Runqingkeylogger",
			//	"Runqingremotedesktop",
//				"remoteshell",
//				"initialization"
//				"download&exec"
				//"urldownload"
					};
			phfs = Arrays.asList(phfsArray);
		}
		
		switch (command) {
		case "APISigsSeqMatch":
			APISigsMatchUtils seqMatchUtils = new APISigsSeqMatch(phfs, signatureFolder, signatureFileExtention, detectionFolders, windowSize, slideWindow, matchedResultDirPath);
			seqMatchUtils.matchPhfsSigs2Folders();
			break;
		case "APISigsLCSMatch":
			APISigsMatchUtils lcsMatchUtils = new APISigsLCSUtils(threadHold, phfs, signatureFolder, signatureFileExtention, detectionFolders, windowSize, slideWindow, matchedResultDirPath);
			lcsMatchUtils.matchPhfsSigs2Folders();
			break;

		default:
			System.err.println("error format");
			break;
		}
	}
	
}





class MatchedResult{
	public SigResults sigResults;
	public Map<String, Integer> matchedProcessWithMatchedNumber = new HashMap<String, Integer>();
	public MatchedResult(SigResults sigResults, Map<String, Integer> matchedProcessWithMatchedNumber) {
		super();
		this.sigResults = sigResults;
		this.matchedProcessWithMatchedNumber = matchedProcessWithMatchedNumber;
	} 
	
	public MatchedResult(){
		super();
	}
	
	public void merge(MatchedResult matchedResult){
		Map<String, Integer> mx = Stream.of(matchedProcessWithMatchedNumber, matchedResult.matchedProcessWithMatchedNumber)
		        .map(Map::entrySet)          // converts each map into an entry set
		        .flatMap(Collection::stream) // converts each set into an entry stream, then
		                                     // "concatenates" it in place of the original set
		        .collect(
		            Collectors.toMap(        // collects into a map
		                Map.Entry::getKey,   // where each entry is based
		                Map.Entry::getValue, // on the entries in the stream
		                Integer::sum       // such that if a value already exist for
		                                     // a given key, the sum of the old
		                                     // and new value is taken
		            )
		        )
		    ;
		matchedProcessWithMatchedNumber = mx;
	}
	
	
}


