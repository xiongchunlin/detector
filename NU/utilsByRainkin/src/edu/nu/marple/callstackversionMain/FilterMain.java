package edu.nu.marple.callstackversionMain;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import edu.nu.marple.SigResults;

public class FilterMain {

	public static void main(String[] args) throws JsonIOException, JsonSyntaxException, IOException {
		// TODO Auto-generated method stub
		
		
		if (args.length >= 1){
			String command = args[0];
			switch (command) {
			case "removeAPIsInBlackLists":
				removeAPIsInBlackLists(args);
				break;
				
			case "filterSigResultsFromMatchedSigResults":
				filterSigResultsFromMatchedSigResults(args);
				break;
			default:
				break;
			}
		}
		
		
		
	}
	
	private static void removeAPIsInBlackLists(String[] args) throws JsonIOException, JsonSyntaxException, IOException {
		Set<String> blackLists = new HashSet<String>();
		blackLists.add("FlushInstructionCache");
		blackLists.add("VirtualAlloc");
		blackLists.add("RtlAllocateHeap");
		blackLists.add("RtlReAllocateHeap");
		blackLists.add("RtlQueryPerformanceCounter");
		
		List<String> phfs = new ArrayList<String>();
		String originalSigDirPaht = "./";
		String originalSigExtension = ".APISigs";
		if (args.length == 4){
			originalSigDirPaht = args[1];
			originalSigExtension = args[2];
			phfs = Arrays.asList(args[3].split("@"));
		}
		
		for (String phf : phfs){
			String originalSigResultsFilePath = originalSigDirPaht + phf + originalSigExtension;
			Set<SigResults> originalSigResultsSet = new Gson().fromJson(new FileReader(originalSigResultsFilePath), new TypeToken<Set<SigResults>>(){}.getType());
			for (SigResults sigResults : originalSigResultsSet){
				sigResults.sig = sigResults.sig.stream().filter(api -> !blackLists.contains(api) && !isInteger(api)).collect(Collectors.toList());
			}
			
			String removedSigResultsFilePath = originalSigDirPaht + phf + originalSigExtension + ".filteredBlackLists";
			BufferedWriter removedWriter = new BufferedWriter(new FileWriter(removedSigResultsFilePath));
			removedWriter.write(new GsonBuilder().setPrettyPrinting().create().toJson(originalSigResultsSet));
			removedWriter.flush();
			removedWriter.close();			
			
		}
		
		
	}
	
	public static boolean isInteger(String str) {  
        Pattern pattern = Pattern.compile("^[-\\+]?[\\d]*$");  
        return pattern.matcher(str).matches();  
	}

	public static void filterSigResultsFromMatchedSigResults(String[] args) throws JsonIOException, JsonSyntaxException, IOException{
		List<String> phfs = new ArrayList<String>();
		String originalSigDirPaht = "./";
		String originalSigExtension = ".APISigs";
		String matchedSigDirPath = "./";
		String matchedSigExtension = "_sigs_detection_result.txt";
		String filterSigDirPath = "./";
		String filteredSigExtension = ".filtered.APISigs";

		if (args.length == 8){
			originalSigDirPaht = args[1];
			originalSigExtension = args[2];
			matchedSigDirPath = args[3];
			matchedSigExtension = args[4];
			filterSigDirPath = args[5];
			filteredSigExtension = args[6];
			phfs = Arrays.asList(args[7].split("@"));
		}
		
		for (String phf : phfs){
			String originalSigResultsFilePath = originalSigDirPaht + phf + originalSigExtension;
			
			File[] listFiles = new File(matchedSigDirPath).listFiles();
			String matchedSigResultsFilePath = "";
		    for(File f: listFiles){
		        if(f.isFile() && f.getName().endsWith(matchedSigExtension)){
		        	if (f.getName().startsWith(phf)){
		        		matchedSigResultsFilePath = f.getAbsolutePath();
		        	}
		        }
		    }
			
			List<Set<SigResults>> finalSigResultsAndRemvedSigResult = filterMatchedSigs(originalSigResultsFilePath, matchedSigResultsFilePath);
			Set<SigResults> filteredSigResultsSet = finalSigResultsAndRemvedSigResult.get(0);
			Set<SigResults> removedSigResults = finalSigResultsAndRemvedSigResult.get(1);
			
			String finalSigFilePath = filterSigDirPath + phf + filteredSigExtension;
			BufferedWriter finallWriter = new BufferedWriter(new FileWriter(finalSigFilePath));
			finallWriter.write(new GsonBuilder().setPrettyPrinting().create().toJson(filteredSigResultsSet));
			finallWriter.flush();
			finallWriter.close();
			
			String removedSigFilePath = filterSigDirPath + phf + filteredSigExtension + ".removed";
			BufferedWriter removedWriter = new BufferedWriter(new FileWriter(removedSigFilePath));
			removedWriter.write(new GsonBuilder().setPrettyPrinting().create().toJson(removedSigResults));
			removedWriter.flush();
			removedWriter.close();
			
			
		}
	}

	public static List<Set<SigResults>> filterMatchedSigs(String originalSigResultsFilePath, String benignMatchedSigResultsFilePath) throws JsonIOException, JsonSyntaxException, FileNotFoundException{
		Map<String, Set<String>> whiteLists = new HashMap<String, Set<String>>();
		Set<String> keyloggerWhitelists = new HashSet<String>();
		keyloggerWhitelists.add("GetAsyncKeyState");
		keyloggerWhitelists.add("GetKeyState");
		keyloggerWhitelists.add("GetKeyboardState");
		keyloggerWhitelists.add("MapVirtualKeyW");
		
		Set<String> remotedesktopWhitelists = new HashSet<String>();
		remotedesktopWhitelists.add("CreateDCW");
		remotedesktopWhitelists.add("BitBlt");
		remotedesktopWhitelists.add("CreateCompatibleDC");
		remotedesktopWhitelists.add("CreateCompatibleBitmap");
		
		Set<String> remoteshellWhitelists = new HashSet<String>();
		remoteshellWhitelists.add("CreatePipe");
		remoteshellWhitelists.add("CreateProcessW");
		remoteshellWhitelists.add("QueueUserAPC");
		
		Set<String> audiorecordWhitelists = new HashSet<String>();
		audiorecordWhitelists.add("mciSendStringW");
		audiorecordWhitelists.add("waveInOpen");
		audiorecordWhitelists.add("waveInClose");
		audiorecordWhitelists.add("waveOutOpen");
		audiorecordWhitelists.add("waveInMessage");
		audiorecordWhitelists.add("waveOutMessage");
		audiorecordWhitelists.add("waveInStart");
		audiorecordWhitelists.add("mmioOpenW");
		audiorecordWhitelists.add("waveInPrepareHeader");
		audiorecordWhitelists.add("waveInUnprepareHeader");
		audiorecordWhitelists.add("waveInAddBuffer");
		audiorecordWhitelists.add("waveOutGetNumDevs");
		
		Set<String> downloadExecWhitelists = new HashSet<String>();
		downloadExecWhitelists.add("ShellExecuteW");
		downloadExecWhitelists.add("ShellExecuteExW");
		downloadExecWhitelists.add("InternetOpenUrlW");
		downloadExecWhitelists.add("WinHttpConnect");
		downloadExecWhitelists.add("WinHttpOpen");
		downloadExecWhitelists.add("URLDownloadToFileW");
		
		whiteLists.put("keylogger", keyloggerWhitelists);
		whiteLists.put("remotedesktop", remotedesktopWhitelists);
		whiteLists.put("remoteshell", remoteshellWhitelists);
		whiteLists.put("audiorecord", audiorecordWhitelists);
		whiteLists.put("download&exec", downloadExecWhitelists);
		
		List<Set<SigResults>> finalSigResultsAndRemvedSigResult = new ArrayList<Set<SigResults>>();
		Set<SigResults> removedSigResults = new HashSet<SigResults>();
		Set<SigResults> originalSigResultsSet = new Gson().fromJson(new FileReader(originalSigResultsFilePath), new TypeToken<Set<SigResults>>(){}.getType());
		Set<MatchedResult> matchedResultsSet = new Gson().fromJson(new FileReader(benignMatchedSigResultsFilePath), new TypeToken<Set<MatchedResult>>(){}.getType());
		for (MatchedResult matchedResults : matchedResultsSet){
			SigResults benignMatchedSigResults = matchedResults.sigResults;
			
			// check whether sig contains elements in the whitelist
			if (originalSigResultsSet.contains(benignMatchedSigResults)){
				boolean isWhiteListed = false; 
				for (String phfName : whiteLists.keySet()){
					if (originalSigResultsFilePath.contains(phfName)){
						Set<String> phfWhitelist = whiteLists.get(phfName);
						Set<String> retainSet = new HashSet<String>(benignMatchedSigResults.sig);
						retainSet.retainAll(phfWhitelist);
						if (retainSet.size() > 0){
							isWhiteListed = true;
						}
						break;
					}
				}
				
				if (!isWhiteListed){
					originalSigResultsSet.remove(benignMatchedSigResults);
					removedSigResults.add(benignMatchedSigResults);
				}
				
			}
		}
		
		finalSigResultsAndRemvedSigResult.add(originalSigResultsSet);
		finalSigResultsAndRemvedSigResult.add(removedSigResults);
		return finalSigResultsAndRemvedSigResult;
	}
}
