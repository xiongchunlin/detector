package edu.nu.marple.callstackversionMain;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import edu.nu.marple.SigResults;


public class SigStatistics {

	public static void main(String[] args) throws JsonIOException, JsonSyntaxException, IOException {
		String sigDirPath = "";
		List<String> phfs = null;
		String sigFileExtension = "";
		String outputDirPath = "";
		
		if (args.length == 4){
			sigDirPath = args[0];
			phfs = Arrays.asList(args[1].split("@"));
			sigFileExtension = args[2];
			outputDirPath = args[3];
		}
		
		for (String phf : phfs){
			String phfSigFilePaht = sigDirPath + phf + sigFileExtension;
			List<SigResults> sigResultsList = new Gson().fromJson(new FileReader(phfSigFilePaht), new TypeToken<List<SigResults>>(){}.getType());
			statisticForEachSigFile(sigResultsList, outputDirPath + phf + ".SigStatistics");
		}
		
	}
	
	public static void statisticForEachSigFile(List<SigResults> sigResultsList, String outputFilePath) throws IOException{
		
		// total number of sigs
		int totalNumberOfSigs = sigResultsList.size();
		
		// API types and occurrence number
		Map<String, Integer> item2OccurrenceNumber = new HashMap<String, Integer>();
		for (SigResults sigResults : sigResultsList){
			for (String item : sigResults.sig){
				if (!item2OccurrenceNumber.containsKey(item)){
					item2OccurrenceNumber.put(item, 1);
				}
				item2OccurrenceNumber.put(item, item2OccurrenceNumber.get(item) + 1);
			}
		}
		
		// sorted the map by value
		Map<String, Integer> sortedByValueItem2OccurrenceNumber = item2OccurrenceNumber.entrySet().stream()
				.sorted(Map.Entry.<String, Integer>comparingByValue().reversed())
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, 
						(u, v) -> {
							throw new IllegalStateException(String.format("Duplicate key %s", u));
			    }
						,LinkedHashMap::new));
		
		
		// output
		BufferedWriter writer = new BufferedWriter(new FileWriter(outputFilePath));
		writer.write("total number of sig : " + totalNumberOfSigs + "\n\n");
		writer.write("item types and occurrence number : " + "\n");
		writer.write(new GsonBuilder().setPrettyPrinting().create().toJson(sortedByValueItem2OccurrenceNumber));
		writer.close();

	}

}
