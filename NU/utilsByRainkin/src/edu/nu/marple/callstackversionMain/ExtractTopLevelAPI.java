package edu.nu.marple.callstackversionMain;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ibm.marple.EventRecordWithCallstack;

import edu.nu.marple.Filter;
import edu.nu.marple.Utils;
import edu.nu.marple.callstacktool.CallstackChain;
import edu.nu.marple.callstacktool.CallstackItem;
import edu.nu.marple.callstacktool.CallstackLoopUtils;
import edu.nu.marple.callstacktool.CallstackUtils;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class ExtractTopLevelAPI implements Runnable{
	static List<String> fileDirectory = new ArrayList<String>();
	static int concurrentCores = 2;
//	
//	public static void main(String[] args) throws IOException {
//		// TODO Auto-generated method stub
//		for (String nowDir : fileDirectory) {
//			File tmpNowDir = new File(nowDir);
//			File[] tmpfiles = tmpNowDir.listFiles(new FilenameFilter() {
//				public boolean accept(File dir, String name) {
//					return name.endsWith(".tracePreprocessed");
//				}
//			});
//			for (File tmpfile : tmpfiles) {		
//				Map<Integer, List<String>> thread2APIs = new HashMap<Integer, List<String>>(); 
//				System.out.println(tmpfile.getPath());
//				BufferedReader br = new BufferedReader(new FileReader(tmpfile));
//				String data;
//				String lastAPI = "";
//				while ((data = br.readLine()) != null) {
//					EventRecordWithCallstack thisRecord = new Gson().fromJson(data, EventRecordWithCallstack.class);		
//					String[] callstackSplit = thisRecord.callstack.split(",");
//					String topLevelDLLWithAPI = callstackSplit[callstackSplit.length-1];
//					if (topLevelDLLWithAPI.contains("mscorwks.dll:IEE") ){
//						topLevelDLLWithAPI = callstackSplit[callstackSplit.length-2];
//					}
//					if (!topLevelDLLWithAPI.trim().isEmpty()){
//						String topLevelAPI = topLevelDLLWithAPI.substring(topLevelDLLWithAPI.indexOf(":")+1);
//						
//						if (!topLevelAPI.equals(lastAPI)){
//							lastAPI = topLevelAPI;
//							
//							// add to corresponding thread
//							int threadId = new Integer(thisRecord.threadID);
//							if (!thread2APIs.containsKey(threadId))
//								thread2APIs.put(threadId, new ArrayList<String>());
//							thread2APIs.get(threadId).add(topLevelAPI);
//						}
//					}
//				}
//				for (int threadId : thread2APIs.keySet()){
//					List<String> outputContent = thread2APIs.get(threadId);
//					outputContent = new Filter<String>().filterConsecutiveBlocks(outputContent, 1, 100, 2);
//					BufferedWriter outputFileWriter = new BufferedWriter(new FileWriter(tmpfile.getPath() + "_threadId_" + threadId + ".topAPI"));
//					Gson gson = new GsonBuilder().setPrettyPrinting().create();
//					outputFileWriter.write(gson.toJson(outputContent));
//					outputFileWriter.close();
//				}
//				
//
//
//			}
//		}
//		
//				
//	}
	
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		
		if (args.length == 2){
			fileDirectory = Arrays.asList(args[0].split("@"));
			concurrentCores = new Integer(args[1]);
		} else {
			String[] list = {
//				"data/withcallstack_pdbparser/test_set/download&exec/"
//				 "data/withcallstack_pdbparser/test_set/keylogger/"
//				 "data/withcallstack_pdbparser/test_set/remoteshell/",
//				 "data/withcallstack_pdbparser/test_set/audiorecord/",
				 "data/withcallstack_5_24/testing set/remotedesktop/"
//				"data/withcallstack_5_24/testing set/remoteshell/"
//				"data/withcallstack_5_24/training set/remotedesktop"
//				"data/withcallstack_5_24/benign/"
				};
			fileDirectory = Arrays.asList(list);
			concurrentCores = 2;
		}
		
		System.out.println(fileDirectory);
		
		for (String nowDir : fileDirectory) {
			File tmpNowDir = new File(nowDir);
			File[] tmpfiles = tmpNowDir.listFiles(new FilenameFilter() {
				public boolean accept(File dir, String name) {
					return name.endsWith(".callstackoutput");
				}
			});

			ExecutorService executor = Executors.newFixedThreadPool(concurrentCores);
			for (File tmpfile : tmpfiles) {	
//				if (!tmpfile.getName().contains("VanToMRAT1.4"))
//					continue;
				
				ExtractTopLevelAPI worker = new ExtractTopLevelAPI();
				worker.targetFile = tmpfile;
	            executor.execute(worker);
				

			}
            executor.shutdown();
            
            try {
		    	executor.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
		    } catch (InterruptedException e) {
		    	e.printStackTrace();
		    }
		}
		
				
	}
	
	

	public File targetFile = null;
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		
		Map<String, List<String>> thread2APIs = new HashMap<String, List<String>>(); 
		System.out.println(targetFile.getPath());				
		Map<String, List<CallstackChain>> thread2callstacks;
		try {
			thread2callstacks = CallstackLoopUtils.extractTopAPIWithExeCallstack(targetFile.getAbsolutePath());		
	//		Map<String, List<CallstackChain>> thread2callstacks = CallstackLoopUtils.extractFullCallstackWithEvent(tmpfile.getAbsolutePath());

			// remove top API in blacklists
			for (String threadkey : thread2callstacks.keySet()){
				List<CallstackChain> callstacks = thread2callstacks.get(threadkey);
				thread2callstacks.put(threadkey, CallstackLoopUtils.removeFromBlackListsCallstackChainVersion(callstacks));
			}
			
			thread2APIs = CallstackLoopUtils.extractTopAPIfromCallstack(thread2callstacks);
			
			for (String threadKey : thread2APIs.keySet()){
				List<String> outputContent = thread2APIs.get(threadKey);
				BufferedWriter outputFileWriter = new BufferedWriter(new FileWriter(targetFile.getPath() + "_threadId_" + threadKey + ".topAPI"));
				Gson gson = new GsonBuilder().setPrettyPrinting().create();
				outputFileWriter.write(gson.toJson(outputContent));
				outputFileWriter.close();
			}
		
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println();
			System.exit(1);
		}
	}

}
