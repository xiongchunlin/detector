package edu.nu.marple.callstackversionMain;

import java.util.List;

public class APISigsSeqMatch extends APISigsMatchUtils {

	public APISigsSeqMatch(List<String> phfs, String signatureFolder, String signatureFileExtention,
			List<String> detectionFolders, int windowSize, int slideWindow, String matchedResultDirPath) {
		super(phfs, signatureFolder, signatureFileExtention, detectionFolders, windowSize, slideWindow, matchedResultDirPath);
		// TODO Auto-generated constructor stub
	}

	@Override
	MatchResultAndScore isMatched(List<String> input, List<String> sig, String phfName) {
		// TODO Auto-generated method stub
		boolean isMatched = isSubSequence(input, sig, input.size(), sig.size());
		double score = 1;
		return new MatchResultAndScore(isMatched, score, 0, "");
	}
	
	// Returns true if str1[] is a subsequence of str2[]
    // m is length of str1 and n is length of str2
    private boolean isSubSequence(List<String> str1, List<String> str2, int m, int n)
    {
        // Base Cases
        if (m == 0) 
            return true;
        if (n == 0) 
            return false;
             
        // If last characters of two strings are matching
        if (str1.get(m-1).equals(str2.get(n-1)))
            return isSubSequence(str1, str2, m-1, n-1);
 
        // If last characters are not matching
        return isSubSequence(str1, str2, m, n-1);
    }

}
