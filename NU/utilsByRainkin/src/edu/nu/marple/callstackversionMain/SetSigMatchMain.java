package edu.nu.marple.callstackversionMain;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.apache.hadoop.io.nativeio.NativeIO.Windows;
import org.jboss.netty.util.internal.ConcurrentHashMap;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import edu.nu.marple.SetSignature;
import scala.Array;

public class SetSigMatchMain implements Runnable {

	public static List<String> phfs = new ArrayList<String>();
	
	public static final String signatureFolder = "generatedSignature/";
	public static final String signatureFileExtention = ".APISetSigs";
	public static List<String> detectionFolders = new ArrayList<String>();
	
	public static Map<String, List<SetSignature>> phf2Sigs = new HashMap<String, List<SetSignature>>();
	public static int concurrentCores = 2;
	public static double threadhold = 0.8;
	public static int windowSize = 10000;
	public static int slideWindow = 3000;
	
	
	public static void main(String[] args) throws JsonIOException, JsonSyntaxException, FileNotFoundException {
		// TODO Auto-generated method stub
		
		if (args.length == 6){
			detectionFolders = Arrays.asList(args[0].split("@"));
			phfs = Arrays.asList(args[1].split("@"));
			windowSize = new Integer(args[2]);
			slideWindow = new Integer(args[3]);
			threadhold = new Double(args[4]);
			concurrentCores = new Integer(args[5]);
		} else {
			 String[] list = new String[] {
//						"data/withcallstack_5_24/testing set/benign", 
//						"data/withcallstack_5_24/testing set/keylogger",
//						"data/withcallstack_5_24/testing set/audiorecord",
//						"data/withcallstack_5_24/testing set/download&exec",
						"data/withcallstack_5_24/testing set/benign"
//						"data/withcallstack_5_24/testing set/obfuscated_remotedesktop"
					 };
			 detectionFolders = Arrays.asList(list);
			 
			 String[] phfsList = 
				 	{
//					"audiorecord",
//					"keylogger" 
					"remotedesktop"
//					"Runqingkeylogger",
				//	"Runqingremotedesktop",
//					"remoteshell",
//					"initialization"
//					"download&exec"
					//"urldownload"
					};
			 
			 phfs = Arrays.asList(phfsList);
		}
		
		System.out.println(phfs);
		System.out.println(detectionFolders);
		
		for( int phfNo = 0; phfNo < phfs.size(); phfNo++ ){
			phf2Sigs.put(phfs.get(phfNo), new Gson().fromJson(new FileReader(signatureFolder + phfs.get(phfNo) + signatureFileExtention), new TypeToken<List<SetSignature>>(){}.getType())) ;
			
			for (String detectionFolder : detectionFolders){
				System.out.println("\n\n\n\n\ndetecting " + detectionFolder);
				File[] listFiles = new File(detectionFolder).listFiles();
				List<File> files = new ArrayList<File>();
			    for(File f: listFiles){
			        if(f.isFile() && f.getName().endsWith(".topAPI")){
			            files.add(f);
			        }
			    }
			    
//			    System.out.println(files);
				ExecutorService executor = Executors.newFixedThreadPool(concurrentCores);
			    for (File inputFile : files){
			    	SetSigMatchMain worker = new SetSigMatchMain();
					worker.targetFile = inputFile;
					worker.sigs = phf2Sigs.get(phfs.get(phfNo));
		            executor.execute(worker);			    	
			    }
			    
			    executor.shutdown();
			    
			    try {
			    	executor.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
			    } catch (InterruptedException e) {
			    	e.printStackTrace();
			    }
			    
			    int totalNumber = thread2matchResult.keySet().size();
			    List<String> matchedTraces = new ArrayList<String>();
			    List<String> unmatchedTraces = new ArrayList<String>();
 			    for (String key : thread2matchResult.keySet()){
			    	boolean matchResult = thread2matchResult.get(key);
			    	if (matchResult){
			    		matchedTraces.add(key);
//			    		System.out.println(key + "  MATCHING =============");
			    	} else {
			    		unmatchedTraces.add(key);
//			    		System.out.println(key + "  not matched *********");
			    	}
			    }
			    
			    System.out.println("total " + totalNumber);
			    System.out.println("+++++++++++++++++++++++++++++++++++++++++");
			    System.out.println("matched " + matchedTraces.size());
			    System.out.println(matchedTraces.stream().collect(Collectors.joining("\n")));
			    System.out.println("+++++++++++++++++++++++++++++++++++++++++");
			    System.out.println("unmatched " + unmatchedTraces.size());
			    System.out.println(unmatchedTraces.stream().collect(Collectors.joining("\n")));
			
			    thread2matchResult.clear();
			}
		}
	}
	
	public File targetFile = null;
	public List<SetSignature> sigs = null;
	public static Map<String, Boolean> thread2matchResult = new ConcurrentHashMap<String, Boolean>();

	
	private static boolean isSetMatched(Set<String> sig, List<String> sequence) {
		// TODO Auto-generated method stub
		Set<String> matchedSig = new HashSet<String>();
		for (String item : sequence){
			if (sig.contains(item)){
				matchedSig.add(item);
			}
		}
		
		double matchedRatio = new Double(matchedSig.size())/new Double(sig.size());
		System.out.println(matchedRatio);
		return matchedRatio >= threadhold ;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		try{
			List<String> sequence = new Gson().fromJson(new FileReader(targetFile), new TypeToken<List<String>>(){}.getType());
//	    	System.out.println(sequence);
	    	String processName = targetFile.getName().substring(0, targetFile.getName().indexOf(".callstackoutput"));
	    	
	    	boolean isMatch = false;
	    	for (SetSignature setSignature : sigs){
	    		Set<String> sig = setSignature.sig;
	    		
	    		int i = 0;
	    		for (; i < sequence.size()-windowSize; i += slideWindow){
	    			List<String> subSeq = sequence.subList(i, i+windowSize);
	    			boolean isMatchCurrentSig = isSetMatched(sig, sequence);
		    		if (isMatchCurrentSig){		    			
		    			thread2matchResult.put(processName, isMatch);
		    			System.out.println("==========================================");
			    		System.out.println("testing traces: \n" + processName + "\n");
			    		System.out.println("matched sig: \n" + sig + "\n");
			    		System.out.println("the traces which generate the above matched sig: \n " + setSignature.traces + "\n");
			    		isMatch = true;
		    		}
	    		}
	    		
	    		List<String> restSeq = sequence.subList(i, sequence.size());
	    		boolean isMatchCurrentSig = isSetMatched(sig, sequence);
	    		if (isMatchCurrentSig){		    			
	    			thread2matchResult.put(processName, isMatch);
	    			System.out.println("==========================================");
		    		System.out.println("testing traces: \n" + processName + "\n");
		    		System.out.println("matched sig: \n" + sig + "\n");
		    		System.out.println("the traces which generate the above matched sig: \n " + setSignature.traces + "\n");
		    		isMatch = true;
	    		}
	    	} 
	    	if (thread2matchResult.containsKey(processName) && thread2matchResult.get(processName) == true)
	    		return;
	    	else 
	    		thread2matchResult.put(processName, isMatch);
		} catch (JsonIOException | JsonSyntaxException | FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
