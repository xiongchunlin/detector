package edu.nu.marple.callstackversionMain;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class NonPhfFilterMain {

public static List<String> phfs = new ArrayList<String>();
	
	public static String signatureFolder = "generatedSignature/";
	public static String signatureFileExtention = ".APISigs";
	public static List<String> detectionFolders = new ArrayList<String>();
	public static int windowSize = 1000;
	public static int slideWindow = 1000;
	public static String matchedResultDirPath = "./";
	public static String command = "";
	public static Double threadHold = null;
	
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		if (args.length >= 8){
			command = args[0];
			detectionFolders = Arrays.asList(args[1].split("@"));
			phfs = Arrays.asList(args[2].split("@"));
			signatureFolder = args[3];
			signatureFileExtention = args[4];
			windowSize = new Integer(args[5]);
			slideWindow = new Integer(args[6]);
			matchedResultDirPath = args[7];
			
			if (args.length == 9){
				threadHold = new Double(args[8]);
			}
			
		} else {
			String[] detectionFoldersArray = new String[] {
//					"data/withcallstack_pdbparser/test_set/benign", 
//					"data/withcallstack_5_24/training set/remoteshell",
//					"data/withcallstack_pdbparser/test_set/audiorecord",
//					"data/withcallstack_pdbparser/test_set/download&exec",
					"data/withcallstack_5_24/testing set/remotedesktop",
//					"data/withcallstack_pdbparser/test_set/obfuscated_remotedesktop"
					"data/withcallstack_5_24/testing set/benign"
					};
			detectionFolders = Arrays.asList(detectionFoldersArray);
			
			String [] phfsArray = new String[] {
//				"audiorecord",
				"keylogger",
				"remotedesktop"
//				"Runqingkeylogger",
			//	"Runqingremotedesktop",
//				"remoteshell",
//				"initialization"
//				"download&exec"
				//"urldownload"
					};
			phfs = Arrays.asList(phfsArray);
		}
		
		
		for (String phf : phfs){
			List<String> nonPhfDetectionFolders = detectionFolders.stream().filter(folder -> !folder.contains(phf)).collect(Collectors.toList());
			System.out.println("***********************************");
			System.out.println("using phf " + phf);
			System.out.println("non phf folder : " + nonPhfDetectionFolders);
			
			List<String> onePhfList = new ArrayList<String>(); 
			onePhfList.add(phf);
			
			switch (command) {
			case "APISigsSeqMatch":
				APISigsMatchUtils seqMatchUtils = new APISigsSeqMatch(onePhfList, signatureFolder, signatureFileExtention, nonPhfDetectionFolders, windowSize, slideWindow, matchedResultDirPath);
				seqMatchUtils.matchPhfsSigs2Folders();
				break;
			case "APISigsLCSMatch":
				APISigsMatchUtils lcsMatchUtils = new APISigsLCSUtils(threadHold, onePhfList, signatureFolder, signatureFileExtention, nonPhfDetectionFolders, windowSize, slideWindow, matchedResultDirPath);
				lcsMatchUtils.matchPhfsSigs2Folders();
				break;

			default:
				System.err.println("error format");
				break;
			}
		}
		
		
	}

	
}
