package edu.nu.marple.callstackversionMain;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;

import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

import edu.nu.marple.SigResults;
import edu.nu.marple.sequence.LongestCommonSubsequence;

public class APISigsLCSUtils extends APISigsMatchUtils {

	public double threadHold = 0.8;
	public APISigsLCSUtils(Double threadHold, List<String> phfs, String signatureFolder, String signatureFileExtention,
			List<String> detectionFolders, int windowSize, int slideWindow, String matchedResultDirPath) {
		super(phfs, signatureFolder, signatureFileExtention, detectionFolders, windowSize, slideWindow, matchedResultDirPath);
		this.threadHold = threadHold;
		// TODO Auto-generated constructor stub
	}
	
	public APISigsLCSUtils(File inputFile, String processName, String phfName, 
			Map<String, Boolean> thread2matchResult, Map<String, List<MatchResultAndScore>> thread2matchResultAndScore,
			Map<SigResults, MatchedResult> onePhfSig2matchedProcess,
			Map<String, List<SigResults>> sigResults, int windowSize, int slideWindow) {
		super(inputFile, processName, phfName, thread2matchResult, thread2matchResultAndScore, onePhfSig2matchedProcess, sigResults, windowSize, slideWindow);
	}

	@Override
	MatchResultAndScore isMatched(List<String> input, List<String> sig, String phfName) {
		Set<String> inputAPITypes = new HashSet<String>(input);
		Set<String> sigAPITypes = new HashSet<String>(sig);
 		LongestCommonSubsequence lcs = new LongestCommonSubsequence(input, sig);
		double score = new Double(lcs.getScore()) / sig.size();
		int difference = inputAPITypes.size() - sigAPITypes.size();
		boolean isMatched = score >= threadHold && inputAPITypes.size() - sigAPITypes.size() <= 10;
		
		String sigMD5 = phfName + "@";
		try {
			sigMD5 += APISigsMatchUtils.getMD5(sig.toString());
		} catch (Exception e) {}
		return new MatchResultAndScore(isMatched, score, difference, sigMD5);
	}
	
	@Override
	void matchOnePhfSigForOneFile(File inputFile, String processName, String phfName, 
					Map<String, Boolean> thread2matchResult, Map<String, List<MatchResultAndScore>> thread2matchResultAndScore,
					Map<SigResults, MatchedResult> onePhfSig2matchedProcess, Map<String, List<SigResults>> sigResults,
					int windowSize, int slideWindow, ExecutorService pool) 
					throws JsonIOException, JsonSyntaxException, IOException {
		/*
                    this.inputFile = inputFile;
                    this.processName = processName;
                    this.phfName = phfName;
                    this.thread2matchResult = thread2matchResult;
                    this.onePhfSig2matchedProcess = onePhfSig2matchedProcess;
                    this.sigResults = sigResults;
                    */
                    
                    Runnable r = new APISigsLCSUtils(inputFile, processName, phfName, thread2matchResult, thread2matchResultAndScore, onePhfSig2matchedProcess, sigResults, windowSize, slideWindow);
                    pool.execute(r);
                    //Thread t = new Thread(r);
                    //t.start();
                    //System.out.println("Detecting "+processName);
	}

}
