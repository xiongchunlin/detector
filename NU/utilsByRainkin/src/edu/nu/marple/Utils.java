package edu.nu.marple;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.hadoop.mapred.lib.InputSampler.SplitSampler;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ibm.marple.EventRecord;

import scala.annotation.meta.setter;
import scala.collection.generic.BitOperations.Int;

public class Utils {
	
	public static String[] specialExeList = {"cmd.exe",
											 "svchost.exe",
											 "shutdown.exe",
											 "dwm.exe",
											 "whoami.exe",
											 "lsass.exe",
											 "taskmgr.exe",
											 "dir.exe",
											 "taskhost.exe",
											 "winupdate.exe",
											 "csrss.exe",
											 "SearchIndexer.exe",
											 "services.exe",
											 "audiodg.exe",
											 "conhost.exe"};
   
	private static EventRecord convertEventRecordJson2Object(String json) {
		EventRecord res = new EventRecord();
		res = new Gson().fromJson(json, EventRecord.class);
		return res;
   	}
   
   	private static EventRecordG convertEventRecordGJson2Object(String json) {
   		return new Gson().fromJson(json, EventRecordG.class);
   	}
   
   	/**
    * parse a .output file to a list of EventRecord
    */
   	public static List<EventRecord> EventRecordFile2EventRecordList(String filepath) throws IOException{
   		File file = new File(filepath);
   		BufferedReader reader = new BufferedReader(new FileReader(file));
   		ArrayList<EventRecord> sequences = new ArrayList<>();

   		String line = null;
   		int cnt = 0;
   		while((line = reader.readLine()) != null)
   		{
   			++cnt;
   			//if (cnt % 1000 == 0) System.out.println(cnt);
   			EventRecord newDataRecord = convertEventRecordJson2Object(line);
   			sequences.add(newDataRecord);
   		}

   		return sequences;
   	}
   
   	/**
    * parse a .output.tracePreprocessed file to a list of EventRecordG
    */
   	public static List<EventRecordG> EventRecordGFile2EventRecordGenerationList(String filepath) throws IOException{
	   File file = new File(filepath);
	   BufferedReader reader = new BufferedReader(new FileReader(file));
	   ArrayList<EventRecordG> sequences = new ArrayList<>();

	   String line = null;
	   while((line = reader.readLine()) != null) {
		   EventRecordG eventRecordG = convertEventRecordGJson2Object(line);
		   sequences.add(eventRecordG);
	   }

	   return sequences;
   	}
   	
   	public static Map<String, String> transferStringPara2MapPara(String para) {
   		Map<String, String> resMap = new HashMap<String, String>();
   		String[] splitResults = para.split(" @ ");
   		for (String argField : splitResults) {
   			String[] splitResults2 = argField.split(":");
   			resMap.put(splitResults2[0], splitResults2[1]);
   		}
   		return resMap;
   	}
   	
   	public static String transferMapPara2StringPara(Map<String, String> para) {
   		String resStr = "";
   		if (para.containsKey("ImageFileName")) {
   			resStr = "ImageFileName:" + para.get("ImageFileName") + " @ CommandLine:" + para.get("CommandLine");
   		} else {
   			for (String key : para.keySet()) {
   				resStr = key + ":" + para.get(key);
   			}
   		}
   		return resStr;
   	}
   
   /**
    * pre proces traces: filter useless syscalls, filter consecutive repeated blocks, remove path for args
    * @pram numberOfBlocksToKeep if exist repeated and consecutive blocks, this value point to the number of these blocks to keep
    * note that this value could be 0 which means doesn;t keep any repeated and consecutive blcosk
    */
   	public static void tracePreProcessForPhf(String traceDirPath, String phfName, String traceExtension, String filteredExtension,
   			int minLengthOfConsecutiveBlock, int maxBlockSize, int numberOfBlocksToKeep, String uselessSyscallFilePath, String usefulEventFilePath) throws IOException{
      File dir = new File(traceDirPath + phfName + "/");
      File[] tmpfiles = dir.listFiles(new FilenameFilter() {
          public boolean accept(File dir, String name) {
             return name.endsWith(traceExtension);
          }
      });
      ArrayList<File> files = new ArrayList<File>();
      for (File json : tmpfiles) files.add(json);
      Collections.sort(files, new MyComparator());

      for (File json : files)
      //if (json.length() > 104857600L) 
      {
         System.out.println(json);
         
         Filter filter = new Filter();
         
         List<EventRecord> sequence = EventRecordFile2EventRecordList(json.getAbsolutePath());
         sequence = filter.filterUselessSyscalls(sequence, uselessSyscallFilePath);
         sequence = filter.filterUselessEvents(sequence, usefulEventFilePath);
         List<EventRecordG> sequence2 = argPreProcess(sequence);
         sequence2 = filter.filterConsecutiveBlocks(sequence2, minLengthOfConsecutiveBlock, maxBlockSize, numberOfBlocksToKeep);
         // remove duplicated sequences
//	         while (!sequence.equals(newSequence)) {
//	        	 System.out.println("wow!");
//	        	 sequence = new ArrayList();
//	        	 sequence.addAll(newSequence);
//	        	 newSequence = filter.filterConsecutiveBlocks(sequence, maxBlockSize);
//	         }
         // write back to a new file
         File filteredTrace = new File(json.getAbsolutePath() + filteredExtension);
//		         filteredTrace.createNewFile();
         FileWriter filteredWriter = new FileWriter(filteredTrace);
         for (EventRecordG e : sequence2) {
        	 String output = new GsonBuilder().create().toJson(e);
        	 filteredWriter.write(output + "\n");
         }
         filteredWriter.flush();
         filteredWriter.close();
      }
   }
   
   	/**
     * pre proces trace args
     */
   	public static List<EventRecordG> argPreProcess(List<EventRecord> sequence) throws IOException {

		//preprocess arguments
		List<EventRecordG> resList = new ArrayList<EventRecordG>();
		for (int i = 0; i < sequence.size(); ++i)
		{
			EventRecord tmpRecord = sequence.get(i);
			EventRecordG resRecord = null;
			String fieldArg = "", fieldArg1 = "", fieldArg2 = "";
			if (tmpRecord.eventName.equals("ALPCALPC-Send-Message") || tmpRecord.eventName.equals("ALPCALPC-Receive-Message")) {
				fieldArg = tmpRecord.arguments.get("ProcessName");
				if (fieldArg == null) fieldArg = "";
				fieldArg = argfieldPreProcess(fieldArg);
				resRecord = new EventRecordG(tmpRecord.eventName, "ProcessName:" + fieldArg);
			} else 
			if (tmpRecord.eventName.equals("DiskIoWrite") || tmpRecord.eventName.equals("FileIoCleanup") || tmpRecord.eventName.equals("FileIoClose") 
				|| tmpRecord.eventName.equals("FileIoCreate") || tmpRecord.eventName.equals("FileIoDelete") || tmpRecord.eventName.equals("FileIoDirEnum") 
				|| tmpRecord.eventName.equals("FileIoFileCreate") || tmpRecord.eventName.equals("FileIoFileDelete") || tmpRecord.eventName.equals("FileIoQueryInfo") 
				|| tmpRecord.eventName.equals("FileIoRead") || tmpRecord.eventName.equals("FileIoSetInfo") || tmpRecord.eventName.equals("FileIoWrite")
				|| tmpRecord.eventName.equals("ImageLoad") || tmpRecord.eventName.equals("ImageUnLoad") || tmpRecord.eventName.equals("FileIoFSControl")) {
				if (tmpRecord.arguments.containsKey("OpenPath")) {
					fieldArg = tmpRecord.arguments.get("OpenPath");
				} else {
					fieldArg = tmpRecord.arguments.get("FileName");
				}
				if (fieldArg == null) fieldArg = "";
				fieldArg = argfieldPreProcess(fieldArg);
				resRecord = new EventRecordG(tmpRecord.eventName, "FileName:" + fieldArg);
			}/* else
			if (tmpRecord.eventName.equals("DiskIoDrvMjFnCall")) {
				fieldArg = tmpRecord.arguments.get("MajorFunction");
				fieldArg = argfieldPreProcess(fieldArg);
				resRecord = new EventRecordG(tmpRecord.eventName, "MajorFunction:" + fieldArg);
			} else
			if (tmpRecord.eventName.equals("TCP_Connect") || tmpRecord.eventName.equals("TCP_Disconnect") || tmpRecord.eventName.equals("TCP_Send") 
					|| tmpRecord.eventName.equals("TCP_Rec")) {
				resRecord = new EventRecordG(tmpRecord.eventName, "None:None");
			}*/ else
			if (tmpRecord.eventName.equals("RegistryCreate") || tmpRecord.eventName.equals("RegistryDelete") || tmpRecord.eventName.equals("RegistryEnumerateKey") || tmpRecord.eventName.equals("RegistryEnumerateValueKey") 
				|| tmpRecord.eventName.equals("RegistryKCBCreate") || tmpRecord.eventName.equals("RegistryKCBDelete") || tmpRecord.eventName.equals("RegistryOpen")
				|| tmpRecord.eventName.equals("RegistryQuery") || tmpRecord.eventName.equals("RegistryQueryValue") || tmpRecord.eventName.equals("RegistrySetInformation")
				|| tmpRecord.eventName.equals("RegistrySetValue") || tmpRecord.eventName.equals("RegistryDeleteValue")) {
				fieldArg = tmpRecord.arguments.get("KeyName");
				if (fieldArg == null) fieldArg = "";
				fieldArg = argfieldPreProcess(fieldArg);
				resRecord = new EventRecordG(tmpRecord.eventName, "KeyName:" + fieldArg);
			} else
			if (tmpRecord.eventName.equals("ProcessStart") || tmpRecord.eventName.equals("ProcessEnd")) {
				fieldArg1 = tmpRecord.arguments.get("ImageFileName");
				fieldArg2 = tmpRecord.arguments.get("CommandLine");
				if (fieldArg1 == null) fieldArg1 = "";
				if (fieldArg2 == null) fieldArg2 = "";
				fieldArg1 = argfieldPreProcess(fieldArg1);
				fieldArg2 = argfieldPreProcess(fieldArg2);
				resRecord = new EventRecordG(tmpRecord.eventName, "ImageFileName:" + fieldArg1 + " @@ " + "CommandLine:" + fieldArg2);
			} else 
			if (tmpRecord.eventName.equals("PerfInfoSysClEnter")){
				resRecord = new EventRecordG(tmpRecord.eventName, "SystemCall:" + tmpRecord.arguments.get("SystemCall"));
			} else
			if (tmpRecord.eventName.equals("ALPCALPC-Unwait") || tmpRecord.eventName.equals("ALPCALPC-Wait-For-Reply")) {
				resRecord = new EventRecordG(tmpRecord.eventName, "");
			}
			resList.add(resRecord);
		}
		
		return resList;
	}
	
	//
	private static HashSet<String> configFile = new HashSet<String>(){{
		add("cnf");
		add("conf");
		add("config");
		add("ini");
		add("manifest");
	}
	};
	
	private static HashSet<String> dataFile = new HashSet<String>(){{
		add("bak");add("aff");add("bk");add("bkl");add("cab");add("crx");add("dat");add("data");add("db");add("db-journal");
		add("db-mj48724A972");add("db-mjD176209F0");add("db-shm");add("db-wal");add("dmp");add("docx");add("dotm");add("hdmp");
		add("json");add("ldb");add("log");add("pdf");add("sdb");add("sqlite");add("sqlite3");add("sqlite-journal");add("txt");
		add("wps");add("xml");add("xml~");add("xpi");add("xrc");add("zip");add("etl");
	}
	};
	
	private static HashSet<String> deviceFile = new HashSet<String>(){{
		add("drv");
	}
	};
	
	private static HashSet<String> excutableFile = new HashSet<String>(){{
		add("bat");add("bin");add("exe");add("hta");add("lnk");add("msi");add("php");add("py");add("vbs");
	}
	};

	private static HashSet<String> fontFile = new HashSet<String>(){{
		add("eot");add("fon");add("ttc");add("ttf");
	}
	};
	
	private static HashSet<String> imageFile = new HashSet<String>(){{
		add("alias");add("bitmap");add("bmp");add("gif");add("icm");add("ico");add("jpg");add("png");
	}
	};

	private static HashSet<String> languageFile = new HashSet<String>(){{
		add("mui");add("nls");
	}
	};
	
	private static HashSet<String> libraryFile = new HashSet<String>(){{
		add("pyd");add("bpl");add("jar");
	}
	};
	
	private static HashSet<String> mediaFile = new HashSet<String>(){{
		add("wav");add("wma");add("wmdb");add("wmv");add("wpl");
	}
	};
	
	private static HashSet<String> systemFile = new HashSet<String>(){{
		add("library-ms");add("sys");
	}
	};
	
	private static HashSet<String> temporaryFile = new HashSet<String>(){{
		add("cache");add("temp");add("temp-tmp");add("tmp");
	}
	};
	
	private static HashSet<String> webpageFile = new HashSet<String>(){{
		add("ashx");add("aspx");add("css");add("dtd");add("htm");add("js");add("url");
	}
	};
	
	private static HashMap<String, String> phfArgPreprocessingCache = new HashMap<String, String>();
	
	private static String argfieldPreProcess(String argumentString){
		String tmpString = argumentString;
		while (tmpString.startsWith(" ") || tmpString.startsWith("\"")) tmpString = tmpString.substring(1, tmpString.length());
		while (tmpString.endsWith(" ") || tmpString.endsWith("\"")) tmpString = tmpString.substring(0, tmpString.length() - 1);
		String[] splitResults = tmpString.split("\\.");
		String suffix = "";
		if (splitResults.length >= 1) suffix = splitResults[splitResults.length-1]; else return "";
		String sysArg = null;
		if(configFile.contains(suffix))
		{
			sysArg = "@ConfigurationFile";
		}
		else if (dataFile.contains(suffix)) {
			sysArg = "@DataFile";
		}
		else if (deviceFile.contains(suffix)) {
			sysArg = "@DeviceFile";
		}
		else if (excutableFile.contains(suffix)){
			sysArg = "@ExecutableFile";
			for (String tmp : specialExeList) 
			if (tmpString.endsWith(tmp)){
				sysArg = tmp;
				break;
			}
		}
		else if (fontFile.contains(suffix)){
			sysArg = "@FontFile";
		}
		else if (imageFile.contains(suffix)){
			sysArg = "@ImageFile";
		}
		else if (languageFile.contains(suffix)){
			
			sysArg = "@LanguageFile";
		}
		else if (libraryFile.contains(suffix)){
			sysArg = "@LibraryFile";
		}
		else if (mediaFile.contains(suffix)){
			sysArg = "@MediaFile";
		}
		else if (systemFile.contains(suffix)){
			sysArg = "@TemporaryFile";
		}
		else if (webpageFile.contains(suffix)){
			sysArg = "@WebpageFile";
		}
		else if (tmpString.endsWith("dll"))
		{
//			System.out.println(tmpString);
			sysArg = tmpString.split("\\\\")[tmpString.split("\\\\").length-1];
		}
		else {
			if (phfArgPreprocessingCache.containsKey(tmpString)) {
				return phfArgPreprocessingCache.get(tmpString);
			}
			sysArg = tmpString;
            sysArg = sysArg.replaceAll("[^\\\\]*\\.dat", "XXX.dat");
            sysArg = sysArg.replaceAll("[^\\\\]*\\.(temp|TMP|tmp)", "XXX.temp");
            sysArg = sysArg.replaceAll("[^\\\\]*\\.(nls|NLS)", "XXX.nls");
            sysArg = sysArg.replaceAll("[^\\\\]*\\.(png|PNG)", "XXX.png");
            sysArg = sysArg.replaceAll("[^\\\\]*\\.css$", "XXX.css");
            sysArg = sysArg.replaceAll("[^\\\\]*\\.(js|JS)$", "XXX.js");
            sysArg = sysArg.replaceAll("[^\\\\]*\\.json$", "XXX.json")  ;
            sysArg = sysArg.replaceAll("[^\\\\]*\\.(htm|html)$", "XXX.htm");
            sysArg = sysArg.replaceAll("\\{.+-.+-.+-.+\\}", "XXX");
            sysArg = sysArg.replaceAll("[A-Z0-9]+-[^\\\\]+-[^\\\\]+-[0-9]+(_Classes)*", "XXX");
            sysArg = sysArg.replaceAll("Plane\\d+$", "Plane");
            sysArg = sysArg.replaceAll("msvideo\\d+$", "msvideo");
            sysArg = sysArg.replaceAll("wave\\d+$", "wave");
            sysArg = sysArg.replaceAll("midi\\d+$", "midi");
            sysArg = sysArg.replaceAll("HarddiskVolume\\d+", "HarddiskVolume");
            sysArg = sysArg.replaceAll("countrycode\\[\\d+\\]$", "countrycode");
            sysArg = sysArg.replaceAll("MonitorFunction\\d+$", "MonitorFunction");
            sysArg = sysArg.replaceAll("^#\\d+$", "#");
            sysArg = sysArg.replaceAll("Categories\\\\.*$", "Categories");
            sysArg = sysArg.replaceAll("Transforms\\\\[^\\\\]*?[0-9][^\\\\]*?$", "Transforms");
            sysArg = sysArg.replaceAll("UnitVersioning_\\d+$", "UnitVersioning");
            sysArg = sysArg.replaceAll("_[a-z0-9\\._]{30,}", "_XXX");
            sysArg = sysArg.replaceAll("[a-z0-9]{25,}", "XXX");
            sysArg = sysArg.replaceAll("(_\\d+){4,}", "");
            sysArg = sysArg.replaceAll("flag[A-Z0-9]{15,}", "flag");
            sysArg = sysArg.replaceAll("(\\\\[a-f0-9]+){2,}", "");
            sysArg = sysArg.replaceAll("([a-f0-9]+-){3,}[a-f0-9]+", "XXX");
            sysArg = sysArg.replaceAll("0x[0-9A-F]{7,}", "XXX");
            sysArg = sysArg.replaceAll("[a-f0-9A-F]{10,}", "XXX");
            sysArg = sysArg.replaceAll("(\\d+\\.){2,}\\d+", "XXX");
            sysArg = sysArg.replaceAll("\\d{7,}", "");
            sysArg = sysArg.replaceAll("XXX,\\d+", "XXX");
            sysArg = sysArg.split("\\\\")[sysArg.split("\\\\").length-1];
            if (sysArg.equals("Mohammad") || sysArg.equals("cs")) sysArg = "LOCALUSERNAME";
		}
		phfArgPreprocessingCache.put(tmpString, sysArg);
		return sysArg;
	}
   
	public static void tracePreProcessForInit(String traceDirPath, String traceExtension, String filteredExtension,
   			int minLengthOfConsecutiveBlock, int maxBlockSize, int numberOfBlocksToKeep, String uselessSyscallFilePath, String usefulEventFilePath) throws IOException{
      File dir = new File(traceDirPath);
      File[] tmpfiles = dir.listFiles(new FilenameFilter() {
         public boolean accept(File dir, String name) {
            return name.endsWith(traceExtension);
         }
      });
      ArrayList<File> files = new ArrayList<File>();
      for (File json : tmpfiles) files.add(json);
      Collections.sort(files, new MyComparator());
      for (File json : files)
      //if (json.length() <= 102072320L) 
      {
         System.out.println(json);
         
         Filter filter = new Filter();
         
         List<EventRecord> sequence = EventRecordFile2EventRecordList(json.getAbsolutePath());
         sequence = filter.filterUselessSyscalls(sequence, uselessSyscallFilePath);
         sequence = filter.filterUselessEvents(sequence, usefulEventFilePath);
         List<EventRecordG> sequence2 = argPreProcessInit(sequence);
         //sequence2 = filter.filterConsecutiveBlocks(sequence2, minLengthOfConsecutiveBlock, maxBlockSize, numberOfBlocksToKeep);
         // remove duplicated sequences
//	         while (!sequence.equals(newSequence)) {
//	        	 System.out.println("wow!");
//	        	 sequence = new ArrayList();
//	        	 sequence.addAll(newSequence);
//	        	 newSequence = filter.filterConsecutiveBlocks(sequence, maxBlockSize);
//	         }
         // write back to a new file
         File filteredTrace = new File(json.getAbsolutePath() + filteredExtension);
//		         filteredTrace.createNewFile();
         FileWriter filteredWriter = new FileWriter(filteredTrace);
         for (EventRecordG e : sequence2) {
        	 String output = new GsonBuilder().create().toJson(e);
        	 filteredWriter.write(output + "\n");
         }
         filteredWriter.flush();
         filteredWriter.close();
      }
   }
	
	public static List<EventRecordG> argPreProcessInit(List<EventRecord> sequence) throws IOException {

		//preprocess arguments
		List<EventRecordG> resList = new ArrayList<EventRecordG>();
		for (int i = 0; i < sequence.size(); ++i)
		{
			EventRecord tmpRecord = sequence.get(i);
			EventRecordG resRecord = null;
			String fieldArg = "", fieldArg1 = "", fieldArg2 = "";
			if (tmpRecord.eventName.equals("ALPCALPC-Send-Message") || tmpRecord.eventName.equals("ALPCALPC-Receive-Message")) {
				fieldArg = tmpRecord.arguments.get("ProcessName");
				if (fieldArg == null) fieldArg = "";
				fieldArg = argfieldPreProcessInit(fieldArg);
				resRecord = new EventRecordG(tmpRecord.eventName, "ProcessName:" + fieldArg);
			} else 
			if (tmpRecord.eventName.equals("DiskIoWrite") || tmpRecord.eventName.equals("FileIoCleanup") || tmpRecord.eventName.equals("FileIoClose") 
				|| tmpRecord.eventName.equals("FileIoCreate") || tmpRecord.eventName.equals("FileIoDelete") || tmpRecord.eventName.equals("FileIoDirEnum") 
				|| tmpRecord.eventName.equals("FileIoFileCreate") || tmpRecord.eventName.equals("FileIoFileDelete") || tmpRecord.eventName.equals("FileIoQueryInfo") 
				|| tmpRecord.eventName.equals("FileIoRead") || tmpRecord.eventName.equals("FileIoSetInfo") || tmpRecord.eventName.equals("FileIoWrite")
				|| tmpRecord.eventName.equals("ImageLoad") || tmpRecord.eventName.equals("ImageUnLoad") || tmpRecord.eventName.equals("FileIoFSControl")) {
				if (tmpRecord.arguments.containsKey("OpenPath")) {
					fieldArg = tmpRecord.arguments.get("OpenPath");
				} else {
					fieldArg = tmpRecord.arguments.get("FileName");
				}
				if (fieldArg == null) fieldArg = "";
				fieldArg = argfieldPreProcessInit(fieldArg);
				resRecord = new EventRecordG(tmpRecord.eventName, "FileName:" + fieldArg);
			}/* else
			if (tmpRecord.eventName.equals("DiskIoDrvMjFnCall")) {
				fieldArg = tmpRecord.arguments.get("MajorFunction");
				fieldArg = argfieldPreProcess(fieldArg);
				resRecord = new EventRecordG(tmpRecord.eventName, "MajorFunction:" + fieldArg);
			} else
			if (tmpRecord.eventName.equals("TCP_Connect") || tmpRecord.eventName.equals("TCP_Disconnect") || tmpRecord.eventName.equals("TCP_Send") 
					|| tmpRecord.eventName.equals("TCP_Rec")) {
				resRecord = new EventRecordG(tmpRecord.eventName, "None:None");
			}*/ else
			if (tmpRecord.eventName.equals("RegistryCreate") || tmpRecord.eventName.equals("RegistryDelete") || tmpRecord.eventName.equals("RegistryEnumerateKey") || tmpRecord.eventName.equals("RegistryEnumerateValueKey") 
				|| tmpRecord.eventName.equals("RegistryKCBCreate") || tmpRecord.eventName.equals("RegistryKCBDelete") || tmpRecord.eventName.equals("RegistryOpen")
				|| tmpRecord.eventName.equals("RegistryQuery") || tmpRecord.eventName.equals("RegistryQueryValue") || tmpRecord.eventName.equals("RegistrySetInformation")
				|| tmpRecord.eventName.equals("RegistrySetValue") || tmpRecord.eventName.equals("RegistryDeleteValue")) {
				fieldArg = tmpRecord.arguments.get("KeyName");
				if (fieldArg == null) fieldArg = "";
				fieldArg = argfieldPreProcessInit(fieldArg);
				resRecord = new EventRecordG(tmpRecord.eventName, "KeyName:" + fieldArg);
			} else
			if (tmpRecord.eventName.equals("ProcessStart") || tmpRecord.eventName.equals("ProcessEnd")) {
				fieldArg1 = tmpRecord.arguments.get("ImageFileName");
				fieldArg2 = tmpRecord.arguments.get("CommandLine");
				if (fieldArg1 == null) fieldArg1 = "";
				if (fieldArg2 == null) fieldArg2 = "";
				fieldArg1 = argfieldPreProcessInit(fieldArg1);
				fieldArg2 = argfieldPreProcessInit(fieldArg2);
				resRecord = new EventRecordG(tmpRecord.eventName, "ImageFileName:" + fieldArg1 + " @@ " + "CommandLine:" + fieldArg2);
			} else 
			if (tmpRecord.eventName.equals("PerfInfoSysClEnter")){
				resRecord = new EventRecordG(tmpRecord.eventName, "SystemCall:" + tmpRecord.arguments.get("SystemCall"));
			} else
			if (tmpRecord.eventName.equals("ALPCALPC-Unwait") || tmpRecord.eventName.equals("ALPCALPC-Wait-For-Reply")) {
				resRecord = new EventRecordG(tmpRecord.eventName, "");
			}
			resList.add(resRecord);
		}
		
		return resList;
	}
	
	private static HashMap<String, String> initArgPreprocessingCache = new HashMap<String, String>();
	
	private static String argfieldPreProcessInit(String argumentString){
		String tmpString = argumentString;
		String sysArg = null;
		
		if(tmpString.endsWith(".exe") || tmpString.endsWith(".dll"))
		{
			sysArg = tmpString.split("\\\\")[tmpString.split("\\\\").length-1];
		}
		
		if(tmpString.endsWith("cmd.exe"))
		{
			sysArg = "cmd.exe";
		}
		else
		{
			if (initArgPreprocessingCache.containsKey(tmpString)) {
				return initArgPreprocessingCache.get(tmpString);
			}
			sysArg = tmpString;
			sysArg = sysArg.replaceAll("[^\\\\]*\\.dat", "XXX.dat");
            sysArg = sysArg.replaceAll("[^\\\\]*\\.(temp|TMP|tmp)", "XXX.temp");
            sysArg = sysArg.replaceAll("[^\\\\]*\\.(nls|NLS)", "XXX.nls");
            sysArg = sysArg.replaceAll("[^\\\\]*\\.(png|PNG)", "XXX.png");
            sysArg = sysArg.replaceAll("[^\\\\]*\\.css$", "XXX.css");
            sysArg = sysArg.replaceAll("[^\\\\]*\\.(js|JS)$", "XXX.js");
            sysArg = sysArg.replaceAll("[^\\\\]*\\.json$", "XXX.json");
            sysArg = sysArg.replaceAll("[^\\\\]*\\.(htm|html)$", "XXX.htm");
            sysArg = sysArg.replaceAll("\\{.+-.+-.+-.+\\}", "XXX");
            sysArg = sysArg.replaceAll("\\\\[^\\\\]+-[^\\\\]+-[^\\\\]+-[^\\\\]+\\\\", "\\\\\\\\");
            sysArg = sysArg.replaceAll("\\\\[^\\\\]+-[^\\\\]+-[^\\\\]+-[^\\\\]+$", "\\XXX");
            sysArg = sysArg.replaceAll("\\{.+-.+-.+-.+-.+\\}", "XXX");
            sysArg = sysArg.replaceAll("[A-Z0-9]+-[^\\\\]+-[^\\\\]+-[0-9]+(_Classes)*", "XXX");
            sysArg = sysArg.replaceAll("Plane\\d+$", "Plane");
            sysArg = sysArg.replaceAll("msvideo\\d+$", "msvideo");
            sysArg = sysArg.replaceAll("wave\\d+$", "wave");
            sysArg = sysArg.replaceAll("midi\\d+$", "midi");
            sysArg = sysArg.replaceAll("HarddiskVolume\\d+", "HarddiskVolume");
            sysArg = sysArg.replaceAll("countrycode\\[\\d+\\]$", "countrycode");
            sysArg = sysArg.replaceAll("MonitorFunction\\d+$", "MonitorFunction");
            sysArg = sysArg.replaceAll("^#\\d+$", "#");
            sysArg = sysArg.replaceAll("Categories\\\\.*$", "Categories");
            sysArg = sysArg.replaceAll("Transforms\\\\[^\\\\]*?[0-9][^\\\\]*?$", "Transforms");
            sysArg = sysArg.replaceAll("UnitVersioning_\\d+$", "UnitVersioning");
            sysArg = sysArg.replaceAll("_[a-z0-9\\._]{30,}", "_XXX");
            sysArg = sysArg.replaceAll("[a-z0-9]{25,}", "XXX");
            sysArg = sysArg.replaceAll("(_\\d+){4,}", "");
            sysArg = sysArg.replaceAll("flag[A-Z0-9]{15,}", "flag");
            sysArg = sysArg.replaceAll("(\\\\[a-f0-9]+){2,}", "");
            sysArg = sysArg.replaceAll("([a-f0-9]+-){3,}[a-f0-9]+", "XXX");
            sysArg = sysArg.replaceAll("0x[0-9A-F]{7,}", "XXX");
            sysArg = sysArg.replaceAll("[a-f0-9A-F]{10,}", "XXX");
            sysArg = sysArg.replaceAll("(\\d+\\.){2,}\\d+", "XXX");
            sysArg = sysArg.replaceAll("\\d{7,}", "");
            sysArg = sysArg.replaceAll("XXX,\\d+", "XXX");
            sysArg = sysArg.replaceAll("\\{.*XXX.*\\}", "");
            sysArg = sysArg.replaceAll("\\\\Mohammad", "\\\\LOCALUSERNAME");
            sysArg = sysArg.replaceAll("\\\\cs", "\\\\LOCALUSERNAME");
		}
		initArgPreprocessingCache.put(tmpString, sysArg);
		return sysArg;
	}
}

//class MyComparator implements Comparator {
//	@Override
//	public int compare(Object o1, Object o2) {
//		SigResults sigo1 = (SigResults) o1;
//		SigResults sigo2 = (SigResults) o2;
//		if (sigo1.sig.size() < sigo2.sig.size())
//			return -1;
//		else if (sigo1.sig.size() == sigo2.sig.size())
//			return 0;
//		else
//			return 1;
//	}
//}

class MyComparator implements Comparator {
	@Override
	public int compare(Object o1, Object o2) {
		File file1 = (File) o1;
		File file2 = (File) o2;
		if (file1.length() < file2.length())
			return -1;
		else if (file1.length() == file2.length()) {
			return 0;
		} else
			return 1;
	}
}
 