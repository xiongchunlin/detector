package edu.nu.marple.seqalign;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;

import edu.nu.marple.GlobalConfig;
import edu.nu.marple.dp.Cell;
import edu.nu.marple.dp.MultiDirectionCell;
import edu.nu.marple.seqalign.SequenceAlignment.Weights;
import scala.collection.script.Start;

public class SmithWatermanExtend<T> extends SequenceAlignmentExtend<T> {
	protected int topK;
	protected List<Set<MultiDirectionCell>> topKhighScoreCells;
	protected List<Integer> topKhighScores;
	protected int highScore;
	private List<Set<List<String>>> matchedAlignments;
	private List<Set<List<String>>> unifiedDiffAlignment;

	/**
	 * By default, the weight of mathc = 1, mismatch = -1, and gap = -1, topK =
	 * 1
	 * 
	 * @param sequence1
	 *            the sequences you want to compare
	 * @param sequence2
	 *            the sequences you want to compare
	 */
	public SmithWatermanExtend(List<T> sequence1, List<T> sequence2) {
		this(sequence1, sequence2, 1, -1, -1, 1);
	}

	/**
	 * The extend version of SmithWatern Algorithm, which support
	 * multi-direction and all highest score Cells' traceback
	 * 
	 * @param sequence1
	 *            the sequences you want to compare
	 * @param sequence2
	 *            the sequences you want to compare
	 * @param match
	 *            the weight of match
	 * @param mismatch
	 *            the weight of mismatch
	 * @param gap
	 *            the weight of space(gap)
	 * @param topK
	 *            the limit number of TopK highest score Cells
	 */
	public SmithWatermanExtend(List<T> sequence1, List<T> sequence2, int match, int mismatch, int gap, int topK) {
		super(sequence1, sequence2, match, mismatch, gap);
		this.topK = topK;
		highScore = 0;
		topKhighScoreCells = new ArrayList<Set<MultiDirectionCell>>();
		topKhighScores = new ArrayList<Integer>();
	}

	protected void fillInCell(MultiDirectionCell currentCell, MultiDirectionCell cellAbove,
			MultiDirectionCell cellToLeft, MultiDirectionCell cellAboveLeft) {
		int rowSpaceScore = cellAbove.getScore() + gap;
		int colSpaceScore = cellToLeft.getScore() + gap;
		int matchOrMismatchScore = cellAboveLeft.getScore();
		if (sequence2.get(currentCell.getRow() - 1).equals(sequence1.get(currentCell.getCol() - 1))) {
			matchOrMismatchScore += match;
		} else {
			matchOrMismatchScore += mismatch;
		}

		if (rowSpaceScore > colSpaceScore) {
			if (matchOrMismatchScore > rowSpaceScore) {
				if (matchOrMismatchScore > 0) {
					currentCell.setScore(matchOrMismatchScore);
					List<MultiDirectionCell> preCells = new ArrayList<MultiDirectionCell>();
					preCells.add(cellAboveLeft);
					currentCell.setPrevCells(preCells);
				}
			} else if (matchOrMismatchScore < rowSpaceScore) {
				if (rowSpaceScore > 0) {
					currentCell.setScore(rowSpaceScore);
					List<MultiDirectionCell> preCells = new ArrayList<MultiDirectionCell>();
					preCells.add(cellAbove);
					currentCell.setPrevCells(preCells);
				}
			} else if (matchOrMismatchScore == rowSpaceScore) {
				if (rowSpaceScore > 0) {
					currentCell.setScore(rowSpaceScore);
					List<MultiDirectionCell> preCells = new ArrayList<MultiDirectionCell>();
					preCells.add(cellAboveLeft);
					preCells.add(cellAbove);
					currentCell.setPrevCells(preCells);

				}
			}
		} else if (rowSpaceScore < colSpaceScore) {
			if (matchOrMismatchScore > colSpaceScore) {
				if (matchOrMismatchScore > 0) {
					currentCell.setScore(matchOrMismatchScore);
					List<MultiDirectionCell> preCells = new ArrayList<MultiDirectionCell>();
					preCells.add(cellAboveLeft);
					currentCell.setPrevCells(preCells);
				}
			} else if (matchOrMismatchScore < colSpaceScore) {
				if (colSpaceScore > 0) {
					currentCell.setScore(colSpaceScore);
					List<MultiDirectionCell> preCells = new ArrayList<MultiDirectionCell>();
					preCells.add(cellToLeft);
					currentCell.setPrevCells(preCells);
				}
			} else if (matchOrMismatchScore == colSpaceScore) {
				if (colSpaceScore > 0) {
					currentCell.setScore(colSpaceScore);
					List<MultiDirectionCell> preCells = new ArrayList<MultiDirectionCell>();
					preCells.add(cellAboveLeft);
					preCells.add(cellToLeft);
					currentCell.setPrevCells(preCells);
				}
			}
		} else if (rowSpaceScore == colSpaceScore) {
			if (matchOrMismatchScore > rowSpaceScore) {
				if (matchOrMismatchScore > 0) {
					currentCell.setScore(matchOrMismatchScore);
					List<MultiDirectionCell> preCells = new ArrayList<MultiDirectionCell>();
					preCells.add(cellAboveLeft);
					currentCell.setPrevCells(preCells);
				}
			} else if (matchOrMismatchScore < rowSpaceScore) {
				if (rowSpaceScore > 0) {
					currentCell.setScore(rowSpaceScore);
					List<MultiDirectionCell> preCells = new ArrayList<MultiDirectionCell>();
					preCells.add(cellAbove);
					preCells.add(cellToLeft);
					currentCell.setPrevCells(preCells);
				}
			} else if (matchOrMismatchScore == rowSpaceScore) {
				if (rowSpaceScore > 0) {
					currentCell.setScore(rowSpaceScore);
					List<MultiDirectionCell> preCells = new ArrayList<MultiDirectionCell>();
					preCells.add(cellAboveLeft);
					preCells.add(cellToLeft);
					preCells.add(cellAbove);
					currentCell.setPrevCells(preCells);
				}
			}
		}

		addToTopkCells(currentCell);
	}

	private void addToTopkCells(MultiDirectionCell currentCell) {
		int currentScore = currentCell.getScore();
		boolean isInserted = false;

		// insert into existing cells
		for (Integer highScore : new ArrayList<Integer>(topKhighScores)) {
			if (currentScore > highScore) {
				isInserted = true;

				int insertIndex = topKhighScores.indexOf(highScore);
				// highest scores
				topKhighScores.add(insertIndex, currentScore);
				if (topKhighScores.size() > topK)
					topKhighScores.remove(topKhighScores.size() - 1);

				// highest score cells
				Set<MultiDirectionCell> insertCells = new HashSet<MultiDirectionCell>();
				insertCells.add(currentCell);
				topKhighScoreCells.add(insertIndex, insertCells);
				if (topKhighScoreCells.size() > topK)
					topKhighScoreCells.remove(topKhighScoreCells.size() - 1);

				break;
			} else if (currentScore == highScore) {
				isInserted = true;

				int insertIndex = topKhighScores.indexOf(currentScore);
				topKhighScoreCells.get(insertIndex).add(currentCell);

				break;
			}
		}

		// if the above insertion fails and topK has free rooms, append current
		// cell
		if (!isInserted && topKhighScores.size() < topK) {
			topKhighScores.add(currentScore);
			Set<MultiDirectionCell> insertCells = new HashSet<MultiDirectionCell>();
			insertCells.add(currentCell);
			topKhighScoreCells.add(insertCells);
		}
	}

	/**
	 * Get all matched alignment subsequences
	 * 
	 * @return a set of matched subsequences
	 */
	public List<Set<List<String>>> getMatchedAlignment() {
		ensureTableIsFilledIn();
		if (this.matchedAlignments != null)
			return this.matchedAlignments;
		List<Set<List<String>>> unifiedDiffAlignment = this.getUnifiedDiffAlignment();
		this.matchedAlignments = unifiedDiffAlignment.stream().map(set->set.stream().map(diff->_extractMatchedAlignment(diff)).collect(Collectors.toSet())).collect(Collectors.toList());
		return this.matchedAlignments;
	}	
	
	/**
	 * for each sig, return a List<List<String>>, 
	 * [0] matched common subsequence
	 * [1] matched indexs in align, not in trace indexs, start from 1
	 * [2] matched indexs in trace1, start from 0
	 * [3] matched indexs in trace2, start from 0
	 */
	public List<Set<List<List<String>>>> getMatchedAndMatchedIndexsAndTrace1IndexsAndTrace2Indexs(){
		ensureTableIsFilledIn();
		List<Set<List<String>>> unifiedDiffAlignment = this.getUnifiedDiffAlignment();
		System.out.println(new Date().getMinutes());
//		return unifiedDiffAlignment.stream().map(set->set.stream().map(diff->_extractMatchedAndMatchedIndexsAndTrace1IndexsAndTrace2Indexs(diff)).collect(Collectors.toSet())).collect(Collectors.toList());
		List<Set<List<List<String>>>> matchedAndMatchedIndexsAndTrace1IndexsAndTrace2Index = new ArrayList<Set<List<List<String>>>>();
		for (Set<List<String>> TopKSet : unifiedDiffAlignment){
			Set<List<List<String>>> TopKmatchedAndMatchedIndexsAndTrace1IndexsAndTrace2Index = new HashSet<List<List<String>>>();
			for (List<String> diff : TopKSet){
				TopKmatchedAndMatchedIndexsAndTrace1IndexsAndTrace2Index.add(_extractMatchedAndMatchedIndexsAndTrace1IndexsAndTrace2Indexs(diff));
			}
			matchedAndMatchedIndexsAndTrace1IndexsAndTrace2Index.add(TopKmatchedAndMatchedIndexsAndTrace1IndexsAndTrace2Index);
		}
		System.out.println(new Date().getMinutes());
		return matchedAndMatchedIndexsAndTrace1IndexsAndTrace2Index;
	}
	

	public List<Set<List<List<String>>>> getMatchedAlignAndIndexs(){
		ensureTableIsFilledIn();
		List<Set<List<String>>> unifiedDiffAlignment = this.getUnifiedDiffAlignment();
		return unifiedDiffAlignment.stream().map(set->set.stream().map(diff->_extractMatchedAlignmentAndIndexs(diff)).collect(Collectors.toSet())).collect(Collectors.toList());
	}
	
	public  List<Set<List<List<String>>>> getFastMatchedAlignAndIndexs(){
		ensureTableIsFilledIn();
		return null;
	}
	
	public List<Set<List<String>>> getMatchedAndIndexsResult(){
		ensureTableIsFilledIn();

		List<Set<List<String>>> matchedAndIndexsSet = new ArrayList<Set<List<String>>>();
		for (Set<MultiDirectionCell> cells : topKhighScoreCells) {
			Set<List<String>> matchedSet = new HashSet<List<String>>();
			for (MultiDirectionCell currentCell : cells) {
				if (currentCell.getPrevCells() != null) {
					for (MultiDirectionCell preCell : currentCell.getPrevCells()) {
						matchedSet.addAll(_getMatchedAndIndexsResult(currentCell, preCell));
					}
				}
			}
			matchedSet = _removeDuplicatedCommonSubsequence(matchedSet);
			this.unifiedDiffAlignment.add(matchedSet);
		}

		return matchedAndIndexsSet;
	}
	
	private Set<List<String>> _getMatchedAndIndexsResult(MultiDirectionCell currentCell, MultiDirectionCell preCell){
		Set<List<String>> matchedAndIndexs = new HashSet<List<String>>();
		return matchedAndIndexs;
	}
	
	
	public List<Set<List<String>>> getUnifiedDiffAlignment(){
		ensureTableIsFilledIn();
		if (this.unifiedDiffAlignment != null)
			return this.unifiedDiffAlignment;

		this.unifiedDiffAlignment = new ArrayList<Set<List<String>>>();
		for (Set<MultiDirectionCell> cells : topKhighScoreCells) {
			Set<List<String>> matchedSet = new HashSet<List<String>>();
			for (MultiDirectionCell currentCell : cells) {
				if (currentCell.getPrevCells() != null) {
					for (MultiDirectionCell preCell : currentCell.getPrevCells()) {
						matchedSet.addAll(_getUnifiedDiffAlignment(currentCell, preCell));
					}
				}
			}
			matchedSet = _removeDuplicatedCommonSubsequence(matchedSet);
			this.unifiedDiffAlignment.add(matchedSet);
		}

		return this.unifiedDiffAlignment;
	}
	
	
	public void diff2Html(String phfDirName, String traceDirName) throws IOException {      
	      // generate a unified diff file
		  List<Set<List<String>>> diffAlignment = this.getUnifiedDiffAlignment();
		  String rootDirPath = "generatedSignature/" + phfDirName + "/" + traceDirName + "/";
		  int topKindex = 1;
		  for (Set<List<String>> topK : diffAlignment){
			  String topKDirPath =  rootDirPath + topKindex + "/";
			  File topKDir = new File(topKDirPath);
			  topKindex++;
			  if (!topKDir.exists()){
				  topKDir.mkdirs();
			  }
			  int diffIndex = 1;
			  for (List<String> diff : topK){
				  StringBuffer diffString = new StringBuffer();
				  // insert indexs
				  String[] startIndexs = diff.get(0).split(" # ");
				  String sequence1StartIndex = startIndexs[0];
				  String sequence2StartIndex = startIndexs[1];
				  diffString.insert(0, "@@ " + sequence1StartIndex + ",10000 " + sequence2StartIndex + ",10000" + " @@");
				  // insert diff item
				  for (int i = 1; i < diff.size();i++){
					  String[] tmp = diff.get(i).split(" # ");
						if (tmp.length == 1){							
							diffString.append("\n\n" + diff.get(i));
						} else {
							diffString.append("\n\n" + tmp[0]);
							diffString.append("\n" + tmp[1]);
						}
					  
				  }
				  
				  String diffFilePath = topKDirPath + diffIndex + ".diff"; 
				  diffIndex++;
				  
				  File inputFile = new File(diffFilePath);
			      inputFile.createNewFile();
			      BufferedWriter in = new BufferedWriter(new FileWriter(inputFile));
			      in.write(diffString.toString());
			      in.flush();
			      in.close();
			      
			      // generate diff html file
			      Process process;			      
			      String a = "python " + GlobalConfig.getInstance().rootPath + "/diff2html.py -i " + "\"" + diffFilePath + "\"" + " -o " + diffFilePath + ".html";
			      process = Runtime.getRuntime().exec("python " + GlobalConfig.getInstance().rootPath + "/diff2html.py -i " + "\"" + diffFilePath + "\"" + " -o " + "\"" + diffFilePath + ".html" + "\"");
			  }
		  }	      	       	     	     
	             
	   }

	/**
	 * Get all matched alignment subsequences' scores
	 */
	public List<Integer> getMatchedAlignmentScores() {
		ensureTableIsFilledIn();
		if (!this.topKhighScores.isEmpty()) {
			return this.topKhighScores;
		} else {
			this.getMatchedAlignment();
			return this.topKhighScores;
		}

	}

	@Override
	protected List<MultiDirectionCell> getInitialPointer(int row, int col) {
		return null;
	}

	@Override
	protected int getInitialScore(int row, int col) {
		return 0;
	}

}
