package edu.nu.marple.seqalign;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import edu.nu.marple.dp.Cell;
import edu.nu.marple.dp.MultiDirectionCell;
import scala.annotation.meta.setter;

public abstract class SequenceAlignmentExtend<T> {
	protected List<T> sequence1;
	protected List<T> sequence2;
	protected MultiDirectionCell[][] scoreTable;
	protected int match;
	protected int gap;
	protected int mismatch;
	protected boolean tableIsFilledIn;
	protected boolean isInitialized;
	protected Map<String, Set<List<String>>> cache = new HashMap<String, Set<List<String>>>();


	public SequenceAlignmentExtend(List<T> sequence1, List<T> sequence2, int match, int mismatch, int gap) {
		this.sequence1 = sequence1;
		this.sequence2 = sequence2;
		this.match = match;
		this.mismatch = mismatch;
		this.gap = gap;
		scoreTable = new MultiDirectionCell[sequence2.size() + 1][sequence1.size() + 1];	
		this.tableIsFilledIn = false;
		this.isInitialized = false;
	}

	public SequenceAlignmentExtend(List<T> sequence1, List<T> sequence2) {
		this(sequence1, sequence2, 1, -1, -1);
	}

	/**
	 * initialize the score table
	 */
	protected void initialize() {
		for (int i = 0; i < scoreTable.length; i++) {
			for (int j = 0; j < scoreTable[i].length; j++) {
				scoreTable[i][j] = new MultiDirectionCell(i, j);
			}
		}
		initializeScores();
		initializePointers();
		
		isInitialized = true;
	}

	protected void initializeScores() {
		for (int i = 0; i < scoreTable.length; i++) {
			for (int j = 0; j < scoreTable[i].length; j++) {
				scoreTable[i][j].setScore(getInitialScore(i, j));
			}
		}
	}

	protected void initializePointers() {
		for (int i = 0; i < scoreTable.length; i++) {
			for (int j = 0; j < scoreTable[i].length; j++) {
				scoreTable[i][j].setPrevCells(getInitialPointer(i, j));
			}
		}
	}

	protected abstract List<MultiDirectionCell> getInitialPointer(int row, int col);

	protected abstract int getInitialScore(int row, int col);

	
	/**
	 * fill in the score table using the initial table
	 */
	protected void fillIn() {
		for (int row = 1; row < scoreTable.length; row++) {
			for (int col = 1; col < scoreTable[row].length; col++) {
				MultiDirectionCell currentCell = scoreTable[row][col];
				MultiDirectionCell cellAbove = scoreTable[row - 1][col];
				MultiDirectionCell cellToLeft = scoreTable[row][col - 1];
				MultiDirectionCell cellAboveLeft = scoreTable[row - 1][col - 1];
				fillInCell(currentCell, cellAbove, cellToLeft, cellAboveLeft);
			}
		}
		
		tableIsFilledIn = true;
	}

	protected abstract void fillInCell(MultiDirectionCell currentCell, MultiDirectionCell cellAbove,
			MultiDirectionCell cellToLeft, MultiDirectionCell cellAboveLeft);
	
	
	/**
	 * find the matched diffString start from currentCell -> preCell
	 * @param currentCell
	 * @param preCell
	 * @return
	 */
	protected Set<List<String>> _getUnifiedDiffAlignment(MultiDirectionCell currentCell, MultiDirectionCell preCell) {
		Set<List<String>> currentMatchedSet = new HashSet<List<String>>();
		StringBuilder diffString = new StringBuilder();
		boolean isAbove = currentCell.getRow() - preCell.getRow() == 1;
		boolean isLeft = currentCell.getCol() - preCell.getCol() == 1;
		boolean isMatch = false;
		if (currentCell.getRow() == 0 || currentCell.getCol() == 0){
			isMatch = false;
		} else {
			isMatch = sequence2.get(currentCell.getRow() - 1).equals(sequence1.get(currentCell.getCol() - 1));
		}		
		
//		if (isLeft && isAbove){
//            if (isMatch){
//               diffString = sequence2.get(currentCell.getRow() - 1).toString();      
//            } else{
//                diffString = "-" + sequence1.get(currentCell.getCol() - 1).toString();
//                diffString += " # " + "+" + sequence2.get(currentCell.getRow() - 1).toString();           
//            }
//         } else if (isLeft){
//            diffString = "-" + sequence1.get(currentCell.getCol() - 1).toString();
//         } else if (isAbove){
//            diffString = "+" + sequence2.get(currentCell.getRow() - 1).toString();
//         }
		if (isLeft && isAbove){
            if (isMatch){
               diffString.append(sequence2.get(currentCell.getRow() - 1).toString());      
            } else{
                diffString.append("-").append(sequence1.get(currentCell.getCol() - 1).toString());
                diffString.append(" # ").append("+").append(sequence2.get(currentCell.getRow() - 1).toString());           
            }
         } else if (isLeft){
            diffString.append("-").append(sequence1.get(currentCell.getCol() - 1).toString());
         } else if (isAbove){
            diffString.append("+").append(sequence2.get(currentCell.getRow() - 1).toString());
         }

		if (preCell.getPrevCells() != null) {
			for (MultiDirectionCell prepreCell : preCell.getPrevCells()) {
				Set<List<String>> preMatchedSet = null;
				String cacheKey = preCell.getRow() + "#" + preCell.getCol() + "@" + prepreCell.getRow() + "#" +  prepreCell.getCol();
				if (cache.containsKey(cacheKey)){
					preMatchedSet = cache.get(cacheKey);
				} else {
					preMatchedSet = _getUnifiedDiffAlignment(preCell, prepreCell);
					cache.put(cacheKey, preMatchedSet);
				}
//				preMatchedSet = _getUnifiedDiffAlignment(preCell, prepreCell);
				
				long start = System.currentTimeMillis();				
//				Set<List<String>> finalMatchedSet = preMatchedSet.stream().map(e -> new ArrayList(e)).collect(Collectors.toSet());
				Set<List<String>> finalMatchedSet = new HashSet<List<String>>();
				if (diffString != null) {
					if (!preMatchedSet.isEmpty()) {
						for (List<String> matched : preMatchedSet) {
							List<String> copy_matched = new ArrayList<String>(matched);
							copy_matched.add(diffString.toString());
							finalMatchedSet.add(copy_matched);							
						}
					} else {
						List<String> matched = new ArrayList<String>();
						matched.add(diffString.toString());
						finalMatchedSet.add(matched);
					}

				}
				currentMatchedSet.addAll(finalMatchedSet);
				
				break; // only choose one
			}
		} else { // stop the recursive
			if (diffString != null){
				List<String> matched = new ArrayList<String>();
				matched.add("-" + currentCell.getCol() + " # " + "+" + currentCell.getRow());
				matched.add(diffString.toString());
				currentMatchedSet.add(matched);
			} 			
		}
		
		// remove duplicated aligns whose common subsequence are the same
		Set<List<String>> finallDiffSet = new HashSet<List<String>>();
		finallDiffSet = _removeDuplicatedCommonSubsequence(currentMatchedSet);
		
		return finallDiffSet;
		
//		return currentMatchedSet;

	}
	
	
	protected class DiffAndIndexs{
		List<String> diff;
		int distance;
	}
	
	

	
	/*
	 *  for each common subsequence, only choose one alignment in which max index distance between two contiguous matched item is minimal  
	 */
	protected Set<List<String>> _removeDuplicatedCommonSubsequence(Set<List<String>> currentMatchedSet) {
		Map<List<String>, Set<List<String>>> matchedAlign2DiffAlign = new HashMap<List<String>, Set<List<String>>>();
		Map<List<String>, Integer> matchedAlign2Gap = new HashMap<List<String>,Integer>();
		Map<List<String>, Set<String>> matchedAlign2MatchedIndex = new HashMap<List<String>, Set<String>>();
		Map<List<String>, DiffAndIndexs> matchedAlign2DiffAndIndexs = new HashMap<List<String>, DiffAndIndexs>();
		Set<List<String>> finallDiffSet = new HashSet<List<String>>();
		
		if (currentMatchedSet.size() > 1){
			for (List<String> diff : currentMatchedSet){
//				List<String> matched = _extractMatchedAlignment(diff);
//				int distance = _calculateConsecutiveGapOrMismatch(diff);
				MatchedAndGap matchedAndGap = _extractMatchedAndGap(diff);
				List<String> matched = matchedAndGap.matched;
				int distance = matchedAndGap.maxConsecutiveNumber;
				if (!matchedAlign2DiffAndIndexs.containsKey(matched)){
					DiffAndIndexs diffAndIndexs = new DiffAndIndexs();
					diffAndIndexs.diff = diff;
					diffAndIndexs.distance = distance;
					matchedAlign2DiffAndIndexs.put(matched, diffAndIndexs);
				} else {
					if (distance < matchedAlign2DiffAndIndexs.get(matched).distance){
						DiffAndIndexs diffAndIndexs = new DiffAndIndexs();
						diffAndIndexs.diff = diff;
						diffAndIndexs.distance = distance;
					}
				}
//				List<String> matched = _extractMatchedAlignment(diff);
//				if (!matchedAlign2Gap.containsKey(matched)){
//					matchedAlign2Gap.put(matched, _calculateConsecutiveGapOrMismatch(diff));
//					matchedAlign2DiffAlign.put(matched, new HashSet<List<String>>());
//					matchedAlign2DiffAlign.get(matched).add(diff);
//					matchedAlign2MatchedIndex.put(matched, new HashSet<String>());
//					matchedAlign2MatchedIndex.get(matched).add(_matchedIndex(diff));
//				} else {
//					int minIndexDistance = _calculateConsecutiveGapOrMismatch(diff);
//					if (minIndexDistance < matchedAlign2Gap.get(matched)){
//						matchedAlign2Gap.put(matched, minIndexDistance);
//						matchedAlign2DiffAlign.get(matched).clear();
//						matchedAlign2DiffAlign.get(matched).add(diff);
//						matchedAlign2MatchedIndex.get(matched).clear();
//						matchedAlign2MatchedIndex.get(matched).add(_matchedIndex(diff));
//					} 
////					else if (minIndexDistance == matchedAlign2Gap.get(matched)) {
////						if (!matchedAlign2MatchedIndex.get(matched).contains(_matchedIndex(diff)))
////							matchedAlign2DiffAlign.get(matched).add(diff);
////					}
//				}
				
			}
//			matchedAlign2DiffAlign.values().forEach(e->finallDiffSet.addAll(e));
			matchedAlign2DiffAndIndexs.values().forEach(e->finallDiffSet.add(e.diff));
		} else {
			finallDiffSet.addAll(currentMatchedSet);
		}
		return finallDiffSet;
	}
	
	protected class MatchedAndGap{
		public List<String> matched;
		public int maxConsecutiveNumber;
	}
	
	protected MatchedAndGap _extractMatchedAndGap(List<String> diff) {
		int consecutiveNumber = 0;
		int maxConsecutiveNumber = 0;
		ArrayList<String> matched = new ArrayList<String>();
		
		for (int i = 1; i < diff.size(); i++){
			String diffItem = diff.get(i);
			if (diffItem.startsWith("+") || diffItem.startsWith("-")){ // gap or mismatch
				consecutiveNumber++;
				if (consecutiveNumber > maxConsecutiveNumber){
					maxConsecutiveNumber = consecutiveNumber;
				}
			} else { // match
				consecutiveNumber = 0;
				matched.add(diffItem);
			}
		}
		
		MatchedAndGap matchedAndGap = new MatchedAndGap();
		matchedAndGap.maxConsecutiveNumber = maxConsecutiveNumber;
		matchedAndGap.matched = matched;
		
		return matchedAndGap;
	}
	
	protected int _calculateConsecutiveGapOrMismatch(List<String> diff) {
		int consecutiveNumber = 0;
		int minConsecutiveNumber = 0;
		
		for (int i = 1; i < diff.size(); i++){
			String diffItem = diff.get(i);
			if (diffItem.startsWith("+") || diffItem.startsWith("-")){ // gap or mismatch
				consecutiveNumber++;
				if (consecutiveNumber > minConsecutiveNumber){
					minConsecutiveNumber = consecutiveNumber;
				}
			} else { // match
				consecutiveNumber = 0;
			}
		}
		
		return minConsecutiveNumber;
	}
	
	protected String _matchedIndex(List<String> diff) {
		ArrayList<Integer> matchedIndex = new ArrayList<Integer>();
		for (int i = 1; i < diff.size(); i++){
			String diffItem = diff.get(i);
			if (!diffItem.startsWith("+") && !diffItem.startsWith("-")){ // match
				matchedIndex.add(i);
			} 
			
//			String[] tmp = diff.get(i).split(" # ");
//			if (tmp.length == 1){
//				if (!tmp[0].startsWith("+") && !tmp[0].startsWith("-")){ // match
//					matchedIndex.add(i);
//				}
//			}
		}
		return matchedIndex.toString();
	}
	
	protected List<String> _extractMatchedAlignment(List<String> diff) {
		ArrayList<String> matched = new ArrayList<String>();
		for (int i = 1; i < diff.size(); i++){
			String diffItem = diff.get(i);
			if (!diffItem.startsWith("+") && !diffItem.startsWith("-")){ // match
				matched.add(diffItem);
			} 
			
//			String[] tmp = diff.get(i).split(" # ");
//			if (tmp.length == 1){
//				if (!tmp[0].startsWith("+") && !tmp[0].startsWith("-")){ // match
//					matched.add(tmp[0]);
//				}
//			}
		}
		return matched;
	}
	
	protected List<List<String>> _extractMatchedAlignmentAndIndexs(List<String> diff) {
		ArrayList<String> matched = new ArrayList<String>();
		ArrayList<String> matchedIndex = new ArrayList<String>();
		for (int i = 1; i < diff.size(); i++){
			String[] tmp = diff.get(i).split(" # ");
			if (tmp.length == 1){
				if (!tmp[0].startsWith("+") && !tmp[0].startsWith("-")){ // match
					matched.add(tmp[0]);
					matchedIndex.add(i + "");
				}
			}
		}
		List<List<String>> alignAndIndexs = new ArrayList<List<String>>();
		alignAndIndexs.add(matched);
		alignAndIndexs.add(matchedIndex);
		return alignAndIndexs;
	}

	/**
	 * Print the dynamic programming table of score
	 */
	public void printDPScoretable() {
		ensureTableIsFilledIn();
		for (int i = 0; i < scoreTable.length; i++) {
			for (int j = 0; j < scoreTable[0].length; j++) {
				System.out.print(scoreTable[i][j].getScore() + "\t");
			}
			System.out.println("");
		}
	}

	/**
	 * Print the dynamic programming table of cells' direction size
	 */
	public void printDPDirectionTable() {
		ensureTableIsFilledIn();
		for (int i = 0; i < scoreTable.length; i++) {
			for (int j = 0; j < scoreTable[0].length; j++) {
				int directionSize = scoreTable[i][j].getPrevCells() == null ? 0
						: scoreTable[i][j].getPrevCells().size();
				System.out.print(directionSize + "\t");
			}
			System.out.println("");
		}
	}
	
	
	/**
	 * make sure score table and initialization have been done
	 */
	protected void ensureTableIsFilledIn() {
		if (!isInitialized) {
			initialize();
		}
		if (!tableIsFilledIn) {
			fillIn();
		}
	}
	
	/**
	 * from a diff alignment, extracting
	 * [0] matched common subsequence
	 * [1] matched indexs in align, not in trace indexs, start from 1
	 * [2] matched indexs in trace1, start from 0
	 * [3] matched indexs in trace2, start from 0
	 */
	protected List<List<String>> _extractMatchedAndMatchedIndexsAndTrace1IndexsAndTrace2Indexs(List<String> diff) {
		ArrayList<String> matched = new ArrayList<String>(); // matched item 
		ArrayList<String> matchedIndex = new ArrayList<String>(); // index of matched item in alignment not in trace 
		ArrayList<String> trace1Indexs = new ArrayList<String>(); // index of matched item in trace1
		ArrayList<String> trace2Indexs = new ArrayList<String>(); // index of matched item in trace2
		ArrayList<String> startIndexs = new ArrayList<String>(); 
		
		// start indexs
		String[] startIndexsArray = diff.get(0).split(" # ");
		String sequenceStartIndex1 = startIndexsArray[0].replace("-", "");
		String sequenceStartIndex2 = startIndexsArray[1].replace("+", "");
		startIndexs.add(sequenceStartIndex1);
		startIndexs.add(sequenceStartIndex2);
		
		int currentIndexInSequence1 = new Integer(sequenceStartIndex1) - 2;
		int currentIndexInSequence2 = new Integer(sequenceStartIndex2) - 2;
		for (int i = 1; i < diff.size(); i++){
			String diffItem = diff.get(i);
			boolean isMismatch = diffItem.indexOf("#") >= 0;
			if (!isMismatch){
				boolean gapInSeq1 = diffItem.startsWith("+");
				boolean gapInSeq2 = diffItem.startsWith("-");
				if (!gapInSeq1 && !gapInSeq2){ // match
					matched.add(diffItem);
					matchedIndex.add(i + "");
					currentIndexInSequence1++;
					currentIndexInSequence2++;
					trace1Indexs.add(currentIndexInSequence1+"");
					trace2Indexs.add(currentIndexInSequence2+"");					
				}
				else if (gapInSeq2){ // gap in sequence2
					currentIndexInSequence1++;
				}
				else if (gapInSeq1){ // gap in sequence1			
					currentIndexInSequence2++;
				}
		} 
		else if (isMismatch){ // mismatch
			currentIndexInSequence1++;
			currentIndexInSequence2++;
		}
			
//			String[] tmp = diff.get(i).split(" # ");
//			if (tmp.length == 1){
//				if (!tmp[0].startsWith("+") && !tmp[0].startsWith("-")){ // match
//					matched.add(tmp[0]);
//					matchedIndex.add(i + "");
//					currentIndexInSequence1++;
//					currentIndexInSequence2++;
//					trace1Indexs.add(currentIndexInSequence1+"");
//					trace2Indexs.add(currentIndexInSequence2+"");
//					
//				}
//				else if (tmp[0].startsWith("-")){ // gap in sequence2
//					currentIndexInSequence1++;
//				}
//				else if (tmp[0].startsWith("+")){ // gap in sequence1			
//					currentIndexInSequence2++;
//				}
//			} 
//			else if (tmp.length == 2){ // mismatch
//				currentIndexInSequence1++;
//				currentIndexInSequence2++;
//			}
		}
		
		List<List<String>> alignAndIndexs = new ArrayList<List<String>>();
		alignAndIndexs.add(matched);
		alignAndIndexs.add(matchedIndex);
		alignAndIndexs.add(trace1Indexs);
		alignAndIndexs.add(trace2Indexs);
		return alignAndIndexs;
	}
	
}
