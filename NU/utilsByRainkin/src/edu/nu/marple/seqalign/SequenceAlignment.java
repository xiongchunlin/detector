
package edu.nu.marple.seqalign;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.sun.xml.internal.bind.v2.TODO;

import edu.nu.marple.GlobalConfig;
import edu.nu.marple.dp.Cell;
import edu.nu.marple.dp.DynamicProgramming;


public abstract class SequenceAlignment extends DynamicProgramming {

   protected int match;
   protected int mismatch;
   protected int gap;
   protected String[] alignments;
   protected String unifiedDiffAlignments;
   protected List<String> matchedAlignment;
   protected Weights weight;
   
   public enum Weights {KEYLOGGER_NAME, REMOTEDESKTOP_NAME, REMOTESHELL_NAME, NULL};
   

//   public SequenceAlignment(String sequence1, String sequence2) {
//      this(sequence1, sequence2, 1, -1, -1);
//   }
//
//   public SequenceAlignment(String sequence1, String sequence2, int match,
//         int mismatch, int gap) {
//      super(sequence1, sequence2);
//
//      this.match = match;
//      this.mismatch = mismatch;
//      this.space = gap;
//   }

   public SequenceAlignment(List<String> sequence1, List<String> sequence2) {
      this(sequence1, sequence2, 1, -1, -1, Weights.NULL);
   }
   
   public SequenceAlignment(List<String> sequence1, List<String> sequence2, int match, int mismatch, int gap, Weights weight){
      super(sequence1, sequence2);
      
      this.match = match;
      this.mismatch = mismatch;
      this.gap = gap;
      this.weight = weight;
   }
   
   
   protected Object getTraceback() {
      StringBuffer align1Buf = new StringBuffer();
      StringBuffer align2Buf = new StringBuffer();
      Cell currentCell = getTracebackStartingCell();
      while (traceBackIsNotDone(currentCell)) {         
         if (currentCell.getRow() - currentCell.getPrevCell().getRow() == 1) {
            align2Buf.insert(0, String.format("%30s", sequence2.get(currentCell.getRow() - 1)));           
         } else {
            align2Buf.insert(0, String.format("%30s", "-"));
         }
         if (currentCell.getCol() - currentCell.getPrevCell().getCol() == 1) {
            align1Buf.insert(0, String.format("%30s", sequence1.get(currentCell.getCol() - 1)));            
         } else {
            align1Buf.insert(0, String.format("%30s", "-"));
         }
         
         currentCell = currentCell.getPrevCell();         
      }

      String[] alignments = new String[] { align1Buf.toString(),
            align2Buf.toString() };

      return alignments;
   }

   protected abstract boolean traceBackIsNotDone(Cell currentCell);

   public int getAlignmentScore() {
      if (alignments == null) {
         getAlignment();
      }

      int score = 0;
      for (int i = 0; i < alignments[0].length(); i++) {
         char c1 = alignments[0].charAt(i);
         char c2 = alignments[1].charAt(i);
         if (c1 == '-' || c2 == '-') {
            score += gap;
         } else if (c1 == c2) {
            score += match;
         } else {
            score += mismatch;
         }
      }

      return score;
   }


   public String[] getAlignment() {
      if (this.alignments != null)
         return this.alignments;
      
      ensureTableIsFilledIn();
      alignments = (String[]) getTraceback();
      return alignments;
   }

   protected abstract Cell getTracebackStartingCell();
   
   public List<String> getMatchedAlignment(){
      if (this.matchedAlignment != null)
         return this.matchedAlignment;
      
      ensureTableIsFilledIn();
      
      List<String> matchedAlignment = new ArrayList<String>();
      Cell currentCell = getTracebackStartingCell();
      while (traceBackIsNotDone(currentCell)) {
         boolean isAbove = currentCell.getRow() - currentCell.getPrevCell().getRow() == 1;
         boolean isLeft = currentCell.getCol() - currentCell.getPrevCell().getCol() == 1;
         boolean isMatch = sequence2.get(currentCell.getRow() - 1).equals(sequence1.get(currentCell.getCol() - 1));
         
         if (isLeft && isAbove && isMatch){
            matchedAlignment.add(0, sequence2.get(currentCell.getRow() - 1));
         }
                  
         currentCell = currentCell.getPrevCell();
      }
      
      this.matchedAlignment = matchedAlignment;
      return this.matchedAlignment;
   }   
   
   public String getUnifiedDiffAlignment() {
      if (this.unifiedDiffAlignments != null)
         return this.unifiedDiffAlignments;
            
      ensureTableIsFilledIn();
      
      StringBuffer diffString = new StringBuffer();      
      Cell currentCell = getTracebackStartingCell();
      while (traceBackIsNotDone(currentCell)) {
         boolean isLeft = false;
         boolean isAbove = false;
         boolean isMatch = false;
         
         if (currentCell.getRow() - currentCell.getPrevCell().getRow() == 1) {
            isAbove = true;
         }
         
         if (currentCell.getCol() - currentCell.getPrevCell().getCol() == 1) {
            isLeft = true;
         }
         
         if (sequence2.get(currentCell.getRow() - 1).equals(sequence1
               .get(currentCell.getCol() - 1))) {
            isMatch = true;
         }
         
         if (isLeft && isAbove){
            if (isMatch){
               diffString.insert(0, "\n\n" + sequence2.get(currentCell.getRow() - 1));      
            } else{
                diffString.insert(0, "\n-" + sequence1.get(currentCell.getCol() - 1));
                diffString.insert(0, "\n\n+" + sequence2.get(currentCell.getRow() - 1));           
            }
         } else if (isLeft){
            diffString.insert(0, "\n\n-" + sequence1.get(currentCell.getCol() - 1));
         } else if (isAbove){
            diffString.insert(0, "\n\n+" + sequence2.get(currentCell.getRow() - 1));
         }
         
         currentCell = currentCell.getPrevCell();
      }
      
      // TODO change the default index to normal index
      diffString.insert(0, "@@ -1,1000 +1,1000 @@");

      this.unifiedDiffAlignments = diffString.toString();
      return this.unifiedDiffAlignments;
      
   }
   
   public void diff2Html(String filePath) throws IOException {      
      // generate a unified diff file
      String diffFilePath = filePath + ".diff"; 
      String diffString = this.getUnifiedDiffAlignment();
      File inputFile = new File(diffFilePath);
      inputFile.createNewFile();
      BufferedWriter in = new BufferedWriter(new FileWriter(inputFile));
      in.write(diffString);
      in.flush();
      in.close();
      
      // generate diff html file
      Process process;
      process = Runtime.getRuntime().exec("python " + GlobalConfig.getInstance().rootPath + "/diff2html.py -i " + diffFilePath + " -o " + filePath);       
   }
   
   protected int gapPenalty(Cell cell, String gapSyscallName) {   
      GlobalConfig config = GlobalConfig.getInstance();
      if (config.weightUselessName.containsKey(gapSyscallName)){
         return cell.getScore() + 0; 
      }
      else {
         return cell.getScore() + gap;
      }
      
   }
   
   protected int sim(Cell currentCell, Cell cellAboveLeft) {
      int matchOrMismatchScore = cellAboveLeft.getScore();
      if (sequence2.get(currentCell.getRow() - 1).equals(sequence1
            .get(currentCell.getCol() - 1))) {
         Map<String, Integer> core = new HashMap<String, Integer>(); 
         switch (this.weight) {
         case KEYLOGGER_NAME:
            core = GlobalConfig.getInstance().weightKeyloggerName;
            break;
         case REMOTEDESKTOP_NAME:
            core = GlobalConfig.getInstance().weightRemoteDesktopName;
            break;
         case REMOTESHELL_NAME:
            core = GlobalConfig.getInstance().weightRemoteShellName;
            break;
         case NULL:
            break;
         default:
            break;
         }
         String syscallName = sequence2.get(currentCell.getRow() -1);
         if (core.containsKey(syscallName)){
            matchOrMismatchScore += 4 * match;
         } else {
            matchOrMismatchScore += match;   
         }
         
      } else {
         matchOrMismatchScore += mismatch;
      }
      return matchOrMismatchScore;
   }
}
