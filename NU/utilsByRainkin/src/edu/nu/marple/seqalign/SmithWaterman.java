
package edu.nu.marple.seqalign;

import java.util.List;

import edu.nu.marple.GlobalConfig;
import edu.nu.marple.dp.Cell;


public class SmithWaterman extends SequenceAlignment {

   private Cell highScoreCell;

//   public SmithWaterman(String sequence1, String sequence2) {
//      super(sequence1, sequence2);
//   }
//
//   public SmithWaterman(String sequence1, String sequence2, int match,
//         int mismatch, int gap) {
//      super(sequence1, sequence2, match, mismatch, gap);
//   }
   
   
   public SmithWaterman(List<String> sequence1, List<String> sequence2) {
      super(sequence1, sequence2);
   }

   public SmithWaterman(List<String> sequence1, List<String> sequence2, int match,
         int mismatch, int gap, Weights weight) {
      super(sequence1, sequence2, match, mismatch, gap, weight);
   }

   protected void initialize() {
      super.initialize();

      highScoreCell = scoreTable[0][0];
   }

   
   protected void fillInCell(Cell currentCell, Cell cellAbove, Cell cellToLeft,
         Cell cellAboveLeft) {
      int rowSpaceScore = gapPenalty(cellAbove, sequence2.get(currentCell.getRow() - 1));
      int colSpaceScore = gapPenalty(cellToLeft, sequence1.get(currentCell.getCol() - 1));
      int matchOrMismatchScore = sim(currentCell, cellAboveLeft);
      
      if (rowSpaceScore >= colSpaceScore) {
         if (matchOrMismatchScore >= rowSpaceScore) {
            if (matchOrMismatchScore > 0) {
               currentCell.setScore(matchOrMismatchScore);
               currentCell.setPrevCell(cellAboveLeft);
            }
         } else {
            if (rowSpaceScore > 0) {           
               currentCell.setScore(rowSpaceScore);
               currentCell.setPrevCell(cellAbove);
            }
         }
      } else {
         if (matchOrMismatchScore >= colSpaceScore) {
            if (matchOrMismatchScore > 0) {
               currentCell.setScore(matchOrMismatchScore);
               currentCell.setPrevCell(cellAboveLeft);
            }
         } else {
            if (colSpaceScore > 0) {
               currentCell.setScore(colSpaceScore);
               currentCell.setPrevCell(cellToLeft);
            }
         }
      }
      if (currentCell.getScore() > highScoreCell.getScore()) {
         highScoreCell = currentCell;
      }
   }


   @Override
   public String toString() {
      return "[NeedlemanWunsch: sequence1=" + sequence1 + ", sequence2="
            + sequence2 + "]";
   }

   @Override
   protected boolean traceBackIsNotDone(Cell currentCell) {
      return currentCell.getScore() != 0;
   }

   @Override
   protected Cell getTracebackStartingCell() {
      return highScoreCell;
   }

   @Override
   protected Cell getInitialPointer(int row, int col) {
      return null;
   }

   @Override
   protected int getInitialScore(int row, int col) {
      return 0;
   }
}
