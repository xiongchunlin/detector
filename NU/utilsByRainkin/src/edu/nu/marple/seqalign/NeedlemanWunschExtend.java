package edu.nu.marple.seqalign;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.swing.plaf.multi.MultiMenuItemUI;

import edu.nu.marple.GlobalConfig;
import edu.nu.marple.dp.Cell;
import edu.nu.marple.dp.MultiDirectionCell;

public class NeedlemanWunschExtend<T> extends SequenceAlignmentExtend<T> {

	private Set<List<String>> matchedAlignments;
	private int higestScore;
	private Set<List<String>> unifiedDiffAlignment;

	public NeedlemanWunschExtend(List<T> sequence1, List<T> sequence2) {
		this(sequence1, sequence2, 1, -1, -1);
	}

	public NeedlemanWunschExtend(List<T> sequence1, List<T> sequence2, int match, int mismatch, int gap) {
		super(sequence1, sequence2, match, mismatch, gap);
		higestScore = 0;
	}

	@Override
	protected List<MultiDirectionCell> getInitialPointer(int row, int col) {
		if (row == 0 && col != 0) {
			List<MultiDirectionCell> pointers = new ArrayList<MultiDirectionCell>();
			pointers.add(scoreTable[row][col - 1]);
			return pointers;
		} else if (col == 0 && row != 0) {
			List<MultiDirectionCell> pointers = new ArrayList<MultiDirectionCell>();
			pointers.add(scoreTable[row - 1][col]);
			return pointers;
		} else {
			return null;
		}
	}

	@Override
	protected int getInitialScore(int row, int col) {
		if (row == 0 && col != 0) {
			return col * gap;
		} else if (col == 0 && row != 0) {
			return row * gap;
		} else {
			return 0;
		}
	}

	@Override
	protected void fillInCell(MultiDirectionCell currentCell, MultiDirectionCell cellAbove,
			MultiDirectionCell cellToLeft, MultiDirectionCell cellAboveLeft) {
		int rowSpaceScore = cellAbove.getScore() + gap;
		int colSpaceScore = cellToLeft.getScore() + gap;
		int matchOrMismatchScore = cellAboveLeft.getScore();
		if (sequence2.get(currentCell.getRow() - 1).equals(sequence1.get(currentCell.getCol() - 1))) {
			matchOrMismatchScore += match;
		} else {
			matchOrMismatchScore += mismatch;
		}

		if (rowSpaceScore > colSpaceScore) {
			if (matchOrMismatchScore > rowSpaceScore) {
				currentCell.setScore(matchOrMismatchScore);
				List<MultiDirectionCell> preCells = new ArrayList<MultiDirectionCell>();
				preCells.add(cellAboveLeft);
				currentCell.setPrevCells(preCells);
			} else if (matchOrMismatchScore < rowSpaceScore) {
				currentCell.setScore(rowSpaceScore);
				List<MultiDirectionCell> preCells = new ArrayList<MultiDirectionCell>();
				preCells.add(cellAbove);
				currentCell.setPrevCells(preCells);
			} else if (matchOrMismatchScore == rowSpaceScore) {
				currentCell.setScore(rowSpaceScore);
				List<MultiDirectionCell> preCells = new ArrayList<MultiDirectionCell>();
				preCells.add(cellAboveLeft);
				preCells.add(cellAbove);
				currentCell.setPrevCells(preCells);
			}
		} else if (rowSpaceScore < colSpaceScore) {
			if (matchOrMismatchScore > colSpaceScore) {
				currentCell.setScore(matchOrMismatchScore);
				List<MultiDirectionCell> preCells = new ArrayList<MultiDirectionCell>();
				preCells.add(cellAboveLeft);
				currentCell.setPrevCells(preCells);
			} else if (matchOrMismatchScore < colSpaceScore) {
				currentCell.setScore(colSpaceScore);
				List<MultiDirectionCell> preCells = new ArrayList<MultiDirectionCell>();
				preCells.add(cellToLeft);
				currentCell.setPrevCells(preCells);
			} else if (matchOrMismatchScore == colSpaceScore) {
				currentCell.setScore(colSpaceScore);
				List<MultiDirectionCell> preCells = new ArrayList<MultiDirectionCell>();
				preCells.add(cellAboveLeft);
				preCells.add(cellToLeft);
				currentCell.setPrevCells(preCells);
			}
		} else if (rowSpaceScore == colSpaceScore) {
			if (matchOrMismatchScore > rowSpaceScore) {
				currentCell.setScore(matchOrMismatchScore);
				List<MultiDirectionCell> preCells = new ArrayList<MultiDirectionCell>();
				preCells.add(cellAboveLeft);
				currentCell.setPrevCells(preCells);
			} else if (matchOrMismatchScore < rowSpaceScore) {
				currentCell.setScore(rowSpaceScore);
				List<MultiDirectionCell> preCells = new ArrayList<MultiDirectionCell>();
				preCells.add(cellAbove);
				preCells.add(cellToLeft);
				currentCell.setPrevCells(preCells);
			} else if (matchOrMismatchScore == rowSpaceScore) {
				currentCell.setScore(rowSpaceScore);
				List<MultiDirectionCell> preCells = new ArrayList<MultiDirectionCell>();
				preCells.add(cellAboveLeft);
				preCells.add(cellToLeft);
				preCells.add(cellAbove);
				currentCell.setPrevCells(preCells);
			}
		}

	}

	/**
	 * Get the matched alignment subsequence
	 */
	public Set<List<String>> getMatchedAlignment() {
		ensureTableIsFilledIn();
		if (this.matchedAlignments != null)
			return this.matchedAlignments;
		Set<List<String>> unifiedDiffAlignment = this.getUnifiedDiffAlignment();
		this.matchedAlignments = unifiedDiffAlignment.stream().map(diff->_extractMatchedAlignment(diff)).collect(Collectors.toSet());
		return this.matchedAlignments;
	}

	/**
	 * Get the unified diff alignment
	 */
	public Set<List<String>> getUnifiedDiffAlignment(){
		ensureTableIsFilledIn();
		if (this.unifiedDiffAlignment != null)
			return this.unifiedDiffAlignment;

		this.unifiedDiffAlignment = new HashSet<List<String>>();
		MultiDirectionCell startCell = scoreTable[scoreTable.length - 1][scoreTable[0].length - 1];		
		for (MultiDirectionCell preCell : startCell.getPrevCells()) {
			unifiedDiffAlignment.addAll(_getUnifiedDiffAlignment(startCell, preCell));
		}

		return this.unifiedDiffAlignment;
	}
	

	public void diff2Html(String phfDirName, String traceDirName) throws IOException {      
	      // generate a unified diff file
		  Set<List<String>> diffAlignment = this.getUnifiedDiffAlignment();
		  String rootDirPath = "generatedSignature/" + phfDirName + "/" + traceDirName + "/";
		  		  
		  File rootDir = new File(rootDirPath);
		  if (!rootDir.exists()){
			  rootDir.mkdirs();
		  }
		  int diffIndex = 1;
		  for (List<String> diff : diffAlignment){
			  StringBuffer diffString = new StringBuffer();
			  // insert indexs
			  String[] startIndexs = diff.get(0).split(" # ");
			  String sequence1StartIndex = startIndexs[0];
			  String sequence2StartIndex = startIndexs[1];
			  diffString.insert(0, "@@ " + sequence1StartIndex + ",10000 " + sequence2StartIndex + ",10000" + " @@");
			  // insert diff item
			  for (int i = 1; i < diff.size();i++){
				  String[] tmp = diff.get(i).split(" # ");
					if (tmp.length == 1){							
						diffString.append("\n\n" + diff.get(i));
					} else {
						diffString.append("\n\n" + tmp[0]);
						diffString.append("\n" + tmp[1]);
					}
				  
			  }
			  
			  String diffFilePath = rootDirPath + diffIndex + ".diff"; 
			  diffIndex++;
			  
			  File inputFile = new File(diffFilePath);
		      inputFile.createNewFile();
		      BufferedWriter in = new BufferedWriter(new FileWriter(inputFile));
		      in.write(diffString.toString());
		      in.flush();
		      in.close();
		      
		      // generate diff html file
		      Process process;			      
		      String a = "python " + GlobalConfig.getInstance().rootPath + "/diff2html.py -i " + "\"" + diffFilePath + "\"" + " -o " + diffFilePath + ".html";
		      process = Runtime.getRuntime().exec("python " + GlobalConfig.getInstance().rootPath + "/diff2html.py -i " + "\"" + diffFilePath + "\"" + " -o " + "\"" + diffFilePath + ".html" + "\"");
		  	  }	
     	     	     
	             
	   }
	
	/**
	 * Get the higest score of global alignment
	 */
	public int getMatchedAlignmentScores() {
		ensureTableIsFilledIn();
		if (this.higestScore != 0) {
			return this.higestScore;
		} else {
			this.higestScore = scoreTable[scoreTable.length - 1][scoreTable[0].length - 1].getScore();
			return this.higestScore;
		}
	}
	
	/**
	 * for each sig, return a List<List<String>>, 
	 * [0] matched common subsequence
	 * [1] matched indexs in align, not in trace indexs, start from 1
	 * [2] matched indexs in trace1, start from 0
	 * [3] matched indexs in trace2, start from 0
	 */
	public Set<List<List<String>>> getMatchedAndMatchedIndexsAndTrace1IndexsAndTrace2Indexs(){
		ensureTableIsFilledIn();
		Set<List<String>> unifiedDiffAlignment = this.getUnifiedDiffAlignment();
		return unifiedDiffAlignment.stream().map(diff->_extractMatchedAndMatchedIndexsAndTrace1IndexsAndTrace2Indexs(diff)).collect(Collectors.toSet());
	}

}
