package edu.nu.marple.seqalign;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import edu.nu.marple.EventRecordG;
import edu.nu.marple.Utils;
import edu.nu.marple.dp.MultiDirectionCell;

public class SmithWatermanExtendAffineGap<T> {
	protected List<T> sequence1;
	protected List<T> sequence2;
	protected MultiDirectionCell[][] MTable;
	protected MultiDirectionCell[][] XTable;
	protected MultiDirectionCell[][] YTable;
	protected int match;
	protected int gapStart;
	protected int gapExtend;
	protected int mismatch;
	protected boolean tableIsFilledIn;
	protected boolean isInitialized;
	protected List<Set<MultiDirectionCell>> topKhighScoreCells;
	protected List<Integer> topKhighScores;
	private List<Set<List<T>>> matchedAlignments;
	protected int topK;
	protected int minusInfinity = -999999;
	protected Map<String, Set<List<T>>> cache = new HashMap<String, Set<List<T>>>();

	
	public SmithWatermanExtendAffineGap(List<T> sequence1, List<T> sequence2, int match, int mismatch, int gapStart, int gapExtend, int topK){
		this.sequence1 = sequence1;
		this.sequence2 = sequence2;
		this.match = match;
		this.mismatch = mismatch;
		this.gapStart = gapStart;
		this.gapExtend = gapExtend;
		this.topK = topK;
		
		MTable = new MultiDirectionCell[sequence2.size() + 1][sequence1.size() + 1];
		XTable = new MultiDirectionCell[sequence2.size() + 1][sequence1.size() + 1];
		YTable = new MultiDirectionCell[sequence2.size() + 1][sequence1.size() + 1];
		topKhighScoreCells = new ArrayList<Set<MultiDirectionCell>>();
		topKhighScores = new ArrayList<Integer>();
		
		isInitialized = false;
		tableIsFilledIn = false;
	}
	
	/**
	 * make sure score table and initialization have been done
	 */
	protected void ensureTableIsFilledIn() {
		if (!isInitialized) {
			initialize();
			isInitialized=true;
		}
		if (!tableIsFilledIn) {
			fillIn();
			tableIsFilledIn=true;
		}
	}

	private void initialize() {
		for (int i = 0; i < sequence2.size() + 1; i++) {
			for (int j = 0; j < sequence1.size() + 1; j++) {
				MTable[i][j] = new MultiDirectionCell(i, j);
				XTable[i][j] = new MultiDirectionCell(i, j);
				YTable[i][j] = new MultiDirectionCell(i, j);
			}
		}
		initializeScores();
		initializePointers();
		
		isInitialized = true;
	}
	
	private void initializeScores(){
		// [0][i] 		
		for (int j = 1; j < sequence1.size() + 1; j++) {
			MTable[0][j].setScore(minusInfinity);
			XTable[0][j].setScore(minusInfinity);
			YTable[0][j].setScore(0);
		}
		
		// [i][0]
		for (int i = 1; i < sequence2.size() + 1; i++) {
			MTable[i][0].setScore(minusInfinity);
			XTable[i][0].setScore(0);
			YTable[i][0].setScore(minusInfinity);
		}
		
		// [0][0]
		MTable[0][0].setScore(0);
		XTable[0][0].setScore(minusInfinity);;
		YTable[0][0].setScore(minusInfinity);;
	}
	
	private void initializePointers(){
		for (int i = 0; i < sequence2.size()+1;i++){
			for (int j = 0; j< sequence1.size()+1;j++){
				MTable[i][j].setPrevCells(null);
				XTable[i][j].setPrevCells(null);
				YTable[i][j].setPrevCells(null);
			}
		}
	}
	
	private void fillIn() {
		for (int row = 1; row < sequence2.size()+1; row++){
			for (int col = 1; col < sequence1.size()+1; col++){
				/* mtable */
				int matchOrMismatchScore = 0;
				if (sequence2.get(row - 1).equals(sequence1.get(col - 1))) {
					matchOrMismatchScore += match;
				} else {
					matchOrMismatchScore += mismatch;
				}				
				int mTableLeftAboveScore = MTable[row-1][col-1].getScore() + matchOrMismatchScore;
				int xTableLeftAboveScore = XTable[row-1][col-1].getScore() + matchOrMismatchScore;
				int yTableLeftAboveScore = YTable[row-1][col-1].getScore() + matchOrMismatchScore;				
				_fillInTable(mTableLeftAboveScore, xTableLeftAboveScore, yTableLeftAboveScore, MTable, row, col, row-1, col-1);
								
				/* xtable */
				int mTableAboveScore = MTable[row-1][col].getScore() + gapStart;
				int xTableAboveScore = XTable[row-1][col].getScore() + gapExtend;
				int yTableAboveScore = YTable[row-1][col].getScore() + gapStart;
				_fillInTable(mTableAboveScore, xTableAboveScore, yTableAboveScore, XTable, row, col, row-1, col);
				
				/* ytable */
				int mTableLeftScore = MTable[row][col-1].getScore() + gapStart;
				int xTableLeftScore = XTable[row][col-1].getScore() + gapStart;
				int yTableLeftScore = YTable[row][col-1].getScore() + gapExtend;
				_fillInTable(mTableLeftScore, xTableLeftScore, yTableLeftScore, YTable, row, col, row, col-1);
				
				/* record higest value */
				int mScore = MTable[row][col].getScore();
				int xScore = XTable[row][col].getScore();
				int yScore = YTable[row][col].getScore();
				int maxScore = Math.max(mScore, Math.max(xScore, yScore));
				boolean isInserted = false;
				
				for (Integer highScore : new ArrayList<Integer>(topKhighScores)){					
					if (maxScore > highScore){
						isInserted = true;
						int insertIndex = topKhighScores.indexOf(highScore);
						
						// add to topK scores
						topKhighScores.add(insertIndex, maxScore);
						if (topKhighScores.size() > topK){
							topKhighScores.remove(topKhighScores.size()-1);
						}
						
						// add to topK cells
						Set<MultiDirectionCell> insertCells = new HashSet<MultiDirectionCell>();
						if (mScore == maxScore){
							insertCells.add(MTable[row][col]);
						} else if (xScore == maxScore){
							insertCells.add(XTable[row][col]);
						} else if (yScore == maxScore){
							insertCells.add(YTable[row][col]);
						}
						topKhighScoreCells.add(insertIndex, insertCells);
						if (topKhighScoreCells.size() > topK)
							topKhighScoreCells.remove(topKhighScoreCells.size() - 1);
						
						break;						
					} else if (maxScore == highScore){
						isInserted = true;
						int insertIndex = topKhighScores.indexOf(highScore);
						Set<MultiDirectionCell> insertIndexCells = topKhighScoreCells.get(insertIndex);
						if (mScore == maxScore){
							insertIndexCells.add(MTable[row][col]);
						} else if (xScore == maxScore){
							insertIndexCells.add(XTable[row][col]);
						} else if (yScore == maxScore){
							insertIndexCells.add(YTable[row][col]);
						}
						
						break;
					}
					
				}
				
				// if the above insertion fails and topK has free rooms, append current
				// cell
				if (!isInserted && topKhighScores.size() < topK) {
					topKhighScores.add(maxScore);
					Set<MultiDirectionCell> insertCells = new HashSet<MultiDirectionCell>();
					if (mScore == maxScore){
						insertCells.add(MTable[row][col]);
					} else if (xScore == maxScore){
						insertCells.add(XTable[row][col]);
					} else if (yScore == maxScore){
						insertCells.add(YTable[row][col]);
					}
					topKhighScoreCells.add(insertCells);
				}
			}
		}
	}
	
	private void _fillInTable(int mScore, int xScore, int yScore, MultiDirectionCell[][] table, int currentRow, int currentCol, int preRow, int preCol){
		if (mScore > xScore){
			if (mScore > yScore){
				if (mScore > 0){
					table[currentRow][currentCol].setScore(mScore);
					List<MultiDirectionCell> preCells = new ArrayList<MultiDirectionCell>();
					preCells.add(MTable[preRow][preCol]);
					table[currentRow][currentCol].setPrevCells(preCells);
				}							
			} else if (mScore < yScore){
				if (yScore > 0){
					table[currentRow][currentCol].setScore(yScore);
					List<MultiDirectionCell> preCells = new ArrayList<MultiDirectionCell>();
					preCells.add(YTable[preRow][preCol]);
					table[currentRow][currentCol].setPrevCells(preCells);
				}
			} else if (mScore == yScore){
				if (mScore > 0){
					table[currentRow][currentCol].setScore(mScore);
					List<MultiDirectionCell> preCells = new ArrayList<MultiDirectionCell>();
					preCells.add(MTable[preRow][preCol]);
					preCells.add(YTable[preRow][preCol]);
					table[currentRow][currentCol].setPrevCells(preCells);
				}												
			}				
		} 
		else if (mScore < xScore){
			if (xScore > yScore){
				if (xScore > 0){
					table[currentRow][currentCol].setScore(xScore);
					List<MultiDirectionCell> preCells = new ArrayList<MultiDirectionCell>();
					preCells.add(XTable[preRow][preCol]);
					table[currentRow][currentCol].setPrevCells(preCells);
				}
			} else if (xScore < yScore){
				if (yScore > 0){
					table[currentRow][currentCol].setScore(yScore);
					List<MultiDirectionCell> preCells = new ArrayList<MultiDirectionCell>();
					preCells.add(YTable[preRow][preCol]);
					table[currentRow][currentCol].setPrevCells(preCells);
				}						
			} else if (xScore == yScore){
				if (xScore > 0){
					table[currentRow][currentCol].setScore(xScore);
					List<MultiDirectionCell> preCells = new ArrayList<MultiDirectionCell>();
					preCells.add(XTable[preRow][preCol]);
					preCells.add(YTable[preRow][preCol]);
					table[currentRow][currentCol].setPrevCells(preCells);
				}						
			}
		} 
		else if (mScore == xScore){
			if (yScore > mScore){
				if (yScore > 0){
					table[currentRow][currentCol].setScore(yScore);
					List<MultiDirectionCell> preCells = new ArrayList<MultiDirectionCell>();
					preCells.add(YTable[preRow][preCol]);
					table[currentRow][currentCol].setPrevCells(preCells);
				}
			} else if (yScore < mScore){
				if (mScore > 0){
					table[currentRow][currentCol].setScore(mScore);
					List<MultiDirectionCell> preCells = new ArrayList<MultiDirectionCell>();
					preCells.add(MTable[preRow][preCol]);
					preCells.add(XTable[preRow][preCol]);
					table[currentRow][currentCol].setPrevCells(preCells);
				}
			} else if (yScore == mScore){
				if (yScore > 0){
					table[currentRow][currentCol].setScore(yScore);
					List<MultiDirectionCell> preCells = new ArrayList<MultiDirectionCell>();
					preCells.add(MTable[preRow][preCol]);
					preCells.add(XTable[preRow][preCol]);
					preCells.add(YTable[preRow][preCol]);
					table[currentRow][currentCol].setPrevCells(preCells);
				}
			}
		}
	}
	
	/**
	 * Get all matched alignment subsequences
	 * 
	 * @return a set of matched subsequences
	 */
	public List<Set<List<T>>> getMatchedAlignment() {
		ensureTableIsFilledIn();
		if (this.matchedAlignments != null)
			return this.matchedAlignments;

		this.matchedAlignments = new ArrayList<Set<List<T>>>();
		for (Set<MultiDirectionCell> cells : topKhighScoreCells) {
			Set<List<T>> matchedSet = new HashSet<List<T>>();
			for (MultiDirectionCell currentCell : cells) {
				if (currentCell.getPrevCells() != null) {
					for (MultiDirectionCell preCell : currentCell.getPrevCells()) {
						matchedSet.addAll(_getMatchedAlignment(currentCell, preCell));
					}
				}
			}
			this.matchedAlignments.add(matchedSet);
		}

		return this.matchedAlignments;
	}

	/**
	 * Get all matched alignment subsequences' scores
	 */
	public List<Integer> getMatchedAlignmentScores() {
		ensureTableIsFilledIn();
		if (!this.topKhighScores.isEmpty()) {
			return this.topKhighScores;
		} else {
			this.getMatchedAlignment();
			return this.topKhighScores;
		}

	}
	
	/**
	 * find the matched alignment start from currentCell -> preCell
	 * @param currentCell
	 * @param preCell
	 * @return
	 */
	protected Set<List<T>> _getMatchedAlignment(MultiDirectionCell currentCell, MultiDirectionCell preCell) {
		Set<List<T>> currentMatchedSet = new HashSet<List<T>>();
		T matchedString = null;
		boolean isAbove = currentCell.getRow() - preCell.getRow() == 1;
		boolean isLeft = currentCell.getCol() - preCell.getCol() == 1;
		boolean isMatch = false;
		if (currentCell.getRow() == 0 || currentCell.getCol() == 0){
			isMatch = false;
		} else {
			isMatch = sequence2.get(currentCell.getRow() - 1).equals(sequence1.get(currentCell.getCol() - 1));
		}		

		if (isLeft && isAbove && isMatch) {
			matchedString = sequence2.get(currentCell.getRow() - 1);
		}

		if (preCell.getPrevCells() != null) {
			for (MultiDirectionCell prepreCell : preCell.getPrevCells()) {
				Set<List<T>> preMatchedSet = null;
				String cacheKey = preCell.getRow() + "#" + preCell.getCol() + "@" + prepreCell.getRow() + "#" +  prepreCell.getCol();
				if (cache.containsKey(cacheKey)){
					preMatchedSet = cache.get(cacheKey);
				} else {
					preMatchedSet = _getMatchedAlignment(preCell, prepreCell);
					cache.put(cacheKey, preMatchedSet);
				}
				
				Set<List<T>> finalMatchedSet = preMatchedSet.stream().map(e -> new ArrayList(e)).collect(Collectors.toSet());								
				if (matchedString != null) {
					if (!finalMatchedSet.isEmpty()) {
						for (List<T> matched : finalMatchedSet) {
							matched.add(matchedString);
						}
					} else {
						List<T> matched = new ArrayList<T>();
						matched.add(matchedString);
						finalMatchedSet.add(matched);
					}

				}
				currentMatchedSet.addAll(finalMatchedSet);
			}
		} else { // stop the recursive
			if (matchedString != null){
				List<T> matched = new ArrayList<T>();
				matched.add(matchedString);
				currentMatchedSet.add(matched);
			} 			
		}

		return currentMatchedSet;

	}
	
	
	
	/**
	 * Print the dynamic programming table of score
	 */
	public void printMTable() {
		ensureTableIsFilledIn();
		for (int i = 0; i < MTable.length; i++) {
			for (int j = 0; j < MTable[0].length; j++) {
				System.out.print(MTable[i][j].getScore() + "\t");
			}
			System.out.println("");
		}
	}
	
	public void printXTable() {
		ensureTableIsFilledIn();
		for (int i = 0; i < XTable.length; i++) {
			for (int j = 0; j < XTable[0].length; j++) {
				System.out.print(XTable[i][j].getScore() + "\t");
			}
			System.out.println("");
		}
	}
	
	public void printYTable() {
		ensureTableIsFilledIn();
		for (int i = 0; i < YTable.length; i++) {
			for (int j = 0; j < YTable[0].length; j++) {
				System.out.print(YTable[i][j].getScore() + "\t");
			}
			System.out.println("");
		}
	}
	
	
	public static void main(String[] args) throws IOException {
//		SmithWatermanExtendAffineGap<String> sw = new SmithWatermanExtendAffineGap<String>(Arrays.asList("GCCCTAGCG".split("")), Arrays.asList("GCGCAGAATG".split("")), 1, -1, -1, -1, 3);
//		System.out.println("m table");
//		sw.printMTable();
//		System.out.println("x table");
//		sw.printXTable();
//		System.out.println("y table");
//		sw.printYTable();
//		System.out.println(sw.topKhighScoreCells);
//		System.out.println(sw.topKhighScores);
//		System.out.println(sw.getMatchedAlignment());
//		System.out.println(sw.getMatchedAlignmentScores());
		
		
      String trace1Path = "utilsByRainkin/traces/Keylogger/DarkComet5.1@1_Keylogger.json.syscall_args_extracted.argPreProcessed.syscall_args_block500_uselessRunqingNew_filtered";
      String trace2Path = "utilsByRainkin/traces/Keylogger/Bozok@1_Keylogger.json.syscall_args_extracted.argPreProcessed.syscall_args_block500_uselessRunqingNew_filtered";
      List<EventRecordG> sequence1 = Utils.EventRecordGFile2EventRecordGenerationList(trace1Path);
      List<EventRecordG> sequence2 = Utils.EventRecordGFile2EventRecordGenerationList(trace2Path);
      SmithWatermanExtendAffineGap swextend = new SmithWatermanExtendAffineGap(sequence1, sequence2, 40, -3, -2, -1, 1);
      System.out.println("======results=====");
//      System.out.println(Utils.localMatchedAlign2String(swextend.getMatchedAlignment()));
      swextend.getMatchedAlignmentScores().forEach(System.out::println);
//      System.out.println("m table");
//      swextend.printMTable();
//      System.out.println("x table");
//      swextend.printXTable();
//      System.out.println("y table");
//      swextend.printYTable();
		
	}
	
	
	
}
