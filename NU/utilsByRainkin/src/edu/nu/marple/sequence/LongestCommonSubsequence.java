
package edu.nu.marple.sequence;

import java.util.List;

import edu.nu.marple.dp.Cell;
import edu.nu.marple.dp.DynamicProgramming;


public class LongestCommonSubsequence extends DynamicProgramming {

//   public LongestCommonSubsequence(String sequence1, String sequence2) {
//      super(sequence1, sequence2);
//   }
   
   public LongestCommonSubsequence(List<String> sequence1, List<String> sequence2) {
      super(sequence1, sequence2);
   }

   
   public String getLongestCommonSubsequence() {
      if (!isInitialized) {
         initialize();
      }
      if (!tableIsFilledIn) {
         fillIn();
      }

      return (String) getTraceback();
   }
   
   public int getScore(){
      if (!isInitialized) {
          initialize();
       }
       if (!tableIsFilledIn) {
          fillIn();
       } 
       
       return scoreTable[scoreTable.length - 1][scoreTable[0].length - 1].getScore();
   }

   @Override
   protected void fillInCell(Cell currentCell, Cell cellAbove, Cell cellToLeft,
         Cell cellAboveLeft) {
      int aboveScore = cellAbove.getScore();
      int leftScore = cellToLeft.getScore();
      int matchScore;
      if (sequence1.get(currentCell.getCol() - 1).equals(sequence2
            .get(currentCell.getRow() - 1))) {
         matchScore = cellAboveLeft.getScore() + 1;
      } else {
         matchScore = cellAboveLeft.getScore();
      }
      int cellScore;
      Cell cellPointer;
      if (matchScore >= aboveScore) {
         if (matchScore >= leftScore) {
            // matchScore >= aboveScore and matchScore >= leftScore
            cellScore = matchScore;
            cellPointer = cellAboveLeft;
         } else {
            // leftScore > matchScore >= aboveScore
            cellScore = leftScore;
            cellPointer = cellToLeft;
         }
      } else {
         if (aboveScore >= leftScore) {
            // aboveScore > matchScore and aboveScore >= leftScore
            cellScore = aboveScore;
            cellPointer = cellAbove;
         } else {
            // leftScore > aboveScore > matchScore
            cellScore = leftScore;
            cellPointer = cellToLeft;
         }
      }
      currentCell.setScore(cellScore);
      currentCell.setPrevCell(cellPointer);
   }

   /*
    * (non-Javadoc)
    * 
    */
   @Override
   protected Cell getInitialPointer(int row, int col) {
      return null;
   }

   /*
    * (non-Javadoc)
    * 
    */
   @Override
   protected int getInitialScore(int row, int col) {
      return 0;
   }

   /*
    * (non-Javadoc)
    * 
    */
   @Override
   protected Object getTraceback() {
      StringBuffer lCSBuf = new StringBuffer();
      Cell currentCell = scoreTable[scoreTable.length - 1][scoreTable[0].length - 1];
      while (currentCell.getScore() > 0) {
         Cell prevCell = currentCell.getPrevCell();
         if ((currentCell.getRow() - prevCell.getRow() == 1 && currentCell
               .getCol()
               - prevCell.getCol() == 1)
               && currentCell.getScore() == prevCell.getScore() + 1) {
            lCSBuf.insert(0, sequence1.get(currentCell.getCol() - 1) + "\n");
         }
         currentCell = prevCell;
      }

      return lCSBuf.toString();
   }
}
