package edu.nu.marple;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Reader;
import java.util.HashMap;
import java.util.Set;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

public class GlobalConfig {
   private static GlobalConfig globalConfig = null;
   
   public HashMap<String, Integer> weightKeyloggerName;
   public HashMap<String, Integer> weightRemoteShellName;
   public HashMap<String, Integer> weightRemoteDesktopName;
   public HashMap<String, Integer> weightUselessName;
   
   public Set<Set<String>> argumentSharingBlacklist;
   
   public String rootPath = "utilsByRainkin/";
   
   private GlobalConfig(){
      File file = null;
      Reader reader = null;
      Gson gson = new Gson();
      try {
         String schemaDir = rootPath + "scoring schema/";
         
         // keylogger
         file = new File(schemaDir + "Keylogger_Name.json");
         reader = new FileReader(file);         
         this.weightKeyloggerName = gson.fromJson(reader, new TypeToken<HashMap<String, Integer>>() {}.getType());
         
         // remoteshell
         file = new File(schemaDir + "RemoteShell_Name.json");
         reader = new FileReader(file);
         this.weightRemoteShellName = gson.fromJson(reader, new TypeToken<HashMap<String, Integer>>() {}.getType());
         
         // remotedesktop
         file = new File(schemaDir + "RemoteDesktop_Name.json");
         reader = new FileReader(file);
         this.weightRemoteDesktopName = gson.fromJson(reader, new TypeToken<HashMap<String, Integer>>() {}.getType());
         
         // useless 
         file = new File(schemaDir + "Useless_Name.json");
         reader = new FileReader(file);
         this.weightUselessName = gson.fromJson(reader, new TypeToken<HashMap<String, Integer>>() {}.getType());         
         
      } catch (FileNotFoundException e) {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }
      
      try {
		loadArgumentSharingBlacklist();
	} catch (JsonIOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (JsonSyntaxException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (FileNotFoundException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
   }
   
   private void loadArgumentSharingBlacklist() throws JsonIOException, JsonSyntaxException, FileNotFoundException {
	   File blacklistFile = new File("DataFlowConfig/argSharingBlacklist.txt");
	   this.argumentSharingBlacklist = new Gson().fromJson(new FileReader(blacklistFile), new TypeToken<Set<Set<String>>>(){}.getType());
   }

public static GlobalConfig getInstance(){
      if (globalConfig == null){
         globalConfig = new GlobalConfig();
      }
      return globalConfig;
   }
   
   private void loadWeight(String filePath) throws FileNotFoundException{
      File file = new File(filePath);
      Reader reader = new FileReader(file);
      Gson gson = new Gson();
      this.weightKeyloggerName = gson.fromJson(reader, new TypeToken<HashMap<String, Integer>>() {}.getType());
      System.out.println(this.weightKeyloggerName);
   }
   
   public void test(){
      System.out.println(this.weightKeyloggerName);
      System.out.println(this.weightRemoteShellName);
      System.out.println(this.weightRemoteDesktopName);
      System.out.println(this.weightUselessName);
   }
}
