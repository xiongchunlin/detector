package edu.nu.marple.main;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.poi.util.TempFile;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import edu.nu.marple.Filter;
import edu.nu.marple.GlobalConfig;
import edu.nu.marple.SigResults;
import edu.nu.marple.EventRecordG;
import edu.nu.marple.Utils;
import edu.nu.marple.filemanipulation.FileReadingUtil;

import java.util.HashSet;

import edu.nu.marple.sigrefine.CountingRefinement;
import edu.nu.marple.sigrefine.CountingScore;
import edu.nu.marple.sigrefine.CountingScoreStat;
import edu.nu.marple.sigrefine.NonPhfFilter;

public class FinallSigsFilterMain {

	public static String traceExtention = ".output.tracePreprocessed";

	public static void main(String[] args) throws IOException {
		List<String> phfs = Arrays.asList(
										  "download&exec"
				//						  "remotedesktop"
//										  "audiorecord",
//										  "keylogger",  
//										  "remoteshell",
//										  "send&exec"
										  );
//		List<Double> scoreThreshold = Arrays.asList(// 0.12,
//				-0.26, 
//				-0.14, 0.28, 0.0, 0.0
//				);
		BufferedWriter nonphfStat = new BufferedWriter(new FileWriter("generatedSignature/nonphf-stat.txt"));
		int nowPhfNo = 0;
		for (String phf : phfs) {
			// System.out.println(phf);
			File file = new File("generatedSignature/" + phf + "_5tracesGlobalResults.txt");
			System.out.println(file.getName());
			List<SigResults> results = new Gson().fromJson(new FileReader(file), new TypeToken<List<SigResults>>() {
			}.getType());

			File dir = new File(GlobalConfig.getInstance().rootPath + "traces/" + phf + "/");
			File[] listsOfDir = dir.listFiles(new FilenameFilter() {
				public boolean accept(File dir, String name) {
					return name.endsWith(traceExtention);
				}
			});
			int totalTraceNumber = listsOfDir.length;

//			// Counting Refinement
//			File dir2 = new File("data/non-phf-filter/benign_for_score/");
//			File[] listsOfDir2 = dir2.listFiles(new FilenameFilter() {
//				public boolean accept(File dir, String name) {
//					return name.endsWith(traceExtention);
//				}
//			});
//			int totalTraceNumber2 = listsOfDir2.length;
//
//			CountingRefinement countingRefinement = new CountingRefinement(phf, traceExtention);
//			CountingScoreStat countingScoreStat = new CountingScoreStat();
//			for (int i = 0; i < results.size(); ++i) {
//				int j = 0;
//				while (j < results.get(i).sig.size()) {
//					EventRecordG itrEventRecordG = EventRecordG.stringToEventRecordG(results.get(i).sig.get(j));
//					if (!countingRefinement.getBenignResults().containsKey(itrEventRecordG))
//						countingRefinement.getBenignResults().put(itrEventRecordG, 0);
//					double ratScore = (double) (countingRefinement.getRATResults().get(itrEventRecordG))
//							/ totalTraceNumber;
//					double benignScore = (double) (countingRefinement.getBenignResults().get(itrEventRecordG))
//							/ totalTraceNumber2;
//					CountingScore nowScore = new CountingScore(itrEventRecordG, ratScore, benignScore);
//					countingScoreStat.setResults.add(nowScore);
//					if (results.get(i).sig.size() > 25 && ratScore - benignScore < scoreThreshold.get(nowPhfNo)) {
//						results.get(i).sig.remove(j);
//					} else {
//						++j;
//					}
//				}
//			}
//
//			countingScoreStat.transferResults();
//			BufferedWriter bw = new BufferedWriter(new FileWriter("generatedSignature/" + phf + "_syscallScore.txt"));
//			for (int i = 0; i < countingScoreStat.listResults.size(); ++i) {
//				bw.write(countingScoreStat.listResults.get(i).syscall.toString() + " "
//						+ countingScoreStat.listResults.get(i).ratScore + " "
//						+ countingScoreStat.listResults.get(i).benignScore + " "
//						+ (countingScoreStat.listResults.get(i).ratScore
//								- countingScoreStat.listResults.get(i).benignScore));
//				bw.newLine();
//			}
//			bw.close();

			// remove duplicated sequences
			Filter filter = new Filter();
			results = results.stream().map(item -> filter.filterConsecutiveBlocksSigResults(item, 1, 500, 1))
					.collect(Collectors.toList());

			// remove length <= 0
			for (int i = 0; i < results.size();) {
				if (results.get(i).sig.size() <= 0) {
					results.remove(i);
				} else
					++i;
			}

			// non-phf filtering, matched count > corresponding threshold
			NonPhfFilter nonPhfFilter = new NonPhfFilter(phf, results);
			results = nonPhfFilter.getResult();

			System.out.println("Filtering phf: " + phf);
			nonphfStat.write("Filtering phf: " + phf);
			nonphfStat.newLine();
			for (Integer ii : nonPhfFilter.FPmatchedCntStat.descendingKeySet()) {
				System.out.println(ii + ": " + nonPhfFilter.FPmatchedCntStat.get(ii));
				nonphfStat.write(ii + ": " + nonPhfFilter.FPmatchedCntStat.get(ii).toString());
				nonphfStat.newLine();
			}

			// remove the superset
			MyComparator mc = new MyComparator();
			Collections.sort(results, mc);
			for (int i = 0; i < results.size(); ++i)
				for (int j = i + 1; j < results.size();) {
					int k = 0, l = 0;
					while (k < results.get(i).sig.size() && l < results.get(j).sig.size()) {
						if (results.get(i).sig.get(k).equals(results.get(j).sig.get(l)))
							++k;
						++l;
					}
					if (k == results.get(i).sig.size()) {
						if (results.get(i).sig.size() == results.get(j).sig.size()
								&& results.get(i).windowsize > results.get(j).windowsize)
							results.get(i).windowsize = results.get(j).windowsize;
						if (results.get(i).windowsize <= results.get(j).windowsize)
							results.remove(j);
						else
							++j;
					} else
						++j;
				}
			
			// sort with credit
			MyComparator2 mc2 = new MyComparator2();
			Collections.sort(results, mc2);
			
			// write back to a new file
			++nowPhfNo;
			File filteredTrace = new File("generatedSignature/" + file.getName().split("\\.")[0] + "_filtered.txt");
			FileWriter filteredWriter = new FileWriter(filteredTrace);
			filteredWriter.write(new GsonBuilder().setPrettyPrinting().create().toJson(results));
			filteredWriter.flush();
			filteredWriter.close();
		}
		nonphfStat.close();
	}
	
	static class MyComparator implements Comparator {
		@Override
		public int compare(Object o1, Object o2) {
			SigResults sigo1 = (SigResults) o1;
			SigResults sigo2 = (SigResults) o2;
			if (sigo1.sig.size() < sigo2.sig.size())
				return -1;
			else if (sigo1.sig.size() == sigo2.sig.size())
				return 0;
			else
				return 1;
		}
	}

	static class MyComparator2 implements Comparator {
		@Override
		public int compare(Object o1, Object o2) {
			SigResults sigo1 = (SigResults) o1;
			SigResults sigo2 = (SigResults) o2;
			if (sigo1.coverage / sigo1.falsePositiveShare < sigo2.coverage / sigo2.falsePositiveShare)
				return -1;
			else if (sigo1.coverage / sigo1.falsePositiveShare == sigo2.coverage / sigo2.falsePositiveShare)
				return 0;
			else
				return 1;
		}
	}

}


