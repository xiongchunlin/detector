package edu.nu.marple.main;

import java.io.File;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import edu.nu.marple.Filter;
import edu.nu.marple.GlobalConfig;
import edu.nu.marple.EventRecordG;
import edu.nu.marple.Utils;

public class TracePreProcessMain {
	public static int numberOfBlocksToKeep = 2;
	public static int minLengthOfConsecutiveBlock = 1;
	public static int maxLengthOfConsecutiveBlock = 500;

	public static void main(String[] args) throws IOException {
		processNonPhfSet();
		processTrainingSet();
		processTestingSet();
	}

	public static void processTrainingSet() {
		List<String> phfs = Arrays.asList( "download&exec"
				//"remotedesktop", "audiorecord", "remoteshell", "keylogger", "send&exec"
				);
		phfs.forEach(t -> {
			try {
				Utils.tracePreProcessForPhf(GlobalConfig.getInstance().rootPath + "traces/", t, ".output",
						".tracePreprocessed", minLengthOfConsecutiveBlock, maxLengthOfConsecutiveBlock,
						numberOfBlocksToKeep, "importantList/useless_e4.txt", "importantList/usefulevent2.txt");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});
	}

	public static void processTestingSet() {
		List<String> phfs = Arrays.asList(
				//"remotedesktop"
				//, "audiorecord", "remoteshell", "keylogger", "send&exec", "allphf-2"
				//"FPs"
				"download&exec"
		);
		phfs.forEach(t -> {
			try {
				Utils.tracePreProcessForPhf("data/test/", t, ".output", ".tracePreprocessed",
						minLengthOfConsecutiveBlock, maxLengthOfConsecutiveBlock, numberOfBlocksToKeep,
						"importantList/useless_e4.txt", "importantList/usefulevent2.txt");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});
	}

	public static void processNonPhfSet() {
		List<String> phfs = Arrays.asList("download&exec"
				//"remotedesktop", "audiorecord", "remoteshell", "keylogger", "send&exec"
				);
		phfs.forEach(t -> {
			try {
				Utils.tracePreProcessForPhf("data/non-phf-filter/", t, ".output", ".tracePreprocessed",
						minLengthOfConsecutiveBlock, maxLengthOfConsecutiveBlock, numberOfBlocksToKeep,
						"importantList/useless_e4.txt", "importantList/usefulevent2.txt");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});
	}

}
