package edu.nu.marple.main;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import edu.nu.marple.SigResults;
import edu.nu.marple.EventRecordG;
import edu.nu.marple.Filter;
import edu.nu.marple.seqalign.NeedlemanWunschExtend;
import edu.nu.marple.seqalign.SmithWatermanExtend;

public class SigExtGlobalAlignExtendMain {
	private static int scoreDistance = 100;
	private static int matchScore = 4;
	private static int mismatchScore = -1;
	private static int gapScore = -1;
	
	public static void main(String[] args) throws IOException {
		String sigDir = "generatedSignature/";
		
		List<String> phfs = Arrays.asList(
										  "download&exec"
//										  "remotedesktop",
//										  "audiorecord",
//										  "remoteshell",
//										  "keylogger",
//										  "send&exec"
										 );
		phfs.forEach(phf -> {
			try {
				globalIteration(phf, sigDir, 5);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		 });
	}

	private static void globalIteration(String phf, String sigDir, int iterationNumber) throws IOException {
		for (int i = 3; i <= iterationNumber; i++){
			_globalIteration(phf, sigDir, i);
		}
	}
	private static void _globalIteration(String phf, String sigDir, int iterationNumber) throws IOException {
		Writer resultsWriter = new BufferedWriter(new FileWriter("generatedSignature/" + phf + "_" + iterationNumber + "tracesGlobalResults.txt"));		
		List<SigResults> finallResults = new ArrayList<SigResults>();

		// load local results
		String resultsFilePath = "";
		if (iterationNumber == 3){
			resultsFilePath = sigDir + phf + "_localResults.txt";
		} else {
			resultsFilePath = sigDir + phf + "_" + (iterationNumber-1) + "tracesGlobalResults.txt";
		}
		
		System.out.println("processing " + resultsFilePath);
		List<SigResults> results = new Gson().fromJson(new FileReader(new File(resultsFilePath)), new TypeToken<List<SigResults>>(){}.getType());
		Set<SigResults> unglobalSigs = new HashSet<SigResults>(results);
		
		// local align for iterationNumber traces
		for (int i = 0; i < results.size();i++){
			for (int j = i+1; j < results.size();j++){
				SigResults sig1 = results.get(i);
				SigResults sig2 = results.get(j);
				
				// ensure that the size of traces label of two sigs == iterationNumber-1, 
				// e.g. itrationNumber = 4, {a,b} should not be alignment because this sig are derived directly from local alignment
				if (sig1.traces.size() != iterationNumber-1 || sig2.traces.size() != iterationNumber-1)
					continue;

				// if number of traces == iterationNumber
				Set<String> traces = new HashSet<String>(sig1.traces);
				traces.addAll(new HashSet<String>(sig2.traces));
				int traceSize = traces.size();
				if (traceSize == iterationNumber){
					System.out.println(sig1.traces + " vs " + sig2.traces);
					List<SigResults> twoLocalResults = globalTwoSigs(sig1, sig2, traces, unglobalSigs);
					finallResults.addAll(twoLocalResults);
				}

			}
		}

		// add unglobal sigs
		finallResults.addAll(unglobalSigs);
		
		// filtering
		Filter<SigResults> filter = new Filter();
		finallResults = filter.removeDuplicatedSigs(finallResults);
		finallResults = filter.removeSubseqSigs(finallResults);
		
		
		String output = new GsonBuilder().setPrettyPrinting().create().toJson(finallResults);
		resultsWriter.write(output);
		resultsWriter.close();
	}

	private static List<SigResults> globalTwoSigs(SigResults sig1, SigResults sig2, Set<String> traces, Set<SigResults> unglobalSigs) {
		List<SigResults> results = new ArrayList<SigResults>();

		List<EventRecordG> sequence1 = sig1.sig
				.stream()
				.map(e->EventRecordG.stringToEventRecordG(e))
				.collect(Collectors.toList());
		List<EventRecordG> sequence2 = sig2.sig
				.stream()
				.map(e->EventRecordG.stringToEventRecordG(e))
				.collect(Collectors.toList());

		NeedlemanWunschExtend<EventRecordG> alignment = new NeedlemanWunschExtend<EventRecordG>(sequence1, sequence2, matchScore, mismatchScore, gapScore);

		// construct SigResults
		Set<List<List<String>>> matchedAndMatchedIndexsAndTrace1AndTrace2Indexs = alignment.getMatchedAndMatchedIndexsAndTrace1IndexsAndTrace2Indexs();								
		for (List<List<String>> solution : matchedAndMatchedIndexsAndTrace1AndTrace2Indexs){
			List<String> matched = solution.get(0);
			List<String> matchedIndexs = solution.get(1);
			List<String> trace1Indexs = solution.get(2);
			List<String> trace2Indexs = solution.get(3);					

			// remove empty sigs
			if (matched.isEmpty() && matchedIndexs.isEmpty())
				continue;

			// remove sigs whose size < 80% of Min(seq1.size(), seq2.size())
			int matchedSize = matched.size();
			int minSize = Math.min(sequence1.size(), sequence2.size());			
			if (matchedSize < 0.8 * minSize){
//				System.out.println("==================");
//				System.out.println(matched);
//				System.out.println(sequence1);
//				System.out.println(sequence2);
				continue;
			} else {
				unglobalSigs.remove(sig1);
				unglobalSigs.remove(sig2);
			}

			// calculate windowsize				
			int windowsize = Math.min(sig1.windowsize, sig2.windowsize);												

			// construct SigResults
			SigResults twoLocalResults = new SigResults(traces, matched, windowsize);
			results.add(twoLocalResults);
		}

		return results;
	}


}
