package edu.nu.marple.main;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.Buffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import edu.nu.marple.GlobalConfig;
import edu.nu.marple.SigResults;
import edu.nu.marple.EventRecordG;
import edu.nu.marple.Filter;
import edu.nu.marple.Utils;
import edu.nu.marple.seqalign.SmithWaterman;
import edu.nu.marple.seqalign.SmithWatermanExtend;
import edu.nu.marple.seqalign.SequenceAlignment.Weights;

public class SigExtLocalAlignExtendMain {
	
	static int match = 4;
	static int mismatch = -1;
	static int gap = -1;

	public static void main(String[] args) {
		List<String> phfs = Arrays.asList(
										  "download&exec"
	//									  "remotedesktop"
//										  "audiorecord",
//										  "remoteshell",
//										  "keylogger",
//										  "send&exec"
										  );
		phfs.forEach(t -> {
			try {
				extractSigsForPhf(t, ".output.tracePreprocessed");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});
	}

	public static void extractSigsForPhf(String phf, String traceExtention) throws IOException {
		File dir = new File(GlobalConfig.getInstance().rootPath + "traces/" + phf + "/");
		File[] listsOfDir = dir.listFiles(new FilenameFilter() {
			public boolean accept(File dir, String name) {
				return name.endsWith(traceExtention);
			}
		});
		
//		try {
//			Thread.sleep(5000);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}

		BufferedWriter sigWriter = null;
		BufferedWriter sigDetailsWriter = null;
		BufferedWriter sigMatchedAndIndexsWriter = null;
		sigWriter = new BufferedWriter(new FileWriter("generatedSignature/" + phf + ".txt"));
		sigDetailsWriter = new BufferedWriter(new FileWriter("generatedSignature/" + phf + "_details.txt"));
		sigMatchedAndIndexsWriter = new BufferedWriter(new FileWriter("generatedSignature/" + phf + "_localResults.txt"));
		
		List<SigResults> results = new ArrayList<SigResults>();
		
		for (int i = 0; i < listsOfDir.length - 1; i++) {
			File trace1File = listsOfDir[i];
			for (int j = i + 1; j < listsOfDir.length; j++) 
			 {
				File trace2File = listsOfDir[j];
				//if (trace1File.getName().equals("DarkComet 5.3-2_PID4660_TID1868.output.tracePreprocessed") && trace2File.getName().equals("Spy-Net 2.7-2_PID3292_TID2948.output.tracePreprocessed")) continue;
				//if (trace1File.getName().equals("DarkComet Legacy-2_PID1904_TID5048.output.tracePreprocessed") && trace2File.getName().equals("Spy-Net 2.7-2_PID3292_TID2948.output.tracePreprocessed")) continue;
				String trace1Phf = trace1File.getParentFile().getName();
				//String trace1Name = trace1File.getPath().split("\\\\")[3].split("\\.event_args_extracted\\.argPreProcessed\\.syscall_args_block500_uselessRunqingNew_filtered")[0];
				String trace1Name = trace1File.getName().split("\\.output\\.tracePreprocessed")[0];
				String trace2Phf = trace2File.getParentFile().getName();
				String trace2Name = trace2File.getName().split("\\.output\\.tracePreprocessed")[0];

				List<EventRecordG> sequence1 = Utils.EventRecordGFile2EventRecordGenerationList(trace1File.getAbsolutePath());
				List<EventRecordG> sequence2 = Utils.EventRecordGFile2EventRecordGenerationList(trace2File.getAbsolutePath());

				System.out.println("===================" + trace1File.getName() + "  " + trace2File.getName());
				SmithWatermanExtend<EventRecordG> localAlign = new SmithWatermanExtend<EventRecordG>(sequence1, sequence2, match, mismatch, gap, 1);
				
				StringBuilder sigOutput = new StringBuilder();

				List<Set<List<List<String>>>> matchedAndMatchedIndexsAndTrace1AndTrace2Indexs = localAlign.getMatchedAndMatchedIndexsAndTrace1IndexsAndTrace2Indexs();
				//localAlign.diff2Html(phf, trace1Name+"_"+trace2Name);
				for (Set<List<List<String>>> topK : matchedAndMatchedIndexsAndTrace1AndTrace2Indexs){									
					for (List<List<String>> solution : topK){
						List<String> matched = solution.get(0);
						List<String> matchedIndexs = solution.get(1);
//						List<String> trace1Indexs = solution.get(2);
//						List<String> trace2Indexs = solution.get(3);					
						
						// remove empty sigs
						if (matched.isEmpty() && matchedIndexs.isEmpty())
							continue;
						
						// calculate windowsize
						Integer windowsize, start, end;
						start = Integer.valueOf(matchedIndexs.get(0));
						end = Integer.valueOf(matchedIndexs.get(matchedIndexs.size() - 1));
						windowsize = (end - start + matchedIndexs.size());
						
						// construct human readable sigs				
						sigOutput.append(matched.stream().collect(Collectors.joining(" -> "))).append("\n");												
						
						// construct SigResults
						Set<String> traces = new HashSet<String>();
						traces.add(trace1Name);
						traces.add(trace2Name);
						SigResults localResults = new SigResults(traces, matched, windowsize);
						results.add(localResults);
					}
				}
				
				// write to hunman-readable format file
				sigWriter.write(sigOutput.toString());
				sigDetailsWriter.write(trace1File.getPath() + " ==align== " + trace2File.getPath() + "\n");
				sigDetailsWriter.write(sigOutput + "\n");

			}
			

		}
		
		// filtering
		Filter<SigResults> filter = new Filter();
		results = filter.removeDuplicatedSigs(results);
		results = filter.removeSubseqSigs(results);
		
		// write to json format file
		String sigsAndMatchedIndexsAndDataflowOutput = new GsonBuilder().setPrettyPrinting().create().toJson(results);
		sigMatchedAndIndexsWriter.write(sigsAndMatchedIndexsAndDataflowOutput);
		
		sigWriter.close();
		sigDetailsWriter.close();
		sigMatchedAndIndexsWriter.close();
	}

}

