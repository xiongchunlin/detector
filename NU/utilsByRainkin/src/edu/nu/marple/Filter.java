package edu.nu.marple;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.stream.Collectors;

import edu.nu.marple.sequence.LongestCommonSubsequence;
import scala.collection.generic.BitOperations.Int;

import com.ibm.marple.EventRecord;

public class Filter<T> {
   

   public List<String> filter(List<String> sequences){
      return filter(sequences, 1, 1);
   }
   
   boolean uselessCheck(Map<String, String> parameter, Set<String> uselessSyscalls) {
	  for (String syscall : uselessSyscalls)
      if (parameter.get("SystemCall").equals((syscall))) {
    	 return true;
      }
	  return false;
   }
   
   /**
    * filter useless syscalls (EventRecord Version)
    */
   public List<EventRecord> filterUselessSyscalls(List<EventRecord> sequences, String uselessSyscallsFilePath) throws FileNotFoundException{
	   // load useless syscalls
	   Scanner scanner =  new Scanner(new File(uselessSyscallsFilePath));
	   HashSet<String> uselessSyscalls = new HashSet<String>();
	   while (scanner.hasNext()){
		   uselessSyscalls.add(scanner.next());
	   }
	   scanner.close();
	   
	   // filter
	   List<EventRecord> result = new ArrayList<EventRecord>();
	   for (EventRecord e : sequences) 
	   if (!e.eventName.equals("PerfInfoSysClEnter") || !uselessCheck(e.arguments, uselessSyscalls)){
		   result.add(e);
	   }
	   return result;
   }
   
   /**
   * filter useless events (EventRecord Version)
   */
  public List<EventRecord> filterUselessEvents(List<EventRecord> sequences, String usefulEventFilePath) throws FileNotFoundException{
	   // load useful events
	   Scanner scanner =  new Scanner(new File(usefulEventFilePath));
	   HashSet<String> usefulEvents = new HashSet<String>();
	   while (scanner.hasNext()){
		   usefulEvents.add(scanner.next());
	   }
	   scanner.close();
	   
	   // filter
	   List<EventRecord> result = new ArrayList<EventRecord>();
	   for (EventRecord e : sequences) 
	   if (usefulEvents.contains(e.eventName)){
		   result.add(e);
	   }
	   return result;
  }
   
   /**
    * remove consecutive and repeated blocks. e.g. a b a b x z -> a b x z (repeated and consecutive)  e.g. a b x a b -> a b x a b (repeated but not consecutive)
    * @param sequences
    * @param minLengthOfConsecutiveBlock the min length of the repeated and consecutive substring, which will be removed 
    * @param maxLengthOfConsecutivBlock the max length of the repeated and consecutive substring, which will be removed 
    * @return filtered sequences
    */
   public List<T> filterConsecutiveBlocks(List<T> sequences, int minLengthOfConsectiveBlock, int maxLengthOfConsecutiveBlock,
		   int numberOfBlocksToKeep){
      if (maxLengthOfConsecutiveBlock <= 0)
         return sequences;
      
      List<T> mergedSequences = new ArrayList<T>(sequences); 
      for (int i = minLengthOfConsectiveBlock; i < maxLengthOfConsecutiveBlock; i++){
         mergedSequences = mergeNLengthConsective(mergedSequences, i, numberOfBlocksToKeep);
      }
      return mergedSequences; 
   }
   
   /**
    * remove repeated blocks by LRS, whiose length must >= lrsMinLength. e.g. a b x a b -> a b x
    * @param sequences
    * @param searchDepth how many suffixs should been compare
    * @param lrsMinLength the minimal length of the longest repeated substring
    * @return filtered sequence s
    */
   public List<String> filter(List<String> sequences, int searchDepth, int lrsMinLength){
      List<String> mergedSequences = new ArrayList<String>(sequences); 
      // merge consecutive duplicated sequences      
//      mergedSequences = mergeNLengthConsective(mergedSequences, 1);
//      mergedSequences = mergeNLengthConsective(mergedSequences, 2);
      mergedSequences = mergeConsective(mergedSequences);
      
      // find longest repeated substring
      List<Integer> lrsStartIndexList = null;
      do{
//         System.out.println("----------------LRS----------------");
         LRS lrs = new LRS(mergedSequences, searchDepth, lrsMinLength);
         int lrsLength = lrs.getLrsLength();
         Set<Integer> lrsStartIndexSet = lrs.getLrsStartIndexSet();
         lrsStartIndexList = new ArrayList<Integer>(lrsStartIndexSet);
         Collections.sort(lrsStartIndexList);         
         for (int i = lrsStartIndexList.size() - 1; i > 0; i--){            
            for (int j = lrsStartIndexList.get(i) + lrsLength - 1 ; j >= lrsStartIndexList.get(i); j--){               
//               System.out.println(mergedSequences.get(j));
               mergedSequences.remove(j); 
            }
         }         
        
      } while(lrsStartIndexList.size() != 0);
      
      return mergedSequences;
   }
   
   public List<T> mergeNLengthConsective(List<T> sequences, int n, int numberOfBlocksToKeep){
      if (sequences.isEmpty() || n > sequences.size()/2 + 1 || n <= 0)
         return sequences;
      
      List<T> finalSequences = new ArrayList<T>(sequences);
      for (int i = 0; i < n; i++){         
         List<T> mergedSequences = new ArrayList<T>();
         mergedSequences.addAll(finalSequences.subList(0, i));
         List<T> mergedTmp = _mergeNLengthConsective(finalSequences.subList(i, finalSequences.size()), n, numberOfBlocksToKeep);         
         mergedSequences.addAll(mergedTmp);
         finalSequences = mergedSequences;
      }
      return finalSequences;
   }
   
   private List<T> _mergeNLengthConsective(List<T> sequences, int n, int numberOfBlocksToKeep){      
      if (sequences.isEmpty() || n > sequences.size()/2 + 1)
         return sequences;
      
      List<T> mergedSequences = new ArrayList<T>();
      int consecutiveBlockTime = 1; // the current number of consecutive and repeated blocks, for string aaa, consecutiveBlocks = 3
//      System.out.println("=====merge=====" + " " + n);
      int i = 0;
      for (; i < sequences.size() + 1 - 2*n; i = i + n){
         if (!sequences.subList(i, i+n).equals(sequences.subList(i+n, i+2*n))){            
	        List<T> repeatedBlocks = sequences.subList(i, i+n);
			if (consecutiveBlockTime == 1){ // doesn't have repeated and consecutie blocks
				mergedSequences.addAll(repeatedBlocks);
			} else { // have at least one repeated and consecutive blcoks
				int realNumberOfBlocksToKeep = Math.min(consecutiveBlockTime, numberOfBlocksToKeep);            
	            for (int k = 0; k < realNumberOfBlocksToKeep; k++){ // note that numberOfBlocks could be 0 which will not keep any blocks
	            	mergedSequences.addAll(repeatedBlocks);
	            }
			}        	            
            consecutiveBlockTime = 1;
         }
         else {
        	 consecutiveBlockTime += 1;
//            System.out.println(sequences.subList(i, i+n));
         }
      }
      
      if (consecutiveBlockTime == 1){ // n=3, abcdefzx, i = 3
    	  mergedSequences.addAll(sequences.subList(i, sequences.size()));
      } else { // n = 3, numberOfBlocksToKeep = 1, abcabcabcxx, i = 6, keep abcxx
	      List<T> repeatedBlocks = sequences.subList(i, i+n);
		  int realNumberOfBlocksToKeep = Math.min(consecutiveBlockTime, numberOfBlocksToKeep);            
          for (int k = 0; k < realNumberOfBlocksToKeep; k++){ // note that numberOfBlocks could be 0 which will not keep any blocks
        	  mergedSequences.addAll(repeatedBlocks);
          }			
          consecutiveBlockTime = 0;
          mergedSequences.addAll(sequences.subList(i+n, sequences.size()));
      }
//      System.out.println("+++++size+++++ " + mergedSequences.size());
      return mergedSequences;
   }
   
   public List<String> mergeConsective(List<String> sequences){
      if (sequences.isEmpty())
         return sequences;
         
      List<String> mergedSequences = new ArrayList<String>(); 
      for (int i = 0; i < sequences.size() - 1; i++){
         if (!sequences.get(i).equals(sequences.get(i+1))){
            mergedSequences.add(sequences.get(i));
         }
      }      
      mergedSequences.add(sequences.get(sequences.size()-1));
      return mergedSequences;
   }
   
   
   /**
    * filterConsecutiveBlocks for a SigResults
    */
   public SigResults filterConsecutiveBlocksSigResults(SigResults result, int minLengthOfConsecutiveBlock,
		   int maxLengthOfConsecutiveBlock, int numberOfBlocksToKeep) {
	   result.sig = (List<String>) filterConsecutiveBlocks((List<T>) result.sig, minLengthOfConsecutiveBlock, maxLengthOfConsecutiveBlock, numberOfBlocksToKeep);
	   return result;
   }
   
	public List<SyscallRecordGAllArgs> mergeNLengthConsectiveAllArgsVersion(List<SyscallRecordGAllArgs> sequences,
			int n) {
		if (sequences.isEmpty() || n > sequences.size() / 2 + 1 || n <= 0)
			return sequences;

		List<SyscallRecordGAllArgs> finalSequences = new ArrayList<SyscallRecordGAllArgs>(sequences);
		for (int i = 0; i < n; i++) {
			List<SyscallRecordGAllArgs> mergedSequences = new ArrayList<SyscallRecordGAllArgs>();
			mergedSequences.addAll(finalSequences.subList(0, i));
			List<SyscallRecordGAllArgs> mergedTmp = _mergeNLengthConsectiveAllArgsVersion(
					finalSequences.subList(i, finalSequences.size()), n);
			mergedSequences.addAll(mergedTmp);
			finalSequences = mergedSequences;
		}
		return finalSequences;
	}

	private List<SyscallRecordGAllArgs> _mergeNLengthConsectiveAllArgsVersion(List<SyscallRecordGAllArgs> sequences,
			int n) {
		if (sequences.isEmpty() || n > sequences.size() / 2 + 1)
			return sequences;

		List<SyscallRecordGAllArgs> mergedSequences = new ArrayList<SyscallRecordGAllArgs>();
		List<SyscallRecordGAllArgs> mergedBlocks = null;  
		int i = 0;
		for (; i < sequences.size() + 1 - 2 * n; i = i + n) {
			if (!sequences.subList(i, i + n).equals(sequences.subList(i + n, i + 2 * n))) {
				if (mergedBlocks == null){  // if not exist repeated and consecutive blocks
					mergedBlocks = sequences.subList(i, i+n);
				}
//				System.out.println(mergedBlocks.get(0).nonStringArgs);
				mergedSequences.addAll(mergedBlocks);
				mergedBlocks = null;       // reset 
			}
			else {
				if (mergedBlocks == null){ // the first time of repeated and consecutive blocks
					mergedBlocks = sequences.subList(i, i + n);
				}
				mergedBlocks = _mergeTwoBlocks(mergedBlocks, sequences.subList(i+n, i+2*n));
			}
		}
		if (mergedBlocks != null){
			mergedSequences.addAll(mergedBlocks);
			i = i + mergedBlocks.size();
			mergedBlocks = null;
		}
		mergedSequences.addAll(sequences.subList(i, sequences.size()));
		return mergedSequences;
	}
   
	/**
	 * merge two list of SyscallRecordGAllArgs, that is ,
	 * keep syscall name and string args not changed, append the latter's nonstringArgs to former's.
	 */
	private List<SyscallRecordGAllArgs> _mergeTwoBlocks(List<SyscallRecordGAllArgs> former, List<SyscallRecordGAllArgs> latter){
		List<SyscallRecordGAllArgs> merged = new ArrayList<SyscallRecordGAllArgs>();
				
		for (int i = 0; i < former.size(); i++){
			SyscallRecordGAllArgs former_item = former.get(i);
			SyscallRecordGAllArgs latter_item = latter.get(i);
			String mergedSyscallName = former_item.name;
			String mergedStringArgs = former_item.parameter;
			String mergedNonStringArgs = "";
			// if non string args of former and latter are the same, keep the former's

			if (!former_item.nonStringArgs.contains(latter_item.nonStringArgs)){
				mergedNonStringArgs = former_item.nonStringArgs + " # " + latter_item.nonStringArgs;
			} else {
				mergedNonStringArgs = former_item.nonStringArgs;
			}
			SyscallRecordGAllArgs merged_item = new SyscallRecordGAllArgs(mergedSyscallName, mergedStringArgs, mergedNonStringArgs);
			merged.add(merged_item);
		}
		
		return merged;
	}
	
	/*
	 * remove duplicated SigResults whose sig field is the same, and choose the one whose windowsize is minial.
	 */
	public List<SigResults> removeDuplicatedSigs(List<SigResults> results){
		Map<List<String>, SigResults> sig2SigResult = new HashMap<List<String>, SigResults>();
		SigResults emptySigResult = new SigResults(new HashSet<String>(), new ArrayList<String>(), Integer.MAX_VALUE);
		for (SigResults sigResult : results){
			SigResults tmpSigResult = sig2SigResult.getOrDefault(sigResult.sig, emptySigResult);
			if (sigResult.windowsize < tmpSigResult.windowsize){
				sig2SigResult.put(sigResult.sig, sigResult);
//				System.out.println(tmpSigResult.sig);
//				System.out.println(tmpSigResult.windowsize);
			}
		}
		List<SigResults> removedResults = new ArrayList<SigResults>(sig2SigResult.values());
		return removedResults;
		
	}
	
	/*
	 * remove sigs whose sig is the sub-sequence of the other one. for example, abc and axbxcxd, remove abc and keep
	 */
	public List<SigResults> removeSubseqSigs(List<SigResults> results){
		Set<SigResults> subseqResults = new HashSet<SigResults>(); // sigs which are subseq of other sigs
		
		for (int i = 0; i < results.size(); i++){
			SigResults first = results.get(i);
			if (subseqResults.contains(first))
				continue;
			
			for (int j = i + 1; j < results.size(); j++){				
				SigResults second = results.get(j);
				if (subseqResults.contains(second))
					continue;
				
				if (first.sig.size() <= second.sig.size()){
					if (isSubSequence(first.sig, second.sig)){
//						System.out.println("==================");
//						System.out.println(first.sig);
//						System.out.println(second.sig);
						subseqResults.add(first);
					}
				} else{
					if (isSubSequence(second.sig, first.sig)){
//						System.out.println("==================");
//						System.out.println(second.sig);
//						System.out.println(first.sig);
						subseqResults.add(second);
					}
				}				
			}					
		}
		
		List<SigResults> removedResults = new ArrayList<SigResults>(results);
		removedResults.removeAll(subseqResults);
		
		return removedResults;
	}
	
	/*
	 * check if str1 is subsequence of str2
	 */
	boolean isSubSequence(List<String> str1, List<String> str2)
    {
		int m = str1.size();
		int n = str2.size();
        int j = 0;
         
        // Traverse str2 and str1, and compare current character 
        // of str2 with first unmatched char of str1, if matched 
        // then move ahead in str1
        for (int i=0; i<n&&j<m; i++)
            if (str1.get(j).equals(str2.get(i)))
                j++;
 
        // If all characters of str1 were found in str2
        return (j==m); 
    }
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<String> one = Arrays.asList("a","b","c","a","b","c","a","b","c","a","b","c","d","e","f","f","g");
		List<String> two = Arrays.asList("a","x", "b", "ccc", "cc", "d");
		System.out.println(one);
		System.out.println(new Filter<String>().filterConsecutiveBlocks(one, 1, 500, 2));
	}
}
