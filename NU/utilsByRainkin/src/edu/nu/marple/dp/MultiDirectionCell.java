package edu.nu.marple.dp;

import java.util.List;

public class MultiDirectionCell {
   private List<MultiDirectionCell> prevCells;
   private int score;
   private int row;
   private int col;

   public MultiDirectionCell (int row, int col) {
      this.row = row;
      this.col = col;
   }
   
   public List<MultiDirectionCell> getPrevCells() {
      return prevCells;
   }

   public void setPrevCells(List<MultiDirectionCell> prevCells) {
      this.prevCells = prevCells;
   }

   public int getScore() {
      return score;
   }

   public void setScore(int score) {
      this.score = score;
   }

   public int getRow() {
      return row;
   }

   public void setRow(int row) {
      this.row = row;
   }

   public int getCol() {
      return col;
   }

   public void setCol(int col) {
      this.col = col;
   }

   @Override
   public String toString() {
      return "MultiDirectionCell [score=" + score + ", row=" + row + ", col="
            + col + "]";
   }
}
