package edu.nu.marple;

public class SyscallRecordGAllArgs extends EventRecordG {

	public String nonStringArgs;
	
	public SyscallRecordGAllArgs(String name, String stringArgs, String nonStringArgs) {
		super(name, stringArgs);
		this.nonStringArgs = nonStringArgs;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SyscallRecordGAllArgs other = (SyscallRecordGAllArgs) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;		
		if (parameter == null) {
			if (other.parameter != null)
				return false;
		} else if (!parameter.equals(other.parameter))
			return false;
		return true;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((nonStringArgs == null) ? 0 : nonStringArgs.hashCode());
		result = prime * result + ((parameter == null) ? 0 : parameter.hashCode());
		return result;
	}
}
