package edu.nu.marple;

import java.util.Set;

public class SetSignature {

	public Set<String> sig;
	public Set<String> traces;
	
	public SetSignature(Set<String> sig, Set<String> traces) {
		super();
		this.sig = sig;
		this.traces = traces;
	}

	public SetSignature() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((sig == null) ? 0 : sig.hashCode());
		result = prime * result + ((traces == null) ? 0 : traces.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SetSignature other = (SetSignature) obj;
		if (sig == null) {
			if (other.sig != null)
				return false;
		} else if (!sig.equals(other.sig))
			return false;
		if (traces == null) {
			if (other.traces != null)
				return false;
		} else if (!traces.equals(other.traces))
			return false;
		return true;
	}
 
	
	
}
