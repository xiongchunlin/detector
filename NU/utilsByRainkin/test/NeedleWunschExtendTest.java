import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import edu.nu.marple.seqalign.SmithWaterman;
import edu.nu.marple.GlobalConfig;
import edu.nu.marple.Utils;
import edu.nu.marple.seqalign.NeedlemanWunschExtend;
import edu.nu.marple.seqalign.SequenceAlignment.Weights;

public class NeedleWunschExtendTest {

	public static void main(String[] args) throws IOException {
	      List<String> sequence1 = Arrays.asList("GCCCTAGCGAA".split(""));
	      List<String> sequence2 = Arrays.asList("GCGCAATG".split(""));
	      
	      NeedlemanWunschExtend globalAlignExtend = new NeedlemanWunschExtend(sequence1, sequence2, 1,-1,-1);
	      System.out.println("====results====");
	      System.out.println(globalAlignExtend.getMatchedAlignment());
	      System.out.println("====ground truth results====");
	      System.out.println("[[G, C, C, A, G], [G, C, G, C, A, A]]");
	      System.out.println("===========highest score============");
	      System.out.println(globalAlignExtend.getMatchedAlignmentScores());
	      System.out.println("=========================");
	      globalAlignExtend.printDPScoretable();
	      System.out.println("=========================");
	      globalAlignExtend.printDPDirectionTable();
		
//      String trace1Path = GlobalConfig.getInstance().rootPath + "traces/Keylogger/DarkComet5.1@1_KeyLogger.json.extracted.filtered";
//      String trace2Path = GlobalConfig.getInstance().rootPath + "traces/Keylogger/njrat0.7@1_Keylogger.json.extracted.filtered";
//      List<String> sequence1 = Utils.file2List(trace1Path);
//      List<String> sequence2 = Utils.file2List(trace2Path);
//      NeedlemanWunschExtend globalAlignExtend = new NeedlemanWunschExtend(sequence1, sequence2, 4, -1, -1);
//      
//      globalAlignExtend.getMatchedAlignment().forEach(System.out::println);
      
//      globalAlignExtend.printDPScoretable();
		}
}
