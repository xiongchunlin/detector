import java.io.File;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import edu.nu.marple.Filter;
import edu.nu.marple.Utils;
import edu.nu.marple.lcssiggen.SyscallRecordG;

public class FilterTest {

	public static void main(String[] args) throws IOException {
		String s1 = "NtUserGetKeyState -> NtUserGetForegroundWindow -> NtUserQueryWindow -> NtQuerySystemInformation -> NtUserQueryWindow -> NtUserGetKeyState -> NtQueryInformationProcess -> NtUserGetKeyState -> NtUserGetForegroundWindow -> NtUserQueryWindow -> NtUserToUnicodeEx -> NtQuerySystemInformation -> NtUserGetKeyState -> NtUserGetForegroundWindow -> NtUserQueryWindow -> NtUserToUnicodeEx -> NtUserGetKeyState -> NtUserGetKeyState -> NtUserGetForegroundWindow -> NtUserQueryWindow -> NtUserToUnicodeEx -> NtQuerySystemInformation -> NtUserQueryWindow -> NtUserGetKeyState -> NtUserGetKeyState -> NtUserGetForegroundWindow -> NtUserQueryWindow -> NtUserToUnicodeEx -> NtQuerySystemInformation -> NtUserGetKeyState -> NtUserGetForegroundWindow -> NtUserQueryWindow -> NtQuerySystemInformation -> NtUserGetKeyState";
		List<String> records = Arrays.asList(s1.split(" -> "));
		List<SyscallRecordG> sequence = records.stream().map(record->new SyscallRecordG(record.split(" @ ")[0], record.split(" @ ").length>1?record.split(" @ ")[1]:"")).collect(Collectors.toList());
		
//      List<SyscallRecordG> sequence = new ArrayList<SyscallRecordG>();
//      SyscallRecordG a1 = new SyscallRecordG("A", "a1");
//      SyscallRecordG a2 = new SyscallRecordG("A", "a2");
//      SyscallRecordG b = new SyscallRecordG("B", "b");
//      SyscallRecordG bb = new SyscallRecordG("B", "b");
//      SyscallRecordG c = new SyscallRecordG("C", "c");
//      SyscallRecordG d = new SyscallRecordG("D", "d");
//      SyscallRecordG e = new SyscallRecordG("NtWaitForSingleObject", "nt");
//      SyscallRecordG g = new SyscallRecordG("NtWaitForSingleObject", "nt");
//      sequence.add(a1);
//      sequence.add(a1);
//      sequence.add(a2);
//      sequence.add(b);
//      sequence.add(b);
//      sequence.add(b);
//      sequence.add(c);
//      sequence.add(d);
		
		/* remove duplicated sequences */
      	List<SyscallRecordG> mergedSequence = null;
		
      	// remove repeated blocks by LRS
		Filter filter = new Filter();
//		mergedSequence = filter.filter(sequence, 1, 2);
//		System.out.println("================== remoev repeated blocks " + mergedSequence.size());
//		System.out.println(mergedSequence.stream().collect(Collectors.joining("\n")));

		// remvoe repeated and consecutive blocks 
		mergedSequence = filter.filterConsecutiveBlocks(sequence, 30);		
		System.out.println(mergedSequence.stream().map(record->record.name + (record.parameter.isEmpty()?"":" @ "+record.parameter)).collect(Collectors.joining(" -> ")));
		System.out.println("================== remoev repeated and consecutive blocks " + mergedSequence.size());
		mergedSequence.forEach(System.out::println);
		
		// filter useless syscalls
		mergedSequence = filter.filterUselessSyscalls(mergedSequence, "coresyscall/useless");
		System.out.println("================== filter useless syscalls " + mergedSequence.size());
		mergedSequence.forEach(System.out::println);

	}

}
