import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import edu.nu.marple.Utils;
import edu.nu.marple.lcssiggen.SyscallRecordG;
import edu.nu.marple.seqalign.SmithWaterman;
import edu.nu.marple.seqalign.SequenceAlignment.Weights;

public class SmithWatermanTest {

	public static void main(String[] args) throws IOException {
//      List<String> sequence1 = Arrays.asList("GCCCTAGCG".split(""));
//      List<String> sequence2 = Arrays.asList("GCGCAATG".split(""));
      
		String trace1Path = "utilsByRainkin/traces/AudioRecord/DarkComet5.1@1_AudioRecording.json.extracted.syscall_block500_uselessOld_filtered";
		String trace2Path = "utilsByRainkin/traces/AudioRecord/darkcometLegacy@1_AudioRecording.json.extracted.syscall_block500_uselessOld_filtered";
		List<String> sequence1 = Utils.file2List(trace1Path);
		List<String> sequence2 = Utils.file2List(trace2Path);		
		System.out.println("\nLocal Alignment");
		SmithWaterman localAlign = new SmithWaterman(sequence1, sequence2, 1,-1,-1, Weights.NULL);
		System.out.println(localAlign.getMatchedAlignment());
//		System.out.println(localAlign.getUnifiedDiffAlignment());
		localAlign.diff2Html("generatedSignature/diff.html");
	}

}
