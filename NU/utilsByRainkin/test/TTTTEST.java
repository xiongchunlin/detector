import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import edu.nu.marple.SigResults;

public class TTTTEST {
	public static void main(String[] args) throws JsonIOException, JsonSyntaxException, FileNotFoundException {
		List<SigResults> results = new Gson().fromJson(new FileReader(new File("generatedSignature/keylogger_localResults.txt")), new TypeToken<List<SigResults>>(){}.getType());
		for (SigResults result : results){
//			System.out.println(result.trace1Name + " === " + result.trace2Name);
			System.out.println(result.traces);
			System.out.println(result.sig);
//			System.out.println(result.indexs);
			System.out.println(result.dataflows);
			System.out.println(result.windowsize);
		}
	}
}
