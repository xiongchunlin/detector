[
  {
    "traces": [
      "spynet2.6-ThreadId2324",
      "jspy-ThreadId1348"
    ],
    "sig": [
      "SystemCall @ SystemCall:NtUserGetForegroundWindow",
      "SystemCall @ SystemCall:NtUserQueryWindow",
      "SystemCall @ SystemCall:NtUserGetAsyncKeyState"
    ],
    "windowsize": 6,
    "coverage": 0.3157894736842105,
    "falsePositiveShare": 0.4117647058823529
  },
  {
    "traces": [
      "DarkComet51",
      "NanoCore1220"
    ],
    "sig": [
      "SystemCall @ SystemCall:NtUserGetKeyboardState",
      "SystemCall @ SystemCall:NtUserMapVirtualKeyEx",
      "SystemCall @ SystemCall:NtUserToUnicodeEx",
      "SystemCall @ SystemCall:NtCallbackReturn"
    ],
    "windowsize": 14,
    "coverage": 0.2631578947368421,
    "falsePositiveShare": 0.17647058823529413
  },
  {
    "traces": [
      "turkojan4.0-ThreadId2324",
      "xrat2.0-ThreadId1240"
    ],
    "sig": [
      "SystemCall @ SystemCall:NtUserQueryWindow",
      "SystemCall @ SystemCall:NtUserGetKeyboardState",
      "SystemCall @ SystemCall:NtUserToUnicodeEx",
      "SystemCall @ SystemCall:NtCallbackReturn"
    ],
    "windowsize": 24,
    "coverage": 0.2631578947368421,
    "falsePositiveShare": 0.29411764705882354
  },
  {
    "traces": [
      "virus8.0-ThreadId2692",
      "DarkComet51"
    ],
    "sig": [
      "SystemCall @ SystemCall:NtUserGetKeyState",
      "SystemCall @ SystemCall:NtUserGetKeyboardState",
      "SystemCall @ SystemCall:NtUserMapVirtualKeyEx",
      "SystemCall @ SystemCall:NtUserGetForegroundWindow",
      "SystemCall @ SystemCall:NtUserQueryWindow"
    ],
    "windowsize": 12,
    "coverage": 0.2631578947368421,
    "falsePositiveShare": 0.0
  },
  {
    "traces": [
      "virus8.0-ThreadId2692",
      "imminentMonitor20"
    ],
    "sig": [
      "SystemCall @ SystemCall:NtAllocateVirtualMemory",
      "SystemCall @ SystemCall:NtFreeVirtualMemory",
      "SystemCall @ SystemCall:NtFlushInstructionCache",
      "SystemCall @ SystemCall:NtUserGetKeyState",
      "SystemCall @ SystemCall:NtFlushInstructionCache"
    ],
    "windowsize": 26,
    "coverage": 0.21052631578947367,
    "falsePositiveShare": 0.0
  },
  {
    "traces": [
      "pupy-ThreadId1900",
      "NanoCore1220"
    ],
    "sig": [
      "SystemCall @ SystemCall:NtCallbackReturn",
      "SystemCall @ SystemCall:NtAllocateVirtualMemory",
      "SystemCall @ SystemCall:NtUserGetKeyboardState",
      "SystemCall @ SystemCall:NtQueryInformationProcess",
      "SystemCall @ SystemCall:NtAllocateVirtualMemory"
    ],
    "windowsize": 21,
    "coverage": 0.10526315789473684,
    "falsePositiveShare": 0.0
  },
  {
    "traces": [
      "pupy-ThreadId1900",
      "DarkComet53"
    ],
    "sig": [
      "SystemCall @ SystemCall:NtUserGetKeyState",
      "SystemCall @ SystemCall:NtUserGetKeyboardState",
      "SystemCall @ SystemCall:NtUserToUnicodeEx",
      "SystemCall @ SystemCall:NtUserGetForegroundWindow",
      "SystemCall @ SystemCall:NtUserQueryWindow",
      "SystemCall @ SystemCall:NtCallbackReturn"
    ],
    "windowsize": 47,
    "coverage": 0.2631578947368421,
    "falsePositiveShare": 0.0
  },
  {
    "traces": [
      "DarkComet53",
      "xrat2.0-ThreadId1240"
    ],
    "sig": [
      "SystemCall @ SystemCall:NtUserGetKeyboardState",
      "SystemCall @ SystemCall:NtUserToUnicodeEx",
      "SystemCall @ SystemCall:NtCallbackReturn",
      "SystemCall @ SystemCall:NtUserGetForegroundWindow",
      "SystemCall @ SystemCall:NtUserQueryWindow",
      "SystemCall @ SystemCall:NtCallbackReturn"
    ],
    "windowsize": 18,
    "coverage": 0.21052631578947367,
    "falsePositiveShare": 0.0
  },
  {
    "traces": [
      "NanoCore1220",
      "xrat2.0-ThreadId1240"
    ],
    "sig": [
      "SystemCall @ SystemCall:NtUserGetKeyboardState",
      "SystemCall @ SystemCall:NtUserToUnicodeEx",
      "SystemCall @ SystemCall:NtFlushInstructionCache",
      "SystemCall @ SystemCall:NtUserGetKeyboardState",
      "SystemCall @ SystemCall:NtUserToUnicodeEx",
      "SystemCall @ SystemCall:NtCallbackReturn"
    ],
    "windowsize": 24,
    "coverage": 0.10526315789473684,
    "falsePositiveShare": 0.0
  },
  {
    "traces": [
      "virus8.0-ThreadId2692",
      "pupy-ThreadId1900"
    ],
    "sig": [
      "SystemCall @ SystemCall:NtUserQueryWindow",
      "SystemCall @ SystemCall:NtAllocateVirtualMemory",
      "SystemCall @ SystemCall:NtUserGetKeyState",
      "SystemCall @ SystemCall:NtUserGetKeyboardState",
      "SystemCall @ SystemCall:NtUserGetForegroundWindow",
      "SystemCall @ SystemCall:NtUserQueryWindow"
    ],
    "windowsize": 19,
    "coverage": 0.10526315789473684,
    "falsePositiveShare": 0.0
  },
  {
    "traces": [
      "jrat-ThreadId1364",
      "jspy-ThreadId1348"
    ],
    "sig": [
      "SystemCall @ SystemCall:NtUserGetDoubleClickTime",
      "SystemCall @ SystemCall:NtCallbackReturn",
      "SystemCall @ SystemCall:NtUserGetKeyState",
      "SystemCall @ SystemCall:NtUserGetAsyncKeyState",
      "SystemCall @ SystemCall:NtCallbackReturn",
      "SystemCall @ SystemCall:NtUserGetDoubleClickTime",
      "SystemCall @ SystemCall:NtCallbackReturn"
    ],
    "windowsize": 17,
    "coverage": 0.10526315789473684,
    "falsePositiveShare": 0.11764705882352941
  },
  {
    "traces": [
      "pupy-ThreadId1900",
      "turkojan4.0-ThreadId2324"
    ],
    "sig": [
      "SystemCall @ SystemCall:NtUserQueryWindow",
      "SystemCall @ SystemCall:NtUserOpenClipboard",
      "SystemCall @ SystemCall:NtUserGetClipboardData",
      "SystemCall @ SystemCall:NtUserCreateLocalMemHandle",
      "SystemCall @ SystemCall:NtUserCloseClipboard",
      "SystemCall @ SystemCall:NtCallbackReturn",
      "SystemCall @ SystemCall:NtUserQueryWindow"
    ],
    "windowsize": 36,
    "coverage": 0.21052631578947367,
    "falsePositiveShare": 0.23529411764705882
  },
  {
    "traces": [
      "pupy-ThreadId1900",
      "xrat2.0-ThreadId1240"
    ],
    "sig": [
      "SystemCall @ SystemCall:NtUserGetKeyState",
      "SystemCall @ SystemCall:NtUserGetForegroundWindow",
      "SystemCall @ SystemCall:NtUserQueryWindow",
      "SystemCall @ SystemCall:NtCallbackReturn",
      "SystemCall @ SystemCall:NtUserGetKeyState",
      "SystemCall @ SystemCall:NtUserGetForegroundWindow",
      "SystemCall @ SystemCall:NtUserQueryWindow"
    ],
    "windowsize": 27,
    "coverage": 0.21052631578947367,
    "falsePositiveShare": 0.11764705882352941
  },
  {
    "traces": [
      "pupy-ThreadId1900",
      "xrat2.0-ThreadId1240"
    ],
    "sig": [
      "SystemCall @ SystemCall:NtUserGetKeyState",
      "SystemCall @ SystemCall:NtUserGetForegroundWindow",
      "SystemCall @ SystemCall:NtUserQueryWindow",
      "SystemCall @ SystemCall:NtCallbackReturn",
      "SystemCall @ SystemCall:NtUserGetKeyState",
      "SystemCall @ SystemCall:NtUserGetKeyboardState",
      "SystemCall @ SystemCall:NtUserToUnicodeEx"
    ],
    "windowsize": 27,
    "coverage": 0.2631578947368421,
    "falsePositiveShare": 0.0
  },
  {
    "traces": [
      "virus8.0-ThreadId2692",
      "xrat2.0-ThreadId1240"
    ],
    "sig": [
      "SystemCall @ SystemCall:NtFlushInstructionCache",
      "SystemCall @ SystemCall:NtUserQueryWindow",
      "SystemCall @ SystemCall:NtUserGetKeyState",
      "SystemCall @ SystemCall:NtFlushInstructionCache",
      "SystemCall @ SystemCall:NtUserGetForegroundWindow",
      "SystemCall @ SystemCall:NtUserQueryWindow",
      "SystemCall @ SystemCall:NtUserToUnicodeEx"
    ],
    "windowsize": 21,
    "coverage": 0.10526315789473684,
    "falsePositiveShare": 0.0
  },
  {
    "traces": [
      "virus8.0-ThreadId2692",
      "NanoCore1220"
    ],
    "sig": [
      "SystemCall @ SystemCall:NtAllocateVirtualMemory",
      "SystemCall @ SystemCall:NtFreeVirtualMemory",
      "SystemCall @ SystemCall:NtFlushInstructionCache",
      "SystemCall @ SystemCall:NtUserGetKeyboardState",
      "SystemCall @ SystemCall:NtFlushInstructionCache",
      "SystemCall @ SystemCall:NtUserMapVirtualKeyEx",
      "SystemCall @ SystemCall:NtFlushInstructionCache",
      "SystemCall @ SystemCall:NtUserToUnicodeEx"
    ],
    "windowsize": 20,
    "coverage": 0.10526315789473684,
    "falsePositiveShare": 0.0
  },
  {
    "traces": [
      "NanoCore1220",
      "turkojan4.0-ThreadId2324"
    ],
    "sig": [
      "DiskIoDrvMjFnCall @ MajorFunction:5",
      "DiskIoDrvMjFnCall @ MajorFunction:0",
      "DiskIoDrvMjFnCall @ MajorFunction:18",
      "DiskIoDrvMjFnCall @ MajorFunction:2",
      "DiskIoDrvMjFnCall @ MajorFunction:4",
      "DiskIoDrvMjFnCall @ MajorFunction:0",
      "DiskIoDrvMjFnCall @ MajorFunction:18",
      "DiskIoDrvMjFnCall @ MajorFunction:2",
      "SystemCall @ SystemCall:NtUserGetKeyboardState",
      "SystemCall @ SystemCall:NtUserMapVirtualKeyEx",
      "SystemCall @ SystemCall:NtUserToUnicodeEx",
      "FileIoQueryInfo @ FileName:@DataFile"
    ],
    "windowsize": 81,
    "coverage": 0.10526315789473684,
    "falsePositiveShare": 0.11764705882352941
  },
  {
    "traces": [
      "NanoCore1220",
      "turkojan4.0-ThreadId2324"
    ],
    "sig": [
      "DiskIoDrvMjFnCall @ MajorFunction:5",
      "DiskIoDrvMjFnCall @ MajorFunction:18",
      "DiskIoDrvMjFnCall @ MajorFunction:2",
      "DiskIoDrvMjFnCall @ MajorFunction:0",
      "DiskIoDrvMjFnCall @ MajorFunction:18",
      "DiskIoDrvMjFnCall @ MajorFunction:2",
      "DiskIoDrvMjFnCall @ MajorFunction:4",
      "DiskIoDrvMjFnCall @ MajorFunction:0",
      "DiskIoDrvMjFnCall @ MajorFunction:18",
      "DiskIoDrvMjFnCall @ MajorFunction:2",
      "SystemCall @ SystemCall:NtUserGetKeyboardState",
      "SystemCall @ SystemCall:NtUserMapVirtualKeyEx",
      "SystemCall @ SystemCall:NtUserToUnicodeEx",
      "FileIoQueryInfo @ FileName:@DataFile"
    ],
    "windowsize": 75,
    "coverage": 0.10526315789473684,
    "falsePositiveShare": 0.11764705882352941
  }
]