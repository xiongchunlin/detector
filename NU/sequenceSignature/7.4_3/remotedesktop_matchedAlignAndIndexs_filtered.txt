[
  {
    "traces": [
      "bluebanana_TheadId2340",
      "darkcomet_TheadId1428"
    ],
    "sig": [
      "SystemCall @ SystemCall:NtGdiOpenDCW",
      "SystemCall @ SystemCall:NtGdiCreateCompatibleDC",
      "SystemCall @ SystemCall:NtGdiCreateCompatibleBitmap",
      "SystemCall @ SystemCall:NtGdiBitBlt",
      "ALPC_SEN @ processname:@ExecutableFile",
      "ALPC_REC @ processname:@ExecutableFile",
      "SystemCall @ SystemCall:NtGdiGetDIBitsInternal",
      "SystemCall @ SystemCall:NtGdiDeleteObjectApp"
    ],
    "windowsize": 18
  },
  {
    "traces": [
      "bluebanana_TheadId2340",
      "pandora_TheadId1488"
    ],
    "sig": [
      "SystemCall @ SystemCall:NtGdiCreateCompatibleDC",
      "ALPC_SEN @ processname:@ExecutableFile",
      "ALPC_REC @ processname:@ExecutableFile",
      "SystemCall @ SystemCall:NtGdiDeleteObjectApp"
    ],
    "windowsize": 10
  },
  {
    "traces": [
      "bluebanana_TheadId2340",
      "proton_TheadId3528"
    ],
    "sig": [
      "SystemCall @ SystemCall:NtOpenSection",
      "SystemCall @ SystemCall:NtQueryAttributesFile",
      "SystemCall @ SystemCall:NtOpenFile",
      "SystemCall @ SystemCall:NtCreateSection",
      "SystemCall @ SystemCall:NtQuerySection",
      "SystemCall @ SystemCall:NtProtectVirtualMemory",
      "SystemCall @ SystemCall:NtQueryKey",
      "RegistryQueryKey @ keyname:3\u00262b8e0b4b\u00260\u002678"
    ],
    "windowsize": 19
  },
  {
    "traces": [
      "bluebanana_TheadId2340",
      "Quasar_TheadId4024"
    ],
    "sig": [
      "SystemCall @ SystemCall:NtQueryObject",
      "SystemCall @ SystemCall:NtOpenSection",
      "SystemCall @ SystemCall:NtQueryAttributesFile",
      "SystemCall @ SystemCall:NtOpenFile",
      "SystemCall @ SystemCall:NtCreateSection",
      "SystemCall @ SystemCall:NtQuerySection",
      "SystemCall @ SystemCall:NtProtectVirtualMemory",
      "SystemCall @ SystemCall:NtQueryKey",
      "SystemCall @ SystemCall:NtQueryObject"
    ],
    "windowsize": 36
  },
  {
    "traces": [
      "bluebanana_TheadId2340",
      "Quasar_TheadId592"
    ],
    "sig": [
      "SystemCall @ SystemCall:NtGdiOpenDCW",
      "SystemCall @ SystemCall:NtGdiBitBlt",
      "ALPC_SEN @ processname:@ExecutableFile",
      "ALPC_REC @ processname:@ExecutableFile",
      "SystemCall @ SystemCall:NtGdiDeleteObjectApp"
    ],
    "windowsize": 12
  },
  {
    "traces": [
      "bluebanana_TheadId2340",
      "spynet_TheadId2036"
    ],
    "sig": [
      "SystemCall @ SystemCall:NtGdiCreateCompatibleDC",
      "ALPC_SEN @ processname:@ExecutableFile",
      "ALPC_REC @ processname:@ExecutableFile",
      "SystemCall @ SystemCall:NtGdiDeleteObjectApp"
    ],
    "windowsize": 10
  },
  {
    "traces": [
      "pandora_TheadId1488",
      "darkcomet_TheadId1428"
    ],
    "sig": [
      "SystemCall @ SystemCall:NtGdiCreateCompatibleDC",
      "ALPC_SEN @ processname:@ExecutableFile",
      "ALPC_REC @ processname:@ExecutableFile",
      "SystemCall @ SystemCall:NtGdiCreateCompatibleDC",
      "SystemCall @ SystemCall:NtUserGetDC"
    ],
    "windowsize": 15
  },
  {
    "traces": [
      "darkcomet_TheadId1428",
      "proton_TheadId3528"
    ],
    "sig": [
      "SystemCall @ SystemCall:NtGdiCreateCompatibleDC",
      "SystemCall @ SystemCall:NtGdiBitBlt",
      "ALPC_SEN @ processname:@ExecutableFile",
      "ALPC_REC @ processname:@ExecutableFile",
      "SystemCall @ SystemCall:NtUserGetDC",
      "SystemCall @ SystemCall:NtGdiGetDeviceCaps"
    ],
    "windowsize": 17
  },
  {
    "traces": [
      "darkcomet_TheadId1428",
      "proton_TheadId3528"
    ],
    "sig": [
      "SystemCall @ SystemCall:NtGdiOpenDCW",
      "SystemCall @ SystemCall:NtGdiCreateCompatibleDC",
      "SystemCall @ SystemCall:NtGdiCreateCompatibleBitmap",
      "ALPC_REC @ processname:@ExecutableFile",
      "SystemCall @ SystemCall:NtUserGetDC",
      "SystemCall @ SystemCall:NtGdiGetDeviceCaps"
    ],
    "windowsize": 17
  },
  {
    "traces": [
      "Quasar_TheadId4024",
      "darkcomet_TheadId1428"
    ],
    "sig": [
      "SystemCall @ SystemCall:NtGdiGetDeviceCaps",
      "SystemCall @ SystemCall:NtGdiOpenDCW",
      "SystemCall @ SystemCall:NtGdiBitBlt",
      "ALPC_SEN @ processname:@ExecutableFile",
      "ALPC_REC @ processname:@ExecutableFile",
      "SystemCall @ SystemCall:NtUserGetDC",
      "SystemCall @ SystemCall:NtGdiGetDeviceCaps",
      "SystemCall @ SystemCall:NtGdiDeleteObjectApp"
    ],
    "windowsize": 22
  },
  {
    "traces": [
      "Quasar_TheadId592",
      "darkcomet_TheadId1428"
    ],
    "sig": [
      "SystemCall @ SystemCall:NtGdiGetDeviceCaps",
      "SystemCall @ SystemCall:NtProtectVirtualMemory",
      "ALPC_REC @ processname:@ExecutableFile",
      "SystemCall @ SystemCall:NtGdiCreateCompatibleDC",
      "SystemCall @ SystemCall:NtGdiBitBlt",
      "ALPC_SEN @ processname:@ExecutableFile",
      "ALPC_REC @ processname:@ExecutableFile",
      "SystemCall @ SystemCall:NtUserGetDC",
      "SystemCall @ SystemCall:NtGdiGetDeviceCaps",
      "SystemCall @ SystemCall:NtGdiDeleteObjectApp"
    ],
    "windowsize": 28
  },
  {
    "traces": [
      "spynet_TheadId2036",
      "darkcomet_TheadId1428"
    ],
    "sig": [
      "ThreadStart",
      "SystemCall @ SystemCall:NtSetInformationThread",
      "SystemCall @ SystemCall:NtCreateFile",
      "SystemCall @ SystemCall:NtGdiGetDeviceCaps",
      "ALPC_REC @ processname:@ExecutableFile",
      "SystemCall @ SystemCall:NtGdiCreateCompatibleDC",
      "ALPC_SEN @ processname:@ExecutableFile",
      "ALPC_REC @ processname:@ExecutableFile",
      "SystemCall @ SystemCall:NtGdiCreateCompatibleDC",
      "SystemCall @ SystemCall:NtUserGetDC",
      "SystemCall @ SystemCall:NtGdiGetDeviceCaps"
    ],
    "windowsize": 39
  },
  {
    "traces": [
      "pandora_TheadId1488",
      "proton_TheadId3528"
    ],
    "sig": [
      "SystemCall @ SystemCall:NtGdiDeleteObjectApp",
      "SystemCall @ SystemCall:NtUserGetDC",
      "SystemCall @ SystemCall:NtGdiGetDeviceCaps",
      "SystemCall @ SystemCall:NtGdiCreateCompatibleDC",
      "SystemCall @ SystemCall:NtGdiCreateDIBSection",
      "ALPC_SEN @ processname:@ExecutableFile",
      "ALPC_REC @ processname:@ExecutableFile",
      "SystemCall @ SystemCall:NtGdiDeleteObjectApp",
      "SystemCall @ SystemCall:NtUserGetDC",
      "SystemCall @ SystemCall:NtGdiCreateCompatibleDC",
      "SystemCall @ SystemCall:NtGdiCreateDIBSection",
      "SystemCall @ SystemCall:NtGdiExtGetObjectW",
      "SystemCall @ SystemCall:NtGdiFlush",
      "SystemCall @ SystemCall:NtGdiDeleteObjectApp"
    ],
    "windowsize": 44
  },
  {
    "traces": [
      "pandora_TheadId1488",
      "Quasar_TheadId4024"
    ],
    "sig": [
      "SystemCall @ SystemCall:NtUserGetDC",
      "SystemCall @ SystemCall:NtGdiGetDeviceCaps",
      "SystemCall @ SystemCall:NtGdiCreateCompatibleDC",
      "SystemCall @ SystemCall:NtGdiCreateDIBSection",
      "ALPC_SEN @ processname:@ExecutableFile",
      "ALPC_REC @ processname:@ExecutableFile",
      "SystemCall @ SystemCall:NtGdiDeleteObjectApp",
      "SystemCall @ SystemCall:NtUserGetDC",
      "SystemCall @ SystemCall:NtGdiCreateCompatibleDC",
      "SystemCall @ SystemCall:NtGdiCreateDIBSection",
      "SystemCall @ SystemCall:NtGdiExtGetObjectW",
      "SystemCall @ SystemCall:NtGdiDeleteObjectApp",
      "SystemCall @ SystemCall:NtGdiFlush",
      "SystemCall @ SystemCall:NtGdiDeleteObjectApp"
    ],
    "windowsize": 46
  },
  {
    "traces": [
      "pandora_TheadId1488",
      "Quasar_TheadId592"
    ],
    "sig": [
      "SystemCall @ SystemCall:NtUserGetDC",
      "SystemCall @ SystemCall:NtGdiGetDeviceCaps",
      "SystemCall @ SystemCall:NtGdiCreateCompatibleDC",
      "SystemCall @ SystemCall:NtGdiCreateDIBSection",
      "ALPC_SEN @ processname:@ExecutableFile",
      "ALPC_REC @ processname:@ExecutableFile",
      "SystemCall @ SystemCall:NtGdiDeleteObjectApp",
      "SystemCall @ SystemCall:NtUserGetDC",
      "SystemCall @ SystemCall:NtGdiCreateCompatibleDC",
      "SystemCall @ SystemCall:NtGdiCreateDIBSection",
      "SystemCall @ SystemCall:NtGdiExtGetObjectW",
      "SystemCall @ SystemCall:NtGdiDeleteObjectApp",
      "SystemCall @ SystemCall:NtGdiFlush",
      "SystemCall @ SystemCall:NtGdiDeleteObjectApp"
    ],
    "windowsize": 50
  },
  {
    "traces": [
      "pandora_TheadId1488",
      "Quasar_TheadId592"
    ],
    "sig": [
      "SystemCall @ SystemCall:NtUserGetDC",
      "SystemCall @ SystemCall:NtGdiGetDeviceCaps",
      "SystemCall @ SystemCall:NtGdiCreateCompatibleDC",
      "SystemCall @ SystemCall:NtGdiCreateDIBSection",
      "ALPC_SEN @ processname:@ExecutableFile",
      "ALPC_REC @ processname:@ExecutableFile",
      "SystemCall @ SystemCall:NtGdiDeleteObjectApp",
      "SystemCall @ SystemCall:NtUserGetDC",
      "SystemCall @ SystemCall:NtGdiCreateCompatibleDC",
      "SystemCall @ SystemCall:NtGdiCreateDIBSection",
      "SystemCall @ SystemCall:NtGdiExtGetObjectW",
      "SystemCall @ SystemCall:NtGdiDeleteObjectApp",
      "SystemCall @ SystemCall:NtGdiFlush",
      "SystemCall @ SystemCall:NtGdiDeleteObjectApp"
    ],
    "windowsize": 50
  },
  {
    "traces": [
      "pandora_TheadId1488",
      "spynet_TheadId2036"
    ],
    "sig": [
      "SystemCall @ SystemCall:NtUserGetDC",
      "SystemCall @ SystemCall:NtGdiCreateCompatibleDC",
      "SystemCall @ SystemCall:NtGdiCreateDIBSection",
      "SystemCall @ SystemCall:NtGdiDeleteObjectApp",
      "SystemCall @ SystemCall:NtGdiExtGetObjectW",
      "SystemCall @ SystemCall:NtGdiDeleteObjectApp",
      "SystemCall @ SystemCall:NtUserGetDC",
      "SystemCall @ SystemCall:NtGdiGetDeviceCaps",
      "SystemCall @ SystemCall:NtGdiCreateCompatibleDC",
      "SystemCall @ SystemCall:NtGdiCreateDIBSection",
      "SystemCall @ SystemCall:NtGdiStretchBlt",
      "ALPC_SEN @ processname:@ExecutableFile",
      "ALPC_REC @ processname:@ExecutableFile",
      "SystemCall @ SystemCall:NtGdiDeleteObjectApp",
      "SystemCall @ SystemCall:NtGdiCreateCompatibleDC",
      "SystemCall @ SystemCall:NtGdiDeleteObjectApp",
      "SystemCall @ SystemCall:NtGdiExtGetObjectW",
      "SystemCall @ SystemCall:NtUserGetDC",
      "SystemCall @ SystemCall:NtGdiCreateCompatibleDC",
      "SystemCall @ SystemCall:NtGdiCreateDIBSection"
    ],
    "windowsize": 49
  },
  {
    "traces": [
      "Quasar_TheadId4024",
      "proton_TheadId3528"
    ],
    "sig": [
      "SystemCall @ SystemCall:NtQuerySystemInformation",
      "SystemCall @ SystemCall:NtFlushInstructionCache",
      "RegistryOpenKey @ keyname:assemblystorageroots",
      "SystemCall @ SystemCall:NtQueryAttributesFile",
      "SystemCall @ SystemCall:NtOpenFile",
      "SystemCall @ SystemCall:NtQueryAttributesFile",
      "FileIoCreateFile @ openpath:gdiplus.dll",
      "SystemCall @ SystemCall:NtOpenFile",
      "FileIoCreateFile @ openpath:gdiplus.dll",
      "SystemCall @ SystemCall:NtCreateSection",
      "ImageLoad @ filename:gdiplus.dll",
      "SystemCall @ SystemCall:NtQuerySection",
      "SystemCall @ SystemCall:NtProtectVirtualMemory",
      "SystemCall @ SystemCall:NtFlushInstructionCache",
      "SystemCall @ SystemCall:NtQuerySystemInformation",
      "SystemCall @ SystemCall:NtGdiOpenDCW",
      "SystemCall @ SystemCall:NtGdiGetDeviceCaps",
      "SystemCall @ SystemCall:NtGdiCreateCompatibleDC",
      "SystemCall @ SystemCall:NtGdiGetDeviceCaps",
      "SystemCall @ SystemCall:NtOpenKeyEx",
      "RegistryOpenKey @ keyname:video",
      "SystemCall @ SystemCall:NtQueryValueKey",
      "RegistryQueryValue @ keyname:knownclasses",
      "SystemCall @ SystemCall:NtOpenKeyEx",
      "RegistryOpenKey @ keyname:0000",
      "SystemCall @ SystemCall:NtQueryValueKey",
      "RegistryQueryValue @ keyname:knownclasses",
      "SystemCall @ SystemCall:NtOpenKeyEx",
      "RegistryOpenKey @ keyname:3\u00262b8e0b4b\u00260\u002678",
      "SystemCall @ SystemCall:NtQueryValueKey",
      "RegistryQueryValue @ keyname:3\u00262b8e0b4b\u00260\u002678",
      "SystemCall @ SystemCall:NtOpenKeyEx",
      "RegistryOpenKey @ keyname:video",
      "SystemCall @ SystemCall:NtQueryValueKey",
      "RegistryQueryValue @ keyname:3\u00262b8e0b4b\u00260\u002678",
      "SystemCall @ SystemCall:NtGdiOpenDCW",
      "SystemCall @ SystemCall:NtGdiCreateCompatibleDC",
      "SystemCall @ SystemCall:NtGdiCreateCompatibleBitmap",
      "SystemCall @ SystemCall:NtGdiGetDIBitsInternal",
      "SystemCall @ SystemCall:NtGdiDeleteObjectApp",
      "SystemCall @ SystemCall:NtProtectVirtualMemory",
      "ALPC_REC @ processname:@ExecutableFile",
      "SystemCall @ SystemCall:NtFlushInstructionCache",
      "FileIoRead @ openpath:gdiplus.dll",
      "SystemCall @ SystemCall:NtUserGetDC",
      "SystemCall @ SystemCall:NtGdiGetDeviceCaps",
      "SystemCall @ SystemCall:NtFlushInstructionCache",
      "FileIoRead @ openpath:gdiplus.dll",
      "SystemCall @ SystemCall:NtFlushInstructionCache",
      "SystemCall @ SystemCall:NtGdiCreateCompatibleDC",
      "SystemCall @ SystemCall:NtGdiCreateDIBSection",
      "SystemCall @ SystemCall:NtGdiExtGetObjectW",
      "SystemCall @ SystemCall:NtFlushInstructionCache",
      "SystemCall @ SystemCall:NtGdiBitBlt",
      "ALPC_SEN @ processname:@ExecutableFile",
      "ALPC_REC @ processname:@ExecutableFile",
      "SystemCall @ SystemCall:NtFlushInstructionCache",
      "SystemCall @ SystemCall:NtGdiFlush",
      "SystemCall @ SystemCall:NtFlushInstructionCache",
      "FileIoRead @ openpath:gdiplus.dll",
      "SystemCall @ SystemCall:NtOpenSection",
      "SystemCall @ SystemCall:NtQueryAttributesFile",
      "FileIoCreateFile @ openpath:windowscodecs.dll",
      "SystemCall @ SystemCall:NtOpenFile",
      "FileIoCreateFile @ openpath:windowscodecs.dll",
      "SystemCall @ SystemCall:NtCreateSection",
      "ImageLoad @ filename:windowscodecs.dll",
      "SystemCall @ SystemCall:NtQuerySection",
      "SystemCall @ SystemCall:NtProtectVirtualMemory",
      "SystemCall @ SystemCall:NtQuerySystemInformation",
      "SystemCall @ SystemCall:NtQueryKey",
      "RegistryOpenKey @ keyname:instance",
      "SystemCall @ SystemCall:NtQueryKey",
      "RegistryOpenKey @ keyname:disabled",
      "SystemCall @ SystemCall:NtQuerySystemInformation",
      "SystemCall @ SystemCall:NtFlushInstructionCache",
      "SystemCall @ SystemCall:NtGdiDeleteObjectApp",
      "SystemCall @ SystemCall:NtFlushInstructionCache",
      "SystemCall @ SystemCall:NtUserGetDC",
      "SystemCall @ SystemCall:NtGdiGetDeviceCaps",
      "SystemCall @ SystemCall:NtGdiCreateCompatibleDC",
      "SystemCall @ SystemCall:NtGdiCreateDIBSection",
      "SystemCall @ SystemCall:NtGdiExtGetObjectW",
      "SystemCall @ SystemCall:NtGdiBitBlt",
      "ALPC_SEN @ processname:@ExecutableFile",
      "ALPC_REC @ processname:@ExecutableFile",
      "SystemCall @ SystemCall:NtGdiFlush",
      "SystemCall @ SystemCall:NtGdiDeleteObjectApp"
    ],
    "windowsize": 272
  },
  {
    "traces": [
      "Quasar_TheadId4024",
      "proton_TheadId3528"
    ],
    "sig": [
      "SystemCall @ SystemCall:NtQuerySystemInformation",
      "SystemCall @ SystemCall:NtFlushInstructionCache",
      "RegistryOpenKey @ keyname:assemblystorageroots",
      "SystemCall @ SystemCall:NtQueryAttributesFile",
      "SystemCall @ SystemCall:NtOpenFile",
      "SystemCall @ SystemCall:NtQueryAttributesFile",
      "FileIoCreateFile @ openpath:gdiplus.dll",
      "SystemCall @ SystemCall:NtOpenFile",
      "FileIoCreateFile @ openpath:gdiplus.dll",
      "SystemCall @ SystemCall:NtCreateSection",
      "ImageLoad @ filename:gdiplus.dll",
      "SystemCall @ SystemCall:NtQuerySection",
      "SystemCall @ SystemCall:NtProtectVirtualMemory",
      "SystemCall @ SystemCall:NtFlushInstructionCache",
      "SystemCall @ SystemCall:NtQuerySystemInformation",
      "SystemCall @ SystemCall:NtGdiOpenDCW",
      "SystemCall @ SystemCall:NtGdiGetDeviceCaps",
      "SystemCall @ SystemCall:NtGdiCreateCompatibleDC",
      "SystemCall @ SystemCall:NtGdiGetDeviceCaps",
      "SystemCall @ SystemCall:NtOpenKeyEx",
      "RegistryOpenKey @ keyname:video",
      "SystemCall @ SystemCall:NtQueryValueKey",
      "RegistryQueryValue @ keyname:knownclasses",
      "SystemCall @ SystemCall:NtOpenKeyEx",
      "RegistryOpenKey @ keyname:0000",
      "SystemCall @ SystemCall:NtQueryValueKey",
      "RegistryQueryValue @ keyname:knownclasses",
      "SystemCall @ SystemCall:NtOpenKeyEx",
      "RegistryOpenKey @ keyname:3\u00262b8e0b4b\u00260\u002678",
      "SystemCall @ SystemCall:NtQueryValueKey",
      "RegistryQueryValue @ keyname:3\u00262b8e0b4b\u00260\u002678",
      "SystemCall @ SystemCall:NtOpenKeyEx",
      "RegistryOpenKey @ keyname:video",
      "SystemCall @ SystemCall:NtQueryValueKey",
      "RegistryQueryValue @ keyname:3\u00262b8e0b4b\u00260\u002678",
      "SystemCall @ SystemCall:NtGdiOpenDCW",
      "SystemCall @ SystemCall:NtGdiCreateCompatibleDC",
      "SystemCall @ SystemCall:NtGdiCreateCompatibleBitmap",
      "SystemCall @ SystemCall:NtGdiGetDIBitsInternal",
      "SystemCall @ SystemCall:NtGdiDeleteObjectApp",
      "SystemCall @ SystemCall:NtProtectVirtualMemory",
      "ALPC_REC @ processname:@ExecutableFile",
      "SystemCall @ SystemCall:NtFlushInstructionCache",
      "FileIoRead @ openpath:gdiplus.dll",
      "SystemCall @ SystemCall:NtUserGetDC",
      "SystemCall @ SystemCall:NtGdiGetDeviceCaps",
      "SystemCall @ SystemCall:NtFlushInstructionCache",
      "FileIoRead @ openpath:gdiplus.dll",
      "SystemCall @ SystemCall:NtFlushInstructionCache",
      "SystemCall @ SystemCall:NtGdiCreateCompatibleDC",
      "SystemCall @ SystemCall:NtGdiCreateDIBSection",
      "SystemCall @ SystemCall:NtGdiExtGetObjectW",
      "SystemCall @ SystemCall:NtFlushInstructionCache",
      "SystemCall @ SystemCall:NtGdiBitBlt",
      "ALPC_SEN @ processname:@ExecutableFile",
      "ALPC_REC @ processname:@ExecutableFile",
      "SystemCall @ SystemCall:NtFlushInstructionCache",
      "SystemCall @ SystemCall:NtGdiFlush",
      "SystemCall @ SystemCall:NtFlushInstructionCache",
      "FileIoRead @ openpath:gdiplus.dll",
      "SystemCall @ SystemCall:NtOpenSection",
      "SystemCall @ SystemCall:NtQueryAttributesFile",
      "FileIoCreateFile @ openpath:windowscodecs.dll",
      "SystemCall @ SystemCall:NtOpenFile",
      "FileIoCreateFile @ openpath:windowscodecs.dll",
      "SystemCall @ SystemCall:NtCreateSection",
      "ImageLoad @ filename:windowscodecs.dll",
      "SystemCall @ SystemCall:NtQuerySection",
      "SystemCall @ SystemCall:NtProtectVirtualMemory",
      "SystemCall @ SystemCall:NtQuerySystemInformation",
      "SystemCall @ SystemCall:NtQueryKey",
      "RegistryOpenKey @ keyname:instance",
      "SystemCall @ SystemCall:NtQueryKey",
      "RegistryOpenKey @ keyname:disabled",
      "SystemCall @ SystemCall:NtQuerySystemInformation",
      "SystemCall @ SystemCall:NtFlushInstructionCache",
      "SystemCall @ SystemCall:NtGdiDeleteObjectApp",
      "SystemCall @ SystemCall:NtFlushInstructionCache",
      "SystemCall @ SystemCall:NtUserGetDC",
      "SystemCall @ SystemCall:NtGdiGetDeviceCaps",
      "SystemCall @ SystemCall:NtGdiCreateCompatibleDC",
      "SystemCall @ SystemCall:NtGdiCreateDIBSection",
      "SystemCall @ SystemCall:NtGdiExtGetObjectW",
      "SystemCall @ SystemCall:NtGdiBitBlt",
      "ALPC_SEN @ processname:@ExecutableFile",
      "ALPC_REC @ processname:@ExecutableFile",
      "SystemCall @ SystemCall:NtGdiDeleteObjectApp",
      "SystemCall @ SystemCall:NtUserGetDC",
      "SystemCall @ SystemCall:NtGdiGetDeviceCaps",
      "SystemCall @ SystemCall:NtUserGetDC",
      "SystemCall @ SystemCall:NtGdiCreateCompatibleDC",
      "SystemCall @ SystemCall:NtGdiCreateDIBSection",
      "SystemCall @ SystemCall:NtGdiExtGetObjectW",
      "SystemCall @ SystemCall:NtGdiBitBlt",
      "ALPC_SEN @ processname:@ExecutableFile",
      "ALPC_REC @ processname:@ExecutableFile",
      "SystemCall @ SystemCall:NtGdiFlush",
      "SystemCall @ SystemCall:NtGdiDeleteObjectApp"
    ],
    "windowsize": 272
  },
  {
    "traces": [
      "Quasar_TheadId592",
      "proton_TheadId3528"
    ],
    "sig": [
      "SystemCall @ SystemCall:NtUserGetDC",
      "SystemCall @ SystemCall:NtGdiGetDeviceCaps",
      "SystemCall @ SystemCall:NtGdiCreateCompatibleDC",
      "SystemCall @ SystemCall:NtGdiCreateDIBSection",
      "SystemCall @ SystemCall:NtGdiExtGetObjectW",
      "SystemCall @ SystemCall:NtGdiBitBlt",
      "ALPC_SEN @ processname:@ExecutableFile",
      "ALPC_REC @ processname:@ExecutableFile",
      "SystemCall @ SystemCall:NtGdiFlush",
      "SystemCall @ SystemCall:NtFlushInstructionCache",
      "SystemCall @ SystemCall:NtUserGetDC",
      "SystemCall @ SystemCall:NtGdiGetDeviceCaps",
      "SystemCall @ SystemCall:NtUserGetDC",
      "SystemCall @ SystemCall:NtGdiCreateCompatibleDC",
      "SystemCall @ SystemCall:NtGdiCreateDIBSection",
      "SystemCall @ SystemCall:NtGdiExtGetObjectW",
      "SystemCall @ SystemCall:NtGdiBitBlt",
      "ALPC_SEN @ processname:@ExecutableFile",
      "ALPC_REC @ processname:@ExecutableFile",
      "SystemCall @ SystemCall:NtGdiFlush",
      "SystemCall @ SystemCall:NtGdiDeleteObjectApp"
    ],
    "windowsize": 55
  },
  {
    "traces": [
      "spynet_TheadId2036",
      "proton_TheadId3528"
    ],
    "sig": [
      "SystemCall @ SystemCall:NtUserGetDC",
      "SystemCall @ SystemCall:NtGdiGetDeviceCaps",
      "SystemCall @ SystemCall:NtGdiCreateCompatibleDC",
      "SystemCall @ SystemCall:NtGdiCreateDIBSection",
      "ALPC_SEN @ processname:@ExecutableFile",
      "ALPC_REC @ processname:@ExecutableFile",
      "SystemCall @ SystemCall:NtGdiDeleteObjectApp"
    ],
    "windowsize": 37
  },
  {
    "traces": [
      "Quasar_TheadId4024",
      "Quasar_TheadId592"
    ],
    "sig": [
      "FileIoRead @ openpath:@ExecutableFile",
      "SystemCall @ SystemCall:NtQuerySystemInformation",
      "SystemCall @ SystemCall:NtUserGetDC",
      "SystemCall @ SystemCall:NtGdiGetDeviceCaps",
      "SystemCall @ SystemCall:NtGdiCreateCompatibleDC",
      "SystemCall @ SystemCall:NtGdiCreateDIBSection",
      "SystemCall @ SystemCall:NtGdiExtGetObjectW",
      "SystemCall @ SystemCall:NtGdiOpenDCW",
      "SystemCall @ SystemCall:NtGdiBitBlt",
      "ALPC_SEN @ processname:@ExecutableFile",
      "ALPC_REC @ processname:@ExecutableFile",
      "SystemCall @ SystemCall:NtGdiDeleteObjectApp",
      "SystemCall @ SystemCall:NtGdiFlush",
      "SystemCall @ SystemCall:NtUserGetDC",
      "SystemCall @ SystemCall:NtGdiGetDeviceCaps",
      "SystemCall @ SystemCall:NtGdiDeleteObjectApp"
    ],
    "windowsize": 69
  },
  {
    "traces": [
      "Quasar_TheadId4024",
      "spynet_TheadId2036"
    ],
    "sig": [
      "SystemCall @ SystemCall:NtUserGetDC",
      "SystemCall @ SystemCall:NtGdiGetDeviceCaps",
      "SystemCall @ SystemCall:NtGdiCreateCompatibleDC",
      "SystemCall @ SystemCall:NtGdiCreateDIBSection",
      "ALPC_SEN @ processname:@ExecutableFile",
      "ALPC_REC @ processname:@ExecutableFile",
      "SystemCall @ SystemCall:NtGdiDeleteObjectApp",
      "SystemCall @ SystemCall:NtUserGetDC",
      "SystemCall @ SystemCall:NtGdiGetDeviceCaps"
    ],
    "windowsize": 48
  },
  {
    "traces": [
      "Quasar_TheadId592",
      "spynet_TheadId2036"
    ],
    "sig": [
      "SystemCall @ SystemCall:NtUserGetDC",
      "SystemCall @ SystemCall:NtGdiGetDeviceCaps",
      "SystemCall @ SystemCall:NtGdiCreateCompatibleDC",
      "SystemCall @ SystemCall:NtGdiCreateDIBSection",
      "ALPC_SEN @ processname:@ExecutableFile",
      "ALPC_REC @ processname:@ExecutableFile",
      "SystemCall @ SystemCall:NtGdiDeleteObjectApp",
      "SystemCall @ SystemCall:NtUserGetDC",
      "SystemCall @ SystemCall:NtGdiGetDeviceCaps"
    ],
    "windowsize": 53
  }
]