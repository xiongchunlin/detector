package edu.nu.marple.callstacktool;

import java.util.ArrayList;

public class APITreeNode {
	public String DLLOffset;
	public String APIName;
	public String APIOffset;
	ArrayList<APITreeNode> sons;
	
	public APITreeNode(String DLLOffset, String APIName, String  APIOffset) {
		this.DLLOffset = DLLOffset;
		this.APIName = APIName;
		this.APIOffset = APIOffset;
		sons = new ArrayList<APITreeNode>();
	}

	@Override
	public String toString() {
		return DLLOffset ;
	}
	
	
}