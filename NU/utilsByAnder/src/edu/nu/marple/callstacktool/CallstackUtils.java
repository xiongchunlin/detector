package edu.nu.marple.callstacktool;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.ibm.marple.EventRecordWithCallstack;
import edu.nu.marple.callstackbasics.AggAPIRecord;
import edu.nu.marple.callstackbasics.EventRecordWithCallstackG;
import edu.nu.marple.callstackbasics.EventRecordWithCallstackM;
import edu.nu.marple.filemanipulation.FileReadingUtil;

public class CallstackUtils {

	public static String[] specialExeList = { "cmd.exe", "svchost.exe", "shutdown.exe", "dwm.exe", "whoami.exe",
			"lsass.exe", "taskmgr.exe", "dir.exe", "taskhost.exe", "winupdate.exe", "csrss.exe", "SearchIndexer.exe",
			"services.exe", "audiodg.exe", "conhost.exe" };

	/**
	 * pre proces traces: filter useless syscalls, filter consecutive repeated
	 * blocks, remove path for args
	 * 
	 * @pram numberOfBlocksToKeep if exist repeated and consecutive blocks, this
	 *       value point to the number of these blocks to keep note that this value
	 *       could be 0 which means doesn;t keep any repeated and consecutive blcosk
	 */
	public static void tracePreProcessForPhf(String traceDirPath, String phfName, String traceExtension,
			String filteredExtension, String uselessSyscallFilePath, String usefulEventFilePath,
			String uselessAPIFilePath) throws IOException {
		File dir = new File(traceDirPath + phfName + "/");
		File[] tmpfiles = dir.listFiles(new FilenameFilter() {
			public boolean accept(File dir, String name) {
				return name.endsWith(traceExtension);
			}
		});
		ArrayList<File> files = new ArrayList<File>();
		for (File json : tmpfiles)
			files.add(json);
		Collections.sort(files, new MyComparator());

		for (File json : files) {
			System.out.println(json);
			CallstackFilter filter = new CallstackFilter();

			List<EventRecordWithCallstack> sequence = FileReadingUtil
					.file2EventRecordWithCallstackList(json.getAbsolutePath());
			sequence = filter.filterUselessSyscalls(sequence, uselessSyscallFilePath);
			sequence = filter.filterUselessEvents(sequence, usefulEventFilePath);
			List<EventRecordWithCallstackG> sequence2 = argPreProcess(sequence);
			sequence2 = callstackPreProcess(sequence2, uselessAPIFilePath);

			// write back to a new file
			File filteredTrace = new File(json.getAbsolutePath() + filteredExtension);
			FileWriter filteredWriter = new FileWriter(filteredTrace);
			for (EventRecordWithCallstackG e : sequence2) {
				String output = new GsonBuilder().create().toJson(e);
				filteredWriter.write(output + "\n");
			}
			filteredWriter.flush();
			filteredWriter.close();
		}
	}

	public static int tracePreProcessForPhf_memorySavingVersion(String traceDirPath, String phfName,
			String rawTraceExtension, String preprocessedTraceFileExtensionAdded, String uselessSyscallFilePath, 
			String usefulEventFilePath,	String uselessAPIFilePath, boolean argPreprocessedOn) throws IOException {
		//Build the file list of raw traces, and sort them according to the size.
		File dir = new File(traceDirPath + phfName + "/");
		File[] tmpfiles = dir.listFiles(new FilenameFilter() {
			public boolean accept(File dir, String name) {
				return name.endsWith(rawTraceExtension);
			}
		});
		ArrayList<File> files = new ArrayList<File>();
		for (File json : tmpfiles)
			files.add(json);
		Collections.sort(files, new MyComparator());
		//Input all useless system call types.
		Scanner scanner = new Scanner(new File(uselessSyscallFilePath));
		HashSet<String> uselessSyscalls = new HashSet<String>();
		while (scanner.hasNext()) {
			uselessSyscalls.add(scanner.next());
		}
		scanner.close();
		//Input all useful event types.
		scanner = new Scanner(new File(usefulEventFilePath));
		HashSet<String> usefulEvents = new HashSet<String>();
		while (scanner.hasNext()) {
			usefulEvents.add(scanner.next());
		}
		scanner.close();
		//Input all useless API types.
		scanner = new Scanner(new File(uselessAPIFilePath));
		HashSet<String> uselessAPIs = new HashSet<String>();
		while (scanner.hasNext()) {
			uselessAPIs.add(scanner.next());
		}
		scanner.close();
		//Start to filter file by file.
		
		int eventNumber = 0;
		for (File json : files) {
			System.out.println(json);
			CallstackFilter filter = new CallstackFilter();
			File filteredTrace = new File(json.getAbsolutePath() + preprocessedTraceFileExtensionAdded);
			FileWriter filteredWriter = new FileWriter(filteredTrace);
			BufferedReader reader = new BufferedReader(new FileReader(json));
			String line = null;
			while ((line = reader.readLine()) != null) {
				++eventNumber;
				EventRecordWithCallstack newDataRecord = new Gson().fromJson(line, EventRecordWithCallstack.class);
				//Eliminate all useless system calls.
				if (newDataRecord.eventName.equals("PerfInfoSysClEnter")
						&& filter.isUselessSyscall(newDataRecord.arguments, uselessSyscalls)) {
					continue;
				}
				//Eliminate all useless events.
				if (!usefulEvents.contains(newDataRecord.eventName)) {
					continue;
				}
				//Preprocess arguments.
				EventRecordWithCallstackG resDataRecord = argPreProcessOnSingleRecord(newDataRecord, argPreprocessedOn);
				if (resDataRecord == null)
					continue;
				//Preprocess callstacks.
				resDataRecord = callstackPreProcessOnSingleRecord(resDataRecord, uselessAPIs);
				//Print the result data record.
				if (resDataRecord != null) {
					String output = new GsonBuilder().create().toJson(resDataRecord);
					filteredWriter.write(output + "\n");
					filteredWriter.flush();
				}
			}
			reader.close();
			filteredWriter.close();
			// List<EventRecordWithCallstack> sequence = FileReadingUtil
			// .file2EventRecordWithCallstackList(json.getAbsolutePath());
			// sequence = filter.filterUselessSyscalls(sequence, uselessSyscallFilePath);
			// sequence = filter.filterUselessEvents(sequence, usefulEventFilePath);
			// List<EventRecordWithCallstackG> sequence2 = argPreProcess(sequence);
			// sequence2 = callstackPreProcess(sequence2, uselessAPIFilePath);
			//
			// // write back to a new file
			// File filteredTrace = new File(json.getAbsolutePath() + filteredExtension);
			// FileWriter filteredWriter = new FileWriter(filteredTrace);
			// for (EventRecordWithCallstackG e : sequence2) {
			// String output = new GsonBuilder().create().toJson(e);
			// filteredWriter.write(output + "\n");
			// }
			// filteredWriter.flush();
			// filteredWriter.close();
		}
		return eventNumber;
	}

	/**
	 * 1. filter useless syscall, events 2. remove unnecessary callstacks
	 */
	public static void tracePreProcessForPhf_PureVersion(String traceDirPath, String phfName, String traceExtension,
			String filteredExtension, String uselessSyscallFilePath, String usefulEventFilePath,
			String uselessAPIFilePath) throws IOException {
		File dir = new File(traceDirPath + phfName + "/");
		File[] tmpfiles = dir.listFiles(new FilenameFilter() {
			public boolean accept(File dir, String name) {
				return name.endsWith(traceExtension);
			}
		});
		ArrayList<File> files = new ArrayList<File>();
		for (File json : tmpfiles)
			files.add(json);
		Collections.sort(files, new MyComparator());

		for (File json : files) {
			System.out.println(json);

			CallstackFilter filter = new CallstackFilter();

			List<EventRecordWithCallstack> sequence = FileReadingUtil
					.file2EventRecordWithCallstackList(json.getAbsolutePath());
			sequence = filter.filterUselessSyscalls(sequence, uselessSyscallFilePath);
			sequence = filter.filterUselessEvents(sequence, usefulEventFilePath);
			List<EventRecordWithCallstackG> sequence2 = argPreProcess(sequence);
			sequence2 = callstackPreProcess_keepDllOffset(sequence2, uselessAPIFilePath);

			// write back to a new file
			File filteredTrace = new File(json.getAbsolutePath() + filteredExtension);
			// filteredTrace.createNewFile();
			FileWriter filteredWriter = new FileWriter(filteredTrace);
			for (EventRecordWithCallstackG e : sequence2) {
				String output = new GsonBuilder().create().toJson(e);
				filteredWriter.write(output + "\n");
			}
			filteredWriter.flush();
			filteredWriter.close();
		}
	}

	public static String getAPI(String callstackLayer) {
		int tmpIndex = callstackLayer.indexOf('(');
		return callstackLayer.substring(tmpIndex + 1, callstackLayer.length() - 1);
	}

	public static String getOffset(String callstackLayer) {
		String[] tmpSplitResult = callstackLayer.split("\\(");
		return tmpSplitResult[0];
	}

	static String[] extraSuffix = { "_WithStatus", "_Ctx", "_ExU", "_ExW", "_ExA", "_Ctx", "ExU", "ExW", "ExA", "_XP",
			"_Ex", "Ex", "_I", "_U", "_W", "_A", "I", "U", "W", "A" };

	public static String generalizeAPI(String currentAPI) {
		String res = null;
		if (currentAPI.contains(":")) {
			res = currentAPI;
			boolean changed = true;
			while (changed) {
				changed = false;
				char lastChar = res.charAt(res.length() - 1);
				while (lastChar >= '0' && lastChar <= '9') {
					res = res.substring(0, res.length() - 1);
					lastChar = res.charAt(res.length() - 1);
					changed = true;
				}
				for (String itrSuffix : extraSuffix)
					if (res.endsWith(itrSuffix)) {
						res = res.substring(0, res.length() - itrSuffix.length());
						changed = true;
						break;
					}
			}
		} else
			res = currentAPI;
		return res;
	}

	public static EventRecordWithCallstackG callstackPreProcessOnSingleRecord(EventRecordWithCallstackG thisRecord,
			HashSet<String> uselessAPIs) {
		if (thisRecord.systemCallStack.equals("")) {
			// resList.add(new EventRecordWithCallstackG(thisRecord));
			return null;
		}

		String[] callStackSplitResult = thisRecord.systemCallStack.split(",");
		String newSystemCallstack = "";
		String newExeCallstack = "";

		int j = 0;
		String currentAPI;

		while (j < callStackSplitResult.length
				&& (currentAPI = getAPI(callStackSplitResult[j])).equals("NOMODULE-SYSTEMMODULE"))
			++j;

		String lastAPI = "";
		while (j < callStackSplitResult.length
				&& !(currentAPI = generalizeAPI(getAPI(callStackSplitResult[j]))).equals("NOMODULE")) {
			if (!(currentAPI.contains(":NOAP") || currentAPI.contains(":NOOAP")
					|| currentAPI.equals("NOMODULE-SYSTEMMODULE") || currentAPI.contains("\\Users\\")
					|| currentAPI.contains(".NET") || currentAPI.equals(lastAPI) || uselessAPIs.contains(currentAPI))) {
				newSystemCallstack += generalizeAPI(currentAPI) + ",";
				lastAPI = currentAPI;
			}
			++j;
		}
		if (newSystemCallstack.length() > 0)
			newSystemCallstack = newSystemCallstack.substring(0, newSystemCallstack.length() - 1);

		while (j < callStackSplitResult.length && (currentAPI = getAPI(callStackSplitResult[j])).equals("NOMODULE")) {
			newExeCallstack += getOffset(callStackSplitResult[j]) + ",";
			++j;
		}
		if (newExeCallstack.length() > 0)
			newExeCallstack = newExeCallstack.substring(0, newExeCallstack.length() - 1);
		if (newSystemCallstack.equals(""))
			return null;
		EventRecordWithCallstackG res = new EventRecordWithCallstackG(thisRecord.threadID, thisRecord.processID,
				thisRecord.eventName, thisRecord.parameter, newSystemCallstack, newExeCallstack);
		return res;
	}

	public static List<EventRecordWithCallstackG> callstackPreProcess(List<EventRecordWithCallstackG> sequence,
			String uselessAPIFilePath) throws IOException {
		List<EventRecordWithCallstackG> resList = new ArrayList<EventRecordWithCallstackG>();
		HashSet<String> uselessAPIs = new HashSet<String>();
		BufferedReader br = new BufferedReader(new FileReader(uselessAPIFilePath));
		String data;
		while ((data = br.readLine()) != null) {
			uselessAPIs.add(data);
		}
		br.close();
		for (int i = 0; i < sequence.size(); ++i) {
			EventRecordWithCallstackG thisRecord = sequence.get(i);
			EventRecordWithCallstackG resRecord = callstackPreProcessOnSingleRecord(thisRecord, uselessAPIs);
			if (resRecord != null)
				resList.add(resRecord);
		}
		return resList;
	}

	public static List<AggAPIRecord> convert2AggAPIRecordList(List<EventRecordWithCallstackG> intermediateList) {
		ArrayList<AggAPIRecord> sequences = new ArrayList<AggAPIRecord>();
		String lastAPI = "-1";
		String lastThreadID = "-1";
		// String lastExeCallStack = "-1";
		for (EventRecordWithCallstackG eventg : intermediateList) {
			String[] callStackSplitResults = eventg.systemCallStack.split(",");
			if (// !lastExeCallStack.equals(eventg.exeCallStack) ||
			!lastAPI.equals(callStackSplitResults[callStackSplitResults.length - 1])
					|| !lastThreadID.equals(eventg.threadID)) {
				AggAPIRecord newAggAPIRecord = new AggAPIRecord(eventg.threadID);
				sequences.add(newAggAPIRecord);
				lastAPI = callStackSplitResults[callStackSplitResults.length - 1];
				lastThreadID = eventg.threadID;
				// lastExeCallStack = eventg.exeCallStack;
			}
			sequences.get(sequences.size() - 1).eventSeq
					.add(new EventRecordWithCallstackM(eventg.eventName, eventg.parameter, eventg.systemCallStack));
		}
		return sequences;
	}

	/*
	 * author: runqing keep previous item of the last callstack item
	 * 7feff688e94(\Device\HarddiskVolume1\Windows\System32\rpcrt4.dll:
	 * RpcBindingFree+0xbd4), abcde ->
	 * \Windows\System32\rpcrt4.dll:RpcBindingFree@@abcde
	 * 
	 */

	public static List<EventRecordWithCallstackG> callstackPreProcess_keepDllOffset(
			List<EventRecordWithCallstackG> sequence, String uselessAPIFilePath) throws IOException {
		List<EventRecordWithCallstackG> resList = new ArrayList<EventRecordWithCallstackG>();
		HashSet<String> uselessAPIs = new HashSet<String>();
		BufferedReader br = new BufferedReader(new FileReader(uselessAPIFilePath));
		String data;
		while ((data = br.readLine()) != null) {
			uselessAPIs.add(data);
		}
		br.close();
		for (int i = 0; i < sequence.size(); ++i) {
			EventRecordWithCallstackG thisRecord = sequence.get(i);
			if (thisRecord.systemCallStack.equals("")) {
				resList.add(new EventRecordWithCallstackG(thisRecord));
				continue;
			}
			String[] splitResults1 = thisRecord.systemCallStack.split(",");
			ArrayList<String> splitResults2 = new ArrayList<String>();
			boolean stage1 = false;
			for (String field : splitResults1) {
				if (!stage1) {
					if (!field.startsWith("fffff")) {
						splitResults2.add(field);
						stage1 = true;
					}
				} else if ((field.contains("(") && field.contains(")")) || field.startsWith("fffff")) {
					splitResults2.add(field);
				} else
					break;
			}

			ArrayList<String> splitResults3 = new ArrayList<String>();
			for (String field : splitResults2) {
				// field = field.substring(field.indexOf("Windows"));
				if (!field.startsWith("fffff")) {
					if (field.indexOf("HarddiskVolume") >= 0) {
						field = field.substring(field.indexOf("HarddiskVolume") + 15);
						field = field.split("\\+")[0];
					}
				}
				splitResults3.add(field);
			}

			for (int j = 0; j < splitResults3.size();)
				if (uselessAPIs.contains(splitResults3.get(j)) || splitResults3.get(j).startsWith("fffff"))
					splitResults3.remove(j);
				else
					++j;
			String newCallstack = "";
			if (splitResults3.size() > 0) {
				newCallstack = splitResults3.get(0);
				String lastField = splitResults3.get(0);
				for (int j = 1; j < splitResults3.size(); ++j) {
					if (!lastField.equals(splitResults3.get(j)))
						newCallstack = newCallstack + "," + splitResults3.get(j);
					lastField = splitResults3.get(j);
				}
			}
			resList.add(new EventRecordWithCallstackG(thisRecord.threadID, thisRecord.processID, thisRecord.eventName,
					thisRecord.parameter, newCallstack, ""));
		}
		return resList;
	}

	public static EventRecordWithCallstackG argPreProcessOnSingleRecord(EventRecordWithCallstack tmpRecord, boolean fieldPreProcessOn) {
		EventRecordWithCallstackG resRecord = null;
		String fieldArg = "", fieldArg1 = "", fieldArg2 = "";
		if (tmpRecord.eventName.equals("ALPCALPC-Send-Message")
				|| tmpRecord.eventName.equals("ALPCALPC-Receive-Message")) {
			fieldArg = tmpRecord.arguments.get("ProcessName");
			if (fieldArg == null)
				fieldArg = "";
			if (!fieldArg.equals("N") && !fieldArg.equals("")) {
				if (fieldPreProcessOn) fieldArg = argfieldPreProcess(fieldArg);
				resRecord = new EventRecordWithCallstackG(tmpRecord.threadID, tmpRecord.processID, tmpRecord.eventName,
						"ProcessName:" + fieldArg, tmpRecord.callStack, "");
			}
		} else if (tmpRecord.eventName.equals("DiskIoWrite") || tmpRecord.eventName.equals("DiskIoRead")
				|| tmpRecord.eventName.equals("FileIoCleanup") || tmpRecord.eventName.equals("FileIoClose")
				|| tmpRecord.eventName.equals("FileIoCreate") || tmpRecord.eventName.equals("FileIoDelete")
				|| tmpRecord.eventName.equals("FileIoDirEnum") || tmpRecord.eventName.equals("FileIoDirNotify")
				|| tmpRecord.eventName.equals("FileIoFileCreate") || tmpRecord.eventName.equals("FileIoFileDelete")
				|| tmpRecord.eventName.equals("FileIoFlush") || tmpRecord.eventName.equals("FileIoQueryInfo")
				|| tmpRecord.eventName.equals("FileIoRead") || tmpRecord.eventName.equals("FileIoRename")
				|| tmpRecord.eventName.equals("FileIoSetInfo") || tmpRecord.eventName.equals("FileIoWrite")
				|| tmpRecord.eventName.equals("ImageLoad") || tmpRecord.eventName.equals("ImageUnLoad")
				|| tmpRecord.eventName.equals("ImageDCEnd") || tmpRecord.eventName.equals("ImageDCStart")
				|| tmpRecord.eventName.equals("FileIoFSControl")) {
			if (tmpRecord.arguments.containsKey("OpenPath")) {
				fieldArg = tmpRecord.arguments.get("OpenPath");
			} else {
				fieldArg = tmpRecord.arguments.get("FileName");
			}
			if (fieldArg == null)
				fieldArg = "";
			if (!fieldArg.equals("UNKNOWN_FILE") && !fieldArg.equals("")) {
				if (fieldPreProcessOn) fieldArg = argfieldPreProcess(fieldArg);
				resRecord = new EventRecordWithCallstackG(tmpRecord.threadID, tmpRecord.processID, tmpRecord.eventName,
						"FileName:" + fieldArg, tmpRecord.callStack, "");
			}
		} else if (tmpRecord.eventName.equals("RegistryCreate") || tmpRecord.eventName.equals("RegistryDelete")
				|| tmpRecord.eventName.equals("RegistryDeleteValue")
				|| tmpRecord.eventName.equals("RegistryEnumerateKey")
				|| tmpRecord.eventName.equals("RegistryEnumerateValueKey")
				|| tmpRecord.eventName.equals("RegistryKCBCreate") || tmpRecord.eventName.equals("RegistryKCBDelete")
				|| tmpRecord.eventName.equals("RegistryOpen") || tmpRecord.eventName.equals("RegistryQuery")
				|| tmpRecord.eventName.equals("RegistryQueryValue")
				|| tmpRecord.eventName.equals("RegistrySetInformation")
				|| tmpRecord.eventName.equals("RegistrySetValue")) {
			fieldArg = tmpRecord.arguments.get("KeyName");
			if (fieldArg == null)
				fieldArg = "";
			if (!fieldArg.equals("UNKOWN_KEY") && !fieldArg.equals("")) {
				if (fieldPreProcessOn) fieldArg = argfieldPreProcess(fieldArg);
				resRecord = new EventRecordWithCallstackG(tmpRecord.threadID, tmpRecord.processID, tmpRecord.eventName,
						"KeyName:" + fieldArg, tmpRecord.callStack, "");
			}
		} else if (tmpRecord.eventName.equals("ProcessStart") || tmpRecord.eventName.equals("ProcessEnd")) {
			fieldArg1 = tmpRecord.arguments.get("ImageFileName");
			fieldArg2 = tmpRecord.arguments.get("CommandLine");
			if (fieldArg1 == null)
				fieldArg1 = "";
			if (fieldArg2 == null)
				fieldArg2 = "";
			if (fieldPreProcessOn) fieldArg1 = argfieldPreProcess(fieldArg1);
			if (fieldPreProcessOn) fieldArg2 = argfieldPreProcess(fieldArg2);
			resRecord = new EventRecordWithCallstackG(tmpRecord.threadID, tmpRecord.processID, tmpRecord.eventName,
					"ImageFileName:" + fieldArg1 + " @@ " + "CommandLine:" + fieldArg2, tmpRecord.callStack, "");
		} else if (tmpRecord.eventName.equals("PerfInfoSysClEnter")) {
			if (!tmpRecord.arguments.get("SystemCall").equals("UNKNOWN_SYSCALL"))
				resRecord = new EventRecordWithCallstackG(tmpRecord.threadID, tmpRecord.processID, tmpRecord.eventName,
						"SystemCall:" + tmpRecord.arguments.get("SystemCall"), tmpRecord.callStack, "");
		} else if (tmpRecord.eventName.equals("ALPCALPC-Unwait")
				|| tmpRecord.eventName.equals("ALPCALPC-Wait-For-Reply")
				|| tmpRecord.eventName.equals("DiskIoFlushBuffers")
				|| tmpRecord.eventName.equals("PerfInfoSysClExit")) {
			resRecord = new EventRecordWithCallstackG(tmpRecord.threadID, tmpRecord.processID, tmpRecord.eventName, "",
					tmpRecord.callStack, "");
		}
		return resRecord;
	}

	/**
	 * pre proces trace args
	 */
	public static List<EventRecordWithCallstackG> argPreProcess(List<EventRecordWithCallstack> sequence)
			throws IOException {

		// preprocess arguments
		List<EventRecordWithCallstackG> resList = new ArrayList<EventRecordWithCallstackG>();
		for (int i = 0; i < sequence.size(); ++i) {
			EventRecordWithCallstack tmpRecord = sequence.get(i);
			EventRecordWithCallstackG resRecord = argPreProcessOnSingleRecord(tmpRecord, true);
			if (resRecord != null) resList.add(resRecord);
		}

		return resList;
	}

	//
	private static HashSet<String> configFile = new HashSet<String>() {
		{
			add("cnf");
			add("conf");
			add("config");
			add("ini");
			add("manifest");
		}
	};

	private static HashSet<String> dataFile = new HashSet<String>() {
		{
			add("bak");
			add("aff");
			add("bk");
			add("bkl");
			add("cab");
			add("crx");
			add("dat");
			add("data");
			add("db");
			add("db-journal");
			add("db-mj48724A972");
			add("db-mjD176209F0");
			add("db-shm");
			add("db-wal");
			add("dmp");
			add("docx");
			add("dotm");
			add("hdmp");
			add("json");
			add("ldb");
			add("log");
			add("pdf");
			add("sdb");
			add("sqlite");
			add("sqlite3");
			add("sqlite-journal");
			add("txt");
			add("wps");
			add("xml");
			add("xml~");
			add("xpi");
			add("xrc");
			add("zip");
			add("etl");
		}
	};

	private static HashSet<String> deviceFile = new HashSet<String>() {
		{
			add("drv");
		}
	};

	private static HashSet<String> excutableFile = new HashSet<String>() {
		{
			add("bat");
			add("bin");
			add("exe");
			add("hta");
			add("lnk");
			add("msi");
			add("php");
			add("py");
			add("vbs");
		}
	};

	private static HashSet<String> fontFile = new HashSet<String>() {
		{
			add("eot");
			add("fon");
			add("ttc");
			add("ttf");
		}
	};

	private static HashSet<String> imageFile = new HashSet<String>() {
		{
			add("alias");
			add("bitmap");
			add("bmp");
			add("gif");
			add("icm");
			add("ico");
			add("jpg");
			add("png");
		}
	};

	private static HashSet<String> languageFile = new HashSet<String>() {
		{
			add("mui");
			add("nls");
		}
	};

	private static HashSet<String> libraryFile = new HashSet<String>() {
		{
			add("pyd");
			add("bpl");
			add("jar");
		}
	};

	private static HashSet<String> mediaFile = new HashSet<String>() {
		{
			add("wav");
			add("wma");
			add("wmdb");
			add("wmv");
			add("wpl");
		}
	};

	private static HashSet<String> systemFile = new HashSet<String>() {
		{
			add("library-ms");
			add("sys");
		}
	};

	private static HashSet<String> temporaryFile = new HashSet<String>() {
		{
			add("cache");
			add("temp");
			add("temp-tmp");
			add("tmp");
		}
	};

	private static HashSet<String> webpageFile = new HashSet<String>() {
		{
			add("ashx");
			add("aspx");
			add("css");
			add("dtd");
			add("htm");
			add("js");
			add("url");
		}
	};

	private static HashMap<String, String> phfArgPreprocessingCache = new HashMap<String, String>();

	private static String argfieldPreProcess(String argumentString) {
		String tmpString = argumentString;
		while (tmpString.startsWith(" ") || tmpString.startsWith("\""))
			tmpString = tmpString.substring(1, tmpString.length());
		while (tmpString.endsWith(" ") || tmpString.endsWith("\""))
			tmpString = tmpString.substring(0, tmpString.length() - 1);
		String[] splitResults = tmpString.split("\\.");
		String suffix = "";
		if (splitResults.length >= 1)
			suffix = splitResults[splitResults.length - 1];
		else
			return "";
		String sysArg = null;
		if (configFile.contains(suffix)) {
			sysArg = "@ConfigurationFile";
		} else if (dataFile.contains(suffix)) {
			sysArg = "@DataFile";
		} else if (deviceFile.contains(suffix)) {
			sysArg = "@DeviceFile";
		} else if (excutableFile.contains(suffix)) {
			sysArg = "@ExecutableFile";
			for (String tmp : specialExeList)
				if (tmpString.endsWith(tmp)) {
					sysArg = tmp;
					break;
				}
		} else if (fontFile.contains(suffix)) {
			sysArg = "@FontFile";
		} else if (imageFile.contains(suffix)) {
			sysArg = "@ImageFile";
		} else if (languageFile.contains(suffix)) {

			sysArg = "@LanguageFile";
		} else if (libraryFile.contains(suffix)) {
			sysArg = "@LibraryFile";
		} else if (mediaFile.contains(suffix)) {
			sysArg = "@MediaFile";
		} else if (systemFile.contains(suffix)) {
			sysArg = "@TemporaryFile";
		} else if (webpageFile.contains(suffix)) {
			sysArg = "@WebpageFile";
		} else if (tmpString.endsWith("dll")) {
			// System.out.println(tmpString);
			sysArg = tmpString.split("\\\\")[tmpString.split("\\\\").length - 1];
		} else {
			if (phfArgPreprocessingCache.containsKey(tmpString)) {
				return phfArgPreprocessingCache.get(tmpString);
			}
			sysArg = tmpString;
			sysArg = sysArg.replaceAll("[^\\\\]*\\.dat", "XXX.dat");
			sysArg = sysArg.replaceAll("[^\\\\]*\\.(temp|TMP|tmp)", "XXX.temp");
			sysArg = sysArg.replaceAll("[^\\\\]*\\.(nls|NLS)", "XXX.nls");
			sysArg = sysArg.replaceAll("[^\\\\]*\\.(png|PNG)", "XXX.png");
			sysArg = sysArg.replaceAll("[^\\\\]*\\.css$", "XXX.css");
			sysArg = sysArg.replaceAll("[^\\\\]*\\.(js|JS)$", "XXX.js");
			sysArg = sysArg.replaceAll("[^\\\\]*\\.json$", "XXX.json");
			sysArg = sysArg.replaceAll("[^\\\\]*\\.(htm|html)$", "XXX.htm");
			sysArg = sysArg.replaceAll("\\{.+-.+-.+-.+\\}", "XXX");
			sysArg = sysArg.replaceAll("[A-Z0-9]+-[^\\\\]+-[^\\\\]+-[0-9]+(_Classes)*", "XXX");
			sysArg = sysArg.replaceAll("Plane\\d+$", "Plane");
			sysArg = sysArg.replaceAll("msvideo\\d+$", "msvideo");
			sysArg = sysArg.replaceAll("wave\\d+$", "wave");
			sysArg = sysArg.replaceAll("midi\\d+$", "midi");
			sysArg = sysArg.replaceAll("HarddiskVolume\\d+", "HarddiskVolume");
			sysArg = sysArg.replaceAll("countrycode\\[\\d+\\]$", "countrycode");
			sysArg = sysArg.replaceAll("MonitorFunction\\d+$", "MonitorFunction");
			sysArg = sysArg.replaceAll("^#\\d+$", "#");
			sysArg = sysArg.replaceAll("Categories\\\\.*$", "Categories");
			sysArg = sysArg.replaceAll("Transforms\\\\[^\\\\]*?[0-9][^\\\\]*?$", "Transforms");
			sysArg = sysArg.replaceAll("UnitVersioning_\\d+$", "UnitVersioning");
			sysArg = sysArg.replaceAll("_[a-z0-9\\._]{30,}", "_XXX");
			sysArg = sysArg.replaceAll("[a-z0-9]{25,}", "XXX");
			sysArg = sysArg.replaceAll("(_\\d+){4,}", "");
			sysArg = sysArg.replaceAll("flag[A-Z0-9]{15,}", "flag");
			sysArg = sysArg.replaceAll("(\\\\[a-f0-9]+){2,}", "");
			sysArg = sysArg.replaceAll("([a-f0-9]+-){3,}[a-f0-9]+", "XXX");
			sysArg = sysArg.replaceAll("0x[0-9A-F]{7,}", "XXX");
			sysArg = sysArg.replaceAll("[a-f0-9A-F]{10,}", "XXX");
			sysArg = sysArg.replaceAll("(\\d+\\.){2,}\\d+", "XXX");
			sysArg = sysArg.replaceAll("\\d{7,}", "");
			sysArg = sysArg.replaceAll("XXX,\\d+", "XXX");
			sysArg = sysArg.split("\\\\")[sysArg.split("\\\\").length - 1];
			if (sysArg.equals("Mohammad") || sysArg.equals("cs"))
				sysArg = "LOCALUSERNAME";
		}
		phfArgPreprocessingCache.put(tmpString, sysArg);
		return sysArg;
	}

	public static List<String> topAPIFile2StringList(String path) {
		JsonReader reader = null;
		try {
			reader = new JsonReader(new FileReader(path));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Type REVIEW_TYPE = new TypeToken<List<String>>() {
		}.getType();
		return new Gson().fromJson(reader, REVIEW_TYPE);
	}

	// class MyComparator implements Comparator {
	// @Override
	// public int compare(Object o1, Object o2) {
	// SigResults sigo1 = (SigResults) o1;
	// SigResults sigo2 = (SigResults) o2;
	// if (sigo1.sig.size() < sigo2.sig.size())
	// return -1;
	// else if (sigo1.sig.size() == sigo2.sig.size())
	// return 0;
	// else
	// return 1;
	// }
	// }

}

class MyComparator implements Comparator {
	@Override
	public int compare(Object o1, Object o2) {
		File file1 = (File) o1;
		File file2 = (File) o2;
		if (file1.length() < file2.length())
			return -1;
		else if (file1.length() == file2.length()) {
			return 0;
		} else
			return 1;
	}
}
