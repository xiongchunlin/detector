package edu.nu.marple.callstacktool;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;

import edu.nu.marple.SigResults;
import edu.nu.marple.callstacktool.CallstackLoopUtils.PossibleResult;

class SigGenerator implements Callable<Set<SigResults>> {
	private boolean enableGetCallstackFromLowestAddress;
	private PossibleResult possibleResult;
	private File json;
	private Set<SigResults> finalSigSet = new HashSet<SigResults>();
	private Map<List<String>, SigResults> sigString2SigResults = new HashMap<List<String>, SigResults>();
		
	public SigGenerator(boolean enableGetCallstackFromLowestAddress, PossibleResult possibleResult, File json) {
		super();
		this.enableGetCallstackFromLowestAddress = enableGetCallstackFromLowestAddress;
		this.possibleResult = possibleResult;
		this.json = json;
	}


	@Override
	public Set<SigResults> call() throws Exception {
		Map<String, List<CallstackChain>> thread2Callstacks = new HashMap<String, List<CallstackChain>>();
		try {
			thread2Callstacks = CallstackLoopUtils.extractTopAPIWithExeCallstack(json.getAbsolutePath());
			System.out.println("finish extract top api");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		for (String threadKey : thread2Callstacks.keySet()) {
			List<CallstackChain> callstacks = thread2Callstacks.get(threadKey);
			if (callstacks.isEmpty())
				continue;


			// remove top API in blacklists
			callstacks = CallstackLoopUtils.removeFromBlackListsCallstackChainVersion(callstacks);
			System.out.println("finish remove top API in blacklists");

//			System.out.println(callstacks);
			List<CallstackChain> preprocessedCallstacks = null;
			if (enableGetCallstackFromLowestAddress)
				preprocessedCallstacks = CallstackLoopUtils.getCallstackListStartFromLowestAddress(callstacks);
			else 
				preprocessedCallstacks = callstacks;
//			System.out.println(preprocessedCallstacks);
			
			Set<List<CallstackChain>> signaturesWithCallstacks = new HashSet<List<CallstackChain>>();
			if (possibleResult == CallstackLoopUtils.PossibleResult.All){
				signaturesWithCallstacks = new CallstackFilter<CallstackChain>()
					.getAllPossibleSequenceByMergingLoop(preprocessedCallstacks);
			} else if (possibleResult == CallstackLoopUtils.PossibleResult.Longest){
				signaturesWithCallstacks.add(new CallstackFilter<CallstackChain>().getOnePossibleSequneceByChoosingLongestLoop(preprocessedCallstacks));
			}
			
			System.out.println("finish one possible sequence");
			
			for (List<CallstackChain> sigWithCallstacks : signaturesWithCallstacks) {
				SigResults finalSig = new SigResults();
				Set<String> traces = new HashSet<String>();
				traces.add(json + "_" + threadKey);
				List<String> sig = new ArrayList<String>();
				for (CallstackChain chain : sigWithCallstacks) {
					CallstackItem APIItem = chain.chains.get(chain.chains.size() - 1);
					String[] tmpSplits = APIItem.APIName.split("_");
					String APIName = tmpSplits[tmpSplits.length - 1];
					sig.add(APIName);
//					sig.add(chain.chains.toString());
				}

				if (sig.size() <= 3) // remove sig length <=3
					continue;

				if (sigString2SigResults.containsKey(sig)) {
					sigString2SigResults.get(sig).traces.addAll(traces);
				} else {
					finalSig.sig = sig;
					finalSig.traces = traces;
					finalSigSet.add(finalSig);
					sigString2SigResults.put(sig, finalSig);
				}
			}
		}
		
		return finalSigSet;
	}
	
}