package edu.nu.marple.callstacktool;

import java.util.List;

public class CallstackChain{
	public List<CallstackItem> chains;

	public CallstackChain(List<CallstackItem> chains) {
		super();
		this.chains = chains;
	}

	

	@Override
	public String toString() {
		return "CallstackChain [chains=" + chains + "]";
	}



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((chains == null) ? 0 : chains.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CallstackChain other = (CallstackChain) obj;
		if (chains == null) {
			if (other.chains != null)
				return false;
		} else if (!chains.equals(other.chains))
			return false;
		return true;
	}

//	public String toLoopString(){
//		StringBuffer stringBuffer = new StringBuffer();
//		for (CallstackItem item : chains){
//			stringBuffer.append(item.DLLOffset).append("_");
//			if (!item.APIName.contains("None"))
//				stringBuffer.append(item.APIName);
//		}
//		return stringBuffer.toString();
//	}
//	
	
	
	
}