package edu.nu.marple.callstacktool;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import javax.swing.tree.DefaultMutableTreeNode;

import org.apache.log4j.Logger;
import org.jgraph.graph.DefaultEdge;
import org.jgrapht.Graph;
import org.jgrapht.io.ComponentNameProvider;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.io.DOTExporter;
import org.jgrapht.io.StringComponentNameProvider;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ibm.marple.EventRecordWithCallstack;

import edu.nu.marple.SetSignature;
import edu.nu.marple.SigResults;
import edu.nu.marple.callstacktool.CallstackLoopUtils.PossibleResult;
import edu.nu.marple.callstackversionMain.ExtractTopLevelAPI;
import edu.nu.marple.filemanipulation.FileReadingUtil;

public class CallstackLoopUtils {

	static Logger logger = Logger.getLogger(CallstackLoopUtils.class);

	
	/**
	 * remove api in the blacklist from the List of CallstackChain
	 * @param callstackChains
	 * @return
	 */
	public static List<CallstackChain> removeFromBlackListsCallstackChainVersion(List<CallstackChain> callstackChains){
		Set<String> blackLists = new HashSet<String>();
		blackLists.add("FlushInstructionCache");
		blackLists.add("VirtualAlloc");
		blackLists.add("RtlAllocateHeap");
		blackLists.add("RtlReAllocateHeap");
		blackLists.add("RtlQueryPerformanceCounter");
		blackLists.add("ResetEvent");
		blackLists.add("SetEvent");
		blackLists.add("MD5Final");
		blackLists.add("HeapFree");
		blackLists.add("VirtualFree");
		blackLists.add("VirtualQuery");
		blackLists.add("SetErrorMode");
		blackLists.add("NtCallbackReturn");
		blackLists.add("SetErrorMode");
		blackLists.add("SetErrorMode");
		
		List<CallstackChain> filteredCallstackChains = new ArrayList<CallstackChain>(); 
		for (CallstackChain chain : callstackChains){
			String topAPIName = chain.chains.get(chain.chains.size()-1).APIName;
			if (!blackLists.contains(topAPIName)){
				if (topAPIName.startsWith("Zw")){
					chain.chains.get(chain.chains.size()-1).APIName = topAPIName.replaceFirst("Zw", "Nt");
				} 
				if (topAPIName.endsWith("A")){
					chain.chains.get(chain.chains.size()-1).APIName = topAPIName.substring(0, topAPIName.length()-1) + "W";
				}
				filteredCallstackChains.add(chain);
			}
		}
		
		return filteredCallstackChains;
	}
	
	public static List<String> removeFromBlackListsStringVersion(List<String> topAPIList){
		Set<String> blackLists = new HashSet<String>();
		blackLists.add("FlushInstructionCache");
		blackLists.add("VirtualAlloc");
		blackLists.add("RtlAllocateHeap");
		blackLists.add("RtlReAllocateHeap");
		blackLists.add("RtlQueryPerformanceCounter");
		blackLists.add("ResetEvent");
		blackLists.add("SetEvent");
		blackLists.add("MD5Final");
		blackLists.add("HeapFree");
		blackLists.add("VirtualFree");
		blackLists.add("VirtualQuery");
		blackLists.add("SetErrorMode");
		blackLists.add("NtCallbackReturn");
		blackLists.add("SetErrorMode");
		blackLists.add("SetErrorMode");
		
		List<String> filteredTopAPIList = new ArrayList<String>(); 
		for (String topAPIName : topAPIList){
			if (!blackLists.contains(topAPIName)){
				String newTopAPIName = topAPIName;
				if (newTopAPIName.startsWith("Zw")){
					newTopAPIName = newTopAPIName.replaceFirst("Zw", "Nt");
				} 
				if (newTopAPIName.endsWith("A")){
					newTopAPIName = newTopAPIName.substring(0, newTopAPIName.length()-1) + "W";
				}
				filteredTopAPIList.add(newTopAPIName);
			}
		}
		
		return filteredTopAPIList;
	}

	
	/**
	 * assumptions: the lower address will happen before the higher address.
	 * This function choose the lowest address as the start point and remove CallstackChain itmes before the lowest address.
	 * @param callstacks
	 * @return
	 */
	public static List<CallstackChain> getCallstackListStartFromLowestAddress(List<CallstackChain> callstacks) {

		// choose the branch level of all the callstacks.
		// e.g. a->b->c1->d1, a->b->c2->d2, a->b->c3->d3,
		// the branch level is 2 (b)
		int branchCallstackLevel = 0;
		final int tmpFinalLevel1 = branchCallstackLevel;
		Set<String> callstackIntheSameLevel = callstacks.stream()
				.map(chain -> chain.chains.get(tmpFinalLevel1).DLLOffset).collect(Collectors.toSet());
		boolean exit = callstackIntheSameLevel.size() == 1;
		while (exit) { 
			branchCallstackLevel++;
			callstackIntheSameLevel = new HashSet<String>();
			boolean isOutofIndex = false;
			for (CallstackChain chain : callstacks) {
				if (branchCallstackLevel < chain.chains.size())
					callstackIntheSameLevel.add(chain.chains.get(branchCallstackLevel).DLLOffset);
				else {
					isOutofIndex = true;
					break;
				}
			}

			if (isOutofIndex == true) {
				branchCallstackLevel--;
				exit = false;
			} else {
				exit = callstackIntheSameLevel.size() == 1;
			}

		}

		final int tmpFinalLevel2 = branchCallstackLevel;
		List<String> topCallstacks = callstacks.stream().map(chain -> chain.chains.get(tmpFinalLevel2).DLLOffset)
				.collect(Collectors.toList());
		int firstMinimalIndex = 0; // the first index of minimal offset
		int startIndex = 0; // if minimal offset occurs two times, choose the
							// second index as the start index. else choose the
							// first index as start index
							// note that consecutive offset will be considered as one time. for
							// example, a a a a b c d e a a a a, startindex = 8
		String minimalOffset = topCallstacks.get(0);
		for (int index = 1; index < topCallstacks.size(); index++) {
			String currentDlloffset = topCallstacks.get(index);
			int compareToPreviousMinimalOffset = currentDlloffset.compareToIgnoreCase(minimalOffset);
			if (compareToPreviousMinimalOffset < 0) {
				minimalOffset = currentDlloffset;
				firstMinimalIndex = index;
				startIndex = index;
			} else if (compareToPreviousMinimalOffset == 0) {
				if (startIndex == firstMinimalIndex && !currentDlloffset.equals(topCallstacks.get(index - 1)))
					startIndex = index;
			}
		}

		List<CallstackChain> preprocessedCallstacks = callstacks.subList(startIndex, callstacks.size());
		return preprocessedCallstacks;

	}
	
	
	/**
	 * extract event/syscall with its full callstack
	 * @param filePath
	 * @return thread2callstackSequence. key is the thread, the value is the callstackchain sequence of the corresponding thread.
	 * @throws IOException
	 */
	public static Map<String, List<CallstackChain>> extractFullCallstackWithEvent(String filePath) throws IOException {
		List<EventRecordWithCallstack> sequence = FileReadingUtil
				.file2EventRecordWithCallstackList(filePath);
		Map<String, List<CallstackChain>> thread2callstacks = new HashMap<String, List<CallstackChain>>();
		for (EventRecordWithCallstack record : sequence) {
			String threadKey = record.processID + "@" + record.threadID;
			if (!thread2callstacks.containsKey(threadKey)) {
				thread2callstacks.put(threadKey, new ArrayList<CallstackChain>());
			}

			if (record.callStack.trim().isEmpty() || record.callStack.length() < 5)
				continue;

			List<String> callstackSplit = Arrays.asList(record.callStack.split(","));
			List<CallstackItem> callstackItemList = callstack2CallstackItem(threadKey, callstackSplit);

			if (!callstackItemList.isEmpty()) {
				callstackItemList.add(new CallstackItem("ROOT", "ROOT", "ROOT", "ROOT"));
				Collections.reverse(callstackItemList);
				String eventName = "EmptyEvent";
				if (record.eventName.equals("PerfInfoSysClEnter")) {
					eventName = record.arguments.getOrDefault("SystemCall", "NotFoundSyscall");
				} else if (!record.eventName.equals("PerfInfoSysClExit")) {
					eventName = record.eventName;
				}
//				eventName = eventName.replace(":", "_").replaceAll("\\.", "_").replace("-", "_");
				callstackItemList.add(new CallstackItem(NoDllOffset, NoModule, eventName, NoAPIOffset));
				CallstackChain callstackChain = new CallstackChain(callstackItemList);
				thread2callstacks.get(threadKey).add(callstackChain);
			}
		}
		return thread2callstacks;
	}


	/**
	 * convert a original callstack to a callstackItem 
	 * this method will handle format related problems
	 * @param ThreadKey
	 * @param callstackSplit
	 * @return
	 */	
	public static String KernelModule = "KernelModule";
	public static String NoModule = "NoModule";
	public static String NoAPI = "NoAPI";
	public static String NoAPIOffset = "NoAPIOffset";
	public static String NoDllOffset = "NoDllOffset";
	private static List<CallstackItem> callstack2CallstackItem(String threadKey, List<String> callstackSplit){
		List<CallstackItem> fullCallstackItemList = new ArrayList<CallstackItem>();
		for (int index = 0; index < callstackSplit.size(); index++) {
			String callstackItem = callstackSplit.get(index);
			String dllOffset = NoDllOffset;
			String moduleName = NoModule;
			String APIName = NoAPI;
			String APIOffset = NoAPIOffset;

			/* split dlloffset, apiname and apioffset. e.g.
			* 0x1234(xxx.dll:API)
			* NOAPI 从imageload找到了所属的dll，但是这个dll没有rva（有可能是dll打不开解析错误）
			* NOOAPI 从imageload找到了所属的dll，但是这个dll也有rva，但是在这些rva中找不到对应的API（在第一个rva的前面）
			* NOMODULE 从imageload都找不到所属dll
			* NOMODULE-SYSTEMMODUL 内核模块
			*/
			int indexOfFirstBracket = callstackItem.indexOf("(");
			if (indexOfFirstBracket > -1) {
				dllOffset = callstackItem.substring(0, indexOfFirstBracket);
//				System.out.println(dllOffset);
				
				if (callstackItem.contains("NOMODULE-SYSTEMMODULE")){
					moduleName = KernelModule;					
				} else if (callstackItem.contains("NOMODULE")){
					moduleName = NoModule;
				} else {
					int indexOfColon = callstackItem.lastIndexOf(":");
					int indexOfSlice = callstackItem.lastIndexOf("\\");
					if (indexOfColon > -1 && indexOfSlice > -1) {							
						if (!callstackItem.contains("NOAPI") && !callstackItem.contains("NOOAPI")){
							moduleName = callstackItem.substring(indexOfFirstBracket + 1, indexOfColon);
							APIName = callstackItem.substring(indexOfColon + 1, callstackItem.length() - 1);
						}
						else {
							// NOAPI or NOOAPI
							moduleName = callstackItem.substring(indexOfFirstBracket + 1, indexOfColon);
							APIName = NoAPI;
						}
													
					} else {
						logger.error("None API exist in callstack " + threadKey + " " + callstackItem);
					}
				}			
				
			} else{
				dllOffset = callstackItem;
				logger.error("Callstack fromat exception: not bracket " + threadKey + " " + callstackItem);
			}
								

			CallstackItem currentCallstackItem = new CallstackItem(dllOffset, moduleName, APIName, APIOffset);
			fullCallstackItemList.add(currentCallstackItem);						
		}
		
		return fullCallstackItemList;
	}
	
	
	/**
	 * preprocess callstack for each application only keep the top API and the
	 * callstack above the top API. Note that when there is a callback
	 * function, there will be several sections of exe address, This function
	 * will choose the last(lower) one as the exe callstack. e.g.
	 * exe1->exe2->SetHook->exe3-API, exe1->exe2->SetHook->exe3-API will be extracted rather than exe1->exe2->SetHook.
	 * 
	 * how to judge whether a address is exe address: 
	 * if the length of address < 8 
	 * or
	 * NoModule
	 * 
	 * 
	 * the order of List<CallstackChain>: invoking address -> invoked address,
	 * thus top API will be founded in the last index of List
	 * 
	 * Note that, this function will automatically merge duplicated top API whose callstacks are the same
	 * 
	 * @param filePath
	 * @return
	 * @throws IOException
	 */
	public static Map<String, List<CallstackChain>> extractTopAPIWithExeCallstack(String filePath) throws IOException {
		List<EventRecordWithCallstack> sequence = FileReadingUtil
				.file2EventRecordWithCallstackList(filePath);
		Map<String, List<CallstackChain>> thread2callstacks = new HashMap<String, List<CallstackChain>>();
		int number = 0;
		Map<String, CallstackChain> thread2lastCallstackChain = new HashMap<String, CallstackChain>();
		
		for (EventRecordWithCallstack record : sequence) {
			number++;
			if (number%10000 == 0){
				System.out.println("=============== " + filePath + " " + number);
			}
			String threadKey = record.processID + "@" + record.threadID;
			if (!thread2callstacks.containsKey(threadKey)) {
				thread2callstacks.put(threadKey, new ArrayList<CallstackChain>());
			}

			if (record.callStack == null || record.callStack.trim().isEmpty() || record.callStack.length() < 5)
				continue;

			List<String> callstackSplit = Arrays.asList(record.callStack.split(","));
			List<CallstackItem> topAPICallstackItemList = new ArrayList<CallstackItem>();
			List<CallstackItem> fullCallstackItemList = callstack2CallstackItem(threadKey, callstackSplit);
			
			// extract top api with callstacks
			int topAPIIndex = -1;
			boolean haveExeAddress = false;
			
			for (int i = 0; i < fullCallstackItemList.size(); i++){
				CallstackItem currentCallstackItem = fullCallstackItemList.get(i);
				String lowcaseModuleName = currentCallstackItem.ModuleName.toLowerCase();
				if (currentCallstackItem.DLLOffset.length() < 8 
						|| currentCallstackItem.ModuleName.equals(NoModule))										
				{						
					haveExeAddress = true;					
					break;
				} else{
					
//					|| (
//							!currentCallstackItem.ModuleName.equals(KernelModule)
//							&& !(
//								lowcaseModuleName.contains("\\windows\\system32") 
//								|| lowcaseModuleName.contains("\\windows\\syswow64")
//								|| lowcaseModuleName.contains("\\windows\\winsxs")
//								)
//							)
//					 	)	
					// only extract API in system directory
//					if (
//							(lowcaseModuleName.contains("\\windows\\system32") 
//							|| lowcaseModuleName.contains("\\windows\\syswow64")
//							|| lowcaseModuleName.contains("\\windows\\winsxs")
//							)														
//							&& !currentCallstackItem.APIName.equals(NoAPI)							
//							&& !currentCallstackItem.ModuleName.equals(KernelModule)
//						){
//						topAPIIndex = i;
//					} else {
//						// ignore .net dll and APINameNone
//					}
					
					if(!currentCallstackItem.ModuleName.equals(KernelModule)){
						if (lowcaseModuleName.contains("\\windows\\system32") 
							|| lowcaseModuleName.contains("\\windows\\syswow64")
							|| lowcaseModuleName.contains("\\windows\\winsxs")){
							if (!currentCallstackItem.APIName.equals(NoAPI)){
								topAPIIndex = i;
							} else {
								// ignore no api
							}
						} else {
							// exe address
							haveExeAddress = true;					
							break;
						}
					} else {
						// ignore kernel module
					}
				}
			}
			
			if (haveExeAddress){
				if (topAPIIndex != -1){
					topAPICallstackItemList = fullCallstackItemList.subList(topAPIIndex, fullCallstackItemList.size());					
				} else {
					// exe directly invoke system call
					logger.info("\nexe directly invoke system call " + threadKey + " " + record.eventName + " " + callstackSplit );
					continue;
				}										
			} else {
				// if callstack doesn't contain exe address
				if (topAPIIndex != -1){
					topAPICallstackItemList = fullCallstackItemList.subList(topAPIIndex, fullCallstackItemList.size());
				} else {
					logger.info("\nexe directly invoke system call " + threadKey + " " + record.eventName + " " + callstackSplit );
					continue;
				}
			}			
			
			// current callstackchain, reset api offset of top api
			topAPICallstackItemList.add(new CallstackItem("ROOT", "ROOT", "ROOT", "ROOT"));
			Collections.reverse(topAPICallstackItemList);
			CallstackItem topAPIItem = topAPICallstackItemList.get(topAPICallstackItemList.size()-1);
			topAPIItem.APIOffset = CallstackLoopUtils.NoAPIOffset;
			topAPIItem.DLLOffset = CallstackLoopUtils.NoDllOffset;
			CallstackChain currentCallstackChain = new CallstackChain(topAPICallstackItemList);
			
			// last acllstackchain
			if (thread2lastCallstackChain.containsKey(threadKey)){
				CallstackChain lastCallstackChain = thread2lastCallstackChain.get(threadKey);
				if (lastCallstackChain.equals(currentCallstackChain)){
					// ignore duplicated top API
					continue;
				} else {
					thread2callstacks.get(threadKey).add(currentCallstackChain);
					thread2lastCallstackChain.put(threadKey, currentCallstackChain);
				}
			} else {
				thread2callstacks.get(threadKey).add(currentCallstackChain);
				thread2lastCallstackChain.put(threadKey, currentCallstackChain);
			}
			
		}

		return thread2callstacks;

	}
	
	
	/**
	 * extract top API from a callstack( choose the last item in the callstack)
	 * 
	 * @param thread2callstacks
	 * @return
	 */
	public static Map<String, List<String>> extractTopAPIfromCallstack(Map<String, List<CallstackChain>> thread2callstacks){
		Map<String, List<String>> thread2APIs = new HashMap<String, List<String>>(); 
		for (String threadKey : thread2callstacks.keySet()){
			List<CallstackChain> callstacks = thread2callstacks.get(threadKey);
			for (CallstackChain chain : callstacks){
				CallstackItem lastItem = chain.chains.get(chain.chains.size()-1);
				if (!thread2APIs.containsKey(threadKey))
					thread2APIs.put(threadKey, new ArrayList<String>());					
				String APIName = lastItem.APIName;
				thread2APIs.get(threadKey).add(APIName);				
			}
		}
		return thread2APIs;
	}
	
	/**
	 * extract Top API set sigs for each trace
	 * 
	 * @param traceDirPath
	 * @param phfName
	 * @param traceExtension
	 * @param filteredExtension
	 * @throws IOException 
	 */
	public static void extractTopAPISetSigForEachTrace(String traceDirPath, String phfName, String traceExtension,
			String filteredExtension) throws IOException {
		File dir = new File(traceDirPath + phfName + "/");
		File[] tmpfiles = dir.listFiles(new FilenameFilter() {
			public boolean accept(File dir, String name) {
				return name.endsWith(traceExtension);
			}
		});
		ArrayList<File> files = new ArrayList<File>();
		for (File json : tmpfiles)
			files.add(json);
		Collections.sort(files, new MyComparator());
		
		Set<SetSignature> finalSigSet = new HashSet<SetSignature>();
		Map<Set<String>, SetSignature> sigString2SigResults = new HashMap<Set<String>, SetSignature>();

		for (File json : files) {
			System.out.println(json);
			Map<String, List<CallstackChain>> thread2callstacks = extractTopAPIWithExeCallstack(json.getAbsolutePath());	
			Map<String, List<String>> thread2APIs = CallstackLoopUtils.extractTopAPIfromCallstack(thread2callstacks);
			
			for (String threadKey : thread2APIs.keySet()){
				SetSignature finalSig = new SetSignature();
				Set<String> sig = new HashSet<String>(thread2APIs.get(threadKey));
				Set<String> traces = new HashSet<String>();
				traces.add(json + "_" + threadKey);
				
				if (sig.size() <= 3) // remove sig length <=3
					continue;

				if (sigString2SigResults.containsKey(sig)) {
					sigString2SigResults.get(sig).traces.addAll(traces);
				} else {
					finalSig.sig = sig;
					finalSig.traces = traces;
					finalSigSet.add(finalSig);
					sigString2SigResults.put(sig, finalSig);
				}
				
				
			}

		}

		String outputFilePath = "generatedSignature/" + phfName + filteredExtension;
		FileWriter outputFile = new FileWriter(new File(outputFilePath));
		outputFile.write(new GsonBuilder().setPrettyPrinting().create().toJson(finalSigSet));
		outputFile.close();
		
	}

	/**
	 * extract all possible sequence of a TOP API sequence with loops.
	 * @param concurrentCores 
	 * 
	 * @param traceDirPath
	 * @param phfName
	 * @param traceExtension
	 * @param filteredExtension
	 * @throws IOException
	 */
	public static void extractTopAPISigForEachTrace(int concurrentCores, String traceDirPath, String phfName, String traceExtension,
			String filteredExtension, PossibleResult possibleResult, boolean enableGetCallstackFromLowestAddress) throws IOException {
		File dir = new File(traceDirPath + phfName + "/");
		File[] tmpfiles = dir.listFiles(new FilenameFilter() {
			public boolean accept(File dir, String name) {
				return name.endsWith(traceExtension);
			}
		});
		ArrayList<File> files = new ArrayList<File>();
		for (File json : tmpfiles)
			files.add(json);
		

		List<Future<Set<SigResults>>> list = new ArrayList<Future<Set<SigResults>>>();
		ExecutorService executor = Executors.newFixedThreadPool(concurrentCores);
		for (File json : files) {
			// if (!json.getName().contains("Babylon 1.5."))
			// continue;
			System.out.println(json);
			
			SigGenerator worker = new SigGenerator(enableGetCallstackFromLowestAddress, possibleResult, json);
			Future<Set<SigResults>> future = executor.submit(worker);
			list.add(future);
		}
		
		List<Set<SigResults>> results = new ArrayList<Set<SigResults>>();
		for(Future<Set<SigResults>> fut : list){
            
	        //print the return value of Future, notice the output delay in console
	        // because Future.get() waits for task to get completed
	        try {
	        	Set<SigResults> result = fut.get();
				System.out.println(new Date()+ "::"+ result);
				results.add(result);
			} catch (InterruptedException | ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            
        }
		
        //shut down the executor service now
        executor.shutdown();
        
        // merge 
        Set<SigResults> finalSigSet = new HashSet<SigResults>();
        Map<List<String>, SigResults> sigString2SigResults = new HashMap<List<String>, SigResults>(); 
        for (Set<SigResults> sigResultsSet : results){
        	for (SigResults sigResults : sigResultsSet){
        		List<String> sig = sigResults.sig;
        		if (sigString2SigResults.containsKey(sig)) {
					sigString2SigResults.get(sig).traces.addAll(sigResults.traces);
				} else {					
					finalSigSet.add(sigResults);
					sigString2SigResults.put(sig, sigResults);
				}
        	}
        }

		String outputFilePath = "generatedSignature/" + phfName + filteredExtension;
		FileWriter outputFile = new FileWriter(new File(outputFilePath));
		outputFile.write(new GsonBuilder().setPrettyPrinting().create().toJson(finalSigSet));
		outputFile.close();
	}
	
	

	
	public static enum PossibleResult {All,Longest}
	/**
	 * extract all possible sequence of a SYSCALL/EVENT sequence with loops.
	 * 
	 * @param traceDirPath
	 * @param phfName
	 * @param traceExtension
	 * @param filteredExtension
	 * @throws IOException
	 */
	public static void extractSyscallSigForEachTrace(String traceDirPath, String phfName, String traceExtension,
			String filteredExtension, PossibleResult possibleResult, boolean enableGetCallstackFromLowestAddress) throws IOException {
		File dir = new File(traceDirPath + phfName + "/");
		File[] tmpfiles = dir.listFiles(new FilenameFilter() {
			public boolean accept(File dir, String name) {
				return name.endsWith(traceExtension);
			}
		});
		ArrayList<File> files = new ArrayList<File>();
		for (File json : tmpfiles)
			files.add(json);
		Collections.sort(files, new MyComparator());
		Set<SigResults> finalSigSet = new HashSet<SigResults>();
		Map<List<String>, SigResults> sigString2SigResults = new HashMap<List<String>, SigResults>();

		for (File json : files) {
			System.out.println(json);
			Map<String, List<CallstackChain>> thread2Callstacks = extractFullCallstackWithEvent(json.getAbsolutePath());

			for (String threadKey : thread2Callstacks.keySet()) {
				List<CallstackChain> callstacks = thread2Callstacks.get(threadKey);
				if (callstacks.isEmpty())
					continue;

				
				List<CallstackChain> preprocessedCallstacks = null;
				if (enableGetCallstackFromLowestAddress)
					preprocessedCallstacks = getCallstackListStartFromLowestAddress(callstacks);
				else 
					preprocessedCallstacks = callstacks;
				
				Set<List<CallstackChain>> signaturesWithCallstacks = new HashSet<List<CallstackChain>>();
				if (possibleResult == PossibleResult.All){
					signaturesWithCallstacks = new CallstackFilter<CallstackChain>()
						.getAllPossibleSequenceByMergingLoop(preprocessedCallstacks);
				} else if (possibleResult == PossibleResult.Longest){
					signaturesWithCallstacks.add(new CallstackFilter<CallstackChain>().getOnePossibleSequneceByChoosingLongestLoop(preprocessedCallstacks));
				}
				
				for (List<CallstackChain> sigWithCallstacks : signaturesWithCallstacks) {
					SigResults finalSig = new SigResults();
					Set<String> traces = new HashSet<String>();
					traces.add(json + "_" + threadKey);
					List<String> sig = new ArrayList<String>();
					for (CallstackChain chain : sigWithCallstacks) {
						CallstackItem APIItem = chain.chains.get(chain.chains.size() - 1);
						String[] tmpSplits = APIItem.APIName.split("_");
						String APIName = tmpSplits[tmpSplits.length - 1];
						sig.add(APIName);
					}

					if (sig.size() <= 3) // remove sig length <=3
						continue;

					if (sigString2SigResults.containsKey(sig)) {
						sigString2SigResults.get(sig).traces.addAll(traces);
					} else {
						finalSig.sig = sig;
						finalSig.traces = traces;
						finalSigSet.add(finalSig);
						sigString2SigResults.put(sig, finalSig);
					}
				}
			}
		}

		String outputFilePath = "generatedSignature/" + phfName + filteredExtension;
		FileWriter outputFile = new FileWriter(new File(outputFilePath));
		outputFile.write(new GsonBuilder().setPrettyPrinting().create().toJson(finalSigSet));
		outputFile.close();
	}
	
	
	
	/**
	 * 
	 * Graph Utils
	 * 
	 * 
	 * 
	 */
	
	public static void extractExeControlFlowGraph(String traceDirPath, String phfName, String traceExtension, String graphFileExtend, 
			String dotExePath) throws Exception {
		// Graph<CallstackItem, DefaultEdge> callgraph = new graph
		File dir = new File(traceDirPath + phfName + "/");
		File[] tmpfiles = dir.listFiles(new FilenameFilter() {
			public boolean accept(File dir, String name) {
				return name.endsWith(traceExtension);
			}
		});
		ArrayList<File> files = new ArrayList<File>();
		for (File json : tmpfiles)
			files.add(json);
		Collections.sort(files, new MyComparator());

		for (File json : files) {
			System.out.println(json);
			Map<String, List<CallstackChain>> thread2Callstacks = extractTopAPIWithExeCallstack(json.getAbsolutePath());
//			Map<String, List<CallstackChain>> thread2Callstacks = extractFullCallstackWithEvent(json.getAbsolutePath());
			Map<String, Graph<CallstackItem, DefaultEdge>> thread2graph = new HashMap<String, Graph<CallstackItem, DefaultEdge>>();

			for (String threadKey : thread2Callstacks.keySet()){
				if (!thread2graph.containsKey(threadKey)){
					thread2graph.put(threadKey, new DefaultDirectedGraph<CallstackItem, DefaultEdge>(DefaultEdge.class));
				}				
				Graph<CallstackItem, DefaultEdge> graph = thread2graph.get(threadKey);
				
				for (CallstackChain chain : thread2Callstacks.get(threadKey)){
					CallstackItem lastItem = null;
					for (CallstackItem item : chain.chains){
						if (lastItem == null){
							graph.addVertex(item);
							lastItem = item;
						} else {
							graph.addVertex(item);
							graph.addEdge(lastItem, item);
							lastItem = item;
						}
					}
				}    			
			}
			
			 for (String threadKey : thread2graph.keySet()){
				 exportCallstackItemToDotGraph(traceDirPath, phfName, json.getName(), graphFileExtend, dotExePath, threadKey, thread2graph.get(threadKey));
			 }
		}
	}
	
	
	/**
	 * Generate call graph for each thread
	 * @throws Exception 
	 * @parm maxNumber the max number of events will be draw in the picture
	 * 
	 */
	public static void extractExeCallTree(String traceDirPath, String phfName, String traceExtension,
			String graphFileExtend, String dotExePath, int maxNumber) throws Exception {
		// Graph<CallstackItem, DefaultEdge> callgraph = new graph
		File dir = new File(traceDirPath + phfName + "/");
		File[] tmpfiles = dir.listFiles(new FilenameFilter() {
			public boolean accept(File dir, String name) {
				return name.endsWith(traceExtension);
			}
		});
		ArrayList<File> files = new ArrayList<File>();
		for (File json : tmpfiles)
			files.add(json);
		Collections.sort(files, new MyComparator());

		for (File json : files) {
//			if (!json.getName().contains("Babylon"))
//				continue;
			System.out.println(json);
			Map<String, List<CallstackChain>> thread2Callstacks = extractTopAPIWithExeCallstack(json.getAbsolutePath());
			Map<String, Graph<CallstackItem, DefaultEdge>> thread2graph = new HashMap<String, Graph<CallstackItem, DefaultEdge>>();

			for (String threadKey : thread2Callstacks.keySet()){
				int  count = 0;
				List<CallstackItem> lastCallstackItems = null;
				
				List<CallstackChain> callstacksForEachEvent = thread2Callstacks.get(threadKey);
				if (callstacksForEachEvent.size() > maxNumber)
					callstacksForEachEvent = callstacksForEachEvent.subList(0, maxNumber);
				if (!thread2graph.containsKey(threadKey)){
					thread2graph.put(threadKey, new DefaultDirectedGraph<CallstackItem, DefaultEdge>(DefaultEdge.class));
				}				
				Graph<CallstackItem, DefaultEdge> graph = thread2graph.get(threadKey);
					
				
				// start looping chain for each event
				for (CallstackChain chain : callstacksForEachEvent){
//					System.out.println(chain.chains);
    				int minLengthofCallstack = 0;
    				if (lastCallstackItems != null){
    					minLengthofCallstack = Math.min(lastCallstackItems.size(), chain.chains.size());
    				}
					List<CallstackItem> tmpCallstackItems = new ArrayList<CallstackItem>();

					CallstackItem rootNode = new CallstackItem("ROOT", "ROOT", "ROOT", "ROOT");
					
					// start loop item in the chain
					if (lastCallstackItems == null){
						insertCallstacksIntoGraph(graph, rootNode, chain.chains, count);
						count = count + chain.chains.size();
						tmpCallstackItems = chain.chains;
						lastCallstackItems = chain.chains;
						continue;
					}
					
					int index = 0;
					for (CallstackItem currentCallstackItem : chain.chains){

						if (index < minLengthofCallstack && lastCallstackItems.get(index).DLLOffset.equals(currentCallstackItem.DLLOffset)){
    							currentCallstackItem = lastCallstackItems.get(index);
        						tmpCallstackItems.add(currentCallstackItem);
    					} else {     					
//    						System.out.println(index);
    						List<CallstackItem> insertedCallstacks = chain.chains.subList(index, chain.chains.size());
    						insertCallstacksIntoGraph(graph, lastCallstackItems.get(index-1), insertedCallstacks, count);
       						count = count + insertedCallstacks.size();
       						for (CallstackItem insertedItem : insertedCallstacks){
       							tmpCallstackItems.add(insertedItem);
       						}
       						break;
    					}
						index++;
					}
					
    				lastCallstackItems = tmpCallstackItems;
				}			
				
			}
			
			
			 for (String threadKey : thread2graph.keySet()){
				 exportCallstackItemToDotGraph(traceDirPath, phfName, json.getName(), graphFileExtend, dotExePath, threadKey, thread2graph.get(threadKey));
			 }

		}
	}
	
	static private void exportCallstackItemToDotGraph(String traceDirPath, String phfName, String inputFileName, String graphFileExtend, String dotExePath, String threadKey, Graph graph){
		 DOTExporter<CallstackItem, DefaultEdge> exporter = new DOTExporter<>(new CallstackItemComponentNameProvider(), null, null);
		 File outputDir = new File(traceDirPath + "/" + phfName + "_graph");
		 if (!outputDir.exists())
			 outputDir.mkdirs();
		 String outputFilePath = outputDir + "\\" + inputFileName + "_" + threadKey + graphFileExtend;
		 File outputFile = new File(outputFilePath);
		 try (FileWriter fw = new FileWriter(outputFilePath))
		 {
			 exporter.exportGraph(graph, fw);
			 fw.flush();
			 String cmd = dotExePath + " -Tpdf \"" + outputFile.getAbsolutePath() + "\" -o \"" + outputFile.getAbsolutePath() + ".pdf\"";
	        Runtime run = Runtime.getRuntime();//返回与当前 Java 应用程序相关的运行时对象  
	        try {  
	            Process p = run.exec(cmd);// 启动另一个进程来执行命令  
	            BufferedInputStream in = new BufferedInputStream(p.getInputStream());  
	            BufferedReader inBr = new BufferedReader(new InputStreamReader(in));  
	            String lineStr;  
	            while ((lineStr = inBr.readLine()) != null)  
	                //获得命令执行后在控制台的输出信息  
	                System.out.println(lineStr);// 打印输出信息  
	            //检查命令是否执行失败。  
	            if (p.waitFor() != 0) {  
	                if (p.exitValue() == 1)//p.exitValue()==0表示正常结束，1：非正常结束  
	                    System.err.println("命令执行失败!");  
	            }  
	            inBr.close();  
	            in.close();  
	        } catch (Exception e) {  
	            e.printStackTrace();  
	        }  
		    
			 
		 }
		 catch (IOException e)
		 {
			 e.printStackTrace();
		 }
	}
	
	
	
	public static void insertCallstacksIntoTree(APITreeNode startNode, List<CallstackItem> insertedItems, List<APITreeNode> tmpCallstackItems){
		APITreeNode lastNode = startNode;
		for (CallstackItem item : insertedItems){
			APITreeNode currentNode =  new APITreeNode(item.DLLOffset, item.APIName, item.APIOffset);
			lastNode.sons.add(currentNode);
			lastNode = currentNode;
			tmpCallstackItems.add(currentNode);
		}
		
	}
	
	/**
	 * insert insertedItems to the startNode of the graph
	 * @param startNode
	 * @param insertedItems
	 */
	public static void insertCallstacksIntoGraph(Graph graph, CallstackItem startNode, List<CallstackItem> insertedItems, int startIndex){
		graph.addVertex(startNode);
		CallstackItem lastCallstackItem = startNode;
		for (CallstackItem item : insertedItems){
			item.index = startIndex;
			startIndex++;
			graph.addVertex(item);
			graph.addEdge(lastCallstackItem, item);
			lastCallstackItem = item;
		}
	}
	
	
	public static APITreeNode constructTree(List<CallstackChain> callstackTraces){
		APITreeNode root = new APITreeNode("ROOT","ROOT","ROOT");
		    
		List<APITreeNode> lastCallstackItems = null;
		for (CallstackChain callstackChain : callstackTraces){
			List<APITreeNode> tmpCallstackItems = new ArrayList<APITreeNode>();
			
			// the first callstack
			if (lastCallstackItems == null){ 
				lastCallstackItems = new ArrayList<APITreeNode>();
				insertCallstacksIntoTree(root, callstackChain.chains, tmpCallstackItems);
				lastCallstackItems = tmpCallstackItems;
				continue;
			}
			
			int minLengthofCallstack = 0;
			if (lastCallstackItems != null){
				minLengthofCallstack = Math.min(lastCallstackItems.size(), callstackChain.chains.size());
			}
			
			// start looping callstack chain
			int index = 0;
			for (CallstackItem currentCallstackItem : callstackChain.chains){
				if (index < minLengthofCallstack && lastCallstackItems.get(index).DLLOffset.equals(currentCallstackItem.DLLOffset)){
					APITreeNode currentNode = lastCallstackItems.get(index);
					tmpCallstackItems.add(currentNode);
				} else {     					
					List<CallstackItem> insertedCallstacks = callstackChain.chains.subList(index, callstackChain.chains.size());
					insertCallstacksIntoTree(lastCallstackItems.get(index-1), insertedCallstacks, tmpCallstackItems);
					
					break;
				}    				
				index++;
			}
			
			lastCallstackItems = tmpCallstackItems;
		
		}
		return root;
	}



	
	
	
//	public static Set<List<String>> extractSequenceForFunction(APITreeNode rootNode){
//	String previousNeighborOffset = "000000";
//	
//	// choose the minimal dll offset as the start point
//	int startIndex = 0;
//	int index = 0;
//	for (APITreeNode son : rootNode.sons){
//		if (son.DLLOffset.compareToIgnoreCase(previousNeighborOffset) > 0){
//			previousNeighborOffset = son.DLLOffset;
//		} else {
//			startIndex = index;
//		}
//		index++;
//	}
//	List<APITreeNode> preprocessedSons = rootNode.sons.subList(startIndex, rootNode.sons.size());
//	List<String> preprocessedStringSons = preprocessedSons.stream().map(node->node.DLLOffset).collect(Collectors.toList());
//	return getAllPossibleSequenceByMergingLoop(preprocessedStringSons);
//	
////	for (TreeNode son : preprocessedSons){
////		extractSequence
////	}
//	
//}
}

class CallstackItemComponentNameProvider implements ComponentNameProvider<CallstackItem>{

	@Override
	public String getName(CallstackItem item) {
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append("\"")
					.append(item.APIName).append("_")
					.append(item.DLLOffset).append("_")
					.append(item.APIOffset).append("_")
					.append(item.index)
					.append("\"");

		return stringBuffer.toString();
	}
	
	
}
