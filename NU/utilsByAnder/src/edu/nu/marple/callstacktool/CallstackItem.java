package edu.nu.marple.callstacktool;

public class CallstackItem{
	public String DLLOffset;
	public String APIName;
	public String APIOffset;
	public String ModuleName;
	public int index = Integer.MAX_VALUE;
	public CallstackItem(String dLLOffset, String moduleName, String aPIName, String aPIOffset) {
		super();
		DLLOffset = dLLOffset;
		ModuleName = moduleName;
		APIName = aPIName;
		APIOffset = aPIOffset;
	}
	
	
	
	@Override
	public String toString() {
		return  ModuleName + "_" + APIName  + "_" + DLLOffset + "_" + APIOffset
				+ "_" + index;
	}



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((APIName == null) ? 0 : APIName.hashCode());
		result = prime * result + ((APIOffset == null) ? 0 : APIOffset.hashCode());
		result = prime * result + ((DLLOffset == null) ? 0 : DLLOffset.hashCode());
		result = prime * result + ((ModuleName == null) ? 0 : ModuleName.hashCode());
		result = prime * result + index;
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CallstackItem other = (CallstackItem) obj;
		if (APIName == null) {
			if (other.APIName != null)
				return false;
		} else if (!APIName.equals(other.APIName))
			return false;
		if (APIOffset == null) {
			if (other.APIOffset != null)
				return false;
		} else if (!APIOffset.equals(other.APIOffset))
			return false;
		if (DLLOffset == null) {
			if (other.DLLOffset != null)
				return false;
		} else if (!DLLOffset.equals(other.DLLOffset))
			return false;
		if (ModuleName == null) {
			if (other.ModuleName != null)
				return false;
		} else if (!ModuleName.equals(other.ModuleName))
			return false;
		if (index != other.index)
			return false;
		return true;
	}
	
}