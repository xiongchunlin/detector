package edu.nu.marple.tokenbaseddetection;

import java.io.File;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import edu.nu.marple.Filter;
import edu.nu.marple.GlobalConfig;
import edu.nu.marple.EventRecordG;
import edu.nu.marple.Utils;

public class TokenBasedPreProcessMain {
	public static int numberOfBlocksToKeep = 2;
	public static int minLengthOfConsecutiveBlock = 1;
	public static int maxLengthOfConsecutiveBlock = 500;
	
	public static String[] focusedPhfs = {"keylogger", "remotedesktop", "initialization2"};

	public static void main(String[] args) throws IOException {
		processTrainingSet();
		processTestingSet();
	}

	public static void processTrainingSet() {
		String[] traceDir = {"utilsByCs/initialization2/benignTrain/","utilsByCs/initialization2/ratTrain/"};
		for (String dir : traceDir) {
			try {
				Utils.tracePreProcessForInit(dir, ".output",
						".tracePreprocessed", minLengthOfConsecutiveBlock, maxLengthOfConsecutiveBlock,
						numberOfBlocksToKeep, "importantList/useless_haitao_init.txt", "importantList/usefulevent2.txt");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public static void processTestingSet() {
		String[] traceDir = {"utilsByCs/initialization2/benignTest/","utilsByCs/initialization2/ratTest/"};
		for (String dir : traceDir) {
			try {
				Utils.tracePreProcessForInit(dir, ".output",
						".tracePreprocessed", minLengthOfConsecutiveBlock, maxLengthOfConsecutiveBlock,
						numberOfBlocksToKeep, "importantList/useless_haitao_init.txt", "importantList/usefulevent2.txt");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
