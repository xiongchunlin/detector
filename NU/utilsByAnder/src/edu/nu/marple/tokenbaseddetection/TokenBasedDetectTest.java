package edu.nu.marple.tokenbaseddetection;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import edu.nu.marple.SigResults;
import edu.nu.marple.filemanipulation.FileReadingUtil;

public class TokenBasedDetectTest {
	
	public static String[] focusedPhfs = {"keylogger", "remotedesktop", "initialization"};
	
	public static List<SigResults> signature = new ArrayList<SigResults>();
	
	public static String details = "detectionReportToken/initialization_detectDetails0121_im39.txt";
	public static String results = "detectionReportToken/initialization_detectResult0121_im39.txt";
	public static String signatureFile = "tokenSignature/signature0121_initialization_im39.txt";
	public static String strBenignDir = "utilsByCs/initialization/benignTest";
	public static String strRatDir = "utilsByCs/initialization/ratTest";
	public static String fileExtension = ".output.tracePreprocessed";
	
	public static int check(File traceFile) throws IOException {
		BufferedWriter bw = new BufferedWriter(new FileWriter(details, true));
		bw.write("start with: " + traceFile.getName()); bw.newLine();
		List<String> sequence = FileReadingUtil.file2EventRecordGStringList(traceFile.getAbsolutePath());
		HashSet<String> occured = new HashSet<String>();
		occured.addAll(sequence);
		int result = 0;
		for (SigResults initSignature : signature)
			if (occured.containsAll(initSignature.sig)) {
				bw.write(new GsonBuilder().serializeSpecialFloatingPointValues().setPrettyPrinting().create().toJson(initSignature));
				bw.newLine();
				++result;
			}
		bw.write("matched number: " + String.valueOf(result)); bw.newLine();
		bw.close();
		return result;
	}

	public static void main(String[] args) throws Exception {
//		File file = new File("InitializationPart/INITsignature.txt");
		File file = new File(signatureFile);

		signature = new Gson().fromJson(new FileReader(file), new TypeToken<List<SigResults>>(){}.getType());
		System.out.println("Signature Cnt: " + signature.size());
		int uniqueSyscallNum = 0;
		for (SigResults nowSig : signature) {
			uniqueSyscallNum += nowSig.sig.size();
		}
		System.out.println("Unique Event Cnt: " + uniqueSyscallNum);
		File benignDir = new File(strBenignDir);
		File ratDir = new File(strRatDir);
		File[] listsOfBenignDir = benignDir.listFiles(new FilenameFilter() {
			public boolean accept(File dir, String name) {
				return name.endsWith(fileExtension);
			}
		});
		File[] listsOfRatDir = ratDir.listFiles(new FilenameFilter() {
			public boolean accept(File dir, String name) {
				return name.endsWith(fileExtension);
			}
		});
		int benignCnt = listsOfBenignDir.length, ratCnt = listsOfRatDir.length, benignDetectCnt = 0, ratDetectCnt = 0;
		BufferedWriter bw = new BufferedWriter(new FileWriter(results));
		bw.write("Benign:"); bw.newLine();
		for (File traceFile : listsOfBenignDir) {
			System.out.println("Checking " + traceFile.getPath());
			int matchedNum = check(traceFile);
			if (matchedNum > 0){
				++benignDetectCnt;
				bw.write(traceFile.getName() + ": " + String.valueOf(matchedNum) + "/" + String.valueOf(signature.size()));
			} else {
				bw.write(traceFile.getName() + ": 0" + "/" + String.valueOf(signature.size()));
			}
			bw.newLine();bw.newLine();
		}
		bw.write("False Positive: " + Integer.toString(benignDetectCnt) + "/" + Integer.toString(benignCnt));
		bw.newLine();bw.newLine();
		bw.write("Rat:"); bw.newLine();
		for (File traceFile : listsOfRatDir) {
			int matchedNum = check(traceFile);
			if (matchedNum > 0){
				++ratDetectCnt;
				bw.write(traceFile.getName() + ": " + String.valueOf(matchedNum) + "/" + String.valueOf(signature.size()));
			} else {
				bw.write(traceFile.getName() + ": 0" + "/" + String.valueOf(signature.size()));
			}
			bw.newLine();
		}
		bw.write("Coverage: " + Integer.toString(ratDetectCnt) + "/" + Integer.toString(ratCnt));
		bw.newLine();
		bw.close();
	}

}
