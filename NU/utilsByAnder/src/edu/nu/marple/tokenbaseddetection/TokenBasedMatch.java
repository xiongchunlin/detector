package edu.nu.marple.tokenbaseddetection;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import edu.nu.marple.EventRecordG;
import edu.nu.marple.SigResults;
import edu.nu.marple.filemanipulation.FileReadingUtil;

public class TokenBasedMatch {
//	public String fileName = "";
//	public HashMap<String, ArrayList<SyscallRecord>> bufferedRecords;
//	public static List<SigResults> signature = new ArrayList<SigResults>();
//	public BufferedWriter detectionDetails;
//	
//	public TokenBasedMatch(String fileName, HashMap<String, ArrayList<SyscallRecord>> bufferedRecords) throws JsonIOException, JsonSyntaxException, FileNotFoundException {
//		String[] tmp = fileName.split("\\\\");
//		this.fileName = tmp[tmp.length - 1];
//		this.bufferedRecords = bufferedRecords;
//		File file = new File("initializationPart/INITsignature.txt");
//		signature = new Gson().fromJson(new FileReader(file), new TypeToken<List<SigResults>>(){}.getType());
//	}
//	
//	public int check(List<SyscallRecord> data, String uuid) throws IOException {
//		detectionDetails.write("Start to match process, uuid: " + uuid); 
//		detectionDetails.newLine();
//		List<EventRecordG> sequence = new ArrayList<EventRecordG>();
//		List<Long> timestamp = new ArrayList<Long>();
//		for (SyscallRecord sysRecord : data) {
//			for (String parameter : sysRecord.parameters) {
//				sequence.add(new EventRecordG(sysRecord.name, parameter));
//				timestamp.add(sysRecord.TimestampMicros.get(0));
//			}
//		}
//		HashSet<String> occured = new HashSet<String>();
//		for (EventRecordG tmpRecord : sequence)
//			occured.add(tmpRecord.toString());
//		int result = 0;
//		for (SigResults initSignature : signature)
//			if (occured.containsAll(initSignature.sig)) {
//				detectionDetails.write(new GsonBuilder().setPrettyPrinting().create().toJson(initSignature));
//				detectionDetails.newLine();
//				++result;
//			}
//		detectionDetails.write("matched number: " + String.valueOf(result)); 
//		detectionDetails.newLine();
//		return result;
//	}
//	
//	public boolean match() throws IOException{
//		detectionDetails = new BufferedWriter(new FileWriter("detectionReport/init_detection_details.txt", true));
//		detectionDetails.write("Start to match file, name: " + fileName); 
//		detectionDetails.newLine();
//		boolean res = false;
//		for (String s : bufferedRecords.keySet()){
//			if (bufferedRecords.get(s).isEmpty())
//				continue;
//			if (check(bufferedRecords.get(s), s) > 0) res = true;
//		}
//		detectionDetails.close();
//		return res;
//	}
}
