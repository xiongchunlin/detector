package edu.nu.marple.tokenbaseddetection;

import java.io.File;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import com.google.gson.GsonBuilder;

import edu.nu.marple.SigResults;
import edu.nu.marple.filemanipulation.FileReadingUtil;


public class TokenBasedSignatureGenerationMain {
	
	public static String[] focusedPhfs = {"keylogger", "remotedesktop", "initialization"};
	
	public static HashMap<String, HashSet<Integer>> benignStat = new HashMap<String, HashSet<Integer>>(), ratStat = new HashMap<String, HashSet<Integer>>();
	public static int signatureLen = 5, signatureCnt = 50;
	public static double nowFP = 0.2, factor = 0.85;
	public static ArrayList<SigResults> signature = new ArrayList<SigResults>();
	public static HashSet<String> selected = new HashSet<String>();
	public static HashSet<Integer> coveredBenignTrace = new HashSet<Integer>(), coveredRatTrace = new HashSet<Integer>();
	
	public static String outputFile = "InitializationPart/signature0121_initialization_novalite.txt";
	public static String strBenignDir = "utilsByCs/initialization/benignTrain";
	public static String strRatDir = "utilsByCs/initialization/ratTrain";
	public static String fileExtension = ".output.tracePreprocessed";
	
	public static void main(String[] args) throws IOException {
		//File benignDir = new File("utilsByAnder/traces/initialization2/benignTrain"), ratDir = new File("utilsByAnder/traces/initialization2/ratTrain");
//		File benignDir = new File("utilsByAnder/traces/urldownload/benignTrain"), ratDir = new File("utilsByAnder/traces/urldownload/ratTrain");
		File benignDir = new File(strBenignDir), ratDir = new File(strRatDir);

		File[] listsOfBenignDir = benignDir.listFiles(new FilenameFilter() {
			public boolean accept(File dir, String name) {
				return name.endsWith(fileExtension);
//				return name.endsWith("syscall_args_block500_uselessRunqingNew_filtered");
			}
		});
		File[] listsOfRatDir = ratDir.listFiles(new FilenameFilter() {
			public boolean accept(File dir, String name) {
				return name.endsWith(fileExtension);
//				return name.endsWith("syscall_args_block500_uselessRunqingNew_filtered");
			}
		});
		
		int benignCnt = listsOfBenignDir.length, ratCnt = listsOfRatDir.length;
		System.out.println(benignCnt + " " + ratCnt);
		for (int i = 0; i < ratCnt; ++i) {
			System.out.println("calculating rat " + i);
			List<String> sequence = FileReadingUtil.file2EventRecordGStringList(listsOfRatDir[i].getAbsolutePath());
			for (String eventG : sequence) {
				if (!ratStat.containsKey(eventG)) {
					ratStat.put(eventG, new HashSet<Integer>());
					benignStat.put(eventG, new HashSet<Integer>());
				}
				ratStat.get(eventG).add(i);
			}
			coveredRatTrace.add(i);
		}
		for (int i = 0; i < benignCnt; ++i) {
			System.out.println("calculating benign app " + i);
			List<String> sequence = FileReadingUtil.file2EventRecordGStringList(listsOfBenignDir[i].getAbsolutePath());
			for (String eventG : sequence) {
				if (!benignStat.containsKey(eventG)) continue;
				benignStat.get(eventG).add(i);
			}
			coveredBenignTrace.add(i);
		}
		System.out.println("Finish calculating trace coverage");
		HashSet<Integer> overallBenign = new HashSet<Integer>(), overallRat = new HashSet<Integer>();
		for (int i = 0; i < signatureCnt; ++i) {
			System.out.println("Start with sig " + i);
			HashSet<Integer> coveredBenignTrace = new HashSet<Integer>(), coveredRatTrace = new HashSet<Integer>();
			for (int j = 0; j < benignCnt; ++j) coveredBenignTrace.add(j);
			for (int j = 0; j < ratCnt; ++j) coveredRatTrace.add(j);
			signature.add(new SigResults());
			signature.get(i).coverage = 1.0;
			for (int j = 0; j < signatureLen; ++j) {
				int maxRatCovered = -1, nowBenignCovered = 0;
				double nowMinFP = 1.0;
				HashSet<Integer> ratResult = null, benignResult = null;
				String newaddSyscall = null;
				for (String syscall : ratStat.keySet()) 
				if (!selected.contains(syscall)){
					HashSet<Integer> tmpRatResult = new HashSet<Integer>(ratStat.get(syscall)), tmpBenignResult = new HashSet<Integer>(benignStat.get(syscall));
					tmpRatResult.retainAll(coveredRatTrace);
					tmpBenignResult.retainAll(coveredBenignTrace);
					if ((double) tmpBenignResult.size() / benignCnt < nowFP) {
						if (tmpRatResult.size() > maxRatCovered || (tmpRatResult.size() == maxRatCovered && (double) tmpBenignResult.size() / benignCnt <nowMinFP)) { 
							maxRatCovered = tmpRatResult.size();
							nowBenignCovered = tmpBenignResult.size();
							ratResult = tmpRatResult;
							benignResult = tmpBenignResult;
							newaddSyscall = syscall;
							nowMinFP = (double) tmpBenignResult.size() / benignCnt;
						}
					}
				}
				if (newaddSyscall == null) break;
				coveredRatTrace = ratResult;
				coveredBenignTrace = benignResult;
				signature.get(i).sig.add(newaddSyscall);
				selected.add(newaddSyscall);
				if (nowBenignCovered == 0) break;
				nowFP *= factor;
			}
			signature.get(i).coverage = coveredRatTrace.size();
			signature.get(i).falsePositiveShare = coveredBenignTrace.size();
			if (signature.get(i).sig.size() == 0) {
				signature.remove(i);
				signatureCnt = i;
				System.out.println("No eligible candidate token");
				break;
			}
			System.out.println(coveredRatTrace.size() + " " + coveredBenignTrace.size());
			overallRat.addAll(coveredRatTrace);
			overallBenign.addAll(coveredBenignTrace);
		}
		
		for (SigResults sigRes : signature) {
			sigRes.coverage /= (double) overallRat.size();
			sigRes.falsePositiveShare /= (double) overallBenign.size();
		}
		
//		File initSig = new File("InitializationPart/URLsignature.txt");
		File initSig = new File(outputFile);
		FileWriter initSigWriter = new FileWriter(initSig);
		initSigWriter.write(new GsonBuilder().serializeSpecialFloatingPointValues().setPrettyPrinting().create().toJson(signature));
		initSigWriter.flush();
		initSigWriter.close();
	}
}
