package edu.nu.marple.callstackstatistics;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;

import com.google.gson.Gson;
import com.ibm.marple.EventRecordWithCallstack;

public class DLLDependency {
	
//	static String[] fileDirectory = {"data/withcallstack/benign/trace/"};
	static String[] fileDirectory = {"data/withcallstack/download&exec/trace/",
									 "data/withcallstack/keylogger/trace/",
									 "data/withcallstack/remotedesktop/trace/"};
	
	static HashMap<String, HashSet<String>> callerStat = new HashMap();
	static HashMap<String, HashSet<String>> calleeStat = new HashMap();
	static HashSet<String> dllStat = new HashSet<String>();
	
	private static EventRecordWithCallstack convertEventRecordGJson2Object(String json) {
   		return new Gson().fromJson(json, EventRecordWithCallstack.class);
   	}
	
	public static void main(String[] args) throws IOException {
		//Do the statistics work
		for (String nowDir : fileDirectory) {
			File tmpNowDir = new File(nowDir);
			File[] tmpfiles = tmpNowDir.listFiles(new FilenameFilter() {
				public boolean accept(File dir, String name) {
					return name.endsWith("");
				}
			});
			for (File tmpfile : tmpfiles) {
				System.out.println(tmpfile.getPath());
				BufferedReader br = new BufferedReader(new FileReader(tmpfile));
				String data;
				while ((data = br.readLine()) != null) {
					EventRecordWithCallstack thisRecord = convertEventRecordGJson2Object(data);
					String[] splitResults1 = thisRecord.callStack.split(",");
					ArrayList<String> splitResults2 = new ArrayList<String>();
					boolean stage1 = false;
					for (String field : splitResults1) { 
						if (!stage1) {
							if (!field.startsWith("fffff")) {
								splitResults2.add(field);
								stage1 = true;
							}
						} else
						if (field.contains("(")) {
							splitResults2.add(field);
						} else
							break;
					}
					
					
//					for (String field : splitResults2)
//						System.out.print(field + ",");
//					System.out.println("");
					
					String lastField = "";
					for (String field : splitResults2) 
					if (field.contains("(") && field.contains(")")){
//						field = field.substring(field.indexOf("Windows"));
						field = field.substring(field.indexOf("HarddiskVolume1") + 15);
						field = field.split(":")[0];
						dllStat.add(field);
						if (!lastField.equals("")) {
							if (!callerStat.containsKey(field)) callerStat.put(field, new HashSet<String>());
							if (!calleeStat.containsKey(lastField)) calleeStat.put(lastField, new HashSet<String>());
							callerStat.get(field).add(lastField);
							calleeStat.get(lastField).add(field);
						}
						lastField = field;
					} else
						lastField = "";
				}
				br.close();
			}
		}
		
		//Print
		BufferedWriter bw = new BufferedWriter(new FileWriter("callstackStat/DLL/DLLdependency.txt"));
		BufferedWriter bw2 = new BufferedWriter(new FileWriter("callstackStat/DLL/DLLdependency_0inedge.txt"));
		BufferedWriter bw3 = new BufferedWriter(new FileWriter("callstackStat/DLL/DLLdependency_0outedge.txt"));
		BufferedWriter bw4 = new BufferedWriter(new FileWriter("callstackStat/DLL/DLLStat.txt"));
		ArrayList<String> keyList1 = new ArrayList<String>();
		ArrayList<String> keyList2 = new ArrayList<String>();
		ArrayList<String> keyList3 = new ArrayList<String>();
		for (String key : callerStat.keySet())
			keyList1.add(key);
		Collections.sort(keyList1);
		for (String key : calleeStat.keySet())
			keyList2.add(key);
		Collections.sort(keyList2);
		for (String key : keyList1) {
			//bw.write(calleeStat.get(key).toString());
			//bw.newLine();
			bw.write(key);
			bw.newLine();
			bw.write(callerStat.get(key).toString());
			bw.newLine();
			bw.newLine();
			if (!calleeStat.containsKey(key)) {
				bw2.write(key);
				bw2.newLine();
			} else {
				calleeStat.get(key).remove(key);
				if (calleeStat.get(key).size() == 0) {
					bw2.write(key);
					bw2.newLine();
				}
			}
				
		}
		for (String key : keyList2) {
			if (!callerStat.containsKey(key)) {
				bw3.write(key);
				bw3.newLine();
			} else {
				callerStat.get(key).remove(key);
				if (callerStat.get(key).size() == 0) {
					bw3.write(key);
					bw3.newLine();
				}
			}
				
		}
		for (String key : dllStat)
			keyList3.add(key);
		Collections.sort(keyList3);
		for (String key : keyList3) {
			bw4.write(key);
			bw4.newLine();
		}
		bw.close();
		bw2.close();
		bw3.close();
		bw4.close();
	}
}
