package edu.nu.marple.callstackstatistics;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;

import com.google.gson.Gson;
import com.ibm.marple.EventRecordWithCallstack;

import edu.nu.marple.callstackbasics.EventRecordWithCallstackG;

public class APIDifficultyMeasurementMain {
	
	static String[] fileDirectory = {"data/withcallstack/benign/",
									 "data/withcallstack/download&exec/",
									 "data/withcallstack/keylogger/",
									 "data/withcallstack/remoteshell/",
									 "data/withcallstack/audiorecord/",
									 "data/withcallstack/remotedesktop/"};
	
	static HashMap<String, TreeNode2> roots = new HashMap<String, TreeNode2>();
	static HashMap<String, Integer> APIcapabilitySum = new HashMap<String, Integer>();
	static HashMap<String, Integer> APIcapabilityCount = new HashMap<String, Integer>();
	static HashMap<String, Integer> APIcapabilityMax = new HashMap<String, Integer>();
	static HashMap<String, Integer> APIcapabilityMin = new HashMap<String, Integer>();
	static HashSet<String> branchAPI = new HashSet<String>();
	static HashSet<String> intermediateBranchAPI = new HashSet<String>();
	
	static HashMap<String, Integer> maliciousOverallAPIcapabilitySum = new HashMap<String, Integer>();
	static HashMap<String, Integer> maliciousOverallAPIcapabilityCount = new HashMap<String, Integer>();
	static HashMap<String, Integer> maliciousOverallAPIcapabilityMax = new HashMap<String, Integer>();
	static HashMap<String, Integer> maliciousOverallAPIcapabilityMin = new HashMap<String, Integer>();
	static HashMap<String, Integer> benignOverallAPIcapabilitySum = new HashMap<String, Integer>();
	static HashMap<String, Integer> benignOverallAPIcapabilityCount = new HashMap<String, Integer>();
	static HashMap<String, Integer> benignOverallAPIcapabilityMax = new HashMap<String, Integer>();
	static HashMap<String, Integer> benignOverallAPIcapabilityMin = new HashMap<String, Integer>();
	static HashSet<String> maliciousBranchAPI = new HashSet<String>();
	static HashSet<String> maliciousIntermediateBranchAPI = new HashSet<String>();
	static HashSet<String> benignBranchAPI = new HashSet<String>();
	static HashSet<String> benignIntermediateBranchAPI = new HashSet<String>();
	
	
	private static EventRecordWithCallstackG convertEventRecordGJson2Object(String json) {
   		return new Gson().fromJson(json, EventRecordWithCallstackG.class);
   	}
	
	private static void calculateDifficulty(TreeNode2 itrNode) {
		if (itrNode.sons.size() == 0) {
			itrNode.capability = 0;
		} else {
			HashSet<String> differentAPIs = new HashSet<String>();
			for (TreeNode2 tmpNode : itrNode.sons) {
				differentAPIs.add(tmpNode.API);
				calculateDifficulty(tmpNode);
//				itrNode.capability += tmpNode.capability;
			}
			itrNode.capability += differentAPIs.size();
		}
		if (!APIcapabilityCount.containsKey(itrNode.API)) {
			APIcapabilityCount.put(itrNode.API, 0);
			APIcapabilitySum.put(itrNode.API, 0);
			APIcapabilityMax.put(itrNode.API, 0);
			APIcapabilityMin.put(itrNode.API, 0);
		}
		APIcapabilityCount.put(itrNode.API, APIcapabilityCount.get(itrNode.API) + 1);
		APIcapabilitySum.put(itrNode.API, APIcapabilitySum.get(itrNode.API) + itrNode.capability);
		if (itrNode.capability > APIcapabilityMax.get(itrNode.API) || APIcapabilityMax.get(itrNode.API) == 0) APIcapabilityMax.put(itrNode.API, itrNode.capability);
		if (itrNode.capability < APIcapabilityMin.get(itrNode.API) || APIcapabilityMin.get(itrNode.API) == 0) APIcapabilityMin.put(itrNode.API, itrNode.capability);
	}
	
	public static void main(String[] args) throws IOException {
		//Do the statistics work
		for (String nowDir : fileDirectory) {
			File tmpNowDir = new File(nowDir);
			File[] tmpfiles = tmpNowDir.listFiles(new FilenameFilter() {
				public boolean accept(File dir, String name) {
					return name.endsWith(".callstackoutput.tracePreprocessed");
				}
			});
			for (File tmpfile : tmpfiles) {
				//initializaiton
				roots = new HashMap<String, TreeNode2>();
				APIcapabilitySum = new HashMap<String, Integer>();
				APIcapabilityCount = new HashMap<String, Integer>();
				APIcapabilityMax = new HashMap<String, Integer>();
				APIcapabilityMin = new HashMap<String, Integer>();
				intermediateBranchAPI = new HashSet<String>();
				branchAPI = new HashSet<String>();
				System.out.println(tmpfile.getPath());
				
				//input and pick up branch APIs
				BufferedReader br = new BufferedReader(new FileReader(tmpfile));
				String data;
				while ((data = br.readLine()) != null) {
					EventRecordWithCallstackG thisRecord = convertEventRecordGJson2Object(data);
					String[] splitResults = thisRecord.systemCallStack.split(",");
					if (!roots.containsKey(thisRecord.threadID)) {
						roots.put(thisRecord.threadID, new TreeNode2());
					}
					if (thisRecord.eventName.equals("PerfInfoSysClEnter")) {
						TreeNode2 itrNode = roots.get(thisRecord.threadID);
						for (int i = splitResults.length - 1; i >= 0; --i) {
							if (i == 0 || i == splitResults.length - 1) {
								branchAPI.add(splitResults[i]);
							}
							if (itrNode.sons.size() == 0) {
								TreeNode2 newNode = new TreeNode2();
								newNode.API = splitResults[i];
								itrNode.sons.add(newNode);
							} else
							if (!itrNode.sons.get(itrNode.sons.size() - 1).API.equals(splitResults[i])) {
								TreeNode2 newNode = new TreeNode2();
								newNode.API = splitResults[i];
								itrNode.sons.add(newNode);
								branchAPI.add(itrNode.API);
								if (i != 0 && i != splitResults.length - 1) intermediateBranchAPI.add(itrNode.API);
							}
							itrNode = itrNode.sons.get(itrNode.sons.size() - 1);
							if (i == 0) {
								if (itrNode.sons.size() == 0 || !itrNode.sons.get(itrNode.sons.size() - 1).API.equals(splitResults[i])) {
									TreeNode2 newNode = new TreeNode2();
									newNode.API = thisRecord.parameter;
									itrNode.sons.add(newNode);
								}
							}
						}
					}
				}
				br.close();
				//calculate capability
				for (String key : roots.keySet()) {
					calculateDifficulty(roots.get(key));
				}
				
				//all capability output
				ArrayList<Res2> finalRes = new ArrayList<Res2>();
				for (String key : APIcapabilityCount.keySet()) 
				if (!key.equals("")) {
					finalRes.add(new Res2(key, (double)APIcapabilitySum.get(key) / APIcapabilityCount.get(key), APIcapabilityMax.get(key), APIcapabilityMin.get(key)));
				}
				
				Collections.sort(finalRes, new MyComparator2());
				
				BufferedWriter bw = new BufferedWriter(new FileWriter(tmpfile.getPath() + ".dif"));
				for (Res2 itrRes : finalRes) {
					bw.write(itrRes.API + " " + String.valueOf(itrRes.capabilityAvg) + " " + String.valueOf(itrRes.capabilityMax) + " " + String.valueOf(itrRes.capabilityMin));
					bw.newLine();
				}
				bw.close();
				
				//branch capbility output
				finalRes = new ArrayList<Res2>();
				for (String key : branchAPI) 
				if (!key.equals("")) {
					finalRes.add(new Res2(key, (double)APIcapabilitySum.get(key) / APIcapabilityCount.get(key), APIcapabilityMax.get(key), APIcapabilityMin.get(key)));
				}
				
				Collections.sort(finalRes, new MyComparator2());
				
				bw = new BufferedWriter(new FileWriter(tmpfile.getPath() + ".branchdif"));
				for (Res2 itrRes : finalRes) {
					bw.write(itrRes.API + " " + String.valueOf(itrRes.capabilityAvg) + " " + String.valueOf(itrRes.capabilityMax) + " " + String.valueOf(itrRes.capabilityMin));
					bw.newLine();
				}
				bw.close();
				
				//intermediate branch capbility output
				finalRes = new ArrayList<Res2>();
				for (String key : intermediateBranchAPI) 
				if (!key.equals("")) {
					finalRes.add(new Res2(key, (double)APIcapabilitySum.get(key) / APIcapabilityCount.get(key), APIcapabilityMax.get(key), APIcapabilityMin.get(key)));
				}
				
				Collections.sort(finalRes, new MyComparator2());
				
				bw = new BufferedWriter(new FileWriter(tmpfile.getPath() + ".interbranchdif"));
				for (Res2 itrRes : finalRes) {
					bw.write(itrRes.API + " " + String.valueOf(itrRes.capabilityAvg) + " " + String.valueOf(itrRes.capabilityMax) + " " + String.valueOf(itrRes.capabilityMin));
					bw.newLine();
				}
				bw.close();
				
				//make statistics for malicious files and benign files
				if (nowDir.equals("data/withcallstack/benign/")) {
					benignBranchAPI.addAll(branchAPI);
					benignIntermediateBranchAPI.addAll(intermediateBranchAPI);
					for (String key : APIcapabilitySum.keySet()) {
						if (benignOverallAPIcapabilitySum.containsKey(key)) {
							benignOverallAPIcapabilitySum.put(key, benignOverallAPIcapabilitySum.get(key) + APIcapabilitySum.get(key));
						} else {
							benignOverallAPIcapabilitySum.put(key, APIcapabilitySum.get(key));
						}
						
						if (benignOverallAPIcapabilityCount.containsKey(key)) {
							benignOverallAPIcapabilityCount.put(key, benignOverallAPIcapabilityCount.get(key) + APIcapabilityCount.get(key));
						} else {
							benignOverallAPIcapabilityCount.put(key, APIcapabilityCount.get(key));
						}
						
						if (benignOverallAPIcapabilityMax.containsKey(key)) {
							benignOverallAPIcapabilityMax.put(key, Math.max(benignOverallAPIcapabilityMax.get(key), APIcapabilityMax.get(key)));
						} else {
							benignOverallAPIcapabilityMax.put(key, APIcapabilityMax.get(key));
						}
						
						if (benignOverallAPIcapabilityMin.containsKey(key)) {
							benignOverallAPIcapabilityMin.put(key, Math.min(benignOverallAPIcapabilityMin.get(key), APIcapabilityMin.get(key)));
						} else {
							benignOverallAPIcapabilityMin.put(key, APIcapabilityMin.get(key));
						}
					}
				} else {
					maliciousBranchAPI.addAll(branchAPI);
					maliciousIntermediateBranchAPI.addAll(intermediateBranchAPI);
					for (String key : APIcapabilitySum.keySet()) {
						if (maliciousOverallAPIcapabilitySum.containsKey(key)) {
							maliciousOverallAPIcapabilitySum.put(key, maliciousOverallAPIcapabilitySum.get(key) + APIcapabilitySum.get(key));
						} else {
							maliciousOverallAPIcapabilitySum.put(key, APIcapabilitySum.get(key));
						}
						
						if (maliciousOverallAPIcapabilityCount.containsKey(key)) {
							maliciousOverallAPIcapabilityCount.put(key, maliciousOverallAPIcapabilityCount.get(key) + APIcapabilityCount.get(key));
						} else {
							maliciousOverallAPIcapabilityCount.put(key, APIcapabilityCount.get(key));
						}
						
						if (maliciousOverallAPIcapabilityMax.containsKey(key)) {
							maliciousOverallAPIcapabilityMax.put(key, Math.max(maliciousOverallAPIcapabilityMax.get(key), APIcapabilityMax.get(key)));
						} else {
							maliciousOverallAPIcapabilityMax.put(key, APIcapabilityMax.get(key));
						}
						
						if (maliciousOverallAPIcapabilityMin.containsKey(key)) {
							maliciousOverallAPIcapabilityMin.put(key, Math.min(maliciousOverallAPIcapabilityMin.get(key), APIcapabilityMin.get(key)));
						} else {
							maliciousOverallAPIcapabilityMin.put(key, APIcapabilityMin.get(key));
						}
					}
				}
			}
		}
		//all benign capability output
		ArrayList<Res2> finalResBenign = new ArrayList<Res2>();
		for (String key : benignOverallAPIcapabilityCount.keySet()) 
		if (!key.equals("")) {
			finalResBenign.add(new Res2(key, (double)benignOverallAPIcapabilitySum.get(key) / benignOverallAPIcapabilityCount.get(key), benignOverallAPIcapabilityMax.get(key), benignOverallAPIcapabilityMin.get(key)));
		}
		Collections.sort(finalResBenign, new MyComparator2());
		
		//all malicious capability output
		ArrayList<Res2> finalResMalicious = new ArrayList<Res2>();
		for (String key : maliciousOverallAPIcapabilityCount.keySet()) 
		if (!key.equals("")) {
			finalResMalicious.add(new Res2(key, (double)maliciousOverallAPIcapabilitySum.get(key) / maliciousOverallAPIcapabilityCount.get(key), maliciousOverallAPIcapabilityMax.get(key), maliciousOverallAPIcapabilityMin.get(key)));
		}
		Collections.sort(finalResMalicious, new MyComparator2());
		
		//dif output
		BufferedWriter bw = new BufferedWriter(new FileWriter("callstackStat/overall_benign_all.dif"));
		BufferedWriter bw2 = new BufferedWriter(new FileWriter("callstackStat/overall_benign.dif"));
		for (Res2 itrRes : finalResBenign) {
			bw.write(itrRes.API + " " + String.valueOf(itrRes.capabilityAvg) + " " + String.valueOf(itrRes.capabilityMax) + " " + String.valueOf(itrRes.capabilityMin) + " ");
			if (maliciousOverallAPIcapabilitySum.containsKey(itrRes.API)) {
				bw.write(String.valueOf((double)maliciousOverallAPIcapabilitySum.get(itrRes.API) / maliciousOverallAPIcapabilityCount.get(itrRes.API)) + " " + String.valueOf(maliciousOverallAPIcapabilityMax.get(itrRes.API)) + " " + String.valueOf(maliciousOverallAPIcapabilityMin.get(itrRes.API)));
				bw2.write(itrRes.API + " " + String.valueOf(itrRes.capabilityAvg) + " " + String.valueOf(itrRes.capabilityMax) + " " + String.valueOf(itrRes.capabilityMin) + " " + String.valueOf((double)maliciousOverallAPIcapabilitySum.get(itrRes.API) / maliciousOverallAPIcapabilityCount.get(itrRes.API)) + " " + String.valueOf(maliciousOverallAPIcapabilityMax.get(itrRes.API)) + " " + String.valueOf(maliciousOverallAPIcapabilityMin.get(itrRes.API)));
				bw2.newLine();
			} else
			{
				bw.write("null null null");
			}
			bw.newLine();
		}
		bw.close();
		bw2.close();
		
		bw = new BufferedWriter(new FileWriter("callstackStat/overall_malicious_all.dif"));
		bw2 = new BufferedWriter(new FileWriter("callstackStat/overall_malicious.dif"));
		for (Res2 itrRes : finalResMalicious) {
			bw.write(itrRes.API + " " + String.valueOf(itrRes.capabilityAvg) + " " + String.valueOf(itrRes.capabilityMax) + " " + String.valueOf(itrRes.capabilityMin) + " ");
			if (benignOverallAPIcapabilitySum.containsKey(itrRes.API)) {
				bw.write(String.valueOf((double)benignOverallAPIcapabilitySum.get(itrRes.API) / benignOverallAPIcapabilityCount.get(itrRes.API)) + " " + String.valueOf(benignOverallAPIcapabilityMax.get(itrRes.API)) + " " + String.valueOf(benignOverallAPIcapabilityMin.get(itrRes.API)));
				bw2.write(itrRes.API + " " + String.valueOf(itrRes.capabilityAvg) + " " + String.valueOf(itrRes.capabilityMax) + " " + String.valueOf(itrRes.capabilityMin) + " " + String.valueOf((double)benignOverallAPIcapabilitySum.get(itrRes.API) / benignOverallAPIcapabilityCount.get(itrRes.API)) + " " + String.valueOf(benignOverallAPIcapabilityMax.get(itrRes.API)) + " " + String.valueOf(benignOverallAPIcapabilityMin.get(itrRes.API)));
				bw2.newLine();
			} else
			{
				bw.write("null null null");
			}
			bw.newLine();
		}
		bw.close();
		bw2.close();
		
		//branchdif output
		bw = new BufferedWriter(new FileWriter("callstackStat/overall_benign_all.branchdif"));
		bw2 = new BufferedWriter(new FileWriter("callstackStat/overall_benign.branchdif"));
		for (Res2 itrRes : finalResBenign) 
		if (benignBranchAPI.contains(itrRes.API)){
			bw.write(itrRes.API + " " + String.valueOf(itrRes.capabilityAvg) + " " + String.valueOf(itrRes.capabilityMax) + " " + String.valueOf(itrRes.capabilityMin) + " ");
			if (maliciousOverallAPIcapabilitySum.containsKey(itrRes.API)) {
				bw.write(String.valueOf((double)maliciousOverallAPIcapabilitySum.get(itrRes.API) / maliciousOverallAPIcapabilityCount.get(itrRes.API)) + " " + String.valueOf(maliciousOverallAPIcapabilityMax.get(itrRes.API)) + " " + String.valueOf(maliciousOverallAPIcapabilityMin.get(itrRes.API)));
				bw2.write(itrRes.API + " " + String.valueOf(itrRes.capabilityAvg) + " " + String.valueOf(itrRes.capabilityMax) + " " + String.valueOf(itrRes.capabilityMin) + " " + String.valueOf((double)maliciousOverallAPIcapabilitySum.get(itrRes.API) / maliciousOverallAPIcapabilityCount.get(itrRes.API)) + " " + String.valueOf(maliciousOverallAPIcapabilityMax.get(itrRes.API)) + " " + String.valueOf(maliciousOverallAPIcapabilityMin.get(itrRes.API)));
				bw2.newLine();
			} else
			{
				bw.write("null null null");
			}
			bw.newLine();
		}
		bw.close();
		bw2.close();
		
		bw = new BufferedWriter(new FileWriter("callstackStat/overall_malicious_all.branchdif"));
		bw2 = new BufferedWriter(new FileWriter("callstackStat/overall_malicious.branchdif"));
		for (Res2 itrRes : finalResMalicious) 
		if (maliciousBranchAPI.contains(itrRes.API)){
			bw.write(itrRes.API + " " + String.valueOf(itrRes.capabilityAvg) + " " + String.valueOf(itrRes.capabilityMax) + " " + String.valueOf(itrRes.capabilityMin) + " ");
			if (benignOverallAPIcapabilitySum.containsKey(itrRes.API)) {
				bw.write(String.valueOf((double)benignOverallAPIcapabilitySum.get(itrRes.API) / benignOverallAPIcapabilityCount.get(itrRes.API)) + " " + String.valueOf(benignOverallAPIcapabilityMax.get(itrRes.API)) + " " + String.valueOf(benignOverallAPIcapabilityMin.get(itrRes.API)));
				bw2.write(itrRes.API + " " + String.valueOf(itrRes.capabilityAvg) + " " + String.valueOf(itrRes.capabilityMax) + " " + String.valueOf(itrRes.capabilityMin) + " " + String.valueOf((double)benignOverallAPIcapabilitySum.get(itrRes.API) / benignOverallAPIcapabilityCount.get(itrRes.API)) + " " + String.valueOf(benignOverallAPIcapabilityMax.get(itrRes.API)) + " " + String.valueOf(benignOverallAPIcapabilityMin.get(itrRes.API)));
				bw2.newLine();
			} else
			{
				bw.write("null null null");
			}
			bw.newLine();
		}
		bw.close();
		bw2.close();
		
		//intermediatebranchcap output
		bw = new BufferedWriter(new FileWriter("callstackStat/overall_benign_all.interbranchdif"));
		bw2 = new BufferedWriter(new FileWriter("callstackStat/overall_benign.interbranchdif"));
		for (Res2 itrRes : finalResBenign) 
		if (benignIntermediateBranchAPI.contains(itrRes.API)){
			bw.write(itrRes.API + " " + String.valueOf(itrRes.capabilityAvg) + " " + String.valueOf(itrRes.capabilityMax) + " " + String.valueOf(itrRes.capabilityMin) + " ");
			if (maliciousOverallAPIcapabilitySum.containsKey(itrRes.API)) {
				bw.write(String.valueOf((double)maliciousOverallAPIcapabilitySum.get(itrRes.API) / maliciousOverallAPIcapabilityCount.get(itrRes.API)) + " " + String.valueOf(maliciousOverallAPIcapabilityMax.get(itrRes.API)) + " " + String.valueOf(maliciousOverallAPIcapabilityMin.get(itrRes.API)));
				bw2.write(itrRes.API + " " + String.valueOf(itrRes.capabilityAvg) + " " + String.valueOf(itrRes.capabilityMax) + " " + String.valueOf(itrRes.capabilityMin) + " " + String.valueOf((double)maliciousOverallAPIcapabilitySum.get(itrRes.API) / maliciousOverallAPIcapabilityCount.get(itrRes.API)) + " " + String.valueOf(maliciousOverallAPIcapabilityMax.get(itrRes.API)) + " " + String.valueOf(maliciousOverallAPIcapabilityMin.get(itrRes.API)));
				bw2.newLine();
			} else
			{
				bw.write("null null null");
			}
			bw.newLine();
		}
		bw.close();
		bw2.close();
		
		bw = new BufferedWriter(new FileWriter("callstackStat/overall_malicious_all.interbranchdif"));
		bw2 = new BufferedWriter(new FileWriter("callstackStat/overall_malicious.interbranchdif"));
		for (Res2 itrRes : finalResMalicious) 
		if (maliciousIntermediateBranchAPI.contains(itrRes.API)){
			bw.write(itrRes.API + " " + String.valueOf(itrRes.capabilityAvg) + " " + String.valueOf(itrRes.capabilityMax) + " " + String.valueOf(itrRes.capabilityMin) + " ");
			if (benignOverallAPIcapabilitySum.containsKey(itrRes.API)) {
				bw.write(String.valueOf((double)benignOverallAPIcapabilitySum.get(itrRes.API) / benignOverallAPIcapabilityCount.get(itrRes.API)) + " " + String.valueOf(benignOverallAPIcapabilityMax.get(itrRes.API)) + " " + String.valueOf(benignOverallAPIcapabilityMin.get(itrRes.API)));
				bw2.write(itrRes.API + " " + String.valueOf(itrRes.capabilityAvg) + " " + String.valueOf(itrRes.capabilityMax) + " " + String.valueOf(itrRes.capabilityMin) + " " + String.valueOf((double)benignOverallAPIcapabilitySum.get(itrRes.API) / benignOverallAPIcapabilityCount.get(itrRes.API)) + " " + String.valueOf(benignOverallAPIcapabilityMax.get(itrRes.API)) + " " + String.valueOf(benignOverallAPIcapabilityMin.get(itrRes.API)));
				bw2.newLine();
			} else
			{
				bw.write("null null null");
			}
			bw.newLine();
		}
		bw.close();
		bw2.close();
	}
}

class TreeNode2 {
	String API;
	ArrayList<TreeNode2> sons;
	Integer capability;
	
	public TreeNode2() {
		API = "";
		sons = new ArrayList<TreeNode2>();
		capability = 0;
	}
}

class Res2 {
	public String API;
	Double capabilityAvg;
	Integer capabilityMax;
	Integer capabilityMin;
	
	public Res2() {
		
	}
	
	public Res2(String API, Double capabilityAvg, Integer capabilityMax, Integer capabilityMin) {
		this.API = API;
		this.capabilityAvg = capabilityAvg;
		this.capabilityMax = capabilityMax;
		this.capabilityMin = capabilityMin;
	}
}

class MyComparator2 implements Comparator {
	@Override
	public int compare(Object o1, Object o2) {
		Res2 Res1 = (Res2) o1;
		Res2 Res2 = (Res2) o2;
		if (Res1.capabilityAvg > Res2.capabilityAvg)
			return -1;
		else if (Res1.capabilityAvg < Res2.capabilityAvg) {
			return 1;
		} else
			return 0;
	}
}