package edu.nu.marple.callstackstatistics;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;

import com.google.gson.Gson;
import com.ibm.marple.EventRecordWithCallstack;

public class SyscalEnterExitChecking {
	
//	static String[] fileDirectory = {"data/withcallstack/benign/trace/"};
	static String[] fileDirectory = {"data/withcallstack/download&exec/",
									 "data/withcallstack/keylogger/",
									 "data/withcallstack/remoteshell/",
									 "data/withcallstack/audiorecord/",
									 "data/withcallstack/remotedesktop/"};
	
	private static EventRecordWithCallstack convertEventRecordGJson2Object(String json) {
   		return new Gson().fromJson(json, EventRecordWithCallstack.class);
   	}
	
	public static void main(String[] args) throws IOException {
		//Do the statistics work
		for (String nowDir : fileDirectory) {
			File tmpNowDir = new File(nowDir);
			File[] tmpfiles = tmpNowDir.listFiles(new FilenameFilter() {
				public boolean accept(File dir, String name) {
					return name.endsWith(".callstackoutput");
				}
			});
			for (File tmpfile : tmpfiles) {
				System.out.println(tmpfile.getPath());
				BufferedReader br = new BufferedReader(new FileReader(tmpfile));
				String data;
				int lineCnt = 0;
				int check = 0;
				boolean flag = false;
				while ((data = br.readLine()) != null) {
					++lineCnt;
					EventRecordWithCallstack thisRecord = convertEventRecordGJson2Object(data);
					if (thisRecord.eventName.equals("PerfInfoSysClEnter") &&
						!thisRecord.arguments.get("SystemCall").equals("NtCallbackReturn"))
						++check;
					else 
					if (thisRecord.eventName.equals("PerfInfoSysClExit"))
						--check;
					if (check < 0 || check > 1) {
						System.out.println("Fail in " + tmpfile.getPath() + " on Line " + lineCnt);
						flag = true;
						break;
					}
				}
				if (!flag) System.out.println("Succeed in " + tmpfile.getPath());
				br.close();
			}
		}
	}
}


