package edu.nu.marple.callstackstatistics;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.ibm.marple.EventRecordWithCallstack;

import edu.nu.marple.SigResults;
import edu.nu.marple.callstackbasics.EventRecordWithCallstackG;
import edu.nu.marple.callstacktool.CallstackFilter;
import edu.nu.marple.callstacktool.CallstackUtils;

/**
 * 
 * @author ander
 * Get the statistics about how many APIs we have in all .callstackoutput data we have.
 */

public class APIParaStatisticsMain {
	
	public static HashSet<String> APISet = new HashSet<String>();
	public static String uselessSyscallFilePath = "importantList/useless_e4.txt";
	public static String usefulEventFilePath = "importantList/usefulevent3.txt";
	
	static HashSet<String> uselessSyscalls = new HashSet<String>();
	static HashSet<String> usefulEvents = new HashSet<String>();
	static CallstackFilter filter = new CallstackFilter();
	
	public static void processOnSingleFile(File itrFile) throws JsonSyntaxException, IOException {
		BufferedReader br = new BufferedReader(new FileReader(itrFile));
		String data;
		int cnt = 0;
		while ((data = br.readLine()) != null) {
			++cnt;
			EventRecordWithCallstack eventRecord = new Gson().fromJson(data, EventRecordWithCallstack.class);
			if (eventRecord.eventName.equals("PerfInfoSysClEnter") ||
					eventRecord.eventName.equals("ALPCALPC-Unwait")
					|| eventRecord.eventName.equals("ALPCALPC-Wait-For-Reply")
					|| eventRecord.eventName.equals("DiskIoFlushBuffers")
					|| eventRecord.eventName.equals("PerfInfoSysClExit")) {
				continue;
			}
			if (!usefulEvents.contains(eventRecord.eventName)) {
				continue;
			}
			EventRecordWithCallstackG resDataRecord = CallstackUtils.argPreProcessOnSingleRecord(eventRecord, false);
			if (resDataRecord == null)
				continue;
			String[] splitResult1 = resDataRecord.parameter.split(" @@ ");
			for (String itrField : splitResult1) {
				String[] splitResult2 = itrField.split(":");
				if (splitResult2.length < 2) {
					System.out.println("weird record in " + itrFile.getPath() + " on line " + cnt);
				} else {
					APISet.add(splitResult2[1]);
				}
			}
		}
		
		br.close();
	}
 	
	public static void recursivelyCount(String path) throws JsonSyntaxException, IOException {
		File dir = new File(path);
		File[] files = dir.listFiles();
		for (File itrFile : files)
		if (!itrFile.isDirectory()) {
			System.out.println(itrFile);
			if (itrFile.getPath().endsWith(".callstackoutput")) processOnSingleFile(itrFile);
		} else {
			recursivelyCount(itrFile.getPath());
		}
	}
	
	public static void main(String[] args) throws JsonSyntaxException, IOException {
		Scanner scanner = new Scanner(new File(uselessSyscallFilePath));
		
		while (scanner.hasNext()) {
			uselessSyscalls.add(scanner.next());
		}
		scanner.close();

		scanner = new Scanner(new File(usefulEventFilePath));
		
		while (scanner.hasNext()) {
			usefulEvents.add(scanner.next());
		}
		scanner.close();
		
		recursivelyCount("G:\\20180526callstackdata");
		List<String> APIName = new ArrayList<String>();
		APIName.addAll(APISet);
		Collections.sort(APIName, new MyComparator());
		BufferedWriter bw = new BufferedWriter(new FileWriter("callstackStat/APIParastatistics.txt"));
		for (String itrAPIName : APIName) {
			bw.write(itrAPIName);
			bw.newLine();
		}
		bw.close();
	}
	
	static class MyComparator implements Comparator {
		@Override
		public int compare(Object o1, Object o2) {
			String sigo1 = (String) o1;
			String sigo2 = (String) o2;
			if (sigo1.compareTo(sigo2) < 0)
				return -1;
			else if (sigo1.compareTo(sigo2) == 0)
				return 0;
			else
				return 1;
		}
	}
}


