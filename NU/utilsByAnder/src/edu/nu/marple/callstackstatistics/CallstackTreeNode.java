package edu.nu.marple.callstackstatistics;

import java.util.ArrayList;

import edu.nu.marple.callstackstatistics.APICapabilityMeasurementMain.TreeNode;

public class CallstackTreeNode {
	String API;
	ArrayList<TreeNode> sons;
	
	public CallstackTreeNode() {
		API = "";
		sons = new ArrayList<TreeNode>();
	}
}
