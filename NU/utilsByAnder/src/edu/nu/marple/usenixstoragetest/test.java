package edu.nu.marple.usenixstoragetest;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import edu.nu.marple.callstackbasics.CallstackGlobalConfig;
import edu.nu.marple.callstackbasics.EventRecordWithCallstackCompressed;
import edu.nu.marple.callstackbasics.EventRecordWithCallstackG;
import edu.nu.marple.callstacktool.CallstackFilter;
import edu.nu.marple.filemanipulation.FileReadingUtil;

public class test {
	
	static class ThreadKey {
		public String pid;
		public String tid;
		
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((pid == null) ? 0 : pid.hashCode());
			result = prime * result + ((tid == null) ? 0 : tid.hashCode());
			return result;
		}
		
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			ThreadKey other = (ThreadKey) obj;
			if (pid == null) {
				if (other.pid != null)
					return false;
			} else if (!pid.equals(other.pid))
				return false;
			if (tid == null) {
				if (other.tid != null)
					return false;
			} else if (!tid.equals(other.tid))
				return false;
			return true;
		}

		public ThreadKey(String pid, String tid) {
			super();
			this.pid = pid;
			this.tid = tid;
		}
		
	}
	
	static class ThreadValue {
		public String eventName;
		public String parameter;
		public String systemCallStack;
		public String exeCallStack;
		
		public ThreadValue() {
			super();
			this.eventName = "";
			this.parameter = "";
			this.systemCallStack = "";
			this.exeCallStack = "";
		}
		
		
		public ThreadValue(String eventName, String parameter, String systemCallStack, String exeCallStack) {
			super();
			this.eventName = eventName;
			this.parameter = parameter;
			this.systemCallStack = systemCallStack;
			this.exeCallStack = exeCallStack;
		}
		
	}
	
	static String encodeSystemCallStack(String systemCallStack, Map<String, Integer> encodingMap) {
		String result = "";
		String[] split = systemCallStack.split(",");
		result = String.valueOf(encodingMap.get(split[0]));
		for (int i = 1; i < split.length; ++i) {
			result = result + "," + String.valueOf(encodingMap.get(split[i]));
		}
		return result;
	}
	
	static void processCurrentData(List<String> dataSequence, BufferedWriter bw, Map<ThreadKey, ThreadValue> map, Map<String, Integer> encodingMap) throws IOException {
		List<EventRecordWithCallstackG> dataSequence2 = new ArrayList();
		for (String line : dataSequence) dataSequence2.add(new Gson().fromJson(line, EventRecordWithCallstackG.class));
		//dataSequence2 = new CallstackFilter<EventRecordWithCallstackG>().getOnePossibleSequneceByChoosingLongestLoop(dataSequence2);
		
		for (EventRecordWithCallstackG itrEvent : dataSequence2) {
			//EventRecordWithCallstackG itrEvent = new Gson().fromJson(line2, EventRecordWithCallstackG.class);
			ThreadKey tk = new ThreadKey(itrEvent.processID, itrEvent.threadID);
			ThreadValue newtv = new ThreadValue(itrEvent.eventName, itrEvent.parameter, itrEvent.systemCallStack, itrEvent.exeCallStack);
			ThreadValue curtv = map.get(tk);
			EventRecordWithCallstackCompressed resDataRecord = null;
			if (curtv == null) {
				resDataRecord = new EventRecordWithCallstackCompressed(
						String.valueOf(encodingMap.get(itrEvent.eventName)), 
						String.valueOf(encodingMap.get(itrEvent.parameter)), 
						encodeSystemCallStack(itrEvent.systemCallStack, encodingMap), 0);
			} else {
				String curEventName = curtv.eventName;
				String curParameter = curtv.parameter;
				String curSystemCallStack = curtv.systemCallStack;
				String[] itrSystemCallStackSplit = itrEvent.systemCallStack.split(",");
				String[] curSystemCallStackSplit = curSystemCallStack.split(",");
				String itrTopAPI = itrSystemCallStackSplit[itrSystemCallStackSplit.length - 1];
				String curTopAPI = curSystemCallStackSplit[curSystemCallStackSplit.length - 1];
				if (!itrTopAPI.equals(curTopAPI)) {
					resDataRecord = new EventRecordWithCallstackCompressed(
							String.valueOf(encodingMap.get(itrEvent.eventName)), 
							String.valueOf(encodingMap.get(itrEvent.parameter)), 
							encodeSystemCallStack(itrEvent.systemCallStack, encodingMap), 0);
				} else {
					int cnt = 0, i, j;
					for (i = itrSystemCallStackSplit.length - 1, j = curSystemCallStackSplit.length - 1; i >= 0 && j >= 0; --i, --j) 
					if (itrSystemCallStackSplit[i].equals(curSystemCallStackSplit[j])) {
						++cnt;
					} else 
						break;
					
					String newEventName = String.valueOf(encodingMap.get(itrEvent.eventName));
					if (itrEvent.eventName.equals(curEventName))
						newEventName = "@";
					
					String newParameter = String.valueOf(encodingMap.get(itrEvent.parameter));
					if (itrEvent.parameter.equals(curParameter))
						newParameter = "@";
					
					String newSystemCallStack = "";
					if (i >= 0) newSystemCallStack = String.valueOf(encodingMap.get(itrSystemCallStackSplit[0]));
					for (int k = 1; k <= i; ++k)
						newSystemCallStack = newSystemCallStack + "," + String.valueOf(encodingMap.get(itrSystemCallStackSplit[k]));
					
					int newDuplicate = cnt;
					
					resDataRecord = new EventRecordWithCallstackCompressed(newEventName, newParameter, newSystemCallStack,
							newDuplicate);
				}
			}
			String output = new GsonBuilder().create().toJson(resDataRecord);
			bw.write(output);
			bw.newLine();
			map.put(tk, newtv);
		}
	}
	
	static void buildEncoding(String lineData, Map<String, Integer> encodingMap) {
		EventRecordWithCallstackG itrEvent = new Gson().fromJson(lineData, EventRecordWithCallstackG.class);
		if (!encodingMap.containsKey(itrEvent.eventName)) {
			encodingMap.put(itrEvent.eventName, encodingMap.size());
		}
		if (!encodingMap.containsKey(itrEvent.parameter)) {
			encodingMap.put(itrEvent.parameter, encodingMap.size());
		}
		String[] systemCallStackSplit = itrEvent.systemCallStack.split(",");
		for (String itrSplitPart : systemCallStackSplit) {
			if (!encodingMap.containsKey(itrSplitPart)) {
				encodingMap.put(itrSplitPart, encodingMap.size());
			}
		}
	}
	
	public static void main(String[] args) throws IOException {
		File dir = new File("data/other/usenix/");
		File[] files = dir.listFiles(new FilenameFilter() {
			public boolean accept(File dir, String name) {
				return name.endsWith(CallstackGlobalConfig.preprocessedTraceFileExtension);
			}
		});
		
		for (File json : files) {
			System.out.println(json.getPath());
			
			BufferedWriter bw = new BufferedWriter(new FileWriter(json.getPath() + ".compressed"));
			Map<String, Integer> encodingMap = new HashMap();
			Map<ThreadKey, ThreadValue> map = new HashMap();
			//List<EventRecordWithCallstackG> inputSequence = FileReadingUtil.file2EventRecordWithCallstackGList(json.getPath());
			
			BufferedReader reader2 = new BufferedReader(new FileReader(json));
			String line2 = null;
			while ((line2 = reader2.readLine()) != null) {
				buildEncoding(line2, encodingMap);
			}
			reader2.close();
			
			BufferedReader reader = new BufferedReader(new FileReader(json));

			String line = null;
			int lineCnt = 0;
			List<String> dataSequence = new ArrayList<String>();
			while ((line = reader.readLine()) != null) {
				++lineCnt;
				if (lineCnt < 100000) {
					dataSequence.add(line);
				} else {
					processCurrentData(dataSequence, bw, map, encodingMap);
					dataSequence.clear();
					lineCnt = 0;
				}
				
			}
			processCurrentData(dataSequence, bw, map, encodingMap);
			reader.close();
			bw.close();
		}
	}
}
