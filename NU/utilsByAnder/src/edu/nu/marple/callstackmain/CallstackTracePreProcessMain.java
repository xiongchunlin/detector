package edu.nu.marple.callstackmain;

import java.io.IOException;
import java.util.Date;
import java.util.Arrays;
import java.util.List;
import edu.nu.marple.GlobalConfig;
import edu.nu.marple.callstackbasics.CallstackGlobalConfig;
import edu.nu.marple.callstacktool.CallstackUtils;

public class CallstackTracePreProcessMain {

	public static void main(String[] args) throws IOException {
		int totalEventNumber = 0;
		Date start = new Date();
		totalEventNumber += processNonPhfSet();
		//processTestingSet();
		//processTrainingSet();
		Date end = new Date();
		System.out.println("Total Event #: " + totalEventNumber);
		System.out.println("Elapsed Time: " + ((end.getTime() - start.getTime()) / 1000));
	}

	public static void processTrainingSet() {
		List<String> phfs = CallstackGlobalConfig.preprocessedTrainingPhfs;
		phfs.forEach(t -> {
			try {
				CallstackUtils.tracePreProcessForPhf_memorySavingVersion(CallstackGlobalConfig.trainingDataRootFolder, t, 
						CallstackGlobalConfig.rawTraceFileExtension, CallstackGlobalConfig.preprocessedTraceFileExtensionAdded,
						CallstackGlobalConfig.uselessSyscallFile, CallstackGlobalConfig.usefulEventFile,
						CallstackGlobalConfig.uselessAPIFile, CallstackGlobalConfig.argPreprocessedOn);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});
	}

	public static void processTestingSet() {
		List<String> phfs = CallstackGlobalConfig.preprocessedTestingPhfs;
		phfs.forEach(t -> {
			try {
				CallstackUtils.tracePreProcessForPhf_memorySavingVersion(CallstackGlobalConfig.testingDataRootFolder, t, 
						CallstackGlobalConfig.rawTraceFileExtension, CallstackGlobalConfig.preprocessedTraceFileExtensionAdded,
						CallstackGlobalConfig.uselessSyscallFile, CallstackGlobalConfig.usefulEventFile,
						CallstackGlobalConfig.uselessAPIFile, CallstackGlobalConfig.argPreprocessedOn);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});
	}

	public static int processNonPhfSet() {
		List<String> phfs = Arrays.asList( // "benign180421"
				// "audiorecord", "remoteshell",
				// "keylogger"
				// "remotedesktop"
				//"chromedownload1",
				//"chromedownload2"
				"usenix"
		// , "send&exec"
		);
		int totalEventNumber = 0;
		
		for (String phf : phfs) {
			try {
				totalEventNumber += CallstackUtils.tracePreProcessForPhf_memorySavingVersion("data/other/", phf, 
						CallstackGlobalConfig.rawTraceFileExtension, CallstackGlobalConfig.preprocessedTraceFileExtensionAdded,
						CallstackGlobalConfig.uselessSyscallFile, CallstackGlobalConfig.usefulEventFile,
						CallstackGlobalConfig.uselessAPIFile, CallstackGlobalConfig.argPreprocessedOn);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return totalEventNumber;
	}

}
