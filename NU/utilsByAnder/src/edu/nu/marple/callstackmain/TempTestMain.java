package edu.nu.marple.callstackmain;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import edu.nu.marple.EventRecordG;
import edu.nu.marple.SigResults;
import edu.nu.marple.Utils;
import edu.nu.marple.callstackbasics.AggAPIRecord;
import edu.nu.marple.callstackbasics.AARSigResult;
import edu.nu.marple.callstackdetectionsystem.SequenceSignatureAggSigMatch;
import edu.nu.marple.detectsystem.EventRecordM;
import edu.nu.marple.detectsystem.SequenceSignatureWFAMatch;
import edu.nu.marple.filemanipulation.FileReadingUtil;

public class TempTestMain {
	
//	public static void matchFromTracefile(String fileName, String reportPath) throws IOException {
//		/*Input from trace file*/
//		List<AggAPIRecord> sequence = FileReadingUtil.file2AggAPIRecordGenerationList(fileName);
//		SequenceSignatureAggSigMatch ssm = new SequenceSignatureAggSigMatch(fileName, sequence);
//		System.out.println("Start to match tracefile: " + fileName);
//		boolean result = ssm.match();
//		
//		// generate csv format detection report
//		List<Map<String, Integer>> allProcessResults = ssm.getAllProcessResults();
//		List<Map<String, Double>> allProcessResults_credit = ssm.getAllProcessResults_credit();
//		Map<String, Integer> phf2Sigcnt = ssm.getPhf2Sigcnt();
//		List<String> phfs = new ArrayList<String>(phf2Sigcnt.keySet());
//		BufferedWriter writer = new BufferedWriter(new FileWriter(reportPath, true));
//		
//		if (!isPhfNameWritten) {
//			writer.write("\t" + phfs.stream().collect(Collectors.joining("\t")));
//			writer_credit.write("\t" + phfs.stream().collect(Collectors.joining("\t")));
//			isPhfNameWritten = true;
//		}
//		
//		writer.write("\n" + nowFile);
//		
//		// generate csv format detection report
//		List<Map<String, Integer>> allProcessResults = ssm.getAllProcessResults();
//		Map<String, Integer> phf2Sigcnt = ssm.getPhf2Sigcnt();
//		List<String> phfs = new ArrayList<String>(phf2Sigcnt.keySet());
//		BufferedWriter writer = new BufferedWriter(new FileWriter(reportPath, true));
//		
//		if (!isPhfNameWritten) {
//			writer.write("\t" + phfs.stream().collect(Collectors.joining("\t")));
//			writer_credit.write("\t" + phfs.stream().collect(Collectors.joining("\t")));
//			isPhfNameWritten = true;
//		}
//		
//		writer.write("\n" + nowFile);
//		writer_credit.write("\n" + nowFile);
//	}
	
	static String[] phfs = {"keylogger", "remotedesktop", "download&exec"};
	static List<AARSigResult>[] finalSigs = new ArrayList[3];
	
	public static void matchFromTracefile_temp(String nowFile, BufferedWriter report, BufferedWriter detailReport) throws IOException {
		List<AggAPIRecord> sequence = FileReadingUtil.file2AggAPIRecordList(nowFile);
		report.write(nowFile);
		report.newLine();
		for (int i = 0; i < 3; ++i) {
			int matchCnt = 0;
			for (AARSigResult itrSig : finalSigs[i]) {
				int sigI = 0, seqI = 0;
				while (sigI < itrSig.aggrecSeq.size() && seqI < sequence.size()) {
					if (itrSig.aggrecSeq.get(sigI).acceptedBy(sequence.get(seqI))) {
						++sigI;
					}
					++seqI;
				}
				if (sigI == itrSig.aggrecSeq.size()) {
					++matchCnt;
					detailReport.write(new GsonBuilder().setPrettyPrinting().create().toJson(itrSig));
					detailReport.newLine();
				}
			}
			report.write(String.valueOf(matchCnt) + "/" + String.valueOf(finalSigs[i].size()) + " ");
			
		}
		report.newLine();
	}
	
	public static void detectFromDir(String dir, final String traceExtention) throws IOException{
		Date startTime = new Date();
		System.out.println("===============start=============== " + " time: " + startTime.getTime());
		//String defaultCDMFile = "data/benign/filezilla.avro";
		//matchFromAvrofile(defaultCDMFile);		
		String testDataDir = dir;
		File path = new File(testDataDir);
		File[] files = path.listFiles(new FilenameFilter() {
			public boolean accept(File dir, String name) {
				return name.endsWith(traceExtention);
			}
		});
		BufferedWriter report = new BufferedWriter(new FileWriter("detectionReport/ReportFor_" + testDataDir.replace("/","#") + ".txt"));
		for (String nowPhf : phfs) {
			report.write(nowPhf + " ");
		}
		report.newLine();
		for (File json : files) {	
			List<AggAPIRecord> sequence = FileReadingUtil.file2AggAPIRecordList(json.getPath());
			if (sequence.size() <= 3)
				continue;
			// if (!json.getName().contains("Babylon 1.5."))
			// continue;
			System.out.println(json);
			String nowFile = json.getPath();
			//matchFromTracefile(nowFile);
			BufferedWriter detailReport = new BufferedWriter(new FileWriter("detectionReport/DetailsFor_" + testDataDir.replace("/","#") + json.getName() + ".txt"));
			matchFromTracefile_temp(nowFile, report, detailReport);
			detailReport.close();
			System.out.println("");
		}
		report.close();
		Date endTime = new Date();
		System.out.println("===============end=============== " + " time: " + endTime.getTime());
	}
	
	public static void main(String[] args) throws FileNotFoundException {
		List<String> testDirs = Arrays.asList(
				  "data/testing/download&exec"
				  //"data/testing/remotedesktop/",
				  //"data/testing/keylogger/",
				  //"data/training/download&exec"
				  //"data/training/remotedesktop/"
				  //"data/training/keylogger/"
				  );
		String traceExtendtion = ".callstackoutput.tracePreprocessed";
		
		for (int i = 0; i < 3; ++i) {
			try {
				finalSigs[i] = new Gson().fromJson(new FileReader("aggSignature/" + phfs[i] + ".AggSig"), new TypeToken<List<AARSigResult>>(){}.getType());
			} catch (JsonIOException | JsonSyntaxException | FileNotFoundException e) {
				e.printStackTrace();
			}
		}
		
		testDirs.forEach(t -> {
			try {
				detectFromDir(t, traceExtendtion);
				//initdetectFromDir(t, inittraceExtention);
			} catch (IOException e) {
				e.printStackTrace();
			}
		});
	}
}
