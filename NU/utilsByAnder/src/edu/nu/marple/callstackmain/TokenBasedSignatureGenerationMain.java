package edu.nu.marple.callstackmain;

import java.io.File;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import com.google.gson.GsonBuilder;

import edu.nu.marple.SigResults;
import edu.nu.marple.callstackbasics.AggAPIRecord;
import edu.nu.marple.callstackbasics.AARSigResult;
import edu.nu.marple.filemanipulation.FileReadingUtil;


public class TokenBasedSignatureGenerationMain {
	
	public static String[] focusedPhfs = {"keylogger", "remotedesktop", "download&exec"};
	
	public static HashMap<AggAPIRecord, HashSet<Integer>> benignStat = new HashMap<AggAPIRecord, HashSet<Integer>>(), ratStat = new HashMap<AggAPIRecord, HashSet<Integer>>();
	public static int signatureLen = 5, signatureCnt = 50;
	public static double nowFP = 0.5, factor = 0.85;
	public static ArrayList<AARSigResult> signature = new ArrayList<AARSigResult>();
	public static HashSet<AggAPIRecord> selected = new HashSet<AggAPIRecord>();
	public static HashSet<Integer> coveredBenignTrace = new HashSet<Integer>(), coveredRatTrace = new HashSet<Integer>();
	
	public static String outputFile = "callstackSignature/remotedesktop_180418.txt";
	public static String strBenignDir = "data/withcallstack/benign";
	public static String strRatDir = "data/withcallstack/remotedesktop";
	public static String fileExtension = ".callstackoutput.tracePreprocessed";
	
	public static void main(String[] args) throws IOException {
		//File benignDir = new File("utilsByAnder/traces/initialization2/benignTrain"), ratDir = new File("utilsByAnder/traces/initialization2/ratTrain");
//		File benignDir = new File("utilsByAnder/traces/urldownload/benignTrain"), ratDir = new File("utilsByAnder/traces/urldownload/ratTrain");
		File benignDir = new File(strBenignDir), ratDir = new File(strRatDir);

		File[] listsOfBenignDir = benignDir.listFiles(new FilenameFilter() {
			public boolean accept(File dir, String name) {
				return name.endsWith(fileExtension);
//				return name.endsWith("syscall_args_block500_uselessRunqingNew_filtered");
			}
		});
		File[] listsOfRatDir = ratDir.listFiles(new FilenameFilter() {
			public boolean accept(File dir, String name) {
				return name.endsWith(fileExtension);
//				return name.endsWith("syscall_args_block500_uselessRunqingNew_filtered");
			}
		});
		
		int benignCnt = listsOfBenignDir.length, ratCnt = listsOfRatDir.length;
		System.out.println(benignCnt + " " + ratCnt);
		for (int i = 0; i < ratCnt; ++i) {
			System.out.println("calculating rat " + i);
			List<AggAPIRecord> sequence = FileReadingUtil.file2AggAPIRecordList(listsOfRatDir[i].getAbsolutePath());
			for (AggAPIRecord aggAPIRec : sequence) {
				if (!ratStat.containsKey(aggAPIRec)) {
					ratStat.put(aggAPIRec, new HashSet<Integer>());
					benignStat.put(aggAPIRec, new HashSet<Integer>());
				}
				ratStat.get(aggAPIRec).add(i);
			}
			coveredRatTrace.add(i);
		}
		for (int i = 0; i < benignCnt; ++i) {
			System.out.println("calculating benign app " + i);
			List<AggAPIRecord> sequence = FileReadingUtil.file2AggAPIRecordList(listsOfBenignDir[i].getAbsolutePath());
			for (AggAPIRecord aggAPIRec : sequence) {
				if (!benignStat.containsKey(aggAPIRec)) {
					ratStat.put(aggAPIRec, new HashSet<Integer>());
					benignStat.put(aggAPIRec, new HashSet<Integer>());
				}
				benignStat.get(aggAPIRec).add(i);
			}
			coveredBenignTrace.add(i);
		}
		boolean changed = true;
		int itrRound = 0;
		while (changed) {
			++itrRound;
			System.out.println("Iteration Round " + itrRound);
			changed = false;
			for (AggAPIRecord aggAPIRec1 : ratStat.keySet()) {
				for (AggAPIRecord aggAPIRec2 : ratStat.keySet()) 
				if (aggAPIRec1.acceptedBy(aggAPIRec2)) {
					if (!ratStat.get(aggAPIRec1).containsAll((ratStat.get(aggAPIRec2)))) {
						ratStat.get(aggAPIRec1).addAll(ratStat.get(aggAPIRec2));
						changed = true;
					}
					if (!benignStat.get(aggAPIRec1).containsAll((benignStat.get(aggAPIRec2)))) {
						benignStat.get(aggAPIRec1).addAll(benignStat.get(aggAPIRec2));
						changed = true;
					}
				}
			}
		}
		System.out.println("Finish calculating trace coverage");
		
		HashSet<Integer> overallBenign = new HashSet<Integer>(), overallRat = new HashSet<Integer>();
		for (int i = 0; i < signatureCnt; ++i) {
			System.out.println("Start with sig " + i);
			HashSet<Integer> coveredBenignTrace = new HashSet<Integer>(), coveredRatTrace = new HashSet<Integer>();
			for (int j = 0; j < benignCnt; ++j) coveredBenignTrace.add(j);
			for (int j = 0; j < ratCnt; ++j) coveredRatTrace.add(j);
			signature.add(new AARSigResult());
			signature.get(i).coverage = 1.0;
			for (int j = 0; j < signatureLen; ++j) {
				int maxRatCovered = -1, nowBenignCovered = 0;
				double nowMinFP = 1.0;
				HashSet<Integer> ratResult = null, benignResult = null;
				AggAPIRecord newaddAggAPIRecord = null;
				for (AggAPIRecord aggAPIRec : ratStat.keySet()) 
				if (!selected.contains(aggAPIRec)){
					HashSet<Integer> tmpRatResult = new HashSet<Integer>(ratStat.get(aggAPIRec)), tmpBenignResult = new HashSet<Integer>(benignStat.get(aggAPIRec));
					tmpRatResult.retainAll(coveredRatTrace);
					tmpBenignResult.retainAll(coveredBenignTrace);
					if ((double) tmpBenignResult.size() / benignCnt < nowFP) {
						if (tmpRatResult.size() > maxRatCovered || (tmpRatResult.size() == maxRatCovered && (double) tmpBenignResult.size() / benignCnt <nowMinFP)) { 
							maxRatCovered = tmpRatResult.size();
							nowBenignCovered = tmpBenignResult.size();
							ratResult = tmpRatResult;
							benignResult = tmpBenignResult;
							newaddAggAPIRecord = aggAPIRec;
							nowMinFP = (double) tmpBenignResult.size() / benignCnt;
						}
					}
				}
				if (newaddAggAPIRecord == null) break;
				coveredRatTrace = ratResult;
				coveredBenignTrace = benignResult;
				signature.get(i).aggrecSeq.add(newaddAggAPIRecord);
				selected.add(newaddAggAPIRecord);
				if (nowBenignCovered == 0) break;
				nowFP *= factor;
			}
			signature.get(i).coverage = coveredRatTrace.size();
			signature.get(i).falsePositiveShare = coveredBenignTrace.size();
			if (signature.get(i).aggrecSeq.size() == 0) {
				signature.remove(i);
				signatureCnt = i;
				System.out.println("No eligible candidate token");
				break;
			}
			System.out.println(coveredRatTrace.size() + " " + coveredBenignTrace.size());
			overallRat.addAll(coveredRatTrace);
			overallBenign.addAll(coveredBenignTrace);
		}
		
		for (AARSigResult sigRes : signature) {
			sigRes.coverage /= (double) overallRat.size();
			sigRes.falsePositiveShare /= (double) overallBenign.size();
		}
		
//		File initSig = new File("InitializationPart/URLsignature.txt");
		File initSig = new File(outputFile);
		FileWriter initSigWriter = new FileWriter(initSig);
		initSigWriter.write(new GsonBuilder().serializeSpecialFloatingPointValues().setPrettyPrinting().create().toJson(signature));
		initSigWriter.flush();
		initSigWriter.close();
	}
}
