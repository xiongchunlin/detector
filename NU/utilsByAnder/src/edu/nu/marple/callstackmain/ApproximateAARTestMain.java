package edu.nu.marple.callstackmain;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gson.GsonBuilder;

import edu.nu.marple.callstackbasics.AggAPIRecord;
import edu.nu.marple.callstackbasics.CallstackGlobalConfig;
import edu.nu.marple.callstackbasics.EventRecordWithCallstackG;
import edu.nu.marple.callstackbasics.TopAPISigResult;
import edu.nu.marple.callstackdetectionsystem.SequenceSignatureAggSigMatch;
import edu.nu.marple.callstacktool.CallstackFilter;
import edu.nu.marple.callstacktool.CallstackUtils;
import edu.nu.marple.filemanipulation.FileReadingUtil;

public class ApproximateAARTestMain {
	
	public static double[] matchOnThread(String threadName, BufferedWriter threadReport, BufferedWriter threadDetail, List<AggAPIRecord> sequence) throws IOException {
		threadReport.write(threadName);
		threadReport.newLine();
		threadDetail.write(threadName);
		threadDetail.newLine();
		
		SequenceSignatureAggSigMatch ssam = new SequenceSignatureAggSigMatch(sequence);
		ssam.matchForTest();
		
		double[] res = new double[SequenceSignatureAggSigMatch.MaxPhfCnt];
		
		for (int itrPhf = 0; itrPhf < SequenceSignatureAggSigMatch.MaxPhfCnt; ++itrPhf) {
			double maxScorePort = -1.0;
			int selectedSigNo = -1;
			for (int itrSig = 0; itrSig < SequenceSignatureAggSigMatch.finalSigs[itrPhf].size(); ++itrSig) {
				if (ssam.matchedMaxScores[itrPhf].get(itrSig) * 1.0 / SequenceSignatureAggSigMatch.maxScores[itrPhf].get(itrSig) > maxScorePort) {
					maxScorePort = ssam.matchedMaxScores[itrPhf].get(itrSig) * 1.0 / SequenceSignatureAggSigMatch.maxScores[itrPhf].get(itrSig);
					selectedSigNo = itrSig;
				}
				
//				if (SequenceSignatureAggSigMatch.finalSigs[itrPhf].get(itrSig).traces.contains("Coringa-RAT 0.1-2.callstackoutput.tracePreprocessed_tid3772") && SequenceSignatureAggSigMatch.finalSigs[itrPhf].get(itrSig).aggrecSeq.size() == 7) {
//					System.out.println(ssam.matchedMaxScores[itrPhf].get(itrSig) + " : " + SequenceSignatureAggSigMatch.maxScores[itrPhf].get(itrSig));
//				}
			}
			res[itrPhf] = maxScorePort;
			String s = String.format("%.2f", maxScorePort);
			threadDetail.write(SequenceSignatureAggSigMatch.phf[itrPhf] + " MaxPort: " + s);
			threadDetail.newLine();
			threadDetail.write(new GsonBuilder().setPrettyPrinting().create().toJson(new TopAPISigResult(SequenceSignatureAggSigMatch.finalSigs[itrPhf].get(selectedSigNo))));
			threadDetail.newLine();
			threadReport.write(s + " ");
		}
		threadDetail.newLine();
		threadReport.newLine();
		
		return res;
	}
	
	public static void detectFromDir(String dir, final String traceExtention) throws IOException{
		Date startTime = new Date();
		System.out.println("===============start=============== " + " time: " + startTime.getTime());
		
		String testDataDir = dir;
		File path = new File(testDataDir);
		File[] files = path.listFiles(new FilenameFilter() {
			public boolean accept(File dir, String name) {
				return name.endsWith(traceExtention);
			}
		});
		
		BufferedWriter threadReport = new BufferedWriter(new FileWriter("detectionReport/ThreadReportFor_" + testDataDir.replace("/","#") + ".txt"));
		for (String nowPhf : SequenceSignatureAggSigMatch.phf) {
			threadReport.write(nowPhf + " ");
		}
		threadReport.newLine();
		BufferedWriter processReport = new BufferedWriter(new FileWriter("detectionReport/ProcessReportFor_" + testDataDir.replace("/","#") + ".txt"));
		for (String nowPhf : SequenceSignatureAggSigMatch.phf) {
			processReport.write(nowPhf + " ");
		}
		processReport.newLine();
		
		BufferedWriter threadDetail = new BufferedWriter(new FileWriter("detectionReport/ThreadDetailsFor_" + testDataDir.replace("/","#") + ".txt"));
		
		for (File json : files) {	
			processReport.write(json.getPath());
			processReport.newLine();
			Map<String, List<EventRecordWithCallstackG>> thread2Trace = new HashMap<>();
			List<EventRecordWithCallstackG> inputSequence = FileReadingUtil.file2EventRecordWithCallstackGList(json.getPath());
			
			for (EventRecordWithCallstackG itrEventG : inputSequence) {
				if (!thread2Trace.containsKey(itrEventG.threadID)) {
					thread2Trace.put(itrEventG.threadID, new ArrayList<EventRecordWithCallstackG>());
				}
				thread2Trace.get(itrEventG.threadID).add(itrEventG);
			}
			double[] maxMatchPort = new double[SequenceSignatureAggSigMatch.MaxPhfCnt];
			for (String itrThreadKey : thread2Trace.keySet()) {
				List<EventRecordWithCallstackG> intermediateSequence = thread2Trace.get(itrThreadKey);
				if (CallstackGlobalConfig.loopReductionOnInputTrace) 
					intermediateSequence = new CallstackFilter<EventRecordWithCallstackG>().getOnePossibleSequneceByChoosingLongestLoop(intermediateSequence);
				
				List<AggAPIRecord> outputSequence = CallstackUtils.convert2AggAPIRecordList(intermediateSequence);
				
				if (outputSequence.size() <= 3)
					continue;
				
				System.out.println(json + "_tid" + itrThreadKey);
				String nowFile = json.getPath() + "_tid" + itrThreadKey;
				
				double[] ret = matchOnThread(nowFile, threadReport, threadDetail, outputSequence);
				
				for (int itrPhf = 0; itrPhf < SequenceSignatureAggSigMatch.MaxPhfCnt; ++itrPhf) 
					maxMatchPort[itrPhf] = Math.max(maxMatchPort[itrPhf], ret[itrPhf]);
			}
			
			for (double maxPort : maxMatchPort) {
				processReport.write(String.format("%.2f", maxPort) + " ");
			}
			processReport.newLine();
		}
		
		threadReport.close();
		threadDetail.close();
		processReport.close();
		
		Date endTime = new Date();
		System.out.println("===============end=============== " + " time: " + endTime.getTime());
		System.out.println("Elapsed Time: " + (endTime.getTime() - startTime.getTime()) / 1000);
	}
	
	public static void main(String[] args) throws FileNotFoundException {
		List<String> testDirs = CallstackGlobalConfig.testDirs;
		String traceExtention = CallstackGlobalConfig.preprocessedTraceFileExtension;
		
		testDirs.forEach(t -> {
			try {
				detectFromDir(t, traceExtention);
			} catch (IOException e) {
				e.printStackTrace();
			}
		});
	}
}
