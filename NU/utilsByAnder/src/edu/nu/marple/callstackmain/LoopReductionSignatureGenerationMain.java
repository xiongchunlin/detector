package edu.nu.marple.callstackmain;

import java.io.File;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.gson.GsonBuilder;

import edu.nu.marple.SigResults;
import edu.nu.marple.callstackbasics.AggAPIRecord;
import edu.nu.marple.callstackbasics.CallstackGlobalConfig;
import edu.nu.marple.callstackbasics.AARSigResult;
import edu.nu.marple.callstackbasics.EventRecordWithCallstackG;
import edu.nu.marple.callstacksigrefine.CallstackNonPhfFiltering;
import edu.nu.marple.callstacktool.CallstackChain;
import edu.nu.marple.callstacktool.CallstackItem;
import edu.nu.marple.callstacktool.CallstackUtils;
import edu.nu.marple.callstacktool.CallstackFilter;
import edu.nu.marple.filemanipulation.FileReadingUtil;


public class LoopReductionSignatureGenerationMain {
	
	public static List<AARSigResult> generateSignatureForPhf(String nowPhf) throws IOException {
		Set<AARSigResult> finalSigSet = new HashSet<AARSigResult>();
		Map<List<AggAPIRecord>, AARSigResult> sigString2SigResults = new HashMap<List<AggAPIRecord>, AARSigResult>();
		
		File dir = new File(CallstackGlobalConfig.trainingDataRootFolder + nowPhf + "/");
		File[] files = dir.listFiles(new FilenameFilter() {
			public boolean accept(File dir, String name) {
				return name.endsWith(CallstackGlobalConfig.preprocessedTraceFileExtension);
			}
		});

		for (File json : files) {
			Map<String, List<EventRecordWithCallstackG>> thread2Trace = new HashMap<>();
			
			System.out.println(json);
			List<EventRecordWithCallstackG> inputSequence = FileReadingUtil.file2EventRecordWithCallstackGList(json.getPath());
			for (EventRecordWithCallstackG itrEventG : inputSequence) {
				if (!thread2Trace.containsKey(itrEventG.threadID)) {
					thread2Trace.put(itrEventG.threadID, new ArrayList<EventRecordWithCallstackG>());
				}
				thread2Trace.get(itrEventG.threadID).add(itrEventG);
			}
			for (String itrThreadKey : thread2Trace.keySet()) {
				List<EventRecordWithCallstackG> intermediateSequence = new CallstackFilter<EventRecordWithCallstackG>().getOnePossibleSequneceByChoosingLongestLoop(thread2Trace.get(itrThreadKey));
				List<AggAPIRecord> outputSequence = CallstackUtils.convert2AggAPIRecordList(intermediateSequence);
				
				if (outputSequence.size() <= 3)
					continue;
				
				AARSigResult finalSig = new AARSigResult();
				Set<String> traces = new HashSet<String>();
				traces.add(json.getName() + "_tid" + itrThreadKey);

				if (sigString2SigResults.containsKey(outputSequence)) {
					sigString2SigResults.get(outputSequence).traces.addAll(traces);
				} else {
					finalSig.aggrecSeq = outputSequence;
					finalSig.traces = traces;
					finalSigSet.add(finalSig);
					sigString2SigResults.put(outputSequence, finalSig);
				}
			}
		}
		
		List<AARSigResult> ret = new ArrayList<>();
		ret.addAll(finalSigSet);
		return ret;
	}
	
	public static void main(String[] args) throws IOException {
		for (String nowPhf : CallstackGlobalConfig.focusedPhfs) {
			List<AARSigResult> finalSigList = generateSignatureForPhf(nowPhf);
			CallstackNonPhfFiltering nonPhfFiltering = new CallstackNonPhfFiltering(nowPhf, finalSigList);
			finalSigList = nonPhfFiltering.getResult();
			System.out.println("Final Signature Cnt: " + finalSigList.size());
			String outputFilePath = "generatedSignature/" + nowPhf + CallstackGlobalConfig.AARSigFileExtension;
			FileWriter outputFile = new FileWriter(new File(outputFilePath));
			outputFile.write(new GsonBuilder().setPrettyPrinting().create().toJson(finalSigList));
			outputFile.close();
		}
	}
}
