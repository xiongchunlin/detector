package edu.nu.marple.sigrefine;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;

import edu.nu.marple.EventRecordG;

public class CountingScoreStat {
	
	public HashSet<CountingScore> setResults;
	public ArrayList<CountingScore> listResults;
	
	public CountingScoreStat() {
		setResults = new HashSet<CountingScore>();
		listResults = new ArrayList<CountingScore>();
	}
	
	public void transferResults() {
		listResults.addAll(setResults);
		Collections.sort(listResults, new Comparator<CountingScore>() {
			@Override
			public int compare(CountingScore o1, CountingScore o2) {
				if ((o1.ratScore - o1.benignScore) > (o2.ratScore - o2.benignScore)) return -1; else
				if ((o1.ratScore - o1.benignScore) == (o2.ratScore - o2.benignScore)) return 0; else
				return 1;
			}	            
		});
	}

}
