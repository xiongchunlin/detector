package edu.nu.marple.sigrefine;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.google.gson.GsonBuilder;
import com.ibm.marple.EventRecord;

import edu.nu.marple.Filter;
import edu.nu.marple.SigResults;
import edu.nu.marple.Utils;
import edu.nu.marple.EventRecordG;
import edu.nu.marple.detectsystem.SequenceSignatureWFAMatch;
import edu.nu.marple.detectsystem.EventRecordM;

public class NonPhfFilter {
	public static Map<String, List<Map<String, List<EventRecordM>>>> phf2BufferedRecords = new HashMap();
	public boolean filtered = false;
	public TreeMap<Integer, Integer> FPmatchedCntStat = new TreeMap<>();
	public static HashMap<String, Integer> phfSpecificMatchCount = new HashMap<String, Integer>();
	public static int unifiedThreshold = 10;

	public static String dataRootFolderPath = "data/non-phf-filter";
	public static String traceExtention = ".output.tracePreprocessed";

	List<SigResults> results = null;

	static {
		phfSpecificMatchCount.put("keylogger", unifiedThreshold); // 3
		phfSpecificMatchCount.put("audiorecord", 15); // 15
		phfSpecificMatchCount.put("remotedesktop", unifiedThreshold); // 5
		phfSpecificMatchCount.put("remoteshell", unifiedThreshold); // 15
		phfSpecificMatchCount.put("send&exec", unifiedThreshold); // 15
		// phfSpecificMatchCount.put("upexe", unifiedThreshold); //15
		phfSpecificMatchCount.put("urldownload", unifiedThreshold); // 15
		phfSpecificMatchCount.put("download&exec", unifiedThreshold); // 15
		// phfSpecificMatchCount.put("registrymanager", unifiedThreshold); //70
		// phfSpecificMatchCount.put("filemanager", unifiedThreshold); //15
		// phfSpecificMatchCount.put("injection_0416", unifiedThreshold);
		// phfSpecificMatchCount.put("loader_function", unifiedThreshold);

		File dataRootFolder = new File(
				dataRootFolderPath); /* Default "sigTorrent/nonphffilter" */
		File[] phfFolder = dataRootFolder.listFiles();

		System.out.println("Start to read data");

		for (int i = 0; i < phfFolder.length; ++i)
			if (phfFolder[i].isDirectory()) {
				File[] files = phfFolder[i].listFiles(new FilenameFilter() {
					public boolean accept(File dir, String name) {
						return name.endsWith(traceExtention);
					}
				});
				phf2BufferedRecords.put(phfFolder[i].getName(), new ArrayList());
				// System.out.println(nowFocusedPhf + ": " + fileCnt);
				for (int j = 0; j < files.length; ++j) {
					System.out.println("Reading " + files[j].getName());
					HashMap<String, List<EventRecordM>> newBufferedRecordsList = new HashMap<String, List<EventRecordM>>();
					List<EventRecordG> sequence = null;
					try {
						sequence = Utils.EventRecordGFile2EventRecordGenerationList(files[j].getPath());
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					ArrayList<EventRecordM> newData = new ArrayList<EventRecordM>();
					for (EventRecordG eve : sequence) {
						newData.add(new EventRecordM(eve.name, eve.parameter, "-1"));
					}
					newBufferedRecordsList.put("0", newData);
					phf2BufferedRecords.get(phfFolder[i].getName()).add(newBufferedRecordsList);
				}
			}
		System.out.println("Finish reading data");
	}

	public NonPhfFilter(String focusedPhf, List<SigResults> signatures) throws IOException {
		results = signatures;
		int[] FPmatchedCnt = new int[signatures.size()];
		int[] CoverageCnt = new int[signatures.size()];
		HashSet<String>[] FPTraces = new HashSet[signatures.size()];
		HashSet<String>[] CoverTraces = new HashSet[signatures.size()];
		for (int i = 0; i < FPmatchedCnt.length; ++i) {
			FPTraces[i] = new HashSet<String>();
			CoverTraces[i] = new HashSet<String>();
		}
		int totalFPCnt = 0;
		int totalCoverageCnt = 0;
		
		for (String folderName : phf2BufferedRecords.keySet()) {
			boolean isFiltering = false;
			if (!folderName.equals(focusedPhf)) {
				System.out.println("Filtering on " + folderName + " of " + phf2BufferedRecords.get(folderName).size() + " traces");
				isFiltering = true;
			} else
				System.out.println("Calculating on " + folderName + " of " + phf2BufferedRecords.get(folderName).size() + " traces");
			int traceNo = 0;
			for (Map<String, List<EventRecordM>> nowBufferedRecordsList : phf2BufferedRecords.get(folderName)) {
				String name = folderName + String.valueOf(traceNo);
				System.out.println("Processing on trace " + traceNo);
				SequenceSignatureWFAMatch ssm = new SequenceSignatureWFAMatch("", nowBufferedRecordsList);
				for (int i = 0; i < FPmatchedCnt.length; ++i) {
					ssm.sequenceSignature[0] = new ArrayList<EventRecordM>();
					ssm.windowsize[0] = signatures.get(i).windowsize;
					ssm.singleCredit[0] = 0;
					ssm.focusedPhf = findPhfNo(ssm, focusedPhf);
					for (int j = 0; j < signatures.get(i).sig.size(); ++j)
						ssm.sequenceSignature[0].add(
								new EventRecordM(EventRecordG.stringToEventRecordG(signatures.get(i).sig.get(j))));
					if (ssm.match()) {
						if (isFiltering) {
							++FPmatchedCnt[i];
							FPTraces[i].add(name);
						} else {
							++CoverageCnt[i];
							CoverTraces[i].add(name);
						}
					}
				}
				++traceNo;
			}
		}

		for (int i = 0; i < FPmatchedCnt.length; ++i)
			if (FPmatchedCntStat.containsKey(FPmatchedCnt[i])) {
				FPmatchedCntStat.put(FPmatchedCnt[i], FPmatchedCntStat.get(FPmatchedCnt[i]) + 1);
			} else
				FPmatchedCntStat.put(FPmatchedCnt[i], 1);

		HashSet<String> FPTotalStat = new HashSet<String>();
		HashSet<String> CoverTotalStat = new HashSet<String>();
		
		for (int i = 0; i < results.size(); ++i)
		if (FPmatchedCnt[i] <= phfSpecificMatchCount.get(focusedPhf)) {
			FPTotalStat.addAll(FPTraces[i]);
			CoverTotalStat.addAll(CoverTraces[i]);
		}
		
		totalFPCnt = FPTotalStat.size();
		totalCoverageCnt = CoverTotalStat.size();
		
		System.out.println("TotalFPCnt: " + totalFPCnt);
		System.out.println("TotalCoverCnt: " + totalCoverageCnt);
		for (int i = 0; i < results.size(); ++i) {
			if (totalCoverageCnt != 0) 
				results.get(i).coverage = (double) CoverageCnt[i] / totalCoverageCnt;
			else
				results.get(i).coverage = 0.0;
			if (totalFPCnt != 0)
				results.get(i).falsePositiveShare = (double) FPmatchedCnt[i] / totalFPCnt;
			else
				results.get(i).falsePositiveShare = 0.0;
		}
		
		File filteredSignature = new File("generatedSignature/" + focusedPhf + "_filtered.txt");
		BufferedWriter bw = new BufferedWriter(new FileWriter(filteredSignature));
		for (int i = 0, No = 0; i < results.size(); ++No) {
			System.out.println("Start on signature " + No);
			if (FPmatchedCnt[No] > phfSpecificMatchCount.get(focusedPhf)) {
				bw.write(new GsonBuilder().setPrettyPrinting().create().toJson(results.get(i)));
				results.remove(i);
			} else {
				++i;
			}
		}
	}

	public List<SigResults> getResult() {
		return results;
	}

	public int findPhfNo(SequenceSignatureWFAMatch ssm, String focusedPhf) {
		for (int i = 0; i < ssm.phf.length; ++i)
			if (ssm.phf[i].equals(focusedPhf)) {
				return i;
			}
		return -1;
	}
}
