package edu.nu.marple.sigrefine;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import edu.nu.marple.GlobalConfig;
import edu.nu.marple.EventRecordG;
import edu.nu.marple.Utils;

public class CountingRefinement {
	
	HashMap<EventRecordG, Integer> RATresults = new HashMap<EventRecordG, Integer>();
	HashMap<EventRecordG, Integer> Benignresults = new HashMap<EventRecordG, Integer>();
	String traceExtention;
	String phf;
	
	public CountingRefinement(String phf, String traceExtention) throws IOException {
		this.phf = phf;
		this.traceExtention = traceExtention;
		calculateRat();
		calculateBenign();
	}
	
	public void calculateRat() throws IOException {
		File dir = new File(GlobalConfig.getInstance().rootPath + "traces/" + phf + "/");
		File[] listsOfDir = dir.listFiles(new FilenameFilter() {
			public boolean accept(File dir, String name) {
				return name.endsWith(traceExtention);
			}
		});	
		
		for (int i = 0; i < listsOfDir.length; ++i) {
			File traceFile = listsOfDir[i];
			List<EventRecordG> sequence = Utils.EventRecordGFile2EventRecordGenerationList(traceFile.getAbsolutePath());
			HashSet<EventRecordG> occured = new HashSet<EventRecordG>();
			occured.addAll(sequence);
			for (EventRecordG syscallRecordG : occured)
				if (RATresults.containsKey(syscallRecordG)) 
					RATresults.put(syscallRecordG, RATresults.get(syscallRecordG) + 1);
				else
					RATresults.put(syscallRecordG, 1);
		}	
	}
	
	public void calculateBenign() throws IOException {
		File dir = new File("data/non-phf-filter/benign_for_score/");
		File[] listsOfDir = dir.listFiles(new FilenameFilter() {
			public boolean accept(File dir, String name) {
				return name.endsWith(traceExtention);
			}
		});
		
		for (int i = 0; i < listsOfDir.length; ++i) {
			File traceFile = listsOfDir[i];
			List<EventRecordG> sequence = Utils.EventRecordGFile2EventRecordGenerationList(traceFile.getAbsolutePath());
			HashSet<EventRecordG> occured = new HashSet<EventRecordG>();
			occured.addAll(sequence);
			for (EventRecordG syscallRecordG : occured)
				if (Benignresults.containsKey(syscallRecordG)) 
					Benignresults.put(syscallRecordG, Benignresults.get(syscallRecordG) + 1);
				else
					Benignresults.put(syscallRecordG, 1);
		}	
	}
	
	public HashMap<EventRecordG, Integer> getRATResults() {
		return RATresults;
	}
	
	
	public HashMap<EventRecordG, Integer> getBenignResults() {
		return Benignresults;
	}
}
