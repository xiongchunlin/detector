package edu.nu.marple.sigrefine;

import edu.nu.marple.EventRecordG;

public class CountingScore {
	public EventRecordG syscall;
	public double ratScore, benignScore;
	
	public CountingScore(EventRecordG syscall, double ratScore, double benignScore) {
		this.syscall = syscall;
		this.ratScore = ratScore;
		this.benignScore = benignScore;
	}
	
	@Override
	public boolean equals(Object X) {
		if (X instanceof CountingScore) {
			return (this.syscall.equals(((CountingScore) X).syscall) &&
					this.ratScore == ((CountingScore) X).ratScore &&
					this.benignScore == ((CountingScore) X).benignScore
					);
		} else 
			return false;
	}
	
	@Override
	public int hashCode() {
		return (int) (this.syscall.hashCode() * 411 + this.ratScore * 41 + this.benignScore * 4);
	}
}
