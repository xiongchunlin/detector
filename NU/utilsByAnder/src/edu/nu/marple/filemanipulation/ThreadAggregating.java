package edu.nu.marple.filemanipulation;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.HashMap;

import edu.nu.marple.Utils;

public class ThreadAggregating {

	public static String traceExtension = ".output";
	public static HashMap<String, BufferedWriter> writer = new HashMap<String, BufferedWriter>();

	public static void main(String[] args) throws IOException {
		processBenignSet();
		//processRatSet();
	}

	public static void processBenignSet() throws IOException {
		String[] traceDir = { "utilsByCs/initialization/ratTest/" };
		writer.clear();
		for (String dir : traceDir) {
			File nowDir = new File(dir);
			File[] files = nowDir.listFiles(new FilenameFilter() {
				public boolean accept(File dir, String name) {
					return name.endsWith(traceExtension);
				}
			});

			for (File json : files) {
				System.out.println(json.getPath());
				String[] splitResults = json.getName().split("_TID");
				if (!writer.containsKey(splitResults[0])) {
					writer.put(splitResults[0], new BufferedWriter(new FileWriter("utilsByCs/initialization2/ratTest/" + splitResults[0] + traceExtension)));
				}
				BufferedReader br = new BufferedReader(new FileReader(json));
				String data;
				while ((data = br.readLine()) != null) {
					writer.get(splitResults[0]).write(data);
					writer.get(splitResults[0]).newLine();
				}
				br.close();
			}
		}
		
		for (BufferedWriter bw : writer.values())
			bw.close();
	}

	public static void processRatSet() throws IOException {
		String[] traceDir = { "utilsByCs/newtraces/ratTrain/", "utilsByCs/newtraces/ratTest/" };
		writer.clear();
		for (String dir : traceDir) {
			File nowDir = new File(dir);
			File[] files = nowDir.listFiles(new FilenameFilter() {
				public boolean accept(File dir, String name) {
					return name.endsWith(traceExtension);
				}
			});

			for (File json : files) {
				System.out.println(json.getPath());
				String[] splitResults = json.getName().split("_TID");
				if (!writer.containsKey(splitResults[0])) {
					writer.put(splitResults[0], new BufferedWriter(new FileWriter("utilsByCs/aggregatetraces_rat/" + splitResults[0] + ".output.tracePreprocessed.aggregated")));
				}
				BufferedReader br = new BufferedReader(new FileReader(json));
				String data;
				while ((data = br.readLine()) != null) {
					writer.get(splitResults[0]).write(data);
					writer.get(splitResults[0]).newLine();
				}
				br.close();
			}
		}
		
		for (BufferedWriter bw : writer.values())
			bw.close();
	}
}
