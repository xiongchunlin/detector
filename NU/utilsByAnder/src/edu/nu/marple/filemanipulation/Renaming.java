package edu.nu.marple.filemanipulation;

import java.io.File;

public class Renaming {
	
	static void recursivelyRename(File file, String prefix) {
		for (File nowFile : file.listFiles()) 
		if (nowFile.isDirectory()){
			recursivelyRename(nowFile, prefix + "_" + nowFile.getName());
		} else {
			File nowFile2 = new File(prefix + "_" + nowFile.getName());
			System.out.println(nowFile.getPath() + " " + nowFile2.getPath());
			System.out.println(nowFile.renameTo(nowFile2));
		}
	}
	
	static void rename(String root) {
		File file = new File(root);
		for (File nowFile : file.listFiles()) 
		if (nowFile.isDirectory()){
			recursivelyRename(nowFile, nowFile.getPath());
		}
	}
	
	public static void main(String[] args) {
		rename("data\\testing\\onlinetesting\\20180704andy");
	}
}
