package edu.nu.marple.filemanipulation;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import edu.nu.marple.callstackbasics.EventRecordWithCallstackG;
import edu.nu.marple.callstacktool.CallstackFilter;

public class FileLoopReductioned {
	
	public static String[] focusedPhfs = {"keylogger", "remotedesktop", "download&exec", "audiorecord", "remoteshell"};

	public static String traceExtension = ".callstackoutput.tracePreprocessed";
	
	private static EventRecordWithCallstackG convertEventRecordGJson2Object(String json) {
   		return new Gson().fromJson(json, EventRecordWithCallstackG.class);
   	}

	public static void main(String[] args) throws IOException {
		mainPro();
	}

	public static void mainPro() throws IOException {
		String[] traceDir = { "data/tmp/" };
		for (String dir : traceDir) {
			File nowDir = new File(dir);
			File[] files = nowDir.listFiles(new FilenameFilter() {
				public boolean accept(File dir, String name) {
					return name.endsWith(traceExtension);
				}
			});

			for (File json : files) {
				System.out.println(json);
				List<EventRecordWithCallstackG> inputSequence = FileReadingUtil.file2EventRecordWithCallstackGList(json.getPath());
				List<EventRecordWithCallstackG> intermediateSequence = new CallstackFilter<EventRecordWithCallstackG>().getOnePossibleSequneceByChoosingLongestLoop(inputSequence);
				BufferedWriter bw = new BufferedWriter(new FileWriter(json.getPath() + ".loopreducted"));
				for (EventRecordWithCallstackG itrEventG : intermediateSequence) {
					bw.write(new GsonBuilder().create().toJson(itrEventG));
					bw.newLine();
				}
				bw.close();
			}
		}
		
	}
}
