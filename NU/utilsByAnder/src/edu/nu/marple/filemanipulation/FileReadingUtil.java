package edu.nu.marple.filemanipulation;

import java.io.BufferedReader;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.ibm.marple.EventRecordWithCallstack;

import edu.nu.marple.EventRecordG;
import edu.nu.marple.callstackbasics.AggAPIRecord;
import edu.nu.marple.callstackbasics.EventRecordWithCallstackG;
import edu.nu.marple.callstackbasics.EventRecordWithCallstackM;
import edu.nu.marple.callstacktool.CallstackUtils;

public class FileReadingUtil {

	public static Logger logger = Logger.getLogger(CallstackUtils.class);

	public static List<String> file2EventRecordGStringList(String filepath) throws IOException {
		File file = new File(filepath);
		BufferedReader reader = new BufferedReader(new FileReader(file));
		ArrayList<String> sequences = new ArrayList<String>();

		String line = null;
		while ((line = reader.readLine()) != null) {
			// System.out.println(line);
			EventRecordG eventg = new Gson().fromJson(line, EventRecordG.class);
			sequences.add(eventg.toString());
		}
		reader.close();

		return sequences;
	}

	/**
	 * parse a .output file to a list of EventRecord
	 */
	public static List<EventRecordWithCallstack> file2EventRecordWithCallstackList(String filepath) throws IOException {
		File file = new File(filepath);
		BufferedReader reader = new BufferedReader(new FileReader(file));
		ArrayList<EventRecordWithCallstack> sequences = new ArrayList<>();

		String line = null;
		while ((line = reader.readLine()) != null) {
			EventRecordWithCallstack newDataRecord = new Gson().fromJson(line, EventRecordWithCallstack.class);
			sequences.add(newDataRecord);
		}
		reader.close();
		return sequences;
	}

	/**
	 * parse a .output.tracePreprocessed file to a list of EventRecordG
	 */
	public static List<EventRecordWithCallstackG> file2EventRecordWithCallstackGList(
			String filepath) throws IOException {
		File file = new File(filepath);
		BufferedReader reader = new BufferedReader(new FileReader(file));
		ArrayList<EventRecordWithCallstackG> sequences = new ArrayList<>();

		String line = null;
		while ((line = reader.readLine()) != null) {
			EventRecordWithCallstackG eventRecordG = new Gson().fromJson(line, EventRecordWithCallstackG.class);
			sequences.add(eventRecordG);
		}
		reader.close();
		return sequences;
	}

	public static List<AggAPIRecord> file2AggAPIRecordList(String filepath) throws IOException {
		File file = new File(filepath);
		BufferedReader reader = new BufferedReader(new FileReader(file));
		ArrayList<AggAPIRecord> sequences = new ArrayList<AggAPIRecord>();

		String line = null;
		String lastAPI = "-1";
		String lastThreadID = "-1";
		//String lastExeCallStack = "-1";
		while ((line = reader.readLine()) != null) {
			EventRecordWithCallstackG eventg = new Gson().fromJson(line, EventRecordWithCallstackG.class);
			String[] callStackSplitResults = eventg.systemCallStack.split(",");
			if (//!lastExeCallStack.equals(eventg.exeCallStack) || 
					!lastAPI.equals(callStackSplitResults[callStackSplitResults.length - 1]) || !lastThreadID.equals(eventg.threadID)) {
				AggAPIRecord newAggAPIRecord = new AggAPIRecord(eventg.threadID);
				sequences.add(newAggAPIRecord);
				lastAPI = callStackSplitResults[callStackSplitResults.length - 1];
				lastThreadID = eventg.threadID;
				//lastExeCallStack = eventg.exeCallStack;
			}
			sequences.get(sequences.size() - 1).eventSeq.add(new EventRecordWithCallstackM(eventg.eventName,
					eventg.parameter, eventg.systemCallStack));
		}
		reader.close();
		return sequences;
	}

	public static List<Integer> fileAlignment2ListWindowsize(String filepath) throws IOException {
		File file = new File(filepath);
		BufferedReader reader = new BufferedReader(new FileReader(file));
		ArrayList<Integer> windowsizeList = new ArrayList<Integer>();

		String line = null;
		int lineNumber = 0;
		while ((line = reader.readLine()) != null) {
			if (line.equals("")) {
				windowsizeList.add(0);
				continue;
			}
			if (lineNumber % 2 != 0) {
				List<String> record_list = Arrays.asList(line.split(" -> "));
				Integer start, end;
				start = Integer.valueOf(record_list.get(0));
				end = Integer.valueOf(record_list.get(record_list.size() - 1));
				windowsizeList.add(end - start + record_list.size());
				// System.out.println(start + " " + end);
			}
			++lineNumber;
		}
		reader.close();
		return windowsizeList;
	}

	public static List<Integer> file2ListWindowsize(String filepath) throws NumberFormatException, IOException {
		File file = new File(filepath);
		BufferedReader reader = new BufferedReader(new FileReader(file));
		ArrayList<Integer> windowsizeList = new ArrayList<Integer>();

		String line = null;
		while ((line = reader.readLine()) != null) {
			windowsizeList.add(Integer.valueOf(line));
		}
		reader.close();
		return windowsizeList;
	}
}
