package edu.nu.marple.filemanipulation;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.HashMap;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ibm.marple.EventRecordWithCallstack;

import edu.nu.marple.Utils;
import edu.nu.marple.callstackbasics.CallstackGlobalConfig;
import edu.nu.marple.callstackbasics.EventRecordWithCallstackG;

public class ThreadSplitting {
	
	public static String[] focusedPhfs = {"keylogger", "remotedesktop", "download&exec", "audiorecord", "remoteshell"};

	public static String traceExtension = CallstackGlobalConfig.preprocessedTraceFileExtension;
	public static HashMap<String, BufferedWriter> writer = new HashMap<String, BufferedWriter>();
	
	private static EventRecordWithCallstackG convertEventRecordGJson2Object(String json) {
   		return new Gson().fromJson(json, EventRecordWithCallstackG.class);
   	}

	public static void main(String[] args) throws IOException {
		processBenignSet();
	}

	public static void processBenignSet() throws IOException {
		String[] traceDir = { "data\\other\\usenix" 
							  //"data/tmp/offlinetest/" 
						    };
		writer.clear();
		for (String dir : traceDir) {
			File nowDir = new File(dir);
			File[] files = nowDir.listFiles(new FilenameFilter() {
				public boolean accept(File dir, String name) {
					return name.endsWith(traceExtension);
				}
			});

			for (File json : files) {
				System.out.println(json.getPath());
				String[] splitResults = json.getName().split(traceExtension);
				String fileName = splitResults[0];
				BufferedReader br = new BufferedReader(new FileReader(json));
				String data;
				while ((data = br.readLine()) != null) {
					EventRecordWithCallstackG thisRecord = convertEventRecordGJson2Object(data);
					String newFileName = fileName + "_PID" + thisRecord.processID + "_TID" + thisRecord.threadID + traceExtension;
					if (!writer.containsKey(newFileName)) {
						System.out.println(dir + "\\" + newFileName);
						writer.put(newFileName, new BufferedWriter(new FileWriter(dir + "\\" + newFileName)));
					}
					writer.get(newFileName).write(new GsonBuilder().create().toJson(thisRecord));
					writer.get(newFileName).newLine();
				}
				br.close();
				for (BufferedWriter bw : writer.values())
					bw.close();
			}
		}
		
	}
}
