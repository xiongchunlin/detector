package edu.nu.marple.filemanipulation;

import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import edu.nu.marple.callstackbasics.AARSigResult;
import edu.nu.marple.callstackbasics.CallstackGlobalConfig;
import edu.nu.marple.callstackbasics.TopAPISigResult;

public class ConvertAAR2TopAPI {
	
	public static void main(String[] args) throws JsonIOException, JsonSyntaxException, IOException {
		String[] phfs = CallstackGlobalConfig.focusedPhfs;
		String inputFolder = CallstackGlobalConfig.AARSigVersionFolder;
		String outputFolder = CallstackGlobalConfig.TopAPISigVersionFolder;
		String signatureFileExtention = CallstackGlobalConfig.AARSigFileExtension;
		String signatureFileExtention2 = CallstackGlobalConfig.TopAPISigFileExtension;
		for (String itrPhf : phfs) {
			List<AARSigResult> finalSigs = new Gson().fromJson(new FileReader(inputFolder + itrPhf + signatureFileExtention), new TypeToken<List<AARSigResult>>(){}.getType());
			BufferedWriter bw = new BufferedWriter(new FileWriter(outputFolder + itrPhf + signatureFileExtention2));
			for (AARSigResult itrSig : finalSigs) {
				bw.write(new GsonBuilder().setPrettyPrinting().create().toJson(new TopAPISigResult(itrSig)));
				bw.newLine();
			}
			bw.close();
		};
	}
}
