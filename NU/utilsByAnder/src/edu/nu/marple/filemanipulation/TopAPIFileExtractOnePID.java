package edu.nu.marple.filemanipulation;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import com.google.gson.Gson;
import edu.nu.marple.callstackbasics.TopAPIRecord;

public class TopAPIFileExtractOnePID {
	
	public static int targetPID = 2244;
	
	public static void main(String[] args) throws IOException {
		 BufferedReader br = new BufferedReader(new FileReader("data/engagement4/output2.out"));
		 BufferedWriter bw = new BufferedWriter(new FileWriter("data/engagement4/filteredoutput2.out"));
		 String data = null;
		 while ((data = br.readLine()) != null) {
			 TopAPIRecord record = new Gson().fromJson(data, TopAPIRecord.class);
			 if (record.processID == targetPID) {
				 bw.write(data);
				 bw.newLine();
			 }
		 }
		 br.close();
		 bw.close();
	}
	
}
