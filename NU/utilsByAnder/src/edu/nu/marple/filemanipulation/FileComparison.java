package edu.nu.marple.filemanipulation;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class FileComparison {
	public static void main(String[] args) throws IOException {
		BufferedReader br1 = new BufferedReader(new FileReader("data/non-phf-filter/audiorecord/Albertino22.output"));
		BufferedReader br2 = new BufferedReader(new FileReader("data/non-phf-filter_before/audiorecord/Albertino22.output"));
		String data1, data2;
		int cnt = 0;
		while ((data1 = br1.readLine()) != null) {
			++cnt;
			data2 = br2.readLine();
			if (!data1.equals(data2)) {
				System.out.println(cnt);
				System.out.println(data1.length() + " " + data2.length());
				for (int i = 0; i < data1.length(); ++i) 
					if (data1.charAt(i) != data2.charAt(i)) {
						System.out.println(i + " " + data1.charAt(i) + " " + data2.charAt(i));
						break;
					}
				break;
			}
		}
		br1.close();
		br2.close();
	}
}
