package edu.nu.marple.callstacksigrefine;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.google.gson.GsonBuilder;

import edu.nu.marple.callstackbasics.AggAPIRecord;
import edu.nu.marple.callstackbasics.CallstackGlobalConfig;
import edu.nu.marple.callstackbasics.AARSigResult;
import edu.nu.marple.callstackbasics.EventRecordWithCallstackG;
import edu.nu.marple.callstackdetectionsystem.SequenceSignatureAggSigMatch;
import edu.nu.marple.callstacktool.CallstackFilter;
import edu.nu.marple.callstacktool.CallstackUtils;
import edu.nu.marple.filemanipulation.FileReadingUtil;

public class CallstackNonPhfFiltering {
	
	public static Map<String, Map<String, List<EventRecordWithCallstackG>>> phf2MapofBufferedRecords = new HashMap<>();
	public TreeMap<Integer, Integer> FPmatchedCntStat = new TreeMap<>();
	public static HashMap<String, Integer> phfSpecificMatchCount = new HashMap<String, Integer>();
	public static HashMap<String, Double> phfSpecificMatchPort = new HashMap<String, Double>();
	public static int unifiedThreshold = 0;
	public static double unifiedPort = 0.8;

	public static String dataRootFolderPath = CallstackGlobalConfig.trainingDataRootFolder;
	public static String traceExtention = CallstackGlobalConfig.preprocessedTraceFileExtension;

	List<AARSigResult> results = null;
	
	int[] FPmatchedCnt;
	int[] CoverageCnt;
	HashSet<String>[] FPTraces;
	HashSet<String>[] CoverTraces;
	int totalFPCnt = 0;
	int totalCoverageCnt = 0;

	static {
		phfSpecificMatchCount.put("keylogger", unifiedThreshold); 
		phfSpecificMatchCount.put("audiorecord", unifiedThreshold); 
		phfSpecificMatchCount.put("remotedesktop", unifiedThreshold); 
		phfSpecificMatchCount.put("remoteshell", unifiedThreshold); 
		phfSpecificMatchCount.put("download&exec", unifiedThreshold); 
		
		phfSpecificMatchPort.put("keylogger", 0.9); 
		phfSpecificMatchPort.put("audiorecord", unifiedPort); 
		phfSpecificMatchPort.put("remotedesktop", 0.9); 
		phfSpecificMatchPort.put("remoteshell", unifiedPort); 
		phfSpecificMatchPort.put("download&exec", unifiedPort);

		File dataRootFolder = new File(
				dataRootFolderPath); 
		File[] phfFolder = dataRootFolder.listFiles();

		System.out.println("Start to read data");

		for (int i = 0; i < phfFolder.length; ++i)
			if (phfFolder[i].isDirectory()) {
				File[] files = phfFolder[i].listFiles(new FilenameFilter() {
					public boolean accept(File dir, String name) {
						return name.endsWith(traceExtention);
					}
				});
				phf2MapofBufferedRecords.put(phfFolder[i].getName(), new HashMap<>());
				// System.out.println(nowFocusedPhf + ": " + fileCnt);
				for (int j = 0; j < files.length; ++j) {
					System.out.println("Reading " + files[j].getPath());
					List<EventRecordWithCallstackG> newBufferedRecords = new ArrayList<>();
					try {
						newBufferedRecords = FileReadingUtil.file2EventRecordWithCallstackGList(files[j].getPath());
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					phf2MapofBufferedRecords.get(phfFolder[i].getName()).put(files[j].getPath() ,newBufferedRecords);
				}
			}
		System.out.println("Finish reading data");
	}
	
	public void filterOnThread(List<AggAPIRecord> sequence, List<AARSigResult> finalSigList, boolean isFiltering, String name, double thresholdPort) throws IOException {
//		if (name.contains("Coringa-RATv0.1-2.callstackoutput.tracePreprocessed") && sequence.get(0).threadID.equals("4004")) {
//			int meiyong = 0;
//			meiyong++;
//		}
		
		SequenceSignatureAggSigMatch ssam = new SequenceSignatureAggSigMatch(sequence);
		double maxPort = -1.0;
		for (int i = 0; i < FPmatchedCnt.length; ++i) {
			double port = ssam.matchForNonPhfFiltering(finalSigList.get(i)); 
			if (port >= thresholdPort) {
				if (isFiltering) {
					++FPmatchedCnt[i];
					FPTraces[i].add(name);
				} else {
					++CoverageCnt[i];
					CoverTraces[i].add(name);
				}
			}
			if (port > maxPort) maxPort = port;
		}
		System.out.println("Max Port for " + name + "_tid" + sequence.get(0).threadID + " :" + maxPort);
	}

	public CallstackNonPhfFiltering(String focusedPhf, List<AARSigResult> finalSigList) throws IOException {
		results = finalSigList;
		FPmatchedCnt = new int[finalSigList.size()];
		CoverageCnt = new int[finalSigList.size()];
		FPTraces = new HashSet[finalSigList.size()];
		CoverTraces = new HashSet[finalSigList.size()];
		for (int i = 0; i < FPmatchedCnt.length; ++i) {
			FPTraces[i] = new HashSet<String>();
			CoverTraces[i] = new HashSet<String>();
		}
		
		for (String folderName : phf2MapofBufferedRecords.keySet()) {
			boolean isFiltering = false;
			if (!folderName.equals(focusedPhf)) {
				System.out.println("Filtering on " + folderName + " of " + phf2MapofBufferedRecords.get(folderName).size() + " traces");
				isFiltering = true;
			} else
				System.out.println("Calculating on " + folderName + " of " + phf2MapofBufferedRecords.get(folderName).size() + " traces");
			for (String traceID : phf2MapofBufferedRecords.get(folderName).keySet()) {
				System.out.println("");
				System.out.println("Processing on " + traceID + " phf:" + focusedPhf);
				
				Map<String, List<EventRecordWithCallstackG>> thread2Trace = new HashMap<>();
				List<EventRecordWithCallstackG> inputSequence = phf2MapofBufferedRecords.get(folderName).get(traceID);
				
				for (EventRecordWithCallstackG itrEventG : inputSequence) {
					if (!thread2Trace.containsKey(itrEventG.threadID)) {
						thread2Trace.put(itrEventG.threadID, new ArrayList<EventRecordWithCallstackG>());
					}
					thread2Trace.get(itrEventG.threadID).add(itrEventG);
				}
				for (String itrThreadKey : thread2Trace.keySet()) {
					List<EventRecordWithCallstackG> intermediateSequence = new CallstackFilter<EventRecordWithCallstackG>().getOnePossibleSequneceByChoosingLongestLoop(thread2Trace.get(itrThreadKey));
					//List<EventRecordWithCallstackG> intermediateSequence = thread2Trace.get(itrThreadKey);
					List<AggAPIRecord> outputSequence = CallstackUtils.convert2AggAPIRecordList(intermediateSequence);
					
					if (outputSequence.size() <= 3)
						continue;
					System.out.println("thread " + itrThreadKey + " size " + outputSequence.size());
					filterOnThread(outputSequence, finalSigList, isFiltering, traceID, phfSpecificMatchPort.get(focusedPhf));
				}
			}
		}

		for (int i = 0; i < FPmatchedCnt.length; ++i)
			if (FPmatchedCntStat.containsKey(FPmatchedCnt[i])) {
				FPmatchedCntStat.put(FPmatchedCnt[i], FPmatchedCntStat.get(FPmatchedCnt[i]) + 1);
			} else
				FPmatchedCntStat.put(FPmatchedCnt[i], 1);

		HashSet<String> FPTotalStat = new HashSet<String>();
		HashSet<String> CoverTotalStat = new HashSet<String>();
		
		for (int i = 0; i < results.size(); ++i)
		if (FPmatchedCnt[i] <= phfSpecificMatchCount.get(focusedPhf)) {
			FPTotalStat.addAll(FPTraces[i]);
			CoverTotalStat.addAll(CoverTraces[i]);
		}
		
		totalFPCnt = FPTotalStat.size();
		totalCoverageCnt = CoverTotalStat.size();
		
		System.out.println("TotalFPCnt: " + totalFPCnt);
		System.out.println("TotalCoverCnt: " + totalCoverageCnt);
		for (int i = 0; i < results.size(); ++i) {
			if (totalCoverageCnt != 0) 
				results.get(i).coverage = (double) CoverageCnt[i] / totalCoverageCnt;
			else
				results.get(i).coverage = 0.0;
			if (totalFPCnt != 0)
				results.get(i).falsePositiveShare = (double) FPmatchedCnt[i] / totalFPCnt;
			else
				results.get(i).falsePositiveShare = 0.0;
		}
		
		File filteredSignature = new File("generatedSignature/" + focusedPhf + "_filtered.AggSig");
		BufferedWriter bw = new BufferedWriter(new FileWriter(filteredSignature));
		for (int i = 0, No = 0; i < results.size(); ++No) {
			System.out.println("Start on signature " + No + ", FPMatchedCnt=" + FPmatchedCnt[No]);
			if (FPmatchedCnt[No] > phfSpecificMatchCount.get(focusedPhf)) {
				bw.write(new GsonBuilder().setPrettyPrinting().create().toJson(results.get(i)));
				results.remove(i);
			} else {
				++i;
			}
		}
		bw.close();
	}

	public List<AARSigResult> getResult() {
		return results;
	}
}
