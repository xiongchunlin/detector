package edu.nu.marple.callstackbasics;

import java.util.ArrayList;

public class AggAPIRecord {
	public ArrayList<EventRecordWithCallstackM> eventSeq;
	public String threadID;
	
	public AggAPIRecord(String threadID) {
		this.threadID = threadID;
		this.eventSeq = new ArrayList<EventRecordWithCallstackM>();
	}
	
	public boolean acceptedBy(AggAPIRecord targetAggAPIRecord) {
		int i = 0, j = 0;
		while (i < targetAggAPIRecord.eventSeq.size() && j < eventSeq.size()) {
			if (targetAggAPIRecord.eventSeq.get(i).equals(eventSeq.get(j))) ++j;
			++i;
		}
		if (j == eventSeq.size()) return true;
		return false;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((eventSeq == null) ? 0 : eventSeq.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AggAPIRecord other = (AggAPIRecord) obj;
		if (eventSeq == null) {
			if (other.eventSeq != null)
				return false;
		} else if (!eventSeq.equals(other.eventSeq))
			return false;
		return true;
	}
}
