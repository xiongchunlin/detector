package edu.nu.marple.callstackbasics;

import java.util.Map;

public class TopAPIRecord {
	public int processID;
	public int threadID;
	public String TimeStamp;
	public String EventName;
	public Map<String, String> arguments;
}
