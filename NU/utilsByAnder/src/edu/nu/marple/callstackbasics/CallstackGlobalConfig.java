package edu.nu.marple.callstackbasics;

import java.util.Arrays;
import java.util.List;

public class CallstackGlobalConfig {
	public static String trainingDataRootFolder = "data/training/";
	public static String testingDataRootFolder = "data/testing/";
	public static String rawTraceFileExtension = ".callstackoutput";
	public static String preprocessedTraceFileExtensionAdded = ".tracePreprocessed";
	public static String preprocessedTraceFileExtension = ".callstackoutput.tracePreprocessed";
	public static String uselessSyscallFile = "importantList/useless_e4.txt";
	public static String usefulEventFile = "importantList/usefulevent3.txt";
	public static String uselessAPIFile = "importantList/uselessAPIs.txt";
	public static String AARSigFileExtension = ".AggSig";
	public static String TopAPISigFileExtension = ".TopSig";
	public static boolean argPreprocessedOn = true;
	public static boolean loopReductionOnInputTrace = true;
	/**
	 * Used in SequenceSignatureAggSigMath
	 */
	public static String AARSigVersionFolder = "aggSignature/20180703_nonphffilter_training_0.9_generalAPI2/";
	public static String TopAPISigVersionFolder = "topAPISignature/20180703_nonphffilter_training_0.9_generalAPI2/";
	public final static int MaxSigCnt = 10000;
	public final static int MaxPhfCnt = 5;
	public final static int MaxSeqSigLen = 4000;
	
	/**
	 * Used in LoopReductionSignatureGenerationMain
	 */
	public static String[] focusedPhfs = {
			"audiorecord",
			"keylogger",
			"download&exec",
			"remoteshell",
			"remotedesktop"
	};
	
	/**
	 * Used in ApproximateAARTestMain
	 */
	public static List<String> testDirs = Arrays.asList(
//			  "data/testing/audiorecord",
//			  "data/testing/keylogger",
//			  "data/testing/remoteshell",
//			  "data/testing/remotedesktop",
//			  "data/testing/download&exec",
//			  "data/testing/benign"
//			  "data/testing/onlinebenign"
//			  "data/testing/onlinetesting/20180704andy"
			 // "data/testing/0704 benign fullcallstack"
			  //"data/testing/weird"
			  "data/other/usenix"
			  );
	
	/**
	 * Used in CallstackTracePreProcessMain
	 */
	public static List<String> preprocessedTrainingPhfs = Arrays.asList(
			"audiorecord",
			"remoteshell",
			"keylogger",
			"download&exec",
			"remotedesktop"
	);
	
	/**
	 * Used in CallstackTracePreProcessMain
	 */
	public static List<String> preprocessedTestingPhfs = Arrays.asList(
//			"audiorecord",
//			"remoteshell",
//			"keylogger",
//			"remotedesktop",
//			"download&exec"
//			"benign"
//	        "onlinebenign"
//			"onlinetesting/20180704andy",
			"0704 benign fullcallstack"
	);
}
