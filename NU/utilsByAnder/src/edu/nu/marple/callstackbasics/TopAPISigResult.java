package edu.nu.marple.callstackbasics;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class TopAPISigResult {
	public Set<String> traces;
	public List<String> topAPISeq;
	public int windowsize;

	public double coverage;
	public double falsePositiveShare;
	
	public TopAPISigResult(AARSigResult sigRes) {
		this.traces = sigRes.traces;
		this.topAPISeq = new ArrayList<String>();
		this.windowsize = sigRes.windowsize;
		this.coverage = sigRes.coverage;
		this.falsePositiveShare = sigRes.falsePositiveShare;
		
		for (AggAPIRecord aggRec : sigRes.aggrecSeq) {
			String[] callStackLayer = aggRec.eventSeq.get(0).systemCallStack.split(",");
			this.topAPISeq.add(callStackLayer[callStackLayer.length - 1]);
		}
	}
}
