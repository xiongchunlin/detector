package edu.nu.marple.callstackbasics;


public class EventRecordWithCallstackG {
	public String processID;
	public String threadID;
	public String eventName;
	public String parameter;
	public String systemCallStack;
	public String exeCallStack;
	
	public EventRecordWithCallstackG(String threadID, String processID, String name, String parameter, String systemcallstack, String execallstack) {
		this.threadID = threadID;
		this.processID = processID;
		this.eventName = name;
		this.parameter = parameter;
		this.systemCallStack = systemcallstack;
		this.exeCallStack = execallstack;
	}
	
	public EventRecordWithCallstackG(EventRecordWithCallstackG eventG) {
		this.threadID = eventG.threadID;
		this.processID = eventG.processID;
		this.eventName = eventG.eventName;
		this.parameter = eventG.parameter;
		this.systemCallStack = eventG.systemCallStack;
		this.exeCallStack = eventG.exeCallStack;
	}
	
	@Override
	public String toString() {
		return eventName + (parameter.isEmpty() ? "" : " @ " + parameter + " : " + systemCallStack);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EventRecordWithCallstackG other = (EventRecordWithCallstackG) obj;
		if (exeCallStack == null) {
			if (other.exeCallStack != null)
				return false;
		} else if (!exeCallStack.equals(other.exeCallStack))
			return false;
		if (eventName == null) {
			if (other.eventName != null)
				return false;
		} else if (!eventName.equals(other.eventName))
			return false;
		if (parameter == null) {
			if (other.parameter != null)
				return false;
		} else if (!parameter.equals(other.parameter))
			return false;
		if (processID == null) {
			if (other.processID != null)
				return false;
		} else if (!processID.equals(other.processID))
			return false;
		if (systemCallStack == null) {
			if (other.systemCallStack != null)
				return false;
		} else if (!systemCallStack.equals(other.systemCallStack))
			return false;
		if (threadID == null) {
			if (other.threadID != null)
				return false;
		} else if (!threadID.equals(other.threadID))
			return false;
		return true;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((exeCallStack == null) ? 0 : exeCallStack.hashCode());
		result = prime * result + ((eventName == null) ? 0 : eventName.hashCode());
		result = prime * result + ((parameter == null) ? 0 : parameter.hashCode());
		result = prime * result + ((processID == null) ? 0 : processID.hashCode());
		result = prime * result + ((systemCallStack == null) ? 0 : systemCallStack.hashCode());
		result = prime * result + ((threadID == null) ? 0 : threadID.hashCode());
		return result;
	}
	
//	public static EventRecordWithCallstackG stringToEventRecordG(String data) {
//		String[] splitResults = data.split(" @ ");
//		EventRecordWithCallstackG returnValue = new EventRecordWithCallstackG(splitResults[0], "");
//		if (splitResults.length > 1) returnValue.parameter = splitResults[1];
//		return returnValue;
//	}
}
