package edu.nu.marple.callstackbasics;

public class EventRecordWithCallstackM {
	public String eventName;
	public String parameter;
	public String systemCallStack;
	
	public EventRecordWithCallstackM(String name, String parameter, String systemcallstack) {
		this.eventName = name;
		this.parameter = parameter;
		this.systemCallStack = systemcallstack;
	}
	
	public EventRecordWithCallstackM(EventRecordWithCallstackM eventM) {
		this.eventName = eventM.eventName;
		this.parameter = eventM.parameter;
		this.systemCallStack = eventM.systemCallStack;
	}
	
//	public EventRecordWithCallstackG(EventRecordM eventrecord) {
//		this.name = eventrecord.name;
//		this.parameter = eventrecord.parameter;
//	}
	
	@Override
	public String toString() {
		return eventName + (parameter.isEmpty() ? "" : " @ " + parameter + " : " + systemCallStack);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EventRecordWithCallstackM other = (EventRecordWithCallstackM) obj;
		if (eventName == null) {
			if (other.eventName != null)
				return false;
		} else if (!eventName.equals(other.eventName))
			return false;
		if (parameter == null) {
			if (other.parameter != null)
				return false;
		} else if (!parameter.equals(other.parameter))
			return false;
		if (systemCallStack == null) {
			if (other.systemCallStack != null)
				return false;
		} else if (!systemCallStack.equals(other.systemCallStack))
			return false;
		return true;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((eventName == null) ? 0 : eventName.hashCode());
		result = prime * result + ((parameter == null) ? 0 : parameter.hashCode());
		result = prime * result + ((systemCallStack == null) ? 0 : systemCallStack.hashCode());
		return result;
	}
	
//	public static EventRecordWithCallstackG stringToEventRecordG(String data) {
//		String[] splitResults = data.split(" @ ");
//		EventRecordWithCallstackG returnValue = new EventRecordWithCallstackG(splitResults[0], "");
//		if (splitResults.length > 1) returnValue.parameter = splitResults[1];
//		return returnValue;
//	}
}
