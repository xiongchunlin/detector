package edu.nu.marple.callstackbasics;

public class EventRecordWithCallstackCompressed {
	public String eventName;
	public String parameter;
	public String systemCallStack;
	public int duplicateFromTop;
	//public String exeCallStack;
	
	public EventRecordWithCallstackCompressed(String eventName, String parameter, String systemCallStack,
			int duplicateFromTop) {
		super();
		this.eventName = eventName;
		this.parameter = parameter;
		this.systemCallStack = systemCallStack;
		this.duplicateFromTop = duplicateFromTop;
		//this.exeCallStack = exeCallStack;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + duplicateFromTop;
		result = prime * result + ((eventName == null) ? 0 : eventName.hashCode());
		//result = prime * result + ((exeCallStack == null) ? 0 : exeCallStack.hashCode());
		result = prime * result + ((parameter == null) ? 0 : parameter.hashCode());
		result = prime * result + ((systemCallStack == null) ? 0 : systemCallStack.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EventRecordWithCallstackCompressed other = (EventRecordWithCallstackCompressed) obj;
		if (duplicateFromTop != other.duplicateFromTop)
			return false;
		if (eventName == null) {
			if (other.eventName != null)
				return false;
		} else if (!eventName.equals(other.eventName))
			return false;
//		if (exeCallStack == null) {
//			if (other.exeCallStack != null)
//				return false;
//		} else if (!exeCallStack.equals(other.exeCallStack))
//			return false;
		if (parameter == null) {
			if (other.parameter != null)
				return false;
		} else if (!parameter.equals(other.parameter))
			return false;
		if (systemCallStack == null) {
			if (other.systemCallStack != null)
				return false;
		} else if (!systemCallStack.equals(other.systemCallStack))
			return false;
		return true;
	}
	
	
}
