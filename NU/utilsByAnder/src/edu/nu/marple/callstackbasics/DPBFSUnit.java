package edu.nu.marple.callstackbasics;

import java.util.ArrayList;
import java.util.List;

public class DPBFSUnit {
	public List<Integer> key = new ArrayList<Integer>();
	public Integer score;
	
	public DPBFSUnit(List<Integer> key, Integer score) {
		this.key = key;
		this.score = score;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((key == null) ? 0 : key.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DPBFSUnit other = (DPBFSUnit) obj;
		if (key == null) {
			if (other.key != null)
				return false;
		} else if (!key.equals(other.key))
			return false;
		return true;
	}
}
