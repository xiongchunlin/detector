package edu.nu.marple.callstackbasics;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class AARSigResult{
	public Set<String> traces;
	public List<AggAPIRecord> aggrecSeq;
	public int windowsize;

	public double coverage;
	public double falsePositiveShare;

	public AARSigResult(Set<String> traces, List<AggAPIRecord> aggseq, int windowsize) {
		super();
		this.traces = traces;
		this.aggrecSeq = aggseq;
		this.windowsize = windowsize;
	}

	public AARSigResult() {
		this.aggrecSeq = new ArrayList<AggAPIRecord>();
		// TODO Auto-generated constructor stub
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((aggrecSeq == null) ? 0 : aggrecSeq.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AARSigResult other = (AARSigResult) obj;
		if (aggrecSeq == null) {
			if (other.aggrecSeq != null)
				return false;
		} else if (!aggrecSeq.equals(other.aggrecSeq))
			return false;
		return true;
	}

	
	
	
}
