package edu.nu.marple.statisticstool;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import com.ibm.marple.EventRecord;

import edu.nu.marple.GlobalConfig;
import edu.nu.marple.SigResults;
import edu.nu.marple.Utils;

public class ListOutAllEventNameAndArgField {
	
	static String traceExtension = ".callstackoutput";
	static String root = "data/withcallstack";
	static HashMap<String, HashSet<String>> stat = new HashMap<String, HashSet<String>>();
	
	public static void recursiveCount(String path) throws IOException {
		File thisFile = new File(path);
		System.out.println(thisFile.getPath());
		if (thisFile.isDirectory()) {
			File[] files = thisFile.listFiles();
			for (File nowFile : files) {
				recursiveCount(nowFile.getAbsolutePath());
			}
		} else
		if (thisFile.getName().endsWith(traceExtension)) {
			List<EventRecord> sequence = Utils.EventRecordFile2EventRecordList(thisFile.getAbsolutePath());
			for (EventRecord record : sequence) {
				if (!stat.containsKey(record.eventName)) {
					stat.put(record.eventName, new HashSet<String>());
				}
				for (String key : record.arguments.keySet()) {
					stat.get(record.eventName).add(key);
				}
			}
		}
	}
	
	public static void main(String[] args) throws IOException {
		BufferedWriter bw = new BufferedWriter(new FileWriter("generatedSignature/eventNameAndArgFieldStatistics.txt"));
		recursiveCount(root);
		Object[] tmpSet = stat.keySet().toArray();
		ArrayList<String> keySet = new ArrayList<String>();
		for (Object tmp : tmpSet) keySet.add((String) tmp);
		MyComparator mc = new MyComparator();
		Collections.sort(keySet, mc);
		for (String pr : keySet) {
			bw.write(pr + ": ");
			for (String pr2 : stat.get(pr)) {
				bw.write(pr2 + ",");
			}
			bw.newLine();
		}
		bw.close();
	}
}

class MyComparator implements Comparator {
	@Override
	public int compare(Object o1, Object o2) {
		String sigo1 = (String) o1;
		String sigo2 = (String) o2;
		return sigo1.compareTo(sigo2);
	}
}
