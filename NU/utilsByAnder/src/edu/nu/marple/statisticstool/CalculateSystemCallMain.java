package edu.nu.marple.statisticstool;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.ibm.marple.EventRecord;

import edu.nu.marple.EventRecordG;
import edu.nu.marple.Utils;

public class CalculateSystemCallMain {

	static HashSet<String> syscallStat = new HashSet<String>();

	public static void summarize(File file) throws IOException {
		List<EventRecordG> sequence = Utils.EventRecordGFile2EventRecordGenerationList(file.getAbsolutePath());
		for (EventRecordG er : sequence)
			if (er.name.equals("PerfInfoSysClEnter")) {
				syscallStat.add(er.parameter.split(":")[1]);
			}
	}

	public static void calculate(String filepath, String traceExtention) throws IOException {
		File path = new File(filepath);
		File[] files = path.listFiles(new FilenameFilter() {
			public boolean accept(File dir, String name) {
				return name.endsWith(traceExtention);
			}
		});

		for (File file : files)
			summarize(file);
		System.out.println(filepath + " summarized!");
	}

	public static void main(String[] args) throws IOException {
		List<String> testDirs = Arrays.asList("data/non-phf-filter/audiorecord", "data/non-phf-filter/keylogger",
				"data/non-phf-filter/remotedesktop", "data/non-phf-filter/remoteshell", "data/non-phf-filter/send&exec",
				"data/test/allphf-2", "data/test/FPs", "data/test/benignxiaoruan");
		String traceExtention = ".output.tracePreprocessed";
		testDirs.forEach(t -> {
			try {
				calculate(t, traceExtention);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});
		BufferedWriter bw = new BufferedWriter(new FileWriter("systemcallStat.txt"));
		for (String syscall : syscallStat) {
			bw.write(syscall);
			bw.newLine();
		}
		bw.write("Unique system call number: ");
		bw.write(Integer.toString(syscallStat.size()));
		bw.newLine();
		bw.close();
	}
}
