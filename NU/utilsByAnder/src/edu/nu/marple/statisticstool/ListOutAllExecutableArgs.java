package edu.nu.marple.statisticstool;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import com.ibm.marple.EventRecord;

import edu.nu.marple.GlobalConfig;
import edu.nu.marple.Utils;

public class ListOutAllExecutableArgs {
	
	static String traceExtension = ".output";
	static HashSet<String> exeStat = new HashSet<String>();

	static String root = "data/original";
	
	public static void recursiveCount(String path) throws IOException {
		File thisFile = new File(path);
		System.out.println(thisFile.getPath());
		if (thisFile.isDirectory()) {
			File[] files = thisFile.listFiles();
			for (File nowFile : files) {
				recursiveCount(nowFile.getAbsolutePath());
			}
		} else
		if (thisFile.getName().endsWith(traceExtension) && thisFile.length() <= 104857600L) {
			System.out.println(thisFile.length());
			List<EventRecord> sequence = Utils.EventRecordFile2EventRecordList(thisFile.getAbsolutePath());
			int count = 0;
			for (EventRecord record : sequence) {
				//System.out.println(count++);
				for (String key : record.arguments.keySet()) {
					if (record.arguments.get(key).contains(".exe")) {
						exeStat.add(record.arguments.get(key));
					}
				}
			}
		}
	}
	
	public static void main(String[] args) throws IOException {
		BufferedWriter bw = new BufferedWriter(new FileWriter("generatedSignature/exeStatistics.txt"));
		recursiveCount(root);
		for (String nowExe : exeStat) {
			bw.write(nowExe);
			bw.newLine();
		}
		bw.close();
	}
	
}
