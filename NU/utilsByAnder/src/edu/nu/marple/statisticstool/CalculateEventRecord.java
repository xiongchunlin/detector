package edu.nu.marple.statisticstool;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import edu.nu.marple.EventRecordG;
import edu.nu.marple.Utils;

public class CalculateEventRecord {
	
	static Map<String, Set<String>> map = new HashMap<String, Set<String>>();
	static Map<String, String> occur = new HashMap<String, String>();
	static Map<String, String> paraoccur = new HashMap<String, String>();
	
	public static void summarize(File file) throws IOException {
		List<EventRecordG> sequence = new ArrayList<EventRecordG>();
		//try {
			sequence = Utils.EventRecordGFile2EventRecordGenerationList(file.getAbsolutePath());
		//} catch (Exception e) {
		//	System.out.println(file.getPath());
		//	System.out.println(e.getMessage());
		//}
		for (EventRecordG er : sequence) {
			if (!map.containsKey(er.name)) {
				map.put(er.name, new HashSet<String>());
				occur.put(er.name, file.getPath());
			}
			String[] splitResults = er.parameter.split(" @ ");
			for (String str : splitResults) {
				String[] splitResults2 = str.split(":");
				map.get(er.name).add(splitResults2[0]);
				if (!paraoccur.containsKey(splitResults2[0]))
					paraoccur.put(splitResults2[0], file.getPath());
			}
		}
	}
	
	public static void calculate(String filepath, String traceExtention) throws IOException {
		File path = new File(filepath);
		File[] files = path.listFiles(new FilenameFilter() {
			public boolean accept(File dir, String name) {
				return name.endsWith(traceExtention);
			}
		});
		
		for (File file : files) summarize(file);
		System.out.println(filepath + " summarized!");
	}
	
	public static void main(String[] args) throws IOException{
		List<String> testDirs = Arrays.asList(
				  "data/non-phf-filter/audiorecord",
				  "data/non-phf-filter/benign",
				  "data/non-phf-filter/injectdll",
				  "data/non-phf-filter/keylogger",
				  "data/non-phf-filter/remotedesktop",
				  "data/non-phf-filter/remoteshell",
				  "data/non-phf-filter/systemtrace"
				  );
		String traceExtention = ".output";
		testDirs.forEach(t -> {
			try {
				calculate(t, traceExtention);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});
		BufferedWriter bw = new BufferedWriter(new FileWriter("eventRecordStat.txt"));
		List<String> keyList = new ArrayList<String>();
		keyList.addAll(map.keySet());
		Collections.sort(keyList);
		for (String key : keyList) {
			bw.write(key + ": ");
			for (String value : map.get(key)) 
				bw.write(value + ", ");
			bw.newLine();
		}
		bw.close();
		BufferedWriter bw2 = new BufferedWriter(new FileWriter("eventRecordFirstOccur.txt"));
		for (String key : occur.keySet()) {
			bw2.write(key + ": " + occur.get(key));
			bw2.newLine();
		}
		bw2.close();
		BufferedWriter bw3 = new BufferedWriter(new FileWriter("parameterFirstOccur.txt"));
		for (String key : paraoccur.keySet()) {
			bw3.write(key + ": " + paraoccur.get(key));
			bw3.newLine();
		}
		bw3.close();
	}
}
