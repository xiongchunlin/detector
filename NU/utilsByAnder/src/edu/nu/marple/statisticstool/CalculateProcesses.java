package edu.nu.marple.statisticstool;

import java.io.File;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

public class CalculateProcesses {
	
	static int normalRat = 0;
	static String traceExtention = ".syscall_args_extracted";
	
	public static void calculateStretch() throws IOException {
		File path = new File("data/syscall_args_txt/dump_stretch_corrected/");
		File[] files = path.listFiles(new FilenameFilter() {
			public boolean accept(File dir, String name) {
				return name.endsWith(traceExtention);
			}
		});
		
		HashSet<String> nameSet = new HashSet<String>();
		
		for (File file : files) {
			//System.out.println(file.getName());
			String name = file.getName();
			String[] tmp = name.split("\\.");
			String[] tmp2 = tmp[0].split("_");
			nameSet.add(tmp2[0].toLowerCase());
		}
		int totalSize = nameSet.size();
		System.out.println("Stretch: " + totalSize);
	}
	
	public static void calculateFaros() throws IOException {
		File path = new File("data/syscall_args_txt/faros/");
		File[] files = path.listFiles(new FilenameFilter() {
			public boolean accept(File dir, String name) {
				return name.endsWith(traceExtention);
			}
		});
		
		HashSet<String> nameSet = new HashSet<String>();
		
		for (File file : files) {
			//System.out.println(file.getName());
			String name = file.getName();
			String[] tmp = name.split("\\.");
			String[] tmp2 = tmp[0].split("_");
			nameSet.add(tmp2[1].toLowerCase());
		}
		int totalSize = nameSet.size();
		System.out.println("Faros: " + totalSize);
	}
	
	public static void calculateInjection() throws IOException {
		File path = new File("data/syscall_args_txt/merged_injection_trace_chunlin/");
		File[] files = path.listFiles(new FilenameFilter() {
			public boolean accept(File dir, String name) {
				return name.endsWith(traceExtention);
			}
		});
		
		HashSet<String> nameSet = new HashSet<String>();
		
		for (File file : files) {
			//System.out.println(file.getName());
			String name = file.getName();
			String[] tmp = name.split("\\.");
			String[] tmp2 = tmp[0].split("_");
			nameSet.add((tmp2[1] + tmp2[2]).toLowerCase());
		}
		int totalSize = nameSet.size();
		//for (String string : nameSet) System.out.println(string);
		System.out.println("Injection: " + totalSize);
	}
	
	public static void calculateObfuscation() throws IOException {
		File path = new File("data/syscall_args_txt/RAT-obfuscation-zhenyuan/");
		File[] files = path.listFiles(new FilenameFilter() {
			public boolean accept(File dir, String name) {
				return name.endsWith(traceExtention);
			}
		});
		
		HashSet<String> nameSet = new HashSet<String>();
		
		for (File file : files) {
			//System.out.println(file.getName());
			String name = file.getName();
			String[] tmp = name.split("\\.");
			String[] tmp2 = tmp[0].split("_");
			if (tmp2.length > 1) nameSet.add(tmp2[1].toLowerCase());
		}
		int totalSize = nameSet.size();
		//for (String string : nameSet) System.out.println(string);
		System.out.println("Obfuscation: " + totalSize);
	}
	
	public static void main(String[] args) throws IOException{
		calculateStretch();
		calculateFaros();
		calculateInjection();
		calculateObfuscation();
	}
}
