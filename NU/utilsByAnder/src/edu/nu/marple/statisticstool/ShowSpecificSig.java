package edu.nu.marple.statisticstool;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import edu.nu.marple.SigResults;

public class ShowSpecificSig {
	public final static String signatureFolder = "sequenceSignature/8.22_1/";
	public final static String signatureFileExtention = "_localResults_filtered.txt";
	public final static String nowPhf = "audiorecord";
	public final static int No = 23;
	
	public static void main(String[] args) throws JsonIOException, JsonSyntaxException, FileNotFoundException {
		List<SigResults> results = new Gson().fromJson(new FileReader(signatureFolder + nowPhf + signatureFileExtention), new TypeToken<List<SigResults>>(){}.getType());
		System.out.println(new GsonBuilder().setPrettyPrinting().create().toJson(results.get(No)));
	}
}
