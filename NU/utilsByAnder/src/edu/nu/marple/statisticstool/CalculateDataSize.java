package edu.nu.marple.statisticstool;

import java.io.File;
import java.io.FilenameFilter;
import java.util.Arrays;
import java.util.List;

public class CalculateDataSize {
	
	static int normalRat = 0;
	
	public static void calculate(String filepath, String traceExtention) {
		File path = new File(filepath);
		File[] files = path.listFiles(new FilenameFilter() {
			public boolean accept(File dir, String name) {
				return name.endsWith(traceExtention);
			}
		});
		
		int totalSize = 0;
		for (File file : files) totalSize += file.length();
		System.out.println(filepath + ": " + totalSize);
		if (filepath.contains("data/non") && !filepath.contains("benign")) normalRat += totalSize;
	}
	
	public static void main(String[] args){
		List<String> testDirs = Arrays.asList(
				  "data/non-phf-filter/audiorecord",
				  "data/non-phf-filter/benign",
				  "data/non-phf-filter/filemanager",
				  "data/non-phf-filter/initialization",
				  "data/non-phf-filter/keylogger",
				  "data/non-phf-filter/password",
				  "data/non-phf-filter/registrymanager",
				  "data/non-phf-filter/remotedesktop",
				  "data/non-phf-filter/remoteshell",
				  "data/non-phf-filter/upexe",
				  "data/non-phf-filter/urldownload",
				  "data/syscall_args_txt/31-unique-benign", 
				  "data/syscall_args_txt/faros", 
				  "data/syscall_args_txt/merged_injection_trace_chunlin", 
				  //"data/syscall_args_txt/real-world-rat-chunlin/",
				  "data/syscall_args_txt/dump_stretch_corrected/",
				  "data/syscall_args_txt/RAT-obfuscation-zhenyuan");
		String traceExtention = ".syscall_args_extracted";
		testDirs.forEach(t -> {
			calculate(t, traceExtention);
		});
		System.out.println("Normal RAT PHF: " + normalRat);
	}
}
