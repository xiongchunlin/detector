package edu.nu.marple.detectsystem;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import edu.nu.marple.EventRecordG;

/**
 * A cancelled approach to get aligned subsequences from 2 sequences.
 * Count the time window and split the corresponding sequence parts, use the Longest Common Sequence algorithm for getting aligned subsequences.
 * @author ander
 *
 */
public class LCSmatch {
	List<EventRecordG> seq1, seq2;
	int len1, len2;
	int[][] f;
	HashSet<List<EventRecordG>>[][] ans;
	
	public LCSmatch(List<EventRecordG> sequence1, List<EventRecordG> sequence2) {
		len1 = sequence1.size();
		len2 = sequence2.size();
		seq1 = sequence1;
		seq2 = sequence2;
		f = new int[len1][len2];
		ans = new HashSet[len1][len2];
		compare();
	}
	
	public boolean contained(EventRecordG s1, EventRecordG s2) {
		if (s1.name.equals(s2.name)) {
			if (s1.parameter.equals("")) return true;
			if (s1.parameter.equals(s2.parameter)) return true;
		}
		return false;
	}
	
	public void compare() {
		for (int p = 0; p < len1; ++p) {
			ans[p][0] = new HashSet<List<EventRecordG>>();
			ArrayList<EventRecordG> newList = new ArrayList<EventRecordG>();
			if (contained(seq1.get(p), seq2.get(0))) {
				f[p][0] = 1;
				newList.add(seq1.get(p));
			} else
				f[p][0] = 0;
			ans[p][0].add(newList);
		}
		for (int q = 0; q < len2; ++q) {
			ans[0][q] = new HashSet<List<EventRecordG>>();
			ArrayList<EventRecordG> newList = new ArrayList<EventRecordG>();
			if (contained(seq1.get(0), seq2.get(q))) {
				f[0][q] = 1;
				newList.add(seq1.get(0));
			} else
				f[0][q] = 0;
			ans[0][q].add(newList);
		}
		for (int p = 1; p < len1; ++p)
			for (int q = 1; q < len2; ++q) {
				f[p][q] = f[p - 1][q];
				ans[p][q] = new HashSet<List<EventRecordG>>();
				ans[p][q].addAll(ans[p - 1][q]);
				if (f[p][q - 1] > f[p][q]) {
					f[p][q] = f[p][q - 1];
					ans[p][q].clear();
					ans[p][q].addAll(ans[p][q - 1]);
				} else 
				if (f[p][q - 1] == f[p][q]){
					ans[p][q].addAll(ans[p][q - 1]);
				}
				if (contained(seq1.get(p), seq2.get(q))) {
					if (f[p - 1][q - 1] + 1 > f[p][q]) {
						f[p][q] = f[p - 1][q - 1] + 1;
						ans[p][q].clear();
						for (List<EventRecordG> oldList : ans[p - 1][q - 1]) {
							ArrayList<EventRecordG> newList = new ArrayList<EventRecordG>();
							newList.addAll(oldList);
							newList.add(seq1.get(p));
							ans[p][q].add(newList);
						}
					} else 
					if (f[p - 1][q - 1] + 1 == f[p][q]) {
						for (List<EventRecordG> oldList : ans[p - 1][q - 1]) {
							ArrayList<EventRecordG> newList = new ArrayList<EventRecordG>();
							newList.addAll(oldList);
							newList.add(seq1.get(p));
							ans[p][q].add(newList);
						}
					}
				}
			}
	}
	
	public Set<List<EventRecordG>> getMatchedAlignment() {
		Set<List<EventRecordG>> result = ans[len1 - 1][len2 - 1];
		return result;
	}
	
	public int getLCSLength() {
		return f[len1 - 1][len2 - 1];
	}
}
