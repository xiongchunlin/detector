package edu.nu.marple.detectsystem;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.ibm.marple.EventRecord;

import edu.nu.marple.SigResults;

/**
 * This class is serving as the most important class for detection algorithm. This algorithm is the reason why we can really achieve online re-time detection.
 * Simply speaking, the goal is to maintain a finite automata representing a string language(a set of strings) including all subsequences of the specific trace.
 * @author ander
 *
 */
public class SequenceSignatureWFAMatch {

	public final static int MaxSigCnt = 10000;
	public final static int MaxPhfCnt = 1;
	public final static int MaxSeqSigLen = 4000;
	public final static String[] phf = {
								 "download&exec"
//			  					 "remotedesktop",
//			  					 "audiorecord",
//			  					 "remoteshell",
//			  					 "keylogger"
			  					 //"send&exec"
			  					 };
	public final static String signatureFolder = "sequenceSignature/18.03.21/";
	public final static String signatureFileExtention = "_5tracesGlobalResults_filtered.txt";
	
	public static int eventCnt = 0;
	/**
	 * Map a EventRecordM to an integer for reduce overhead for finding indices when using WFA.
	 */
	public static HashMap<EventRecordM, Integer> mapping = new HashMap<EventRecordM, Integer>();
	/**
	 * For each eventType(name) of EventRecordM, store the possible parameters found in signatures for some quick filtering on noisy data.
	 * all different EventRecordMs will be mapped to [0, eventCnt). 
	 */
	public static HashMap<String, HashSet<String>> usefulParameter = new HashMap<String, HashSet<String>>();
	public static BufferedWriter detectionReport, detectionDetails;
	
	/**
	 * Every round on applying detection on different PHF. Reload signature sequences and corresponding data.
	 */
	public int phfSignatureCnt = 0;
	public int focusedPhf = 0;
	public List<EventRecordM>[] sequenceSignature = new List[MaxSigCnt];
	public double[] singleCredit = new double[MaxSigCnt];
	public Integer[] windowsize = new Integer[MaxSigCnt];
	public int[] firstOccurrenceEvidence = new int[MaxSeqSigLen];
	
	/**
	 * Loaded data for the detection system for this time.
	 */
	public Map<String, List<EventRecordM>> bufferedRecords;
	/**
	 * If the data are from a trace file, this represents the name of the trace file. Otherwise, it is "".
	 */
	public String fileName;
	
	/**
	 * Detection result related part.
	 */
	public Map<String, Integer> currentProcessResult = new HashMap<String, Integer>();
	public List<Map<String, Integer>> allProcessesResults = new ArrayList<Map<String, Integer>>();
	public Map<String, Integer> phf2sigcnt = new HashMap<String, Integer>();
	public Map<String, Double> currentProcessResult_credit = new HashMap<String, Double>();
	public List<Map<String, Double>> allProcessesResults_credit = new ArrayList<Map<String, Double>>();
	
	public SequenceSignatureWFAMatch(String fileName, Map<String, List<EventRecordM>> bufferedRecords) {
		String[] tmp = fileName.split("\\\\");
		this.fileName = tmp[tmp.length - 1];
		this.bufferedRecords = bufferedRecords;
	}
	
	static {
		/**
		 * Count unique events
		 */
		BufferedReader signatureFile = null;
		for (int itrPhf = 0 ; itrPhf < MaxPhfCnt; ++itrPhf) {
			try {
				signatureFile = new BufferedReader(new FileReader(signatureFolder + phf[itrPhf] + signatureFileExtention));
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
			List<SigResults> results = null;
			try {
				results = new Gson().fromJson(new FileReader(signatureFolder + phf[itrPhf] + signatureFileExtention), new TypeToken<List<SigResults>>(){}.getType());
			} catch (JsonIOException | JsonSyntaxException | FileNotFoundException e) {
				e.printStackTrace();
			}
			String[] splitResult;
			for (int i = 0; i < results.size(); ++i) {
				for (int j = 0; j < results.get(i).sig.size(); ++j) {
					/**
					 * For every eventType "A" occurring in signatures. We build an EventRecordM with ("A", ""). 
					 */
					splitResult = results.get(i).sig.get(j).split(" @ ");
					EventRecordM newEventRM = new EventRecordM(splitResult[0], "", "-1");
					if (!mapping.containsKey(newEventRM)) {
						mapping.put(newEventRM, (Integer)eventCnt);
						++eventCnt;
					}
					if (!usefulParameter.containsKey(splitResult[0])) {
						usefulParameter.put(splitResult[0], new HashSet<String>());
						usefulParameter.get(splitResult[0]).add("");
					}
					/**
					 * Signature event has parameters.
					 * We build another EventRecordM with("A", "A.para").
					 */
					if (splitResult.length > 1 && !splitResult[1].equals("")) {
						EventRecordM newEventRM2 = new EventRecordM(splitResult[0], splitResult[1], "-1");
						usefulParameter.get(splitResult[0]).add(splitResult[1]);
						if (!mapping.containsKey(newEventRM2)) {
							mapping.put(newEventRM2, (Integer)eventCnt);
							++eventCnt;
						}
					}
				}
			}
			try {
				signatureFile.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		try {
			detectionReport = new BufferedWriter(new FileWriter("detectionReport/phf_detection_report.txt"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			detectionDetails = new BufferedWriter(new FileWriter("detectionReport/phf_detection_details.txt"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public boolean matchOnSingleProcedure(List<EventRecordM> data) throws IOException {
		/**
		 * Build WILD FiniteAutomata(a DFA accepting all subsequences of a Event trace).
		 */
		int traceLen = data.size();
		ArrayList<WFANode> WFA = new ArrayList<WFANode>();
		int[] lastPos = new int[eventCnt + 1];
		boolean result = false;
		/**
		 * For all different EventRecordMs will be mapped to [0, eventCnt), so actually no EventRecordM is mapped to eventCnt. It is always used for pointing to root node(0).
		 */
		lastPos[eventCnt] = 0;
		WFA.add(new WFANode(eventCnt));
		int WFAnodeCnt = 1;
		for (int i = 0; i < traceLen; ++i) {
			if (usefulParameter.containsKey(data.get(i).name)) {
				int[] newlastPos = new int[eventCnt + 1];
				for (int j = 0; j <= eventCnt; ++j) newlastPos[j] = lastPos[j];
				/**
				 * When you see a EventRecordM with("A", "A.para"), build two corresponding WFANode for ("A", "A.para") and ("A", "").
				 * This is because when we are doing the signature matching, a ("A", "") in the signature should be matched when you see that ("A", "A.para") occurs in the trace.
				 * Deep understanding can be reached in our paper or ask Xutong for details.
				 */
				for (String itrParameter : usefulParameter.get(data.get(i).name)) 
				if (itrParameter.equals("") || data.get(i).parameter.equals(itrParameter)) {
					EventRecordM newEventRM = new EventRecordM(data.get(i).name, itrParameter, "-1");
					int nextEventNo = mapping.get(newEventRM);
					WFA.add(new WFANode(eventCnt));
					for (int j = 0; j <= eventCnt; ++j) 
						for (int k = lastPos[j]; k >= 0 && WFA.get(k).succ[nextEventNo] < 0; k = WFA.get(k).pred)
							WFA.get(k).succ[nextEventNo] = WFAnodeCnt;
					WFA.get(WFAnodeCnt).pred = lastPos[nextEventNo];
					WFA.get(WFAnodeCnt).pos = i + 1;
					newlastPos[nextEventNo] = WFAnodeCnt++;
				}
				lastPos = newlastPos;
			}
		}
		/**
		 * Matching Signature for offline test
		 */
		if (!fileName.equals("")) {
			int matchedCnt = 0;
			double accumulatedCredit = 1.0;
			for (int i = 0; i < phfSignatureCnt; ++i) {
				for (int j = 0; j < WFAnodeCnt; ++j) {
					int cursorNode = j, sigLen = sequenceSignature[i].size();
					Arrays.fill(firstOccurrenceEvidence, -1);
					for (int k = 0; k < sigLen; ++k) {
						cursorNode = WFA.get(cursorNode).succ[mapping.get(sequenceSignature[i].get(k))];
						if (cursorNode == -1) break;
						firstOccurrenceEvidence[k] = WFA.get(cursorNode).pos;
					}
					/**
					 * Find a match, print the details and the reports.
					 */
					if (cursorNode != -1 && firstOccurrenceEvidence[sequenceSignature[i].size() - 1] - firstOccurrenceEvidence[0] <= windowsize[i]) {
						detectionDetails.write(phf[focusedPhf] + " Matched!");
						detectionDetails.newLine();
						detectionDetails.write("Signature No: " + i);
						detectionDetails.newLine();
						detectionDetails.write("Single credit: " + singleCredit[i]);
						detectionDetails.newLine();
						detectionDetails.write("Start Timestamp: " + data.get(firstOccurrenceEvidence[0] - 1).time);
						detectionDetails.newLine();
						detectionDetails.write("End Timestamp: " + data.get(firstOccurrenceEvidence[sequenceSignature[i].size() - 1] - 1).time);
						detectionDetails.newLine();
						for (int k = 0; k < sequenceSignature[i].size(); ++k) {
							detectionDetails.write(firstOccurrenceEvidence[k] + " ");
						}
						detectionDetails.newLine();
						result = true;
						++matchedCnt;
						accumulatedCredit *= singleCredit[i];
						detectionDetails.write(phf[focusedPhf] + " Credit:" + String.valueOf(accumulatedCredit));
						detectionDetails.newLine();
						break;
					}
				}
			}
			detectionReport.write(matchedCnt + "/" + phfSignatureCnt);
			detectionReport.newLine();
			detectionReport.write(String.valueOf(accumulatedCredit));
			detectionReport.newLine();
			currentProcessResult.put(phf[focusedPhf], matchedCnt);
			if (matchedCnt > 0) 
				currentProcessResult_credit.put(phf[focusedPhf], accumulatedCredit);
			else
				currentProcessResult_credit.put(phf[focusedPhf], 0.0);
		}
		/**
		 * Matching Signature for non-phf filtering test
		 * The signature for non-phf testing will be put on sequenceSignature[0]
		 */
		else {
			for (int j = 0; j < WFAnodeCnt; ++j) {
				int cursorNode = j, sigLen = sequenceSignature[0].size();
				Arrays.fill(firstOccurrenceEvidence, -1);
				for (int k = 0; k < sigLen; ++k) {
					cursorNode = WFA.get(cursorNode).succ[mapping.get(sequenceSignature[0].get(k))];
					if (cursorNode == -1) break;
					firstOccurrenceEvidence[k] = WFA.get(cursorNode).pos;
				}
				/**
				 * Find the match, report the NonPHF filtering results.
				 */
				if (cursorNode != -1 && firstOccurrenceEvidence[sequenceSignature[0].size() - 1] - firstOccurrenceEvidence[0] <= windowsize[0]) {
					result = true;
					break;
				}
			}
		}
		return result;
	}
	
	public boolean matchForTest() throws IOException {
		boolean matched = false;
		detectionReport.write("Results for " + fileName);
		detectionReport.newLine();
		detectionDetails.write("Start to match trace: " + fileName);
		detectionDetails.newLine();
		for (focusedPhf = 0 ; focusedPhf < MaxPhfCnt; ++focusedPhf) {
			/**
			 * input for signature of focusedPHF.
			 */
			detectionReport.write("PHF: " + phf[focusedPhf]);
			detectionReport.newLine();
			detectionDetails.write("Matching PHF: " + phf[focusedPhf]);
			detectionDetails.newLine();
			List<SigResults> results = new Gson().fromJson(new FileReader(signatureFolder + phf[focusedPhf] + signatureFileExtention), new TypeToken<List<SigResults>>(){}.getType());
			phfSignatureCnt = results.size();
			String[] splitResult;
			for (int i = 0; i < phfSignatureCnt; ++i) {
				windowsize[i] = results.get(i).windowsize;
				sequenceSignature[i] = new ArrayList<EventRecordM>();
				singleCredit[i] = results.get(i).coverage / results.get(i).falsePositiveShare;
				for (int j = 0; j < results.get(i).sig.size(); ++j) {
					splitResult = results.get(i).sig.get(j).split(" @ ");
					EventRecordM newEventRM = new EventRecordM(splitResult[0], "", "-1");
					if (splitResult.length > 1) newEventRM.parameter = splitResult[1];
					sequenceSignature[i].add(newEventRM);
				}
			}
			/**
			 * invoke matching.
			 */
			this.phf2sigcnt.put(phf[focusedPhf], phfSignatureCnt);
			int procedureCnt = 0;
			for (String s : bufferedRecords.keySet()) {
				if (bufferedRecords.get(s).isEmpty())
					continue;
				currentProcessResult = allProcessesResults.get(procedureCnt);
				currentProcessResult_credit = allProcessesResults_credit.get(procedureCnt);
				++procedureCnt;
				detectionReport.write("process " + procedureCnt);
				detectionReport.newLine();
				detectionDetails.write("Start to match process, uuid: " + s);
				detectionDetails.newLine();					
				if (matchOnSingleProcedure(bufferedRecords.get(s))) {
					matched = true;
				}
			}
		}
		detectionReport.newLine();
		detectionDetails.newLine();
		return matched;
	}
	
	public boolean matchForNonPhfFiltering() throws IOException {
		/**
		 * No need for signature input because it will be given in NonPhfFilter.java.
		 * So directly invoke matching.
		 */
		for (int i = 0; i < sequenceSignature[0].size(); ++i) {
			EventRecordM newEventRM = new EventRecordM(sequenceSignature[0].get(i).name, "", "-1");
			if (!mapping.containsKey(newEventRM)) {
				mapping.put(newEventRM, (Integer)eventCnt);
				++eventCnt;
			}
			if (!usefulParameter.containsKey(newEventRM.name)) {
				usefulParameter.put(newEventRM.name, new HashSet<String>());
				usefulParameter.get(newEventRM.name).add("");
			}
			if (!sequenceSignature[0].get(i).parameter.equals("")) {
				EventRecordM newEventRM2 = new EventRecordM(sequenceSignature[0].get(i).name, sequenceSignature[0].get(i).parameter, "-1");
				usefulParameter.get(sequenceSignature[0].get(i).name).add(sequenceSignature[0].get(i).parameter);
				if (!mapping.containsKey(newEventRM2)) {
					mapping.put(newEventRM2, (Integer)eventCnt);
					++eventCnt;
				}
			}
		}
		for (String s : bufferedRecords.keySet()) {
			if (bufferedRecords.get(s).isEmpty())
				continue;
			if (matchOnSingleProcedure(bufferedRecords.get(s))) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Invoke match function for the data passed to this detection system.
	 * @return
	 * @throws IOException
	 */
	public boolean match() throws IOException{
		for (String s : bufferedRecords.keySet()){
			allProcessesResults.add(new HashMap<String, Integer>());
			allProcessesResults_credit.add(new HashMap<String, Double>());
		}		
		
		boolean matched = false;
		if (!fileName.equals("")) {
			matched = matchForTest();
		} else {
			matched = matchForNonPhfFiltering();
		}
		return matched;
	}
	
	public List<Map<String, Integer>> getAllProcessResults(){
		return this.allProcessesResults;
	}
	
	public Map<String, Integer> getPhf2Sigcnt(){
		return this.phf2sigcnt;
	}
	
	public List<Map<String, Double>> getAllProcessResults_credit(){
		return this.allProcessesResults_credit;
	}
	
}
