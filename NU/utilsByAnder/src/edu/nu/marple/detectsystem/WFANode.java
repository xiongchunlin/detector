package edu.nu.marple.detectsystem;

import java.util.Arrays;

/**
 * This class is the class representing for basic automata node in WFA.
 * succ: the outgoing target node from this node.
 * pred: the previous node in the "main chain" of WFA.
 * pos: the original position number in the trace of this node.
 * @author ander
 *
 */
public class WFANode {
	int[] succ;
	int pred, pos;
	
	WFANode(int scrcnt) {
		succ = new int[scrcnt];
		Arrays.fill(succ, -1);
		pred = -1;
		pos = -1;
	}
}
