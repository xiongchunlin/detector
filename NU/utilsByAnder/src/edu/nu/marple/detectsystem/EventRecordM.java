package edu.nu.marple.detectsystem;

import com.ibm.marple.EventRecord;

import edu.nu.marple.EventRecordG;
import edu.nu.marple.Utils;

/**
 * This class serves as the basic eventRecord class in matching phase(i.e. all kinds of detection parts)
 * namd and parameter are considered as the most important part.
 * time is used for absolute time window mechanism which is not used for now.
 * @author ander
 *
 */
public class EventRecordM {
	public String name;
	public String parameter;
	public String time;
	
	public EventRecordM(String name, String parameter, String time) {
		this.name = name;
		this.parameter = parameter;
		this.time = time;
	}
	
	public EventRecordM(EventRecordG eventrecord) {
		this.name = eventrecord.name;
		this.parameter = eventrecord.parameter;
		this.time = "-1";
	}
	
	public EventRecordM(EventRecord eventrecord) {
		this.name = eventrecord.eventName;
		this.parameter = Utils.transferMapPara2StringPara(eventrecord.arguments);
	}
	
	@Override public boolean equals(Object anObject) {
		if (anObject instanceof EventRecordM) 
			if (((EventRecordM)anObject).name.equals(this.name))
				if (((EventRecordM)anObject).parameter == this.parameter ||
					((EventRecordM)anObject).parameter.equals(this.parameter))
				return true;
		return false;
	}
	
	@Override public int hashCode() {
		if (parameter != null)
			return (41 * (name.hashCode() + 41) + parameter.hashCode());
		else
			return (41 * (name.hashCode() + 41));
	}
}
