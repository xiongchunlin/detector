package edu.nu.marple.detectsystem;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import edu.nu.marple.EventRecordG;
import edu.nu.marple.SigResults;
import edu.nu.marple.Utils;
import edu.nu.marple.sigrefine.NonPhfFilter;

public class SignatureRoughSelection {
	public static Map<String, List<EventRecordM>>[] bufferedRecordsList = null;
	public static String nowFocusedPhf = "";
	public boolean filtered = true;
	
	public SignatureRoughSelection(String focusedPhf, String traceExtention, SigResults signature) {
		try {
			if (!focusedPhf.equals(nowFocusedPhf)) {
				nowFocusedPhf = focusedPhf;
				inputBufferedRecordsList(focusedPhf, traceExtention);
			}
			
			int matchedCnt = 0;
			for (int i = 0; i < bufferedRecordsList.length; ++i) {
				SequenceSignatureWFAMatch ssm = new SequenceSignatureWFAMatch("", bufferedRecordsList[i]);
				ssm.sequenceSignature[0] = new ArrayList<EventRecordM>();
				ssm.windowsize[0] = signature.windowsize;
				ssm.focusedPhf = findPhfNo(ssm, focusedPhf);
				for (int j = 0; j < signature.sig.size(); ++j)
					ssm.sequenceSignature[0].add(new EventRecordM(EventRecordG.stringToEventRecordG(signature.sig.get(j))));
				if (ssm.match()) {
					filtered = false;
					break;
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public boolean getResult() {
		return filtered;
	}
	
	public int findPhfNo(SequenceSignatureWFAMatch ssm, String focusedPhf) {
		for (int i = 0; i < ssm.phf.length; ++i) 
		if (ssm.phf[i].equals(focusedPhf)){
			return i;
		}
		return -1;
	}
	
	public void inputBufferedRecordsList(String focusedPHF, String traceExtention) throws IOException {
		File dataFolder = new File("data/test/" + focusedPHF); /*Default "sigTorrent/nonphffilter"*/
		
		File[] files = dataFolder.listFiles(new FilenameFilter() {
			public boolean accept(File dir, String name) {
				return name.endsWith(traceExtention);
			}
		});
		int fileCnt = files.length;
		bufferedRecordsList = new HashMap[fileCnt];
		//System.out.println(nowFocusedPhf + ": " + fileCnt);
		
		int cnt = 0;
		for (int j = 0; j < files.length; ++j) {
			bufferedRecordsList[cnt] = new HashMap<String, List<EventRecordM>>();
			List<EventRecordG> sequence = Utils.EventRecordGFile2EventRecordGenerationList(files[j].getPath());
			ArrayList<EventRecordM> newData = new ArrayList<EventRecordM>();
			for (EventRecordG eve : sequence) {
				newData.add(new EventRecordM(eve.name, eve.parameter, "-1"));
			}
			bufferedRecordsList[cnt].put("0", newData);
			++cnt;
		}
	}
	
	public static void main(String[] args) {
		List<String> phfs = Arrays.asList("remotedesktop",
										  "remoteshell");
		phfs.forEach(t -> {
			System.out.println("PHF: " + t);
			File file = new File("sequenceSignature/8.22_1/" + t + "_localResults_filtered.txt");
			try {
				List<SigResults> results = new Gson().fromJson(new FileReader(file), new TypeToken<List<SigResults>>(){}.getType());
				int No = 0;
				for (int i = 0; i < results.size();) {
					++No;
					System.out.println("Start on signature " + No);
					SignatureRoughSelection select = new SignatureRoughSelection(t, ".output.tracePreprocessed", results.get(i));
					if (select.getResult()) {
						results.remove(i);
						System.out.println("signature " + No + " filtered!");
					} else 
						++i;
				}
				System.out.println("Remaining: " + results.size());
				BufferedWriter filteredWriter = new BufferedWriter(new FileWriter("sequenceSignature/8.22_1/" + t + "_final.txt"));
				filteredWriter.write(new GsonBuilder().setPrettyPrinting().create().toJson(results));
				filteredWriter.close();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});
		
	}
}
