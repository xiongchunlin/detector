package edu.nu.marple.callstackdetectionsystem;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import edu.nu.marple.callstackbasics.AggAPIRecord;
import edu.nu.marple.callstackbasics.CallstackGlobalConfig;
import edu.nu.marple.callstackbasics.AARSigResult;
import edu.nu.marple.callstackbasics.EventRecordWithCallstackM;

/**
 * This class is serving as the most important class for detection algorithm. This algorithm is the reason why we can really achieve online re-time detection.
 * Simply speaking, the goal is to maintain a finite automata representing a string language(a set of strings) including all subsequences of the specific trace.
 * @author ander
 *
 */
public class SequenceSignatureAggSigMatch {

	public final static int MaxSigCnt = CallstackGlobalConfig.MaxSigCnt;
	public final static int MaxSeqSigLen = CallstackGlobalConfig.MaxSeqSigLen;
	public final static String[] phf = CallstackGlobalConfig.focusedPhfs;
	public final static int MaxPhfCnt = CallstackGlobalConfig.focusedPhfs.length;
	public final static String signatureFolder = CallstackGlobalConfig.AARSigVersionFolder;
	public final static String signatureFileExtention = CallstackGlobalConfig.AARSigFileExtension;
	/**
	 * Every round on applying detection on different PHF. Reload signature sequences and corresponding data.
	 */
	public static boolean sigRead = false;
	public static List<AARSigResult>[] finalSigs = new ArrayList[MaxPhfCnt];
	public static List<Integer>[] maxScores = new ArrayList[MaxPhfCnt];
	/**
	 * Loaded data for the detection system for this time.
	 */
	public List<AggAPIRecord> bufferedRecords;
	/**
	 * If the data are from a trace file, this represents the name of the trace file. Otherwise, it is "".
	 */
	public int traceAggrecSeqCnt = 0;
	public int[] traceBaseoffset;
	public List<Integer>[] matchedMaxScores = new ArrayList[MaxPhfCnt];
	
	public SequenceSignatureAggSigMatch(List<AggAPIRecord> bufferedRecords) {
		this.bufferedRecords = bufferedRecords;
		traceAggrecSeqCnt = bufferedRecords.size();
		traceBaseoffset = new int[traceAggrecSeqCnt];
		for (int i = 1; i < traceAggrecSeqCnt; ++i) {
			traceBaseoffset[i] = traceBaseoffset[i - 1] + bufferedRecords.get(i - 1).eventSeq.size();
		}
	}
	
	public static int calculateMaxScore(AARSigResult AggSigRes) {
		int resMaxScore = 0;
		for (AggAPIRecord itrAggAPIRec : AggSigRes.aggrecSeq) {
			for (EventRecordWithCallstackM itrEventM : itrAggAPIRec.eventSeq) 
				resMaxScore += itrEventM.systemCallStack.split(",").length + 1;
		}
		return resMaxScore;
	}
	
	public int calculateDelta(EventRecordWithCallstackM sigData, EventRecordWithCallstackM traceData) {
		if (!sigData.eventName.equals(traceData.eventName) || !sigData.parameter.equals(traceData.parameter)) {
			return -1;
		}
		String[] traceDataCallstack = traceData.systemCallStack.split(",");
		String[] sigDataCallstack = sigData.systemCallStack.split(",");
		for (int i = 0; i < traceDataCallstack.length && i < sigDataCallstack.length; ++i) 
		if (!traceDataCallstack[i].equals(sigDataCallstack[i])) {
			return i + 1;
		}
		return Math.min(traceDataCallstack.length, sigDataCallstack.length) + 1;
	}
	
	public int matchOnSignature(AARSigResult aggSigRes) {
		int sigAggrecSeqCnt = aggSigRes.aggrecSeq.size();
		int[] sigBaseoffset = new int[sigAggrecSeqCnt];
		for (int i = 1; i < sigAggrecSeqCnt; ++i) {
			sigBaseoffset[i] = sigBaseoffset[i - 1] + aggSigRes.aggrecSeq.get(i - 1).eventSeq.size();
		}
		int N = traceBaseoffset[traceAggrecSeqCnt - 1] + bufferedRecords.get(traceAggrecSeqCnt - 1).eventSeq.size(), 
				M = sigBaseoffset[sigAggrecSeqCnt - 1] + aggSigRes.aggrecSeq.get(sigAggrecSeqCnt - 1).eventSeq.size();
		int[][] F = new int[N + 1][M + 1];
		for (int i = 0; i < traceAggrecSeqCnt; ++i) {
			int traceEventCnt = bufferedRecords.get(i).eventSeq.size();
			for (int j = 0; j < traceEventCnt; ++j) {
				for (int k = 0; k < sigAggrecSeqCnt; ++k) {
					int sigEventCnt = aggSigRes.aggrecSeq.get(k).eventSeq.size();
					for (int l = 0; l < sigEventCnt; ++l) {
						F[traceBaseoffset[i] + j + 1][sigBaseoffset[k] + l + 1] = Math.max(F[traceBaseoffset[i] + j + 1][sigBaseoffset[k] + l], 
								F[traceBaseoffset[i] + j][sigBaseoffset[k] + l + 1]);
						EventRecordWithCallstackM sigData = aggSigRes.aggrecSeq.get(k).eventSeq.get(l);
						EventRecordWithCallstackM traceData = bufferedRecords.get(i).eventSeq.get(j);
						int delta = calculateDelta(sigData, traceData);
						if (delta > 0 && F[traceBaseoffset[i] + j][sigBaseoffset[k] + l] + delta > F[traceBaseoffset[i] + j + 1][sigBaseoffset[k] + l + 1]) {
							F[traceBaseoffset[i] + j + 1][sigBaseoffset[k] + l + 1] = F[traceBaseoffset[i] + j][sigBaseoffset[k] + l] + delta;
						}
					}
				}
			}
		}
		return F[N][M];
	}
	
	/**
	 * Invoke match function for the data passed to this detection system.
	 * @return
	 * @throws IOException
	 */
	public void matchForTest() throws IOException{
		if (!sigRead) {
			try {
				for (int i = 0; i < MaxPhfCnt; ++i) {
					finalSigs[i] = new Gson().fromJson(new FileReader(signatureFolder + phf[i] + signatureFileExtention), new TypeToken<List<AARSigResult>>(){}.getType());
				}
			} catch (JsonIOException | JsonSyntaxException | FileNotFoundException e) {
				e.printStackTrace();
			}
			for (int i = 0; i < MaxPhfCnt; ++i) {
				maxScores[i] = new ArrayList<Integer>();
				for (AARSigResult itrAggSigRes : finalSigs[i]) {
					maxScores[i].add(calculateMaxScore(itrAggSigRes));
				}
			}
			sigRead = true;
		}
		for (int itrPhf = 0; itrPhf < MaxPhfCnt; ++itrPhf) {
			matchedMaxScores[itrPhf] = new ArrayList<Integer>();
			int sigCnt = finalSigs[itrPhf].size();
			for (int itrSig = 0; itrSig < sigCnt; ++itrSig) {
				matchedMaxScores[itrPhf].add(matchOnSignature(finalSigs[itrPhf].get(itrSig)));
			}
		}
	}
	
	public double matchForNonPhfFiltering(AARSigResult aggSigRes) {
		int numerator = matchOnSignature(aggSigRes), denominator = calculateMaxScore(aggSigRes);
		double res = numerator * 1.0 / denominator;
//		if (aggSigRes.traces.contains("Coringa-RAT 0.1-2.callstackoutput.tracePreprocessed_tid3772") && aggSigRes.aggrecSeq.size() == 7) {
//			System.out.println(fenzi + " : " + fenmu);
//			System.out.println("");
//		}
		return res;
	}
	
}
