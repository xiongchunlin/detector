package edu.nu.marple.callstackdetectionsystem;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.ibm.marple.EventRecord;

import edu.nu.marple.SigResults;
import edu.nu.marple.callstackbasics.AggAPIRecord;
import edu.nu.marple.callstackbasics.AARSigResult;
import edu.nu.marple.callstackbasics.DPBFSUnit;
import edu.nu.marple.callstackbasics.EventRecordWithCallstackM;

/**
 * This class is serving as the most important class for detection algorithm.
 * This algorithm is the reason why we can really achieve online re-time
 * detection. Simply speaking, the goal is to maintain a finite automata
 * representing a string language(a set of strings) including all subsequences
 * of the specific trace.
 * 
 * @author ander
 *
 */
public class SequenceSetSignatureTopAPISigMatch {

	public final static int MaxSigCnt = 10000;
	public final static int MaxPhfCnt = 5;
	public final static int MaxSeqSigLen = 4000;
	public final static String[] phf = { "audiorecord", "download&exec", "remotedesktop", "remoteshell", "keylogger" };
	public final static String signatureFolder = "seqSetSignature/YourVersionNo/";
	public final static String signatureFileExtention = ".AggSig";
	/**
	 * Every round on applying detection on different PHF. Reload signature
	 * sequences and corresponding data.
	 */
	public static List<List<List<String>>>[] finalSigs = new ArrayList[MaxPhfCnt];
	public static List<Integer>[] maxScores = new ArrayList[MaxPhfCnt];
	/**
	 * Loaded data for the detection system for this time.
	 */
	public List<String> bufferedRecords;
	/**
	 * If the data are from a trace file, this represents the name of the trace
	 * file. Otherwise, it is "".
	 */
	public List<Integer>[] matchedMaxScores = new ArrayList[MaxPhfCnt];
	public LinkedList<DPBFSUnit> bfsqueue = new LinkedList<DPBFSUnit>();
	public Map<List<Integer>, DPBFSUnit> bfsmap = new HashMap<>();

	public SequenceSetSignatureTopAPISigMatch(List<String> bufferedRecords) {
		this.bufferedRecords = bufferedRecords;
	}

	static {
		try {
			for (int i = 0; i < MaxPhfCnt; ++i) {
				finalSigs[i] = new Gson().fromJson(new FileReader(signatureFolder + phf[i] + signatureFileExtention),
						new TypeToken<List<List<List<String>>>>() {
						}.getType());
			}
		} catch (JsonIOException | JsonSyntaxException | FileNotFoundException e) {
			e.printStackTrace();
		}
		for (int i = 0; i < MaxPhfCnt; ++i) {
			maxScores[i] = new ArrayList<Integer>();
			for (List<List<String>> SetSeqSigRes : finalSigs[i]) {
				maxScores[i].add(calculateMaxScore(SetSeqSigRes));
			}
		}
	}

	public static int calculateMaxScore(List<List<String>> SetSeqSigRes) {
		int resMaxScore = 0;
		for (List<String> itrSeqTopAPISig : SetSeqSigRes) {
			resMaxScore += itrSeqTopAPISig.size();
		}
		return resMaxScore;
	}

	public int calculateDelta(String sigData, String traceData) {
		if (sigData.equals(traceData))
			return 1;
		return 0;
	}

	public boolean stepCheck(DPBFSUnit itrUnit, int step) {
		int res = 0;
		for (int i : itrUnit.key)
			res += i;
		return (res == step);
	}

	public int matchOnSignature(List<List<String>> setSeqTopAPISig) {
		int statusDimCnt = setSeqTopAPISig.size() + 1;
		int ret = 0;
		int[] dimMaxValue = new int[statusDimCnt];
		List<Integer> initKey = new ArrayList<>();
		for (int i = 0; i < statusDimCnt; ++i) {
			initKey.add(0);
			if (i == statusDimCnt - 1)
				dimMaxValue[i] = bufferedRecords.size();
			else
				dimMaxValue[i] = setSeqTopAPISig.get(i).size();
		}
		DPBFSUnit initUnit = new DPBFSUnit(initKey, 0);
		bfsqueue.offer(initUnit);
		bfsmap.put(initKey, initUnit);
		int step = 0;

		while (bfsqueue.size() > 0) {
			for (DPBFSUnit itrUnit : bfsqueue)
				if (stepCheck(itrUnit, step)) {
					for (int i = 0; i < statusDimCnt; ++i) {
						List<Integer> newKey = new ArrayList<>(itrUnit.key);
						if (newKey.get(i) < dimMaxValue[i])
							newKey.set(i, newKey.get(i) + 1);
						else
							continue;
						DPBFSUnit extendedUnit;
						if (!bfsmap.containsKey(newKey)) {
							extendedUnit = new DPBFSUnit(newKey, itrUnit.score);
							bfsmap.put(newKey, extendedUnit);
							bfsqueue.add(extendedUnit);
						} else {
							extendedUnit = bfsmap.get(newKey);
							if (extendedUnit.score < itrUnit.score)
								extendedUnit.score = itrUnit.score;
						}
					}
				} else
					break;

			for (DPBFSUnit itrUnit : bfsqueue)
				if (stepCheck(itrUnit, step)) {
					if (itrUnit.key.get(statusDimCnt - 1) < dimMaxValue[statusDimCnt - 1]) {
						for (int i = 0; i < statusDimCnt - 1; ++i) {
							List<Integer> newKey = new ArrayList<>(itrUnit.key);
							if (newKey.get(i) < dimMaxValue[i]) {
								newKey.set(i, newKey.get(i) + 1);
								newKey.set(statusDimCnt - 1, newKey.get(statusDimCnt - 1) + 1);
							} else
								continue;
							DPBFSUnit extendedUnit;
							if (!bfsmap.containsKey(newKey)) {
								extendedUnit = new DPBFSUnit(newKey,
										itrUnit.score + calculateDelta(setSeqTopAPISig.get(i).get(newKey.get(i) - 1),
												bufferedRecords.get(newKey.get(statusDimCnt - 1) - 1)));
								bfsmap.put(newKey, extendedUnit);
								bfsqueue.add(extendedUnit);
							} else {
								extendedUnit = bfsmap.get(newKey);
								if (extendedUnit.score < itrUnit.score
										+ calculateDelta(setSeqTopAPISig.get(i).get(newKey.get(i) - 1),
												bufferedRecords.get(newKey.get(statusDimCnt - 1) - 1)))
									extendedUnit.score = itrUnit.score
											+ calculateDelta(setSeqTopAPISig.get(i).get(newKey.get(i) - 1),
													bufferedRecords.get(newKey.get(statusDimCnt - 1) - 1));
							}
						}
					}
				} else
					break;

			while (!bfsqueue.isEmpty() && stepCheck(bfsqueue.getFirst(), step)) {
				if (bfsqueue.getFirst().score > ret)
					ret = bfsqueue.getFirst().score;
				bfsmap.remove(bfsqueue.getFirst().key);
				bfsqueue.removeFirst();
			}

			++step;
		}

		return ret;
	}

	/**
	 * Invoke match function for the data passed to this detection system.
	 * 
	 * @return
	 * @throws IOException
	 */
	public void matchForTest() throws IOException {
		for (int itrPhf = 0; itrPhf < MaxPhfCnt; ++itrPhf) {
			matchedMaxScores[itrPhf] = new ArrayList<Integer>();
			int sigCnt = finalSigs[itrPhf].size();
			for (int itrSig = 0; itrSig < sigCnt; ++itrSig) {
				matchedMaxScores[itrPhf].add(matchOnSignature(finalSigs[itrPhf].get(itrSig)));
			}
		}
	}

	// public double matchForNonPhfFiltering(AggSigResult aggSigRes) {
	// int numerator = matchOnSignature(aggSigRes), denominator =
	// calculateMaxScore(aggSigRes);
	// double res = numerator * 1.0 / denominator;
	//// if (aggSigRes.traces.contains("Coringa-RAT
	// 0.1-2.callstackoutput.tracePreprocessed_tid3772") &&
	// aggSigRes.aggrecSeq.size() == 7) {
	//// System.out.println(fenzi + " : " + fenmu);
	//// System.out.println("");
	//// }
	// return res;
	// }

}
